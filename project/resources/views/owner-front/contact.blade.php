@extends('layouts.owner-front')


@section('content')


<!--Main Breadcrumb Area Start -->
<div class="main-breadcrumb-area" style="background-image: url({{ $gs->breadcumb_banner ? asset('assets/'.$owner->username.'/owner/images/'.$gs->breadcumb_banner):asset('assets/images/noimage.png') }});">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <ul class="pages">
            <li>
              <a href="{{route('owner.front.index',$owner->username)}}">
                {{ __('Home') }}
              </a>
            </li>
            <li>
              <a href="{{route('owner.front.contact',$owner->username)}}">
                {{__('Contact Us')}}
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!--Main Breadcrumb Area End -->

<!-- Contact Us Area Start -->
<section class="contact-us">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="section-heading">
                    <h2 class="title">
                        {{__('Contact Us')}}
                    </h2>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-lg-7">
                <div class="left-area">
                    <div class="contact-form">
                        <div class="gocover" style="background: url({{ asset('assets/'.$owner->username.'/owner/images/'.$gs->loader) }}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                            <form id="contactform" action="{{route('owner.front.contact.submit',$owner->username)}}" method="POST">
                            @csrf
                            @include('includes.admin.form-both')
                                <ul>
                                <li>
                                    <input type="text" name="name" class="input-field" placeholder="{{__('Name')}} *">
                                </li>
                                <li>
                                    <input type="email" name="email" class="input-field" placeholder="{{__('Email')}} *">
                                </li>
                                <li>
                                    <input type="text" name="phone" class="input-field" placeholder="{{__('Phone')}} *">
                                </li>
                                <li>
                                <li>
                                    <textarea class="input-field textarea" name="text" placeholder="{{__('Your Message')}} *"></textarea>
                                </li>
                                <input type="hidden" name="to" value="{{ $ps->contact_email }}">
                            </ul>
                            @if($gs->is_capcha == 1)

                            <ul class="captcha-area">
                                <li>
                                    <p><img class="codeimg1" src="{{asset("assets/images/capcha_code.png")}}" alt=""> <i class="fas fa-sync-alt pointer refresh_code"></i></p>

                                </li>
                                <li>
                                    <input name="codes" type="text" class="input-field" placeholder="{{ __('Capcha Code') }}" required="">

                                </li>
                            </ul>

                            @endif
                            <button class="submit-btn mybtn1" type="submit">{{ __('Send Message') }}</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="right-area">

                    @if($ps->site != null || $ps->email != null )
                    <div class="contact-info ">
                        <div class="left ">
                                <div class="icon">
                                        <i class="icofont-email"></i>
                                </div>
                        </div>
                        <div class="content d-flex align-self-center">
                            <div class="content">
                                    @if($ps->site != null && $ps->email != null)
                                    <a href="{{$ps->site}}" target="_blank">{{$ps->site}}</a>
                                    <a href="mailto:{{$ps->email}}">{{$ps->email}}</a>
                                    @elseif($ps->site != null)
                                    <a href="{{$ps->site}}" target="_blank">{{$ps->site}}</a>
                                    @else
                                    <a href="mailto:{{$ps->email}}">{{$ps->email}}</a>
                                    @endif
                            </div>
                        </div>
                    </div>
                    @endif
                    @if($ps->street != null)
                    <div class="contact-info">
                            <div class="left">
                                    <div class="icon">
                                            <i class="icofont-google-map"></i>
                                    </div>
                            </div>
                            <div class="content d-flex align-self-center">
                                <div class="content">
                                        <p>
                                            @if($ps->street != null)
                                                {!! $ps->street !!}
                                            @endif
                                        </p>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($ps->phone != null || $ps->fax != null )
                        <div class="contact-info">
                                <div class="left">
                                        <div class="icon">
                                                <i class="icofont-smart-phone"></i>
                                        </div>
                                </div>
                                <div class="content d-flex align-self-center">
                                    <div class="content">
                                        @if($ps->phone != null && $ps->fax != null)
                                        <a href="tel:{{$ps->phone}}">{{$ps->phone}}</a>
                                        <a href="tel:{{$ps->fax}}">{{$ps->fax}}</a>
                                        @elseif($ps->phone != null)
                                        <a href="tel:{{$ps->phone}}">{{$ps->phone}}</a>
                                        @else
                                        <a href="tel:{{$ps->fax}}">{{$ps->fax}}</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                    @endif


                            <div class="social-links">
                                <h4 class="title">{{ __('Find Us Here')}} :</h4>
                                <ul>

                                    @php
                                        $social_settings = App\Models\Socialsetting::where('register_id',$owner->id)->first();
                                    @endphp

                                 @if($social_settings->f_status == 1)
                                  <li>
                                    <a href="{{ $social_settings->facebook }}" class="facebook" target="_blank">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                  </li>
                                  @endif

                                  @if($social_settings->g_status == 1)
                                  <li>
                                    <a href="{{ $social_settings->gplus }}" class="google-plus" target="_blank">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                  </li>
                                  @endif

                                  @if($social_settings->t_status == 1)
                                  <li>
                                    <a href="{{ $social_settings->twitter }}" class="twitter" target="_blank">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                  </li>
                                  @endif

                                  @if($social_settings->l_status == 1)
                                  <li>
                                    <a href="{{ $social_settings->linkedin }}" class="linkedin" target="_blank">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                  </li>
                                  @endif

                                  @if($social_settings->d_status == 1)
                                  <li>
                                    <a href="{{ $social_settings->dribble }}" class="dribbble" target="_blank">
                                        <i class="fab fa-dribbble"></i>
                                    </a>
                                  </li>
                                  @endif

                                    </ul>
                            </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact Us Area End-->

@endsection
