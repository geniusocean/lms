@extends('layouts.owner-front')

@section('content')
	<!-- Hero Area Start -->
	<section class="hero-area">
		<div class="intro-content" style="background-image: url({{$ps->hero_bg ? asset('assets/'.$owner->username.'/owner/images/'.$ps->hero_bg) : ''}})">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="slider-content">
							<!-- layer 1 -->
							<div class="layer-1">
								<h2 class="title">{{__('GLearn on your schedule')}}</h2>
							</div>
							<!-- layer 2 -->
							<div class="layer-2">
								<p class="text">
									{{__('Anywhere, anytime. Start learning today!')}}
								</p>
							</div>
							<!-- layer 3 -->
							<div class="layer-3">
								<div class="serch-form2">
									<form action="{{ route('owner.front.catalog', [$owner->username,Request::route('category'),Request::route('subcategory')]) }}" method="GET">
										<input name="search" type="text" placeholder="{{__('What do you want to learn ?')}}" required>
										<button type="submit">
											<i class="fas fa-search"></i>
										</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Hero Area End -->

	<!-- Feature Area Start -->
	<section class="feature-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="feature-section-inner">
						<div class="row">
							@foreach ($services as $service)
							<div class="col-lg-4">
								<div class="single-feature pb-4">
									<div class="left-icon">
										<img src="{{asset('assets/'.$owner->username.'/owner/images/services/'.$service->photo)}}" alt="service">
									</div>
									<div class="right-content">
										<h4 class="title">{{$service->title}}</h4>
										<p>{{$service->details}}s</p>
									</div>
								</div>
							</div>
							@endforeach


						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Feature Area End -->

	<!-- Tab Product Area Start  -->
	<section class="tab-product-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="top-heading-area">
						<h4 class="title">
							{{ __("The world's largest selection of courses") }}
						</h4>
						<p>
							{{ __("Choose from 100,000 online video courses with new additions published every month") }}
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="product-tabs">
						<ul class="nav" role="tablist">

							@foreach(DB::table('categories')->whereRegisterId($owner->id)->whereStatus(1)->whereIsTop(1)->take(6)->get() as $tcat)

							<li class="nav-item" role="presentation">
								<a class="nav-link {{ $loop->first ? 'active' : '' }}" id="pills-{{ $tcat->slug }}-tab" data-toggle="pill" href="#pills-{{ $tcat->slug }}" role="tab" aria-controls="pills-{{ $tcat->slug }}" aria-selected="true">
									{{ $tcat->name }}
								</a>
							</li>

							@endforeach

						</ul>

						  <div class="tab-content" id="pills-tabContent">

							@foreach(\App\Models\Category::whereRegisterId($owner->id)->whereStatus(1)->whereIsTop(1)->take(6)->get() as $topcat)

							<div class="tab-pane fade {{ $loop->first ? 'show active' : '' }}" id="pills-{{ $topcat->slug }}" role="tabpanel" aria-labelledby="pills-{{ $topcat->slug }}-tab">
								<div class="slick-product-slider">

								@foreach($topcat->courses()->whereRegisterId($owner->id)->whereStatus(1)->get() as $top_course)

									<div class="slider-item">
										<div class="single-course">
											<div class="img">
												<img src="{{asset('assets/'.$owner->username.'/owner/images/courses/'.$top_course->photo)}}" alt="course">
											</div>
											<div class="content">
												<p class="author">


													<a href="{{!empty($top_course->instructor) ? route('owner.instructor.details',[$owner->username,$top_course->instructor->instructor_slug]) : route('owner.instructor.details',[$owner->username,DB::table('admins')->where('id',$owner->id)->first()->slug])}}">
														{{ !empty($top_course->instructor) ? $top_course->instructor->instructor_name : DB::table('admins')->where('id',$owner->id)->first()->name }}
													  </a>

												</p>

												<a href="{{route('owner.front.course',[$owner->username,$top_course->slug])}}">
													<h4 class="title">
														{{ $top_course->showTitle() }}
													</h4>
												</a>

												@if(App\Models\Rating::normalRating($top_course->id) != 0)
												<div class="reating-area">
												  <span class="number">{{ App\Models\Rating::normalRating($top_course->id) }}</span>
												  <div class="stars">
													  <div class="ratings">
														  <div class="empty-stars"></div>
														  <div class="full-stars" style="width:{{ App\Models\Rating::ratings($top_course->id) }}%"></div>
														</div>
												  </div>
												  <div class="total-star">
													({{ App\Models\Rating::ratingCount($top_course->id) }})
												  </div>
												</div>
												@endif
                                                @if($top_course->price == 0)
                                                    {{ __('Free') }}
                                                @else
                                                    <p class="price">

														{{$curr->sign}}{{round(($top_course->price * $curr->value),2)}} <del>{{ $curr->sign }}{{ round(($top_course->discount_price * $curr->value),2) }}</del>
                                                    </p>
                                                @endif
											</div>
										</div>
									</div>

								@endforeach

								</div>
							</div>

							@endforeach

						  </div>

					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Tab Product Area End -->

	<!-- Product Area Start -->
	<section class="product-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-heading">
						<h2 class="title">{{__('Top Courses')}}
						</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="product-slider">
						@foreach ($top_courses as $course)
						<div class="single-slider">
							<div class="single-course">
								<div class="img">
									<img src="{{asset('assets/'.$owner->username.'/owner/images/courses/'.$course->photo)}}" alt="course">
								</div>
								<div class="content">
									<p class="author">
										<a href="{{!empty($course->instructor) ? route('owner.instructor.details',[$owner->username,$course->instructor->instructor_slug]) : route('owner.instructor.details',[$owner->username,DB::table('admins')->where('id',$owner->id)->first()->slug])}}">
										  {{ !empty($course->instructor) ? $course->instructor->instructor_name : DB::table('admins')->where('id',$owner->id)->first()->name }}
										</a>
									  </p>

                                    <a href="{{route('owner.front.course',[$owner->username,$course->slug])}}">
										<h4 class="title">
											{{ $course->showTitle() }}
                                        </h4>
                                    </a>

									@if(App\Models\Rating::normalRating($course->id) != 0)
									<div class="reating-area">
									  <span class="number">{{ App\Models\Rating::normalRating($course->id) }}</span>
									  <div class="stars">
										  <div class="ratings">
											  <div class="empty-stars"></div>
											  <div class="full-stars" style="width:{{ App\Models\Rating::ratings($course->id) }}%"></div>
											</div>
									  </div>
									  <div class="total-star">
										({{ App\Models\Rating::ratingCount($course->id) }})
									  </div>
									</div>
									@endif
									@if($course->price == 0)

									<p class="price">
									  {{ __('Free') }}
									</p>

									@else

									<p class="price">
										{{$curr->sign}}{{round(($course->price * $curr->value),2)}} <del>{{ $curr->sign }}{{ round(($course->discount_price * $curr->value),2) }}</del>
									</p>

									@endif
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Product Area End -->

	<!-- Product Area Start -->
	<section class="product-section pb-70">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-heading">
						<h2 class="title">{{  __('New Courses')  }}
						</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="product-slider">
						@foreach ($new_courses as $course)
						<div class="single-slider">
							<div class="single-course">
								<div class="img">
									<img src="{{asset('assets/'.$owner->username.'/owner/images/courses/'.$course->photo)}}" alt="course">
								</div>
								<div class="content">
									<p class="author">
										<a href="{{!empty($course->instructor) ? route('owner.instructor.details',[$owner->username,$course->instructor->instructor_slug]) : route('owner.instructor.details',[$owner->username,DB::table('admins')->where('id',$owner->id)->first()->slug])}}">
										  {{ !empty($course->instructor) ? $course->instructor->instructor_name : DB::table('admins')->where('id',$owner->id)->first()->name }}
										</a>
									  </p>
                                    <a href="{{route('owner.front.course',[$owner->username,$course->slug])}}">
										<h4 class="title">
											{{ $course->showTitle() }}
										</h4>
                                    </a>

									@if(App\Models\Rating::normalRating($course->id) != 0)
									<div class="reating-area">
									  <span class="number">{{ App\Models\Rating::normalRating($course->id) }}</span>
									  <div class="stars">
										  <div class="ratings">
											  <div class="empty-stars"></div>
											  <div class="full-stars" style="width:{{ App\Models\Rating::ratings($course->id) }}%"></div>
											</div>
									  </div>
									  <div class="total-star">
										({{ App\Models\Rating::ratingCount($course->id) }})
									  </div>
									</div>
									@endif
									@if($course->price == 0)

									<p class="price">
									  {{ __('Free') }}
									</p>

									@else

									<p class="price">
										{{$curr->sign}}{{round(($course->price * $curr->value),2)}} <del>{{ $curr->sign }}{{ round(($course->discount_price * $curr->value),2) }}</del>
									</p>

									@endif
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Product Area End -->

	<!-- Categories Area Start -->
	<section class="category-section pb-70">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-heading">
						<h2 class="title">{{__('Top Categories')}}
						</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<ul class="category-list">
						@foreach (DB::table('categories')->whereRegisterId($owner->id)->whereStatus(1)->whereIsTop(1)->get() as $topcat)
						<li>
							<a href="{{route('owner.front.catalog',[$owner->username,$topcat->slug])}}">
								<img src="{{asset('assets/'.$owner->username.'/owner/images/categories/'.$topcat->photo)}}" alt="">{{$topcat->name}}
							</a>
						</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!-- Categories Area End -->

	<!-- Become Author Area Start -->
	<section class="become-author-section pb-70">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					@if($ps->instructor_img)
					<div class="left-img">
						<img src="{{asset('assets/'.$owner->username.'/owner/images/'.$ps->instructor_img)}}" alt="">
					</div>
					@endif
				</div>
				<div class="col-lg-6 col-md-6 align-self-center">
					<div class="content">
						<h4 class="title">
							{{ __('Become an instructor') }}
						</h4>
						<p>
						    {{ __('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed velit officiis accusamus tenetur totam magnam sunt eveniet quos porro dignissimos neque, quaerat tempora nostrum.') }}
						</p>
                        <a href="{{$ps->instructor_btn_url}}" class="mybtn1"  data-toggle="modal" data-target="#bcomeinstructormodal">
                            {{ __('Start Teaching Today') }}
                        </a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Become Author Area End -->

	<!-- Blog Area Start -->
	 <section class="blog pb-70">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-heading">
						<h2 class="title">
							{{__('Our Blogs')}}
						</h2>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				@foreach ($blogs as $blog)
				<div class="col-lg-4 col-md-6">
					<a href="{{route('owner.front.blogshow',[$owner->username,$blog->id])}}" class="single-blog">
						<div class="img">
							<img src="{{asset('assets/'.$owner->username.'/owner/images/blogs/'.$blog->photo)}}" alt="blog-photo">
						</div>
						<div class="content">
							<span>
								<h4 class="title">
									{{$blog->title}}
								</h4>
							</span>
							<div class="text">
								<p>
									{{substr(strip_tags($blog->details),0,170)}}
								</p>
							</div>

							<ul class="top-meta">
								<li>
									<span>
										<i class="far fa-clock"></i>{{ \Carbon\Carbon::parse($blog->created_at)->diffForHumans() }}
									</span>
								</li>

							</ul>
						</div>
					</a>
				</div>
				@endforeach
			</div>
			<div class="row">
				<div class="col-lg-12 text-center">
					<a href="{{route('owner.front.blog',$owner->username)}}" class="view-all-btn">{{__('View All')}}</a>
				</div>
			</div>
		</div>
	</section>
	<!-- Blog Area End -->
@endsection
