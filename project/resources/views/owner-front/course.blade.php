@extends('layouts.owner-front')

@section('content')
<!--Main Breadcrumb Area Start -->
<div class="main-breadcrumb-area" style="background-image: url({{ $gs->breadcumb_banner ? asset('assets/'.$owner->username.'/owner/images/'.$gs->breadcumb_banner):asset('assets/images/noimage.png') }});">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ul class="pages">
          <li>
            <a href="{{route('owner.front.index',$owner->username)}}">
              {{ __('Home') }}
            </a>
          </li>
          <li>
            <a href="{{ route('owner.front.catalog',[$owner->username,$course->category->slug]) }}">
              {{ $course->category->name }}
            </a>
          </li>
          @if($course->subcategory_id != null)
          <li>
            <a href="{{route('owner.front.catalog', [$owner->username,$course->category->slug, $course->subcategory->slug])}}">{{ $course->subcategory->name }}</a>
         </li>
          @endif
          <li>
            <a href="{{ route('owner.front.course',[$owner->username,$course->slug]) }}">
              {{$course->title}}
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!--Main Breadcrumb Area End -->

<!-- Course Details Page Start -->
<section class="course-details-page">
  <div class="details-top-jumboron">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="content">
            <div class="left-area">
              <div class="top-area">
                <p>{{ __('Created by') }} : {{ !empty($course->instructor) ? $course->instructor->showName() : DB::table('admins')->where('id',$owner->id)->first()->name }}</p>
                <p>{{ __('Last Updated') }} {{ date('D',strtotime($course->updated_at)) }}, {{ date('d-M-Y',strtotime($course->updated_at)) }}</p>
                <p>English</p>
              </div>
            <h4 class="title">
              {{$course->title}}
            </h4>
            <p class="text">
              {{$course->sortdescription}}
            </p>
            @if(App\Models\Rating::normalRating($course->id) != 0)
            <div class="reating-area">
              <span class="number">{{ App\Models\Rating::normalRating($course->id) }}</span>
              <div class="stars">
                  <div class="ratings">
                      <div class="empty-stars"></div>
                      <div class="full-stars" style="width:{{ App\Models\Rating::ratings($course->id) }}%"></div>
                    </div>
              </div>
              <div class="total-star">
                ({{ App\Models\Rating::ratingCount($course->id) }})
              </div>
            </div>
            @endif
            <ul class="short-info">
                @if($course->convertTime() != '')
                <li>
                  {{ $course->convertTime() }}
                </li>
                @endif
                <li>
                  {{ $course->sections()->withCount('lessons')->get()->sum('lessons_count') }} {{ __('lecture(s)') }}
                </li>
                <li>
                    {{ $course->level }} {{ __('Level') }}
                </li>
            </ul>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="details-main-content">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 order-last order-lg-first">
          @if($course->outcomes != null)
          <div class="what-you-learn">
            <h4 class="title">
              {{ __("What you'll learn") }}
            </h4>
            <ul class="learn-list">
            @foreach(explode(',,,', $course->outcomes) as $ot)
              <li>
                {{ $ot }}
              </li>

            @endforeach
            </ul>
          </div>
          @endif
          <div class="course-content">
            <h4 class="title">
              {{ __('Course Content') }}
            </h4>

            <div class="content-accordion">

                @foreach($course->sections()->oldest('pos')->get() as $section)

                <div class="mycard">
                    <div class="mycard-header" id="heading{{ $section->id }}">
                        <span class="header-title" data-toggle="collapse" data-target="#collapse{{ $section->id }}" aria-expanded="{{ $loop->first ? 'true' : '' }}" aria-controls="collapse{{ $section->id }}">
                            <b>{{ $section->title }}</b> <span>{{ $course->convertSectionTime($section->id) }}</span>
                        </span>
                    </div>

                    <div id="collapse{{ $section->id }}" class="collapse {{ $loop->first ? 'show' : '' }}" aria-labelledby="heading{{ $section->id }}">
                        <div class="mycard-body">
                            <div class="video-list">

                                @foreach($section->lessons()->oldest('pos')->get() as $lesson)

                                <div class="single-video">
                                    <div class="left">
                                         <img src="{{ $lesson->type == 'Lesson' ? asset('assets/images/video.png') : asset('assets/images/quiz.png')  }}" alt="" height="16"> {{ $lesson->title }}
                                    </div>
                                    @if($lesson->duration != null)
                                    <div class="right">
                                        <span class="time">{{ $lesson->duration }}</span>
                                    </div>
                                    @endif
                                </div>

                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>

                @endforeach


            </div>
          </div>
          @if($course->requirements != null)
          <div class="requirements-list">
            <h4 class="title">{{ __('Requirements') }}</h4>
            <ul>

            @foreach(explode(',,,', $course->requirements) as $rq)

              <li>
                {{ $rq }}
              </li>

            @endforeach

            </ul>
          </div>
          @endif
          <div class="course-description">
            <h4 class="title">{{__('Description')}}</h4>
            <div class="description-content">
              {!! $course->description !!}
            </div>
          </div>
          <div class="students-also-bought">
            <h4 class="header-title">
              {{ __('Related Courses') }}
            </h4>
            <div class="row">
              <div class="col-lg-12">
                <div class="product-slider2">

                  @foreach($course->category->courses()->where('register_id',$owner->id)->whereStatus(1)->where('id','!=',$course->id)->take(8)->get() as $rcourse)

                  <div class="single-slider">
										<div class="single-course">
											<div class="img">
												<img src="{{asset('assets/images/courses/'.$rcourse->photo)}}" alt="course">
											</div>
											<div class="content">
                                                <p class="author">
                        <a href="{{!empty($rcourse->instructor) ? route('owner.instructor.details',[$owner->username,$rcourse->instructor->instructor_slug]) : route('owner.instructor.details',[$owner->username,DB::table('admins')->where('id',$owner->id)->first()->slug])}}">
                          {{ !empty($rcourse->instructor) ? $rcourse->instructor->instructor_name : DB::table('admins')->where('id',$owner->id)->first()->name }}
                        </a>
                                                </p>
												<a href="{{route('owner.front.course',[$owner->username,$rcourse->slug])}}">
													<h4 class="title">
														{{ $rcourse->showTitle() }}
													</h4>
												</a>

                        @if(App\Models\Rating::normalRating($rcourse->id) != 0)
                        <div class="reating-area">
                          <span class="number">{{ App\Models\Rating::normalRating($rcourse->id) }}</span>
                          <div class="stars">
                              <div class="ratings">
                                  <div class="empty-stars"></div>
                                  <div class="full-stars" style="width:{{ App\Models\Rating::ratings($rcourse->id) }}%"></div>
                                </div>
                          </div>
                          <div class="total-star">
                            ({{ App\Models\Rating::ratingCount($rcourse->id) }})
                          </div>
                        </div>
                        @endif
												<p class="price">
                          {{$curr->sign}}{{round(($rcourse->price * $curr->value),2)}} <del>{{ $curr->sign }}{{ round(($rcourse->discount_price * $curr->value),2) }}</del>
												</p>
											</div>
										</div>
                  </div>

                  @endforeach

                  </div>
                </div>
              </div>
            </div>

            <div class="about-instructor">
                <h4 class="header-title">
                  {{ __('About The Instructor') }}:
                </h4>
                @if($instructor == 'admin')

                <div class="about-instructor-content">
                  <div class="about-left-content">
                    <div class="images">
                    <img src="{{asset('assets/'.$owner->username.'/owner/images/admins/'.$admin->photo)}}" alt="">
                    </div>
                    <ul class="info-list">
                    <li>
                      <i class="fas fa-star"></i> <span>{{ \App\Models\Rating::InstructorOwnerRatings($owner->id) }}</span> {{ __('Instructor Rating') }}
                    </li>
                    <li>
                      <i class="fas fa-comment"></i> <span>{{ \App\Models\Course::where('register_id','=',$owner->id)->withCount('ratings')->get()->sum('ratings_count') }}</span> {{ __('Reviews') }}
                    </li>
                    <li>
                      <i class="fas fa-user"></i> <span>{{ \App\Models\Course::where('register_id','=',$owner->id)->withCount('enrolled_courses')->get()->sum('enrolled_courses_count') }}</span> {{ __('Students') }}
                    </li>
                    <li>
                      <i class="fas fa-play-circle"></i> <span>{{ DB::table('courses')->whereRegisterId($owner->id)->count() }}</span> {{ __('Courses') }}
                    </li>
                    </ul>
                  </div>
                  <div class="about-right-content">
                    <h4 class="name">
                    {{ $admin->name }}
                    </h4>
                    <p class="job-title">
                    {{ $admin->address }}
                    </p>
                    <div class="about-description">
                      {!! $admin->biography !!}
                    </div>
                  </div>
                </div>

                @else

                <div class="about-instructor-content">
                  <div class="about-left-content">
                    <div class="images">
                      <img src="{{asset('assets/'.$owner->username.'/owner/images/users/'.$instructor->photo)}}" alt="">
                    </div>
                    <ul class="info-list">
                      <li>
                        <i class="fas fa-star"></i> <span>{{ \App\Models\Rating::InstructorNormalRatings($instructor->id) }}</span> {{ __('Instructor Rating') }}
                      </li>
                      <li>
                        <i class="fas fa-comment"></i> <span>{{ $instructor->courses()->withCount('ratings')->get()->sum('ratings_count') }}</span> {{ __('Reviews') }}
                      </li>
                      <li>
                        <i class="fas fa-user"></i> <span>{{ $instructor->courses()->withCount('enrolled_courses')->get()->sum('enrolled_courses_count') }}</span> {{ __('Students') }}
                      </li>
                      <li>
                        <i class="fas fa-play-circle"></i> <span>{{ $instructor->courses->count() }}</span> {{ __('Courses') }}
                      </li>
                    </ul>
                  </div>
                  <div class="about-right-content">
                    <h4 class="name">
                      {{ $instructor->instructor_name }}
                    </h4>
                    <p class="job-title">
                      {{ $instructor->address }}
                    </p>
                    <div class="about-description">
                        {!! $instructor->bio !!}
                    </div>
                  </div>
                </div>


                @endif
              </div>

              <div class="course-review-area">
                <h4 class="header-title">
                  {{ __('Student Feedback') }}:
                </h4>
                <div class="rating-report-box" id="rating-load">
                  <div class="rating-left-area">
                    <span class="rating-number">{{ App\Models\Rating::normalRating($course->id) }}</span>

                    <div class="stars">
                        <div class="ratings">
                            <div class="empty-stars"></div>
                            <div class="full-stars" style="width:{{ App\Models\Rating::ratings($course->id) }}%"></div>
                          </div>
                    </div>

                    <span class="text">{{ __('Course Rating') }}</span>
                  </div>
                  <div class="rating-right-area">
                    <div class="rating-progress-bar">
                      <div class="progress">
                        <div class="progress-bar bg-info" role="progressbar" style="width: {{ App\Models\Rating::customRatings($course->id,5) }}%" aria-valuenow="{{ App\Models\Rating::customRatings($course->id,5) }}" aria-valuemin="0" aria-valuemax="100">{{ App\Models\Rating::customRatings($course->id,5) }}%</div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-info" role="progressbar" style="width: {{ App\Models\Rating::customRatings($course->id,4) }}%" aria-valuenow="{{ App\Models\Rating::customRatings($course->id,4) }}" aria-valuemin="0" aria-valuemax="100">{{ App\Models\Rating::customRatings($course->id,4) }}%</div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-info" role="progressbar" style="width: {{ App\Models\Rating::customRatings($course->id,3) }}%" aria-valuenow="{{ App\Models\Rating::customRatings($course->id,3) }}" aria-valuemin="0" aria-valuemax="100">{{ App\Models\Rating::customRatings($course->id,3) }}%</div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-info" role="progressbar" style="width: {{ App\Models\Rating::customRatings($course->id,2) }}%" aria-valuenow="{{ App\Models\Rating::customRatings($course->id,2) }}" aria-valuemin="0" aria-valuemax="100">{{ App\Models\Rating::customRatings($course->id,2) }}%</div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar bg-info" role="progressbar" style="width: {{ App\Models\Rating::customRatings($course->id,1) }}%" aria-valuenow="{{ App\Models\Rating::customRatings($course->id,1) }}" aria-valuemin="0" aria-valuemax="100">{{ App\Models\Rating::customRatings($course->id,1) }}%</div>
                      </div>
                    </div>
                  </div>
                </div>


                <div class="user-review-area">
                  <h4 class="header-title">
                    {{ __('Review(s)') }}:
                  </h4>

                  <div id="course-reviews">
                    @foreach ($course->ratings as $rating)
                        <div class="user-single-review">
                        <div class="review-left">
                            <img src="{{ asset('assets/'.$owner->username.'/owner/images/users/'.$rating->user->photo) }}" alt="">
                            <h6 class="name">{{ $rating->user->showName() }}</h6>
                            <span class="time">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$rating->review_date)->diffForHumans() }}</span>
                        </div>
                        <div class="review-right">
                            <div class="stars">
                            @for ($i = 1; $i <= $rating->rating; $i++)
                                <i class="fas fa-star"></i>
                            @endfor
                            </div>
                            <p class="review-content">
                            {{$rating->review}}
                            </p>
                        </div>
                        </div>
                    @endforeach
                  </div>

                <div id="coment-area">
                  <h4 class="header-title">
                    {{ __('Write a review') }}:
                  </h4>

                <div class="review-area">

                    <div class="star-area">
                        <ul class="star-list">
                            <li class="stars" data-val="1">
                                <i class="fas fa-star"></i>
                            </li>
                            <li class="stars" data-val="2">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </li>
                            <li class="stars" data-val="3">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </li>
                            <li class="stars" data-val="4">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </li>
                            <li class="stars active" data-val="5">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="write-comment-area">

                    <div class="gocover"
                    style="background: url({{ asset('assets/'.$owner->username.'/owner/images/'.$gs->loader) }}) no-repeat scroll center center rgba(45, 45, 45, 0.5);">
                  </div>

                    @if(Auth::check())

                    <form id="reviewform" action="{{ route('owner.student.review.submit',$owner->username) }}" data-href="{{ route('owner.front.reviews',[$owner->username,$course->id]) }}" data-side-href="{{ route('owner.front.side.reviews',[$owner->username,$course->id]) }}" method="POST">

                    @else

                    <form action="{{ route('owner.student.review.submit',$owner->username) }}" method="POST">

                    @endif

                        {{  csrf_field() }}

                        <input type="hidden" id="rating" name="rating" value="5">
                        <input type="hidden" name="course_id" value="{{ $course->id }}">
                        <div class="row">
                            <div class="col-lg-12">
                                <textarea name="review" placeholder="{{ __('Write Your Review') }}" required=""></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="mybtn1" id="review-btn" type="submit">{{ __('Submit') }}</button>
                            </div>
                        </div>
                    </form>
                </div>


                  </div>


                </div>
              </div>


          </div>
          {{-- rhis part --}}

        <div class="col-lg-4">
          <div class="right-content">
            <div class="video-area">
              <img src="{{asset('assets/'.$owner->username.'/owner/images/courses/'.$course->photo)}}" alt="course">
              <a href="{{ $course->course_overview_url }}" class="popup-button play-video mfp-iframe">
                <i class="fas fa-play"></i>
              </a>
            </div>
            <div class="price-area">
              @if($course->price == 0)

                {{ __('Free') }}

              @else

              {{$curr->sign}}{{round(($course->price * $curr->value),2)}} <del>{{ $curr->sign }}{{ round(($course->discount_price * $curr->value),2) }}</del>


              @endif


            </div>



            <div class="action-buttons">
              @if($course->price == 0)

              <a href="{{ route('owner-student-enroll',$owner->username) }}?course_id={{ $course->id }}" class="buy-now  btn-success" ><i class="far fa-credit-card"></i>{{ __('Enroll Now') }}</a>

              @else 

              <a href="javascript:;" data-href="{{ route('owner.course.cart.add',[$owner->username,$course->id]) }}?ref={{ $affilate_user }}&type={{$type}}" class="add-cart add-to-cart btn-danger"><i class="fas fa-cart-plus"></i>{{ __('Add To Cart') }}</a>
              <a href="{{ route('owner.course.cart.quickadd',[$owner->username,$course->id]) }}?ref={{ $affilate_user }}" class="buy-now  btn-success" ><i class="far fa-credit-card"></i>{{ __('Buy Now') }}</a>

              @endif

                @if(Auth::check())

                <a href="javascript:;" data-href="{{ route('student-wishlist-add',$course->id) }}" class="add-wishlist add-to-wish btn-info"><i class="far fa-heart"></i>{{ __('Wishlist') }}</a>

                @else

                <a href="{{ route('owner-student-wishlist-add',[$owner->username,$course->id]) }}" class="add-wishlist btn-info"><i class="far fa-heart"></i>{{ __('Wishlist') }}</a>

                @endif
                @if(Auth::check())

                <a href="javascript:;" data-href="{{ \Request::url().'?ref='.Auth::user()->affilate_code }}" class="add-wishlist add-to-affilate btn-primary"><i class="fas fa-link"></i>{{ __('Copy Affiliate Link') }}</a>

                @endif
            </div>

            <div class="course-includes">
              <h4 class="title">{{__('This course includes')}}</h4>
              <ul class="include-list">

                @foreach (array_combine(explode(',,,', $course->include_icon),explode(',,,', $course->include_text)) as  $key => $value)
                  <li>
                    <i class="{{$key}}"></i> {{$value}}
                  </li>
                @endforeach

              </ul>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Course Details Page End -->
@endsection
