@extends('layouts.owner-front')
@section('content')


<!--Main Breadcrumb Area Start -->
<div class="main-breadcrumb-area" style="background-image: url({{ $gs->breadcumb_banner ? asset('assets/'.$owner->username.'/owner/images/'.$gs->breadcumb_banner):asset('assets/images/noimage.png') }});">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <ul class="pages">
            <li>
              <a href="{{route('owner.front.index',$owner->username)}}">
                {{ __('Home') }}
              </a>
            </li>
            <li>
                <a href="{{ route('owner.front.page',[$owner->username,$page->slug]) }}">
                    {{ $page->title }}
                </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!--Main Breadcrumb Area End -->



<section class="about mt-5">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="about-info">
            <h3 class="title">
              {{ $page->title }}
            </h3>
            <p>
              {!! $page->details !!}
            </p>

          </div>
        </div>
      </div>
    </div>
  </section>

@endsection
