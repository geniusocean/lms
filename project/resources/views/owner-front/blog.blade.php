@extends('layouts.owner-front')
@section('content')


<!--Main Breadcrumb Area Start -->
<div class="main-breadcrumb-area" style="background-image: url({{ $gs->breadcumb_banner ? asset('assets/'.$owner->username.'/owner/images/'.$gs->breadcumb_banner):asset('assets/images/noimage.png') }});">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ul class="pages">
          <li>
            <a href="{{route('owner.front.index',$owner->username)}}">
              {{ __('Home') }}
            </a>
          </li>
          <li>
            <a href=" {{ route('owner.front.blog',$owner->username) }} ">
              {{ __('Blog') }}
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!--Main Breadcrumb Area End -->

<!-- Blog Area Start -->
<section class="blog blog-page" id="blog">
  <div class="container">
    <div class="row">
      <div class="col-lg-8" id="ajaxContent">

        <div class="row">

          @if(count($blogs) > 0)

            @foreach ($blogs as $blog)

            <div class="col-md-6">
              <a href="{{route('front.blogshow',[$owner->username,$blog->id])}}" class="single-blog">
                <div class="img">
                  <img src="{{asset('assets/'.$owner->username.'/owner/images/blogs/'.$blog->photo)}}" alt="blog-photo">
                </div>
                <div class="content">
                  <span>
                    <h4 class="title">
                      {{$blog->title}}
                    </h4>
                  </span>
                  <div class="text">
                    <p>
                      {{substr(strip_tags($blog->details),0,170)}}
                    </p>
                  </div>

                  <ul class="top-meta">
                    <li>
                      <span>
                        <i class="far fa-clock"></i>{{ \Carbon\Carbon::parse($blog->created_at)->diffForHumans() }}
                      </span>
                    </li>

                  </ul>
                </div>
              </a>
            </div>

            @endforeach
          @else
            {{__('No Blog Found')}}
          @endif

        </div>

        <div class="page-center">
          @if(isset($_GET['search']))
            {!! $blogs->appends(['search' => $_GET['search']])->links() !!}   
          @else
            {!! $blogs->links() !!}   
          @endif 
        </div>
      </div>
      <div class="col-lg-4">
        <div class="blog-aside">
          <div class="serch-widget">
            <h4 class="title">
              {{ __('Search') }}
            </h4>
            <form action="{{route('owner.front.blogsearch',$owner->username)}}" method="get">
              <input type="text" name="search" placeholder="{{__('Search Content')}}">
              <button class="submit" type="submit">{{__('Search')}}</button>
            </form>
          </div>
          <div class="categori">
            <h4 class="title">{{ __('Categories') }}</h4>
            <ul class="categori-list">
              @foreach ($bcat as $cat)
              <li>
                <a href="{{ route('owner.front.blogcategory',[$owner->username,$cat->slug,$cat->id]) }}">
                  <span>{{$cat->name}}</span>
                  <span>{{$cat->blogs->count()}}</span>
                </a>
              </li>
              @endforeach

            </ul>
          </div>
          <div class="recent-post-widget">
            <h4 class="title">{{ __('Recent Post') }}</h4>
            <ul class="post-list">
              @foreach (App\Models\Blog::orderBy('created_at', 'desc')->where('register_id',$owner->id)->limit(4)->get() as $blog)
              <li>
                  <div class="post">
                      <div class="post-img">
                          <img style="width: 73px; height: 59px;" src="{{ asset('assets/'.$owner->username.'/owner/images/blogs/'.$blog->photo) }}" alt="blog-image">
                      </div>
                      <div class="post-details">
                          <a href="{{ route('owner.front.blogshow',[$owner->username , $blog->id]) }}">
                              <h4 class="post-title">
                                  {{strlen($blog->title) > 45 ? substr($blog->title,0,45)." .." : $blog->title}}
                              </h4>
                          </a>
                          <p class="date">
                              {{ date('M d - Y',(strtotime($blog->created_at))) }}
                          </p>
                      </div>
                  </div>
              </li>
              @endforeach
            </ul>
          </div>

          <div class="tags">
            <h4 class="title">{{__('Tags')}}</h4>
            <span class="separator"></span>
            <ul class="tags-list">
              @foreach($tags as $tag)
                @if(!empty($tag))
                      <li>
                          <a href="{{ route('owner.front.blogtags',[$owner->username,$tag]) }}">{{ $tag }} </a>
                      </li>
                @endif 
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Blog Area End-->

@endsection