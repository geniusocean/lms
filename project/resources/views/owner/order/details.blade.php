@extends('layouts.owner')

@section('content')
    <div class="card">
        <div class="d-sm-flex align-items-center justify-content-between">
        <h5 class=" mb-0 text-gray-800 pl-3">{{ __('Purchase Details') }}<a class="btn btn-primary btn-rounded btn-sm ml-2" href="{{route('owner-purchase-index',$owner->username)}}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h5>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('owner.dashboard',$owner->username) }}">{{ __('Dashboard') }}</a></li>
            <li class="breadcrumb-item"><a href="javascript:;">{{ __('All Purchase') }}</a></li>
            <li class="breadcrumb-item"><a href="{{ route('owner-purchase-index',$owner->username) }}">{{ __('Purchase') }}</a></li>
        </ol>
        </div>
    </div>
    <section class="success">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="product-invoice">
                        <main class="columns">
                            <div class="inner-container">

                            <div class="table-responsive">
                                <table class="table invoice">
                                    <tbody>
                                    <tr class="header">
                                        <td class="">
                                            <img src="{{ asset('assets/'.$owner->username.'/owner/images/'.$gs->logo) }}" alt="Company Name">
                                        </td>
                                        <td class="align-right">
                                        <h2>{{__('Invoice')}}</h2>
                                        </td>
                                    </tr>
                                    <tr class="intro">
                                        <td class="">
                                            <strong>{{ __('Name') }}:</strong> {{ $order->user->showName() }}<br>
                                            <strong>{{ __('Email') }}:</strong> {{ $order->user->email }}
                                        </td>
                                        <td class="text-right">
                                            <strong>{{ __('Order Number') }}:</strong> {{ $order->order_number }}<br>
                                            <strong>{{ __('Purchase Date') }}:</strong> {{ date('l m, Y', strtotime($order->created_at))  }}
                                        </td>
                                    </tr>
                                    <tr class="details">
                                        <td colspan="2">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            {{__('Course Name')}}
                                                        </th>
                                                        <th>
                                                            {{__('Total')}}
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($cart['items'] as $course)
                                                        <tr class="item">
                                                            <td>
                                                                {{ $course['item']['title'] }}
                                                            </td>
                                                            <td>
                                                                {{ $order->currency_sign }}{{ round(($course['price'] * $order->currency_value),2) }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="totals">
                                        <td></td>
                                        <td>
                                            <table class="table">
                                                <tbody>
                                                    <tr class="total">
                                                        <td class="label text-right">{{__('Total')}}</td>
                                                        <td class="num">{{ $order->currency_sign }}{{ round(($order->pay_amount * $order->currency_value),2) }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </main>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection