@extends('layouts.owner')

@section('content')


    <div class="card">
        <div class="d-sm-flex align-items-center justify-content-between">
        <h5 class=" mb-0 text-gray-800 pl-3">{{ __('Purchase History') }}</h5>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('owner.dashboard',$owner->username) }}">{{ __('Dashboard') }}</a></li>
            <li class="breadcrumb-item"><a href="javascript:;">{{ __('All Purchase') }}</a></li>
            <li class="breadcrumb-item"><a href="{{ route('owner-purchase-index',$owner->username) }}">{{ __('Purchase History') }}</a></li>
        </ol>
        </div>
    </div>


    <!-- Row -->
    <div class="row mt-3">
      <!-- Datatables -->
      <div class="col-lg-12">

        @include('includes.admin.form-success')

        <div class="card mb-4">
          <div class="table-responsive p-3">
            <table class="table align-items-center table-flush" id="geniustable">
              <thead class="thead-light">
                <tr>
                  <th>{{ __('#Order') }}</th>
                  <th>{{ __('Date') }}</th>
                  <th>{{ __('Price') }}</th>
                  <th>{{ __('Payment Type') }}</th>
                  <th>{{ __('Deatils') }}</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
      <!-- DataTable with Hover -->

    </div>
    <!--Row-->

{{-- STATUS MODAL --}}

<div class="modal fade confirm-modal" id="details" tabindex="-1" role="dialog"
	aria-labelledby="statusModalTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
		<h5 class="modal-title">{{ __("Purchase details") }}</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		</div>
		<div class="modal-body">
			
		</div>
		<div class="modal-footer">
		<a href="javascript:;" class="btn btn-secondary" data-dismiss="modal">{{ __("Back") }}</a>
		</div>
	</div>
	</div>
</div>

{{-- STATUS MODAL ENDS --}}

{{-- STATUS MODAL --}}

    <div class="modal fade confirm-modal" id="statusModal" tabindex="-1" role="dialog"
        aria-labelledby="statusModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">{{ __("Update Status") }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <p class="text-center">{{ __("You are about to change the status.") }}</p>
                <p class="text-center">{{ __("Do you want to proceed?") }}</p>
            </div>
            <div class="modal-footer">
            <a href="javascript:;" class="btn btn-secondary" data-dismiss="modal">{{ __("Cancel") }}</a>
            <a href="javascript:;" class="btn btn-success btn-ok">{{ __("Update") }}</a>
            </div>
        </div>
        </div>
    </div>

{{-- STATUS MODAL ENDS --}}


@endsection


@section('scripts')

<script type="text/javascript">

var table = $('#geniustable').DataTable({
			   ordering: false,
               processing: true,
               serverSide: true,
               ajax: '{{ route('owner-purchase-datatables',$owner->username) }}',
               columns: [
                { data: 'order_number', name: 'order_number' },
                { data: 'created_at', searchable: false, orderable: false},
                { data: 'price'},
                { data: 'method', name: 'method' },
                { data: 'details', name: 'details' },
                     ],
                language : {
                	processing: '<img src="{{asset('assets/'.$owner->username.'/owner/images/'.$gs->admin_loader)}}">'
                }
            });
            

    $(document).on('click', '#purchaseDetails', function () {
      let detailsUrl = $(this).data('href');
      $.get(detailsUrl, function( data ) {
        $( "#details .modal-body" ).html( data );
      });
    })

</script>

@endsection
