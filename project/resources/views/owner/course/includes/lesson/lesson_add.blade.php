{{-- LESSON MODAL ADD --}}

<div class="modal fade" id="lessonModalAdd" tabindex="-1" role="dialog" aria-labelledby="lessonModalAddTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">{{ __("Add New Lesson") }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body ml-2 mr-2">

                <form class="c-form" action="{{ route('owner-lesson-store',$owner->username) }}" method="POST" enctype="multipart/form-data">

                    @include('includes.admin.form-both')

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="section_id">{{ __('Section') }} *</label>
                        <select class="form-control" name="section_id" id="section_id" required>
                            @foreach($data->sections()->oldest('id')->get() as $section)
                                <option value="{{ $section->id }}">{{ $section->title }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="title">{{ __('Title') }} *</label>
                        <input class="form-control" type="text" name="title"  id="title" required="" placeholder="{{ __('Enter Title') }}">
                    </div>

                    <div class="form-group">
                        <label for="video_file">{{ __('Type') }} *</label>
                        <select class="form-control video_file" name="video_file" id="video_file" required>
                                <option value="youtube">{{ __('YouTube') }}</option>
                                <option value="vimeo">{{ __('Vimeo') }}</option>
                                <option value="file">{{ __('File') }}</option>
                                <option value="url">{{ __('Link') }}</option>
                                <option value="document">{{ __('Document') }}</option>
                                <option value="image">{{ __('Image') }}</option>
                                <option value="iframe">{{ __('Iframe') }}</option>
                        </select>
                    </div>

                    <div class="file_show">

                        <div class="form-group">
                            <label for="link">{{ __("Link") }} *</label>
                            <input class="form-control" type="text" name="url"  id="link"  placeholder="{{ __("Enter Youtube Video Link") }}">
                        </div>

                        <div class="form-group">
                            <label for="duration">{{ __("Duration") }} *</label>
                            <input type="text" class="form-control"  name="duration" id="duration" value="00:00:00" required="">
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="details">{{ __('Details') }} *</label>
                        <textarea class="form-control" name="details" id="details" placeholder="{{ __('Enter Details') }}" required></textarea>
                    </div>

                    <input type="hidden" name="type" value="Lesson">

                    <div class="text-right">
                        <button class="btn btn-success submit-btn" type="submit">{{ __('Submit') }}</button>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
            <a href="javascript:;" class="btn btn-secondary" data-dismiss="modal">{{ __("Close") }}</a>
            </div>
        </div>
    </div>
</div>

{{-- LESSON MODAL ADD ENDS --}}
