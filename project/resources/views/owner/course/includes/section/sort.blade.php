{{-- SECTION SORT MODAL --}}

<div class="modal fade" id="sectionSortModal" tabindex="-1" role="dialog" aria-labelledby="sectionSortModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ __("Sort Sections") }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body ml-2 mr-2">
                <form action="{{ route('owner-section-sort-update',[$owner->username,$data->id]) }}" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row" id="parent-div">
                                        <div class="col-md-12">
                                            <div class="bg-dragula p-2 p-lg-4">
                                                <h5 class="mt-0">{{ __('List Of Sections') }}                                 <button type="submit" class="btn btn-outline-primary btn-sm btn-rounded float-right" id="section-sort-btn">{{ __('Update Sorting') }}</button>
                                                </h5>
                                                <div id="section-list" class="py-2">

                                                    @foreach($data->sections()->oldest('pos')->get() as $section)
                                                    <!-- Item -->
                                                        <div class="card mb-0 mt-2 draggable-item">
                                                            <div class="card-body">
                                                                <div class="media">
                                                                    <div class="media-body">
                                                                        <h5 class="mb-1 mt-0">{{ $section->title }}</h5>
                                                                        <input type="hidden" name="pos[]" value="{{ $section->pos }}">
                                                                    </div> <!-- end media-body -->
                                                                </div> <!-- end media -->
                                                            </div> <!-- end card-body -->
                                                        </div> <!-- end col -->
                                                    @endforeach

                                                </div> <!-- end company-list-1-->
                                            </div> <!-- end div.bg-light-->
                                        </div> <!-- end col -->
                                    </div> <!-- end row -->
                                </div> <!-- end card-body -->
                            </div> <!-- end card -->
                        </div> <!-- end col -->
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-secondary" data-dismiss="modal">{{ __("Close") }}</a>
            </div>
        </div>
    </div>
</div>

{{-- SECTION SORT MODAL ENDS --}}
