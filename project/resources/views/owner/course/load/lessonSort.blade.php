<form action="{{ route('owner-lesson-sort-update',[$owner->username,$data->id]) }}" method="POST">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row" id="parent-div">
                        <div class="col-md-12">
                            <div class="bg-dragula p-2 p-lg-4">
                                <h5 class="mt-0">{{ __('List Of Lessons') }}
                                    <button type="submit" class="btn btn-outline-primary btn-sm btn-rounded float-right" id="lesson-sort-btn">{{ __('Update Sorting') }}</button>
                                </h5>
                                <div id="lesson-list" class="py-2">
                                    @foreach($data->lessons()->oldest('pos')->get() as $lesson)
                                    <!-- Item -->
                                        <div class="card mb-0 mt-2 draggable-item">
                                            <div class="card-body">
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h5 class="mb-1 mt-0">{{ $lesson->title }}</h5>
                                                        <input type="hidden" name="pos[]" value="{{ $lesson->pos }}">
                                                    </div> <!-- end media-body -->
                                                </div> <!-- end media -->
                                            </div> <!-- end card-body -->
                                        </div> <!-- end col -->
                                    @endforeach
                                </div> <!-- end company-list-1-->
                            </div> <!-- end div.bg-light-->
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div> <!-- end col -->
    </div>
</form>

<script>

// SORTING SECTION

var el = document.getElementById('lesson-list');
Sortable.create(el, {
  animation: 100,
  group: 'list-1',
  draggable: '.draggable-item',
  handle: '.draggable-item',
  sort: true,
  filter: '.sortable-disabled',
  chosenClass: 'active'
});

// SORTING SECTION ENDS

</script>
