@extends('layouts.owner')

@section('content')

<div class="card">
    <div class="d-sm-flex align-items-center justify-content-between">
    <h5 class=" mb-0 text-gray-800 pl-3">{{ __('Edit Role') }} <a class="btn btn-primary btn-rounded btn-sm" href="{{route('owner-role-index',$owner->username)}}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h5>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('owner.dashboard',$owner->username) }}">{{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">{{ __('Roles') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('owner-role-index',$owner->username) }}">{{ __('Manage Roles') }}</a></li>
        <li class="breadcrumb-item"><a href="{{route('owner-role-edit',[$owner->username,$data->id])}}">{{ __('Edit Role') }}</a></li>
    </ol>
    </div>
</div>

<div class="row justify-content-center mt-3">
  <div class="col-lg-6">
    <!-- Form Basic -->
    <div class="card mb-4">
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">{{ __('Edit Role Form') }}</h6>
      </div>

      <div class="card-body">
        <div class="gocover" style="background: url({{asset('assets/'.$owner->username.'/owner/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
        <form class="geniusform" action="{{route('owner-role-update',[$owner->username,$data->id])}}" method="POST" enctype="multipart/form-data">

            @include('includes.admin.form-both')

            {{ csrf_field() }}

            
            <div class="form-group">
              <label for="inp-name">{{ __('Role Name') }}</label>
              <input type="text" class="form-control" id="inp-name" name="name"  placeholder="{{ __('Enter Role Name') }}" value="{{$data->name}}" required>
          </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Manage Categories" {{ $data->sectionCheck('Manage Categories') ? 'checked' : '' }} class="custom-control-input" id="manage_category">
                    <label class="custom-control-label" for="manage_category">{{__('Manage Categories')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Manage Students" {{ $data->sectionCheck('Manage Students') ? 'checked' : '' }} class="custom-control-input" id="manage_student">
                    <label class="custom-control-label" for="manage_student">{{__('Manage Students')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Manage Instructors" {{ $data->sectionCheck('Manage Instructors') ? 'checked' : '' }} class="custom-control-input" id="manage_instrutor">
                    <label class="custom-control-label" for="manage_instrutor">{{__('Manage Instructors')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Manage Courses" {{ $data->sectionCheck('Manage Courses') ? 'checked' : '' }} class="custom-control-input" id="manage_course">
                    <label class="custom-control-label" for="manage_course">{{__('Manage Courses')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Manage Referrals" class="custom-control-input" id="manage_referrals">
                    <label class="custom-control-label" for="manage_referrals">{{__('Manage Referrals')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Manage Blog" {{ $data->sectionCheck('Manage Blog') ? 'checked' : '' }} class="custom-control-input" id="manage_blog">
                    <label class="custom-control-label" for="manage_blog">{{__('Manage Blog')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="General Settings" {{ $data->sectionCheck('General Settings') ? 'checked' : '' }} class="custom-control-input" id="general_setting">
                    <label class="custom-control-label" for="general_setting">{{__('General Settings')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Home Page Settings" {{ $data->sectionCheck('Home Page Settings') ? 'checked' : '' }} class="custom-control-input" id="home_page_setting">
                    <label class="custom-control-label" for="home_page_setting">{{__('Home Page Settings')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Menu Page Settings" {{ $data->sectionCheck('Menu Page Settings') ? 'checked' : '' }} class="custom-control-input" id="menu_page_setting">
                    <label class="custom-control-label" for="menu_page_setting">{{__('Menu Page Settings')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Email Settings" {{ $data->sectionCheck('Email Settings') ? 'checked' : '' }} class="custom-control-input" id="email_setting">
                    <label class="custom-control-label" for="email_setting">{{__('Email Settings')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Payment Settings" {{ $data->sectionCheck('Payment Settings') ? 'checked' : '' }} class="custom-control-input" id="payment_setting">
                    <label class="custom-control-label" for="payment_setting">{{__('Payment Settings')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Language Settings" class="custom-control-input" id="language_setting">
                    <label class="custom-control-label" for="language_setting">{{__('Language Settings')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Seo Tools" class="custom-control-input" id="seo_tools">
                    <label class="custom-control-label" for="seo_tools">{{__('Seo Tools')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Social Settings" {{ $data->sectionCheck('Social Settings') ? 'checked' : '' }} class="custom-control-input" id="social_setting">
                    <label class="custom-control-label" for="social_setting">{{__('Social Settings')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Manage Roles" {{ $data->sectionCheck('Manage Roles') ? 'checked' : '' }} class="custom-control-input" id="roles">
                    <label class="custom-control-label" for="roles">{{__('Manage Roles')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Manage Staff" {{ $data->sectionCheck('Manage Staff') ? 'checked' : '' }} class="custom-control-input" id="staff">
                    <label class="custom-control-label" for="staff">{{__('Manage Staff')}}</label>
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="custom-control custom-switch">
                    <input type="checkbox" name="section[]" value="Subscribers" {{ $data->sectionCheck('Subscribers') ? 'checked' : '' }} class="custom-control-input" id="subscribers">
                    <label class="custom-control-label" for="subscribers">{{__('Subscribers')}}</label>
                    </div>
                </div>
              </div>
            </div>

            <button type="submit" id="submit-btn" class="btn btn-primary">{{ __('Submit') }}</button>


        </form>
      </div>
    </div>
  </div>

</div>
<!--Row-->

@endsection
