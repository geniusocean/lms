@extends('layouts.owner')

@section('content')

<div class="content-area">
  <div class="card">
    <div class="d-sm-flex align-items-center justify-content-between">
    <h5 class=" mb-0 text-gray-800 pl-3">{{ __('Group Email') }}</h5>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('owner.dashboard',$owner->username) }}">{{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">{{ __('Email Settings') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('owner-group-show',$owner->username) }}">{{ __('Group Email') }}</a></li>
    </ol>
    </div>
  </div>
</div>

<div class="row justify-content-center mt-3">
  <div class="col-lg-6">
    <!-- Form Basic -->
    <div class="card mb-4">
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      </div>

      <div class="card-body">
        <div class="gocover" style="background: url({{asset('assets/'.$owner->username.'/owner/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
        <form class="geniusform" action="{{route('owner-group-submit',$owner->username)}}" method="POST" enctype="multipart/form-data">

            @include('includes.admin.form-both')

            {{ csrf_field() }}


            <div class="form-group">
              <label for="inp-name">{{ __('Select User Type') }}*</label>
                <select name="type" required="" class="input-field">
                    <option value=""> {{ __('Choose User Type') }} </option>
                    <option value="0">{{ __('Customers') }}</option>
                    <option value="1">{{ __('Vendors') }}</option>
                    <option value="2">{{ __('Subscribers') }}</option>
                </select>
            </div>

            <div class="form-group">
              <label for="inp-name">{{ __('Email Subject') }} *</label>
              <small>{{ __('(In Any Language)') }}</small>
              <input type="text" class="input-field" name="subject" placeholder="{{ __('Email Subject') }}" value="" required="">
            </div>


            <div class="form-group">
              <label for="inp-name">{{ __('Email Body') }} *</label>
              <small>{{ __('(In Any Language)') }}</small>
              <textarea class="form-control summernote" name="body" placeholder="{{ __('Email Body') }}"></textarea> 
            </div>

            <button type="submit" id="submit-btn" class="btn btn-primary">{{ __('Submit') }}</button>

        </form>
      </div>
    </div>
  </div>
</div>

@endsection