@extends('layouts.owner')

@section('content')

<div class="card">
  <div class="d-sm-flex align-items-center justify-content-between">
  <h5 class=" mb-0 text-gray-800 pl-3">{{ __('Add New Rule') }} <a class="btn btn-primary btn-rounded btn-sm" href="{{route('owner-referral-index',$owner->username)}}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h5>
  <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('owner.dashboard',$owner->username) }}">{{ __('Dashboard') }}</a></li>
      <li class="breadcrumb-item"><a href="javascript:;">{{ __('Manage Referrals') }}</a></li>
      <li class="breadcrumb-item"><a href="{{ route('owner-referral-index',$owner->username) }}">{{ __('Referrals') }}</a></li>
      <li class="breadcrumb-item"><a href="{{route('owner-referral-create',$owner->username)}}">{{ __('Add New Rule') }}</a></li>
  </ol>
  </div>
</div>

<div class="row justify-content-center mt-3">
<div class="col-lg-6">
  <!-- Form Basic -->
  <div class="card mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      <h6 class="m-0 font-weight-bold text-primary">{{ __('Add New Rule Form') }}</h6>
    </div>

    <div class="card-body">
      <div class="gocover" style="background: url({{asset('assets/'.$owner->username.'/owner/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
      <form class="geniusform" action="{{route('owner-referral-store',$owner->username)}}" method="POST" enctype="multipart/form-data">

          @include('includes.admin.form-both')

          {{ csrf_field() }}

          <div class="form-group">
              <label for="r-title">{{ __('Title') }}</label>
              <input type="text" class="form-control" id="r-title" name="title" placeholder="{{ __('Enter Title') }}" required="" value="">
          </div>

          <div class="form-group">
            <label for="r-type">{{ __('Type') }}</label>
            <select name="type" class="form-control" id="r-type" required>
              <option value="Student">{{__('Student')}}</option>
              <option value="Teacher">{{__('Teacher')}}</option>
            </select>
          </div>


        <div class="form-group">
          <label for="r-times">{{ __('Times') }}</label>
          <input type="number" min="1" class="form-control" id="r-times" name="times" placeholder="{{ __('Enter Times') }}" required="" value="">
        </div>


        <div class="form-group">
          <label for="r-discount">{{ __('Discount') }}</label>
          <input type="number" min="1" id="r-discount" class="form-control" name="discount" placeholder="{{ __('Enter Discount') }}" required="" value="">
        </div>

          <button type="submit" id="submit-btn" class="btn btn-primary">{{ __('Submit') }}</button>

      </form>
    </div>
  </div>

  <!-- Form Sizing -->

  <!-- Horizontal Form -->

</div>

</div>

@endsection