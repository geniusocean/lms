@extends('layouts.owner')

@section('content')

<div class="card">
  <div class="d-sm-flex align-items-center justify-content-between">
  <h5 class=" mb-0 text-gray-800 pl-3">{{ __('Add New Currency') }} <a class="btn btn-primary btn-rounded btn-sm" href="{{route('owner-currency-index',$owner->username)}}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h5>
  <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('owner.dashboard',$owner->username) }}">{{ __('Dashboard') }}</a></li>
      <li class="breadcrumb-item"><a href="javascript:;">{{ __('Manage Currencies') }}</a></li>
      <li class="breadcrumb-item"><a href="{{ route('owner-currency-index',$owner->username) }}">{{ __('Main Currency') }}</a></li>
      <li class="breadcrumb-item"><a href="{{route('owner-currency-create',$owner->username)}}">{{ __('Add New Currency') }}</a></li>
  </ol>
  </div>
</div>

<div class="row justify-content-center mt-3">
<div class="col-lg-6">
  <!-- Form Basic -->
  <div class="card mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      <h6 class="m-0 font-weight-bold text-primary">{{ __('Add New Currency Form') }}</h6>
    </div>

    <div class="card-body">
      <div class="gocover" style="background: url({{asset('assets/'.$owner->username.'/owner/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
      <form class="geniusform" action="{{route('owner-currency-store',$owner->username)}}" method="POST" enctype="multipart/form-data">

          @include('includes.admin.form-both')

          {{ csrf_field() }}

          <div class="form-group">
              <label for="c-name">{{ __('Currency Name') }}</label>
              <input type="text" class="form-control" name="name" placeholder="{{ __('Enter Currency Name') }}" required="" value="">
          </div>

          <div class="form-group">
              <label for="inp-sign">{{ __('Currency sign') }}</label>
              <input type="text" class="form-control"  id="inp-sign" name="sign" placeholder="{{ __('Enter Currency Sign') }}" required="" value="">
          </div>

          <div class="form-group">
            <label for="inp-value">{{ __('Value') }}</label>
            <input type="text" class="form-control" id="inp-value" name="value" placeholder="{{ __('Enter Currency Value') }}" required="" value="">
        </div>

          <button type="submit" id="submit-btn" class="btn btn-primary">{{ __('Submit') }}</button>

      </form>
    </div>
  </div>

  <!-- Form Sizing -->

  <!-- Horizontal Form -->

</div>

</div>

@endsection