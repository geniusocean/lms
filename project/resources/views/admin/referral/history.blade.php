@extends('layouts.admin')

@section('content')
<div class="card">
	<div class="d-sm-flex align-items-center justify-content-between">
	<h5 class=" mb-0 text-gray-800 pl-3">{{ __('Referral History') }}</h5>
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }}</a></li>
		<li class="breadcrumb-item"><a href="javascript:;">{{ __('Manage Referrals') }}</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin-referral-history-index') }}">{{ __('Referral History') }}</a></li>
	</ol>
	</div>
</div>

    <!-- Row -->
    <div class="row mt-3">
		<!-- Datatables -->
		<div class="col-lg-12">

          @include('includes.form-success')

		  @include('includes.admin.form-success')

		  <div class="card mb-4">
			<div class="table-responsive p-3">
			  <table class="table align-items-center table-flush" id="geniustable">
				<thead class="thead-light">
				  <tr>
					<th>{{ __('Referrer') }}</th>
					<th>{{ __('Buyer') }}</th>
					<th>{{ __('Course') }}</th>
					<th>{{ __('Refer Type') }}</th>
				  </tr>
				</thead>
			  </table>
			</div>
		  </div>
		</div>
		<!-- DataTable with Hover -->

	  </div>
	  <!--Row-->





@endsection



@section('scripts')

    <script type="text/javascript">

		var table = $('#geniustable').DataTable({
			   ordering: false,
               processing: true,
               serverSide: true,
               ajax: '{{ route('admin-referral-history-datatables') }}',
               columns: [
                        { data: 'referrer_id', name: 'referrer_id' },
						{ data: 'affilate_id', name: 'affilate_id' },
						{ data: 'course_id', name: 'course_id' },
                        { data: 'type', name: 'type' },

                     ],
               language: {
                	processing: '<img src="{{asset('assets/images/'.$gs->admin_loader)}}">'
                }
            });

    </script>
@endsection
