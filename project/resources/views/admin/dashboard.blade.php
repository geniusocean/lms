@extends('layouts.admin')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{ __('Dashboard') }}</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }}</a></li>
    </ol>
  </div>

  <div class="row mb-3">

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card h-100">
        <div class="card-body">
          <div class="row align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">{{ __('Courses') }}</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ \DB::table('courses')->whereRegisterId(0)->whereStatus(1)->count() }}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-graduation-cap fa-2x text-primary"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card h-100">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">{{ __('Students') }}</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ \DB::table('users')->whereRegisterId(0)->where('is_instructor','!=',2)->whereStatus(1)->count() }}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-user-graduate fa-2x text-success"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card h-100">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">{{ __('Instructors') }}</div>
              <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{ \DB::table('users')->whereRegisterId(0)->where('is_instructor','=',2)->whereStatus(1)->count() }}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-user-tie fa-2x text-info"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card h-100">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">{{ __('Posts') }}</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ \DB::table('blogs')->whereRegisterId(0)->count() }}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-comments fa-2x text-warning"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Area Chart -->
    <div class="col-xl-12 col-lg-12">
      <div class="card mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-primary">{{ __('Total Sales in Last 30 Days') }}</h6>

        </div>
        <div class="card-body">
          <div class="chart-area">
            <canvas id="lineChart"></canvas>
          </div>
        </div>
      </div>
    </div>



  </div>
  <!--Row-->

@endsection

@section('scripts')


<script type="text/javascript">
    
    displayLineChart();

    function displayLineChart() {
        var data = {
            labels: [
            {!!$days!!}
            ],
            datasets: [{
                label: "Prime and Fibonacci",
                fillColor: "#3dbcff",
                strokeColor: "#0099ff",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [
                {!!$sales!!}
                ]
            }]
        };
        var ctx = document.getElementById("lineChart").getContext("2d");
        var options = {
            responsive: true
        };
        var lineChart = new Chart(ctx).Line(data, options);
    }

</script>
  

@endsection
