@extends('layouts.admin')
@section('content')

    <div class="card">
        <div class="d-sm-flex align-items-center justify-content-between">
        <h5 class=" mb-0 text-gray-800 pl-3">{{ __('Add New Owner') }}</h5>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }}</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin-owner-create') }}">{{ __('Add New Owner') }}</a></li>
        </ol>
        </div>
    </div>

    <div class="row justify-content-center mt-3">
      <div class="col-lg-6">
        <!-- Form Basic -->
        <div class="card mb-4">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">{{ __('Add New Owner Form') }}</h6>
          </div>

          <div class="card-body">
            <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
            <form class="geniusform" action="{{ route('admin-owner-store') }}" method="POST" enctype="multipart/form-data">

                @include('includes.admin.form-both')

                {{ csrf_field() }}

                <div class="form-group">
                    <label>{{ __('Profile Picture') }} <small class="small-font">({{ __('Preferred Size 600 X 600') }})</small></label>
                    <div class="wrapper-image-preview">
                        <div class="box">
                            <div class="back-preview-image" style="background-image: url({{ asset('assets/images/placeholder.jpg') }});"></div>
                            <div class="upload-options">
                                <label class="img-upload-label" for="img-upload"> <i class="fas fa-camera"></i> {{ __('Upload Picture') }} </label>
                                <input id="img-upload" type="file" class="image-upload" name="photo" accept="image/*">
                            </div>
                        </div>
                    </div>
                </div>

               

                <div class="form-group">
                    <label for="inp-name">{{ __('Name') }}</label>
                    <input type="text" class="form-control" id="inp-name" name="name"  placeholder="{{ __('Enter Name') }}"  required>
                </div>
                <div class="form-group">
                    <label for="inp-name">{{ __('Unsername') }}</label>
                    <input type="text" class="form-control" id="inp-name" name="username"  placeholder="{{ __('Enter Username') }}"  required>
                </div>
                
                <div class="form-group">
                    <label for="inp-eml">{{ __('Email Address') }}</label>
                    <input type="email" class="form-control" id="inp-eml" name="email"  placeholder="{{ __('Enter Email Address') }}" required>
                </div>

                <div class="form-group">
                    <label for="inp-phn">{{ __('Phone') }}</label>
                    <input type="text" class="form-control" id="inp-phn" name="phone"  placeholder="{{ __('Phone Number') }}"  required>
                </div>

                <div class="form-group">
                    <label for="inp-phn">{{ __('Password') }}</label>
                    <input type="password" class="form-control" id="inp-phn" name="password"  placeholder="{{ __('Enter Password') }}"  required>
                </div>

             
                <div class="form-group">
                    <label for="inp-bio">{{ __('Biography') }}</label>
                    <textarea name="biography" class="input-field summernote" placeholder="{{ __('Biography') }}"></textarea>
                </div>

                <div class="form-group">
                    <label for="inp-name">{{ __('Address') }}</label>
                    <input type="text" class="form-control" id="inp-address" name="address"  placeholder="{{ __('Enter Address') }}" >
                </div>

                <div class="row mt-3 is_top">
                    <div class="col-lg-3">
                        <div class="left-area">

                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="form-check">
                            <input type="checkbox" name="is_preloaded" class="form-check-input" value="1" id="is_preloaded">
                            <label class="form-check-label" for="is_preloaded">{{ __('Allow preloaded content') }}</label>
                          </div>
                    </div>
                </div>

                <button type="submit" id="submit-btn" class="btn btn-primary">{{ __('Submit') }}</button>

            </form>
          </div>
        </div>
        <!-- Form Sizing -->
      </div>


    </div>
    <!--Row-->


@endsection
