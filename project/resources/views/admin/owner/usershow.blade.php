@extends('layouts.load')

@section('content')

<div class="content-area no-padding">
    <div class="add-product-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="product-description">
                    <div class="body-area">
                        <div class="table-responsive show-table">
                            <table class="table">
                                <tr>
                                    <th>{{__('User ID')}}#</th>
                                    <td>{{$data->id}}</td>
                                </tr>
                                <tr>
                                    <th>{{__('User Photo')}}</th>
                                    <td>
                                    <img src="{{ $data->photo ? asset('assets/'.$username.'/owner/images/users/'.$data->photo):asset('assets/images/noimage.png')}}" alt="No Image">

                                    </td>
                                </tr>
                                <tr>
                                    <th>{{__('User First Name')}}</th>
                                    <td>{{$data->first_name}}</td>
                                </tr>
                                <tr>
                                    <th>{{__('User Last Name')}}</th>
                                    <td>{{$data->last_name}}</td>
                                </tr>
                                <tr>
                                    <th>{{__('Zip')}}</th>
                                    <td>{{$data->zip}}</td>
                                </tr>
                                <tr>
                                    <th>{{__('City')}}</th>
                                    <td>{{$data->city}}</td>
                                </tr>
                                <tr>
                                    <th>{{__('Address')}}</th>
                                    <td>{{$data->address}}</td>
                                </tr>
                                <tr>
                                    <th>{{__('User Email')}}</th>
                                    <td>{{$data->email}}</td>
                                </tr>
                                <tr>
                                    <th>{{__('User Phone')}}</th>
                                    <td>{{$data->phone}}</td>
                                </tr>
                                <tr>
                                    <th>{{__('Joined')}}</th>
                                    <td>{{$data->created_at->diffForHumans()}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection