@extends('layouts.load')

@section('content')
<div class="content-area">
    <div class="add-product-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="product-description">
                    <div class="body-area">
                    @include('includes.admin.form-error') 
                    <form id="geniusformdata"  action="{{ route('admin-owner-edit',$data->id) }}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="row">
                            <div class="col-lg-4">
                              <div class="left-area">
                                  <h4 class="heading">{{__('Profile Image')}} *</h4>
                              </div>
                            </div>
                            <div class="col-lg-7">
                              <div class="img-upload">

                                <div id="image-preview" class="img-preview" style="background: url({{ $data->photo ? asset('assets/images/owners/'.$data->photo) : asset('assets/images/noimage.png') }});">

                                      <label for="image-upload" class="img-label" id="image-label"><i class="icofont-upload-alt"></i>{{__('Upload Image')}}</label>
                                      <input type="file" name="photo" class="img-upload" id="image-upload">
                                 </div>
                                <p class="text">{{__('Prefered Size')}}: {{__('(600x600) or Square Sized Image')}}</p>
                              </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-4">
                                <div class="left-area">
                                    <h4 class="heading">{{__('User Name')}} *</h4>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <input type="text" class="input-field" name="username" placeholder="{{__('User Name')}}" value="{{ $data->username }}" readonly>
                                <div id="owner-slug">
                                    <p>
                                        <b>URL:</b> {{ route('frontowner.index',$data->username) }}
                                    </p>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="left-area">
                                    <h4 class="heading">{{__('Name')}} *</h4>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <input type="text" class="input-field" name="name" placeholder="{{__('Name')}}" required="" value="{{ $data->name }}">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-4">
                                <div class="left-area">
                                    <h4 class="heading">{{__('Email')}} *</h4>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <input type="email" class="input-field" name="email" placeholder="{{__('Email Address')}}" required="" value="{{ $data->email }}" readonly>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="left-area">
                                    <h4 class="heading">{{__('Phone')}} *</h4>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <input type="text" class="input-field" name="phone" placeholder="{{ __('Phone Number') }}" required="" value="{{ $data->phone }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="left-area">
                                    <h4 class="heading">{{__('Password')}} *</h4>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <input type="password" class="input-field" name="password" placeholder="{{ __('Password') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="left-area">

                                </div>
                            </div>
                            <div class="col-lg-7">

                                <div class="checkbox-wrapper mt-0">
                                    <input type="checkbox" name="is_preloaded" id="is_preloaded" value="1" {{ $data->is_preloaded == 1 ? 'checked' : '' }}>
                                    <label for="is_preloaded">{{ __('Allow preloaded content') }}</label>
                                </div>

                            </div>
                        </div>



                        <div class="row">
                            <div class="col-lg-4">
                                <div class="left-area">

                                </div>
                            </div>
                            <div class="col-lg-7">

                                <div class="checkbox-wrapper mt-0">
                                    <input type="checkbox" name="is_future_update" id="is_future_update" {{ $data->is_future_update == 1 ? 'checked' : '' }} value="1">
                                    <label for="is_future_update">{{ __('Allow future update') }}</label>
                                </div>

                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-4">
                                <div class="left-area">
                                    
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <button class="addProductSubmit-btn" type="submit">{{__('Update')}}</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection