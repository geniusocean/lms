
        <form class="c-form" action="{{ route('admin-lesson-update',$data->id) }}" method="POST" enctype="multipart/form-data">

            @include('includes.admin.form-both')

            {{ csrf_field() }}

            <div class="form-group">
                <label for="section_id">{{ __('Select Section') }} *</label>
                <select class="form-control" name="section_id" id="section_id" required>
                    @foreach($data->section->course->sections()->oldest('id')->get() as $section)
                        <option value="{{ $section->id }}" {{ $section->id == $data->section_id ? 'selected' : '' }}>{{ $section->title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="title">{{ __('Title') }} *</label>
                <input class="form-control" type="text" name="title"  id="title" required="" placeholder="{{ __('Enter Title') }}" value="{{ $data->title }}">
            </div>

            <div class="form-group">
                <label for="video_file">{{ __('Video Type') }} *</label>
                <select class="form-control video_file" name="video_file" id="video_file" required>
                    <option value="youtube" {{ $data->video_file == 'youtube' ? 'selected' : '' }}>{{ __('YouTube') }}</option>
                    <option value="vimeo" {{ $data->video_file == 'vimeo' ? 'selected' : '' }}>{{ __('Vimeo') }}</option>
                    <option value="file" {{ $data->video_file == 'file' ? 'selected' : '' }}>{{ __('File') }}</option>
                    <option value="url" {{ $data->video_file == 'url' ? 'selected' : '' }}>{{ __('Link') }}</option>
                    <option value="document" {{ $data->video_file == 'document' ? 'selected' : '' }}>{{ __('Document') }}</option>
                    <option value="image" {{ $data->video_file == 'image' ? 'selected' : '' }}>{{ __('Image') }}</option>
                    <option value="iframe" {{ $data->video_file == 'iframe' ? 'selected' : '' }}>{{ __('Iframe') }}</option>
                </select>
            </div>

            <div class="file_show">

                @if($data->video_file == 'youtube')

                <div class="form-group">
                    <label for="link">{{ __('Link') }} *</label>
                    <input class="form-control" type="text" name="url"  id="link"  placeholder="eg. https://www.youtube.com/watch?v=aFIsyEfXDIw" value="{{ $data->url }}">
                </div>

                <div class="form-group">
                    <label for="duration">{{ __('Duration') }} *</label>
                    <input type="text" class="form-control"  name="duration" id="duration-l" value="{{ $data->duration }}" required="">
                </div>

                @elseif($data->video_file == 'file')

                <div class="form-group">
                    <label>{{ __('File') }} *</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="file" name="file">
                            <label class="custom-file-label" for="file">{{ __('Select File') }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="duration">{{ __('Duration') }} *</label>
                    <input type="text" class="form-control"  name="duration" id="duration-l" value="{{ $data->duration }}" required="">
                </div>

                @elseif($data->video_file == 'vimeo')

                <div class="form-group">
                    <label for="link">{{ __('Link') }} *</label>
                    <input class="form-control" type="text" name="url"  id="link"  placeholder="eg. https://vimeo.com/403146037" value="{{ $data->url }}">
                </div>

                <div class="form-group">
                    <label for="duration">{{ __('Duration') }} *</label>
                    <input type="text" class="form-control"  name="duration" id="duration-l" value="{{ $data->duration }}" required="">
                </div>

                @elseif($data->video_file == 'url')

                <div class="form-group">
                    <label for="link">{{ __('Link') }} *</label>
                    <input class="form-control" type="text" name="url"  id="link"  placeholder="{{ __('Enter Link') }}" value="{{ $data->url }}">
                </div>

                <div class="form-group">
                    <label for="duration">{{ __('Duration') }} *</label>
                    <input type="text" class="form-control"  name="duration" id="duration-l" value="{{ $data->duration }}" required="">
                </div>

                @elseif($data->video_file == 'document')

                <div class="form-group">
                    <label>{{ __('Attachment') }} <small>{{ __("Must be type of .pdf, .txt") }}</small></label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="file" name="file">
                            <label class="custom-file-label" for="file">{{ __('Select File') }}</label>
                        </div>
                    </div>
                </div>

                @elseif($data->video_file == 'image')

                <div class="form-group">
                    <label>{{ __('Attachment') }} <small>{{ __("Must be type of .jpg, .jpeg, .png, .gif") }}</small></label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="file" name="file">
                            <label class="custom-file-label" for="file">{{ __('Select File') }}</label>
                        </div>
                    </div>
                </div>

                @else

                <div class="form-group">
                    <label for="iframe">{{ __("Iframe Embed Code") }} *</label>
                    <textarea class="form-control" name="iframe_code" required="" placeholder="{{ __("Enter Iframe Embed Code") }}">{{ $data->iframe_code }}</textarea>
                </div>

                @endif

            </div>

            <div class="form-group">
                <label for="details">{{ __('Details') }} *</label>
                <textarea class="form-control" name="details" id="details" placeholder="{{ __('Enter Details') }}" required>{{ $data->details }}</textarea>
            </div>

            <input type="hidden" name="type" value="Lesson">

            <div class="text-right">
                <button class="btn btn-success submit-btn" type="submit">{{ __('Submit') }}</button>
            </div>
        </form>
