<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="GeniusOcean">
  <meta name="author" content="GeniusOcean">
  <link rel="icon"  type="image/x-icon" href="{{asset('assets/images/'.$gs->favicon)}}"/>
  <title>{{$gs->title}}</title>
  <link href="{{ asset('assets/admin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/admin/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/admin/css/ruang-admin.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/admin/css/custom.css') }}" rel="stylesheet">

</head>

<body class="bg-gradient-login">
  <!-- Login Content -->
  <div class="container-login">
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card shadow-sm my-5">
          <div class="card-body p-0">
            <div class="row">
              <div class="col-lg-12">
                <div class="login-form">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">{{ __('Change Password') }}</h1>
                  </div>
                  <form id="passwordform" action="{{ route('admin.change.password') }}" method="POST" class="user">
                    {{ csrf_field() }}
                    @include('includes.admin.form-login')

                    <div class="form-group">
                      <input name="cpass"  type="password" class="form-control"
                        placeholder="{{ __('Current Password') }}" required >
                    </div>

                    <div class="form-group">
                        <input name="newpass"  type="password" class="form-control"
                          placeholder="{{ __('New Password') }}" required >
                      </div>

                      <div class="form-group">
                        <input name="renewpass"  type="password" class="form-control"
                          placeholder="{{ __('Re-Type New Password') }}" required >
                      </div>


                    <div class="form-group">
                        <input type="hidden" name="admin_id" value="{{ $id }}">
                        <input type="hidden" name="file_token" value="{{ $token }}">
                        <button type="submit" class="btn btn-primary btn-block">{{ __('Submit') }}</button>
                    </div>

                  </form>

                  <div class="text-center">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Login Content -->
  <script src="{{ asset('assets/admin/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/admin/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('assets/admin/js/ruang-admin.js') }}"></script>

    <script>

        $("#passwordform").on('submit',function(e){
          e.preventDefault();
          $('button.submit-btn').prop('disabled',true);
        $('.gocover').show();
              $.ajax({
               method:"POST",
               url:$(this).prop('action'),
               data:new FormData(this),
               dataType:'JSON',
               contentType: false,
               cache: false,
               processData: false,
               success:function(data)
               {
                $('.gocover').hide();
                  if ((data.errors)) {
                  $('.alert-success').hide();
                  $('.alert-danger').show();
                  $('.alert-danger ul').html('');
                    for(var error in data.errors)
                    {
                      $('.alert-danger ul').append('<li>'+ data.errors[error] +'</li>');
                    }
                  }
                  else {

                    $('.alert-success').show();
                    $('.alert-success p').html(data);
                  }
                  $('button.submit-btn').prop('disabled',false);
               }

              });

        });

        </script>


  </body>

</html>
