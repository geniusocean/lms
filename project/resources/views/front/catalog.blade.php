@extends('layouts.front')
@section('content')
<!--Main Breadcrumb Area Start -->
<div class="main-breadcrumb-area" style="background-image: url({{ $gs->breadcumb_banner ? asset('assets/images/'.$gs->breadcumb_banner):asset('assets/images/noimage.png') }});">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ul class="pages">
          <li>
            <a href="{{route('front.index')}}">
              {{__('Home')}}
            </a>
          </li>
          @if(!empty($cat))
          <li class="{{ empty($subcat) ? 'active' : '' }}">
            <a href="{{ route('front.catalog',$cat->slug) }}">
              {{$cat->name}}
            </a>
          </li>
          @endif
          @if(!empty($subcat))
          <li class="active">
            <a href="{{route('front.catalog', [$cat->slug, $subcat->slug])}}">{{ $subcat->name }}</a>
         </li>
          @endif
        </ul>
      </div>
    </div>
  </div>
</div>
<!--Main Breadcrumb Area End -->

<!-- Category Page Area Start -->
<section class="categories-page">
  <div class="container">
    <div class="row">

      @include('includes.catalog')

      <div class="col-lg-9">
        <div class="all-products">
          <div class="row">
            @if(count($courses) > 0)

            <div class="ajax-loader" style="background: url({{asset('assets/images/'.$gs->loader)}}) no-repeat scroll center center rgba(0,0,0,.6);"></div>

            <div class="col-lg-12" id="ajaxContent">

              @foreach ($courses as $course)
              <div  class="single-course-long">
                <div class="img">
                  <img src="{{asset('assets/images/courses/'.$course->photo)}}" alt="">
                </div>
                <div class="content">
                  <div class="left-area">
                    <p class="author">
                      <a href="{{!empty($course->instructor) ? route('instructor.details',$course->instructor->instructor_slug) : route('instructor.details',DB::table('admins')->first()->slug)}}">
                        {{ !empty($course->instructor) ? $course->instructor->instructor_name : DB::table('admins')->first()->name }}
                      </a>
                    </p>
                  <h4 class="title">
                    <a href="{{ route('front.course',$course->slug) }}">
                      {{$course->title}}
                    </a>
                  </h4>
                  <a href="{{ route('front.course',$course->slug) }}" class="text">
                    {{$course->short_description}}
                  </a>
                  @if(App\Models\Rating::normalRating($course->id) != 0)
                  <div class="reating-area">
                    <span class="number">{{ App\Models\Rating::normalRating($course->id) }}</span>
                    <div class="stars">
                        <div class="ratings">
                            <div class="empty-stars"></div>
                            <div class="full-stars" style="width:{{ App\Models\Rating::ratings($course->id) }}%"></div>
                          </div>
                    </div>
                    <div class="total-star">
                      ({{ App\Models\Rating::ratingCount($course->id) }})
                    </div>
                  </div>
                  @endif
                  <ul class="short-info">
                    @if($course->convertTime() != '')
                    <li>
                      {{ $course->convertTime() }}
                    </li>
                    @endif
                    <li>
                      {{ $course->sections()->withCount('lessons')->get()->sum('lessons_count') }} {{ __('lecture(s)') }}
                    </li>
                    <li>
                        {{ $course->level }} {{ __('Level') }}
                    </li>
                  </ul>
                  </div>
                  <div class="right-area">
                    @if($course->price == 0)

                    <p class="price">
                      {{ __('Free') }}
                    </p>

                    @else

                    <p class="price">
                      {{ $curr->sign }}{{ round(($course->price * $curr->value),2) }} <del>{{ $curr->sign }}{{ round(($course->discount_price),2) }}</del>
                    </p>

                    @endif
                  </div>

                </div>
              </div>
              @endforeach

              <div class="page-center mt-5">
                {!! $courses->appends(['search' => request()->input('search')])->links() !!}
              </div>


            </div>
            @else
            <h4>{{__('Course Not Found')}}</h4>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Category Page Area End -->
@endsection

@section('scripts')

<script>



    $("#sortby").on('change', function() {

    $("#preloader").show();
    filter();

    });

    $(".course-type").on('change', function() {

    if(this.checked){
        $('.course-type').not(this).prop('checked', false);
        $("#preloader").show();
        filter();
    }

    });


    $(".ratings").on('change', function() {

      $("#preloader").show();
        filter();

    });

    $(".durations").on('change', function() {

    if(this.checked){
        $('.durations').not(this).prop('checked', false);
        $("#preloader").show();
        filter();
    }

    });


    $(".course-level").on('change', function() {

    if(this.checked){
        $('.course-level').not(this).prop('checked', false);
        $("#preloader").show();
        filter();
    }

    });


  function filter() {
    let filterlink = '';

    if ($("#sortby").val() != '') {
      if (filterlink == '') {
        filterlink += '{{route('front.catalog', [Request::route('category'), Request::route('subcategory')])}}' + '?'+$("#sortby").attr('name')+'='+$("#sortby").val();
      } else {
        filterlink += '&'+$("#sortby").attr('name')+'='+$("#sortby").val();
      }
    }

    $(".course-type").each(function() {
      if ($(this).is(':checked')) {
        if (filterlink == '') {
          filterlink += '{{route('front.catalog', [Request::route('category'), Request::route('subcategory')])}}' + '?'+$(this).attr('name')+'='+$(this).val();
        } else {
          filterlink += '&'+$(this).attr('name')+'='+$(this).val();
        }
      }
    });


    $(".ratings").each(function() {
      if ($(this).is(':checked')) {
        if (filterlink == '') {
          filterlink += '{{route('front.catalog', [Request::route('category'), Request::route('subcategory')])}}' + '?'+$(this).attr('name')+'='+$(this).val();
        } else {
          filterlink += '&'+$(this).attr('name')+'='+$(this).val();
        }
      }
    });


    $(".durations").each(function() {
      if ($(this).is(':checked')) {
        if (filterlink == '') {
          filterlink += '{{route('front.catalog', [Request::route('category'), Request::route('subcategory')])}}' + '?'+$(this).attr('name')+'='+$(this).val();
        } else {
          filterlink += '&'+$(this).attr('name')+'='+$(this).val();
        }
      }
    });

    $(".course-level").each(function() {
      if ($(this).is(':checked')) {
        if (filterlink == '') {
          filterlink += '{{route('front.catalog', [Request::route('category'), Request::route('subcategory')])}}' + '?'+$(this).attr('name')+'='+$(this).val();
        } else {
          filterlink += '&'+$(this).attr('name')+'='+$(this).val();
        }
      }
    });


    $("#ajaxContent").load(encodeURI(filterlink), function(data) {
      // add query string to pagination
      addToPagination();
      $("#preloader").fadeOut(1000);
    });
  }

  // append parameters to pagination links
  function addToPagination() {


    // add to attributes in pagination links
    $('ul.pagination li a').each(function() {
      let url = $(this).attr('href');
      let queryString = '?' + url.split('?')[1]; // "?page=1234...."

      let urlParams = new URLSearchParams(queryString);
      let page = urlParams.get('page'); // value of 'page' parameter

      let fullUrl = '{{route('front.catalog', [Request::route('category'),Request::route('subcategory')])}}?page='+page+'&search='+'{{urlencode(request()->input('search'))}}';


      if ($("#sortby").val() != '') {
        fullUrl += '&sort='+encodeURI($("#sortby").val());
      }

      $(".course-type").each(function() {
        if ($(this).is(':checked')) {
          fullUrl += '&'+encodeURI($(this).attr('name'))+'='+encodeURI($(this).val());
        }
      });

      $(".course-level").each(function() {
        if ($(this).is(':checked')) {
          fullUrl += '&'+encodeURI($(this).attr('name'))+'='+encodeURI($(this).val());
        }
      });

      $(".ratings").each(function() {
        if ($(this).is(':checked')) {
          fullUrl += '&'+encodeURI($(this).attr('name'))+'='+encodeURI($(this).val());
        }
      });

      $(".durations").each(function() {
        if ($(this).is(':checked')) {
          fullUrl += '&'+encodeURI($(this).attr('name'))+'='+encodeURI($(this).val());
        }
      });


      $(this).attr('href', fullUrl);

    });
  }

</script>



@endsection
