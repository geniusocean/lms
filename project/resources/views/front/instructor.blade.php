@extends('layouts.front')

@section('content')
    <!-- Main Breadcrumb Area Start -->
	<div class="main-breadcrumb-area" style="background-image: url({{ $gs->breadcumb_banner ? asset('assets/images/'.$gs->breadcumb_banner):asset('assets/images/noimage.png') }});">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<ul class="pages">
						<li>
							@if($type == 'instructor')

							<a href="{{route('instructor.details',$instructor->instructor_slug)}}">
								{{__('Instructor Profile Page')}}
							</a>

							@else

							<a href="{{route('instructor.details',$instructor->slug)}}">
								{{__('Instructor Profile Page')}}
							</a>

							@endif
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!--Main Breadcrumb Area End -->

	<!-- Teacher Page Start -->
	<section class="teacher-page">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="about-instructor">
						@if($type == 'instructor')
						<div class="about-instructor-content">
							<div class="about-left-content">
								<div class="images">
									<img src="{{asset('assets/images/users/'.$instructor->photo)}}" alt="instructor">
								</div>
								<ul class="info-list">
									<li>
										<i class="fas fa-star"></i> <span>{{ \App\Models\Rating::InstructorNormalRatings($instructor->id) }}</span> {{ __('Instructor Rating') }}
									</li>
									<li>
										<i class="fas fa-comment"></i> <span>{{ $instructor->courses()->withCount('ratings')->get()->sum('ratings_count') }}</span> {{ __('Reviews') }}
									</li>
									<li>
										<i class="fas fa-user"></i> <span>{{ $instructor->courses()->withCount('enrolled_courses')->get()->sum('enrolled_courses_count') }}</span> {{ __('Students') }}
									</li>
									<li>
										<i class="fas fa-play-circle"></i> <span>{{$instructor->courses->count()}}</span> {{__('Courses')}}
									</li>
								</ul>
								<ul class="social-links">
                                    @if($instructor->facebook)
									<li>
										<a href="{{$instructor->facebook}}">
											<i class="fab fa-facebook-f"></i>
										</a>
                                    </li>
                                    @endif
                                    @if($instructor->linkedin)
									<li>
										<a href="{{$instructor->linkedin}}">
											<i class="fab fa-linkedin-in"></i>
										</a>
                                    </li>
                                    @endif
                                    @if($instructor->twitter)
									<li>
										<a href="{{$instructor->twitter}}">
											<i class="fab fa-twitter"></i>
										</a>
                                    </li>
                                    @endif
								  </ul>
							</div>
							<div class="about-right-content">
								<h4 class="name">
									{{$instructor->instructor_name}}
								</h4>
								<p class="job-title">
									{{$instructor->address}}
								</p>
								<div class="about-description">
                                    {!! $instructor->bio  !!}
								</div>
							</div>
						</div>
						@else
						<div class="about-instructor-content">
							<div class="about-left-content">
							  <div class="images">
								<img src="{{asset('assets/images/admins/'.$instructor->photo)}}" alt="">
							  </div>
							  <ul class="info-list">
								<li>
								  <i class="fas fa-star"></i> <span>{{ \App\Models\Rating::InstructorNormalRatings(0) }}</span> {{ __('Instructor Rating') }}
								</li>
								<li>
								  <i class="fas fa-comment"></i> <span>{{ \App\Models\Course::where('user_id','=',0)->withCount('ratings')->get()->sum('ratings_count') }}</span> {{ __('Reviews') }}
								</li>
								<li>
								  <i class="fas fa-user"></i> <span>{{ \App\Models\Course::where('user_id','=',0)->withCount('enrolled_courses')->get()->sum('enrolled_courses_count') }}</span> {{ __('Students') }}
								</li>
								<li>
								  <i class="fas fa-play-circle"></i> <span>{{ DB::table('courses')->whereUserId(0)->count() }}</span> {{ __('Courses') }}
								</li>
							  </ul>
							</div>
							<div class="about-right-content">
							  <h4 class="name">
								{{ $instructor->name }}
							  </h4>
							  <p class="job-title">
								{{ $instructor->address }}
							  </p>
							  <div class="about-description">
								  {!! $instructor->biography !!}
							  </div>
							</div>
						  </div>
						@endif
					</div>
				</div>
            </div>

            <div id="ajaxContent">
                <div class="row all-videos pb-0">
                    <div class="col-lg-12">
                        <div class="section-heading">
                            <h2 class="title">{{__('All Courses')}}
                            </h2>
                        </div>
                    </div>
                    @foreach ($courses as $course)
                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <div class="single-course">
                            <div class="img">
                                <img src="{{asset('assets/images/courses/'.$course->photo)}}" alt="course">
                            </div>
                            <div class="content">
                                <p class="author">
                                    <a href="{{!empty($course->instructor) ? route('instructor.details',$course->instructor->instructor_slug) : route('instructor.details',DB::table('admins')->first()->slug)}}">
                                        {{ !empty($course->instructor) ? $course->instructor->instructor_name : DB::table('admins')->first()->name }}
                                    </a>

                                </p>
                                <a href="{{route('front.course',$course->slug)}}">
                                    <h4 class="title">
                                        {{ $course->showTitle() }}
                                    </h4>
                                </a>

                                @if(App\Models\Rating::normalRating($course->id) != 0)
                                <div class="reating-area">
                                <span class="number">{{ App\Models\Rating::normalRating($course->id) }}</span>
                                <div class="stars">
                                    <div class="ratings">
                                        <div class="empty-stars"></div>
                                        <div class="full-stars" style="width:{{ App\Models\Rating::ratings($course->id) }}%"></div>
                                        </div>
                                </div>
                                <div class="total-star">
                                    ({{ App\Models\Rating::ratingCount($course->id) }})
                                </div>
                                </div>
                                @endif
								@if($course->price == 0)

								<p class="price">
								  {{ __('Free') }}
								</p>

								@else

								<p class="price">
									{{$curr->sign}}{{round(($course->price * $curr->value),2)}} <del>{{ $curr->sign }}{{ round(($course->discount_price * $curr->value),2) }}</del>
								</p>

								@endif
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>

                <div class="page-center pb-5">

                    {!! $courses->links() !!}

                </div>
            </div>
		</div>
	</section>
	<!-- Teacher Page End -->
@endsection
