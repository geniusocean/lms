
<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>{{ $gs->title }}</title>

	<!-- favicon -->
	<link rel="shortcut icon" href="{{asset('assets/images/'.$gs->favicon)}}" type="image/x-icon">
	<!-- bootstrap -->
	<link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.min.css')}}">
	<!-- Plugin css -->
    <link rel="stylesheet" href="{{asset('assets/front/css/plugin.css')}}">
    <!-- Toastr css -->
	<link rel="stylesheet" href="{{asset('assets/front/css/toastr.css')}}">
	<!-- stylesheet -->
	<link rel="stylesheet" href="{{asset('assets/front/css/style.css')}}">
	<!-- responsive -->
	<link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}">

</head>

<body class="py-5">

<div class="container checkout">
	<div class="row justify-content-center mb-5">
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-12">
					<span class="payment-header-text float-left"><b>{{ __('Make payment') }}</b></span>
					<a href="{{ route('front.cart') }}" class="close-btn-light float-right"><i class="fa fa-times"></i></a>
				</div>
			</div>
		</div>
	</div>

	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-3">
					<p class="pb-2 payment-header">{{ __('Payment Gateway') }}</p>
                        @if($paypal->status == 1)
                            <div class="row payment-gateway paypal">
                                <div class="col-12">
                                    <img class="tick-icon paypal-icon d-none" src="{{ asset('assets/images/tick.png') }}">
                                    <img class="payment-gateway-icon" data-title="paypal" src="{{ asset('assets/images/'.$paypalData['photo']) }}">
                                </div>
                            </div>
                        @endif
                        @if($stripe->status == 1)
                            <div class="row payment-gateway stripe">
                                <div class="col-12">
                                    <img class="tick-icon stripe-icon d-none" src="{{ asset('assets/images/tick.png') }}">
                                    <img class="payment-gateway-icon" data-title="stripe" src="{{ asset('assets/images/'.$stripeData['photo']) }}">
                                </div>
                            </div>
                        @endif
					<!--paystack payment gateway addon-->

				</div>

				<div class="col-md-1"></div>

				<div class="col-md-8">
					<div class="w-100">
                        <p class="pb-2 payment-header">{{ __('Order Summary') }}</p>
                            @foreach($courses as $course)



                            <p class="item float-left">
								<span class="count-item">{{ $loop->index + 1}}</span>
								<span class="item-title">{{ $course['item']['title'] }}
									<span class="item-price">
										{{ $curr->sign }}{{ round(($course['price'] * $curr->value),2) }}
									</span>
								</span>
								<span class="by-owner">
                                    {{ __('By') }} {{ $course['instructor'] }}
                                </span>
                            </p>



                            @endforeach
                    </div>

                    @if($discount != 0)

					<div class="w-100 float-left mt-4 indicated-price pb-0">
						<div class="float-right total-price">{{ $curr->sign }}{{ round(($discount * $curr->value),2) }}</div>
						<div class="float-right discount">{{ __('Discount') }}</div>
					</div>

                    @endif

					<div class="w-100 float-left mt-4 indicated-price">
						<div class="float-right total-price">{{ $curr->sign }}{{ round(($totalPrice * $curr->value),2) }}</div>
						<div class="float-right total">{{ __('Total') }}</div>
					</div>
					<div class="w-100 float-left">
                        @if($paypal->status == 1)
                            <form action="{{ route('paypal.submit') }}" method="post" class="paypal-form form d-none">

                                @csrf

                                <hr class="border mb-4">

                                <input type="hidden" name="discount" value="{{ $discount }}">

                                <input type="hidden" name="method" value="{{ $paypal->name }}">

                                <input type="hidden" name="pay_amount" value="{{ $totalPrice }}">

                                <button type="submit" class="payment-button float-right">{{ __('Pay By Paypal') }}</button>

                            </form>
                        @endif
                        @if($stripe->status == 1)
                            <form action="{{ route('stripe.submit') }}" method="post" class="stripe-form form d-none">

                                @csrf

                                <hr class="border mb-4">

                                <input type="hidden" name="discount" value="{{ $discount }}">

                                <input type="hidden" name="method" value="{{ $stripe->name }}">

                                <input type="hidden" name="pay_amount" value="{{ $totalPrice }}">

                                <div class="form-group">
                                    <input type="text" class="form-control" name="cardNumber"  placeholder="{{ __('Card Number') }}" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="cardCVC"  placeholder="{{ __('Cvv') }}" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="month"  placeholder="{{ __('Month') }}" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="year"  placeholder="{{ __('Year')}}" required>
                                </div>
                                <button type="submit" class="payment-button float-right">{{ __('Pay By Stripe') }}</button>

                            </form>
                        @endif
			        </div>
				</div>
			</div>
		</div>
	</div>
</div>

	<!-- Become an instructor modal End -->
	<script>

        var mainurl = "{{ url('/') }}";

        var gs      = {!! json_encode(DB::table('generalsettings')->where('id','=',1)->first(['is_loader'])) !!};

        var langg = {
            'cart_already' : '{{ __('Already Added To Cart.') }}',
            'add_cart'     : '{{ __('Successfully Added To Cart.') }}',
            'cart_empty'   : '{{ __('Cart is empty.') }}'
        };

	</script>

	<!-- jquery -->
	<script src="{{asset('assets/front/js/jquery.js')}}"></script>
	<!-- popper -->
    <script src="{{asset('assets/front/js/popper.min.js')}}"></script>
	<!-- bootstrap -->
	<script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
    <!-- plugin js-->
	<script src="{{asset('assets/front/js/plugin.js')}}"></script>
	{{-- toastr.js --}}
	<script src="{{asset('assets/front/js/toastr.js')}}"></script>
	<!-- custom js-->
	<script src="{{asset('assets/front/js/custom.js')}}"></script>
	<!-- main -->
	<script src="{{asset('assets/front/js/main.js')}}"></script>

	@yield('scripts')

	@if(session()->has('success'))
    <script>
       toastr["success"]("{{__(session('success'))}}");
    </script>
    @endif

    @if(session()->has('error'))
    <script>
       toastr["error"]("{{__(session('error'))}}");
    </script>
    @endif

    <script>

        $('.payment-gateway-icon').on('click',function(){
            var $this = $(this);
            $this.prev().removeClass('d-none');
            $('.payment-gateway-icon').not(this).prev().addClass('d-none');
            $('.form').addClass('d-none');
            if($this.data('title') == 'paypal' ){
                $('.paypal-form').removeClass('d-none');
            }else{
                $('.stripe-form').removeClass('d-none');
            }

        });

    </script>


</body>

</html>
