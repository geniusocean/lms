@extends('layouts.front')


@section('content')


<!--Main Breadcrumb Area Start -->
<div class="main-breadcrumb-area" style="background-image: url({{ $gs->breadcumb_banner ? asset('assets/images/'.$gs->breadcumb_banner):asset('assets/images/noimage.png') }});">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <ul class="pages">
            <li>
              <a href="{{route('front.index')}}">
                {{ __('Home') }}
              </a>
            </li>
            <li>
              <a href="{{route('front.contact')}}">
                {{__('Contact Us')}}
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!--Main Breadcrumb Area End -->

<!-- Contact Us Area Start -->
<section class="contact-us">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="section-heading">
                    <h2 class="title">
                        {{__('Contact Us')}}
                    </h2>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-lg-7">
                <div class="left-area">
                    <div class="contact-form">
                        <div class="gocover" style="background: url({{ asset('assets/images/'.$gs->loader) }}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
                            <form id="contactform" action="{{route('front.contact.submit')}}" method="POST">
                            @csrf
                            @include('includes.admin.form-both')
                                <ul>
                                <li>
                                    <input type="text" name="name" required class="input-field" placeholder="{{__('Name')}} *">
                                </li>
                                <li>
                                    <input type="email" name="email" required class="input-field" placeholder="{{__('Email')}} *">
                                </li>
                                <li>
                                    <input type="text" name="phone" required class="input-field" placeholder="{{__('Phone')}} *">
                                </li>
                                <li>
                                <li>
                                    <textarea class="input-field textarea" required name="text" placeholder="{{__('Your Message')}} *"></textarea>
                                </li>
                                <input type="hidden" name="to" value="{{ $ps->contact_email }}">
                            </ul>
                            @if($gs->is_capcha == 1)

                            <ul class="captcha-area">
                                <li>
                                    <p><img class="codeimg1" src="{{asset("assets/images/capcha_code.png")}}" alt=""> <i class="fas fa-sync-alt pointer refresh_code"></i></p>

                                </li>
                                <li>
                                    <input name="codes" type="text" class="input-field" placeholder="{{ __('Capcha Code') }}" required="">

                                </li>
                            </ul>

                            @endif
                            <button class="submit-btn mybtn1" type="submit">{{ __('Send Message') }}</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">

                <div class="right-area">
                    @if($ps->site != null || $ps->email != null )
                    <div class="contact-info">
                        <div class="left ">
                                <div class="icon">
                                        <img src="{{ asset('assets/images/emal.png') }}" alt="">
                                </div>
                        </div>
                        <div class="content">
                                <h4 class="title">
                                    {{ __('Email') }}
                                </h4>
                                @if($ps->email != null)
                                <a href="mailto:{{$ps->email}}">
                                    {{ $ps->email }}
                                </a>
                                @endif
                                @if($ps->email != null)
                                <a href="{{ $ps->site }}">
                                    {{ $ps->site }}
                                </a>
                                @endif
                        </div>
                    </div>
                    @endif
                    @if($ps->street != null)
                    <div class="contact-info">
                        <div class="left ">
                                <div class="icon">
                                        <img src="{{ asset('assets/images/location.png') }}" alt="">
                                </div>
                        </div>
                        <div class="content">
                                <h4 class="title">
                                    {{ __('Location') }}
                                </h4>
                                    <p>
                                        {{ $ps->street }}
                                    </p>

                        </div>
                    </div>
                    @endif
                    @if($ps->phone != null || $ps->fax != null )
                    <div class="contact-info">
                        <div class="left ">
                                <div class="icon">
                                        <img src="{{ asset('assets/images/call.png') }}" alt="">
                                </div>
                        </div>
                        <div class="content">
                                <h4 class="title">
                                    {{ __('Phone') }}
                                </h4>
                                @if($ps->phone != null)
                                    <a href="tel:{{$ps->phone}}">
                                        {{$ps->phone}}
                                    </a>
                                @endif
                                @if($ps->fax != null)
                                    <a href="tel:{{$ps->fax}}">
                                        {{$ps->fax}}
                                    </a>
                                @endif
                        </div>
                    </div>
                    @endif
                    <div class="social-links">
                        <h4 class="title">{{ __('Find us here') }} :</h4>
                        <ul>
                            @if(App\Models\Socialsetting::find(1)->f_status == 1)
                            <li>
                              <a href="{{ App\Models\Socialsetting::find(1)->facebook }}" class="facebook" target="_blank">
                                  <i class="fab fa-facebook-f"></i>
                              </a>
                            </li>
                            @endif

                            @if(App\Models\Socialsetting::find(1)->g_status == 1)
                            <li>
                              <a href="{{ App\Models\Socialsetting::find(1)->gplus }}" class="google-plus" target="_blank">
                                  <i class="fab fa-google-plus-g"></i>
                              </a>
                            </li>
                            @endif

                            @if(App\Models\Socialsetting::find(1)->t_status == 1)
                            <li>
                              <a href="{{ App\Models\Socialsetting::find(1)->twitter }}" class="twitter" target="_blank">
                                  <i class="fab fa-twitter"></i>
                              </a>
                            </li>
                            @endif

                            @if(App\Models\Socialsetting::find(1)->l_status == 1)
                            <li>
                              <a href="{{ App\Models\Socialsetting::find(1)->linkedin }}" class="linkedin" target="_blank">
                                  <i class="fab fa-linkedin-in"></i>
                              </a>
                            </li>
                            @endif

                            @if(App\Models\Socialsetting::find(1)->d_status == 1)
                            <li>
                              <a href="{{ App\Models\Socialsetting::find(1)->dribble }}" class="dribbble" target="_blank">
                                  <i class="fab fa-dribbble"></i>
                              </a>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>

            </div>
          </div>
        </div>
    </div>
</section>
<!-- Contact Us Area End-->

@endsection
