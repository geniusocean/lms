@extends('layouts.front')

@section('content')

<!--Main Breadcrumb Area Start -->
<div class="main-breadcrumb-area" style="background-image: url({{ $gs->breadcumb_banner ? asset('assets/images/'.$gs->breadcumb_banner):asset('assets/images/noimage.png') }});">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <ul class="pages">
            <li>
              <a href="{{route('front.index')}}">
                {{ __('Home') }}
              </a>
            </li>
            <li>
              <a href="{{route('front.payment.return')}}">
                {{ __('Success') }}
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!--Main Breadcrumb Area End -->

<section class="success">

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="product-invoice">
                    <main class="columns">
                        <div class="inner-container">

                        <div class="table-responsive">
                            <table class="table invoice">
                                <tbody>
                                <tr class="header">
                                    <td class="">
                                    <img src="{{ asset('assets/images/'.$gs->logo) }}" alt="Company Name">
                                    </td>
                                    <td class="align-right">
                                    <h2>{{ __('Invoice') }}</h2>
                                    </td>
                                </tr>
                                <tr class="intro">
                                    <td class="">
                                    <strong>{{ __('Name') }}:</strong> {{ $order->user->showName() }}<br>
                                    <strong>{{ __('Email') }}:</strong> {{ $order->user->email }}
                                    </td>
                                    <td class="text-right">
                                    <strong>{{ __('Order Number') }}:</strong> {{ $order->order_number }}<br>
                                    <strong>{{ __('Purchase Date') }}:</strong> {{ date('l m, Y', strtotime($order->created_at))  }}
                                    </td>
                                </tr>
                                <tr class="details">
                                    <td colspan="2">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        {{ __('Course Name') }}
                                                    </th>
                                                    <th>
                                                        {{ __('Total') }}
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @foreach($tempcart->items as $course)
                                                <tr class="item">
                                                    <td>
                                                        {{ $course['item']['title'] }}
                                                    </td>
                                                    <td>
                                                        {{$order->currency_sign}}{{round(($course['price'] * $order->currency_value),2)}} 
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="totals">
                                    <td></td>
                                    <td>
                                        <table class="table">
                                            <tbody>
                                                @if($order->discount != 0)
                                                <tr class="subtotal">
													<td class="label text-right">{{ __('Discount') }}</td>
													<td id="padding-right-11" class="num">
                                                        {{$order->currency_sign}}{{round(($order->discount * $order->currency_value),2)}} 
                                                    </td>
												</tr>
                                                @endif
                                                <tr class="total">
                                                    <td class="label text-right">{{ __('Total') }}</td>
                                                    <td class="num">{{$order->currency_sign}}{{round(($order->pay_amount * $order->currency_value),2)}} </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>

</section>

@endsection
