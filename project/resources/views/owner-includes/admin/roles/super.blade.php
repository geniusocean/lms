<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#cat" aria-expanded="true"
    aria-controls="collapseTable">
    <i class="fas fa-fw fa-sitemap"></i>
    <span>{{ __('Manage Categories') }}</span>
  </a>
  <div id="cat" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{ route('owner-cat-index',$owner->username) }}">{{ __('Main Categories') }}</a>
      <a class="collapse-item" href="{{ route('owner-subcat-index',$owner->username) }}">{{ __('Subcategories') }}</a>
    </div>
  </div>
</li>

<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#student" aria-expanded="true"
    aria-controls="collapseTable">
    <i class="fas fa-fw fa-user-graduate"></i>
    <span>{{  __('Manage Students') }}</span>
  </a>
  <div id="student" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{ route('owner-student-index',$owner->username) }}">{{ __('Student Lists') }}</a>
      <a class="collapse-item" href="{{ route('owner-enroll-history-index',$owner->username) }}">{{ __('Enroll History') }}</a>
    </div>
  </div>
</li>

<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#instructor" aria-expanded="true"
    aria-controls="collapseTable">
    <i class="fas fa-fw fa-user-tie"></i>
    <span>{{  __('Manage Instructors') }}</span>
  </a>
  <div id="instructor" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{ route('owner-instructor-index',$owner->username) }}">{{ __('Instructors List') }}</a>
      <a class="collapse-item" href="{{ route('owner-instructor-application',$owner->username) }}">{{ __('Instructors Request') }}</a>
      <a class="collapse-item" href="{{ route('owner-withdraw-index',$owner->username) }}">{{ __('Withdraw Request') }}</a

    </div>
  </div>
</li>

<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#course" aria-expanded="true"
    aria-controls="collapseTable">
    <i class="fas fa-fw fa-graduation-cap"></i>
    <span>{{ __('Manage Courses') }}</span></a>
  </a>
  <div id="course" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{ route('owner-course-index',$owner->username) }}">{{ __('Course List') }}</a>
      <a class="collapse-item" href="{{ route('owner-purchase-index',$owner->username) }}">{{ __('Purhchase History') }}</a>
    </div>
  </div>
</li>


<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#referral" aria-expanded="true"
    aria-controls="collapseTable">
    <i class="fas fa-fw fa-link"></i>
    <span>{{ __('Manage Referrals') }}</span></a>
  </a>
  <div id="referral" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{ route('owner-referral-index',$owner->username) }}">{{ __('Referrals') }}</a>
    <a class="collapse-item" href="{{ route('owner-referral-history-index',$owner->username) }}">{{ __('Referral History') }}</a>
    </div>
  </div>
</li>

<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#blog" aria-expanded="true"
    aria-controls="collapseTable">
    <i class="fas fa-fw fa-newspaper"></i>
    <span>{{  __('Manage Blog') }}</span>
  </a>
  <div id="blog" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{ route('owner-cblog-index',$owner->username) }}">{{ __('Categories') }}</a>
      <a class="collapse-item" href="{{ route('owner-blog-index',$owner->username) }}">{{ __('Posts') }}</a>
    </div>
  </div>
</li>

<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTable1" aria-expanded="true"
    aria-controls="collapseTable">
    <i class="fas fa-fw fa-cogs"></i>
    <span>{{  __('General Settings') }}</span>
  </a>
  <div id="collapseTable1" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{ route('owner-gs-logo',$owner->username) }}">{{ __('Logo') }}</a>
      <a class="collapse-item" href="{{ route('owner-gs-fav',$owner->username) }}">{{ __('Favicon') }}</a>
      <a class="collapse-item" href="{{ route('owner-gs-load',$owner->username) }}">{{ __('Loader') }}</a>
      <a class="collapse-item" href="{{ route('owner-gs-breadcumb',$owner->username) }}">{{ __('Breadcumb Banner') }}</a>
      <a class="collapse-item" href="{{ route('owner-gs-contents',$owner->username) }}">{{ __('Website Contents') }}</a>
    </div>
  </div>
</li>

<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#home" aria-expanded="true"
    aria-controls="collapseTable">
    <i class="fas fa-fw fa-home"></i>
    <span>{{  __('Home Page Settings') }}</span>
  </a>
  <div id="home" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{ route('owner-hero-section-index',$owner->username) }}">{{ __('Hero Section') }}</a>
      <a class="collapse-item" href="{{ route('owner-service-index',$owner->username) }}">{{ __('Service Section') }}</a>
      <a class="collapse-item" href="{{ route('owner-instructor-section-index',$owner->username) }}">{{ __('Instructor Section') }}</a>
    </div>
  </div>
</li>


<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#menu" aria-expanded="true"
    aria-controls="collapseTable">
    <i class="fas fa-fw fa-edit"></i>
    <span>{{  __('Menu Page Settings') }}</span>
  </a>
  <div id="menu" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{ route('owner-ps-contact',$owner->username) }}">{{ __('Contact Us Page') }}</a>
      <a class="collapse-item" href="{{ route('owner-page-index',$owner->username) }}">{{ __('Other Pages') }}</a>
    </div>
  </div>
</li>


<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#email_settings" aria-expanded="true"
    aria-controls="collapseTable">
    <i class="fas fa-fw fa-at"></i>
    <span>{{  __('Email Settings') }}</span>
  </a>
  <div id="email_settings" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{ route('owner-mail-index',$owner->username) }}">{{ __('Email Template') }}</a>
      <a class="collapse-item" href="{{ route('owner-mail-config',$owner->username) }}">{{ __('Email Configurations') }}</a>
      <a class="collapse-item" href="{{ route('owner-group-show',$owner->username) }}">{{ __('Group Email') }}</a>
    </div>
  </div>
</li>

<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#payment_gateways" aria-expanded="true"
    aria-controls="collapseTable">
    <i class="fas fa-fw fa-newspaper"></i>
    <span>{{  __('Payment Settings') }}</span>
  </a>
  <div id="payment_gateways" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{ route('owner-payment-info',$owner->username) }}">{{ __('Payment Informations') }}</a>
      <a class="collapse-item" href="{{ route('owner-currency-index',$owner->username) }}">{{ __('Currencies') }}</a>
      <a class="collapse-item" href="{{ route('owner-payment-index',$owner->username) }}">{{  __('Payment Gateways')  }}</a>
    </div>
  </div>
</li>

<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#langs" aria-expanded="true"
    aria-controls="collapseTable">
    <i class="fas fa-language"></i>
    <span>{{  __('Language Settings') }}</span>
  </a>
  <div id="langs" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{route('owner-lang-index',$owner->username)}}">{{ __('Website Language') }}</a>
      <a class="collapse-item" href="{{route('owner-tlang-index',$owner->username)}}">{{ __('Admin Panel Language') }}</a>
    </div>
  </div>
</li>

<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#social_settings" aria-expanded="true"
    aria-controls="collapseTable">
    <i class="fas fa-fw fa-share-square"></i>
    <span>{{  __('Social Settings') }}</span>
  </a>
  <div id="social_settings" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{route('owner-social-index',$owner->username)}}">{{ __('Social Links') }}</a>
      <a class="collapse-item" href="{{route('owner-social-facebook',$owner->username)}}">{{ __('Facebook Login') }}</a>
      <a class="collapse-item" href="{{route('owner-social-google',$owner->username)}}">{{ __('Google Login') }}</a>

    </div>
  </div>
</li>


<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#seoTools" aria-expanded="true"
    aria-controls="collapseTable">
    <i class="fas fa-wrench"></i>
    <span>{{  __('SEO Tools') }}</span>
  </a>
  <div id="seoTools" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <a class="collapse-item" href="{{route('owner-seotool-analytics',$owner->username)}}">{{ __('Google Analytics') }}</a>
      <a class="collapse-item" href="{{route('owner-seotool-keywords',$owner->username)}}">{{ __('Website Meta Keywords') }}</a>

    </div>
  </div>
</li>


<li class="nav-item">
  <a class="nav-link" href="{{ route('owner-role-index',$owner->username) }}">
    <i class="fas fa-fw fa-users-cog"></i>
    <span>{{ __('Manage Roles') }}</span></a>
</li>

<li class="nav-item">
  <a class="nav-link" href="{{ route('owner-staff-index',$owner->username) }}">
    <i class="fas fa-fw fa-users"></i>
    <span>{{ __('Manage Staff') }}</span></a>
</li>


<li class="nav-item">
  <a class="nav-link" href="{{ route('owner-subs-index',$owner->username) }}">
    <i class="fas fa-fw fa-users-cog"></i>
    <span>{{ __('Subscribers') }}</span></a>
</li>

