@extends('layouts.front')
@section('content')

<!--Main Breadcrumb Area Start -->
<div class="main-breadcrumb-area" style="background-image: url({{ $gs->breadcumb_banner ? asset('assets/images/'.$gs->breadcumb_banner):asset('assets/images/noimage.png') }});">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ul class="pages">
          <li>
            <a href="{{route('front.index')}}">
              {{ __('Home') }}
            </a>
          </li>
          <li>
            <a href="javascript:;">
              {{ __('Error') }}
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!--Main Breadcrumb Area End -->

<section class="fourzerofour">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="content">
            <img src="{{ $gs->error_banner ? asset('assets/images/'.$gs->error_banner):asset('assets/images/noimage.png') }}" alt="">
            <h4 class="heading">
              {{ __('Oops! You are lost...') }}
            </h4>
            <p class="text">
              {{ __('The page you are looking for might have been moved, renamed, or might never existed.') }}
            </p>
            <a class="mybtn1" href="{{ route('front.index') }}">{{ __('Back Home') }}</a>
          </div>
        </div>
      </div>
    </div>
  </section>


@endsection