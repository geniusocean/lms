@extends('layouts.owner-instructor')

@section('content')

    <div class="card">
        <div class="d-sm-flex align-items-center justify-content-between">
        <h5 class=" mb-0 text-gray-800 pl-3">{{ __('Withdraws') }}</h5>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('owner-instructor-dashboard',$owner->username) }}">{{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('owner-instructor-wt-index',$owner->username) }}">{{ __('Withdraws') }}</a></li>
        </ol>
        </div>
    </div>


    <!-- Row -->
    <div class="row mt-3">
      <!-- Datatables -->
      <div class="col-lg-12">

        @include('includes.admin.form-success')
        
        <div class="card mb-4">
          <div class="table-responsive p-3">
            <table class="table align-items-center table-flush" id="geniustable">
              <thead class="thead-light">
                <tr>
                  <th>{{ __('Withdraw Date') }}</th>
                  <th>{{ __('Method') }}</th>
                  <th>{{ __('Account') }}</th>
                  <th>{{ __('Amount') }}</th>
                  <th>{{ __('Status') }}</th>
                </tr>
              </thead>
              <tbody>
                @foreach($withdraws as $withdraw)
                  <tr>
                      <td>{{date('d-M-Y',strtotime($withdraw->created_at))}}</td>
                      <td>{{$withdraw->method}}</td>
                      @if($withdraw->method != "Bank")
                          <td>{{$withdraw->acc_email}}</td>
                      @else
                          <td>{{$withdraw->iban}}</td>
                      @endif
                      <td>{{$sign->sign}}{{ round($withdraw->amount * $sign->value , 2) }}</td>
                      <td>{{ucfirst($withdraw->status)}}</td>
                  </tr>  
                  @endforeach         
            </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- DataTable with Hover -->

    </div>
    <!--Row-->


{{-- DELETE MODAL --}}

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

    <div class="modal-header d-block text-center">
        <h4 class="modal-title d-inline-block">{{ __('Confirm Delete') }}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
    </div>
                <div class="modal-body">
            <p class="text-center">{{ __('You are about to delete this Conversation.') }}</p>
            <p class="text-center">{{ __('Do you want to proceed?') }}</p>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancle') }}</button>
                    <a class="btn btn-danger btn-ok">{{ __('Delete') }}</a>
                </div>
            </div>
        </div>
    </div>


{{-- DELETE MODAL ENDS --}}


@endsection


@section('scripts')

<script type="text/javascript">

var table = $('#geniustable').DataTable();
$(function() {
            $(".btn-area").append('<div class="col-sm-12 col-md-4 text-right">'+
                '<a class="btn btn-primary" href="{{route('owner-instructor-wt-create',$owner->username)}}">'+
            '<i class="fas fa-plus"></i> {{'New Withdraw'}}'+
            '</a>'+
            '</div>');
        });	

</script>




@endsection
