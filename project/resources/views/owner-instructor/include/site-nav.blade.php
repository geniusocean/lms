
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#course" aria-expanded="true"
      aria-controls="collapseTable">
      <i class="fas fa-fw fa-graduation-cap"></i>
      <span>{{ __('Manage Courses') }}</span></a>
    </a>
    <div id="course" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="{{ route('owner-instructor-course-index',$owner->username) }}">{{ __('Course List') }}</a>
        <a class="collapse-item" href="{{ route('owner-instructor-purchase-index',$owner->username) }}">{{ __('Purhchase History') }}</a>
      </div>
    </div>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="{{ route('owner-instructor-messages',$owner->username) }}">
      <i class="fas fa-fw fa-graduation-cap"></i>
      <span>{{ __('Messages') }}</span></a>
  </li>


  <li class="nav-item">
    <a class="nav-link" href="{{ route('owner-instructor-wt-index',$owner->username) }}">
      <i class="fas fa-fw fa-dollar-sign"></i>
      <span>{{ __('Withdraws') }}</span></a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="{{ route('owner-instructor-affilate-index',$owner->username) }}">
      <i class="fas fa-fw fa-link"></i>
      <span>{{ __('Affilate Program') }}</span></a>
  </li>
