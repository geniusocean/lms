@extends('layouts.owner-instructor')

@section('content')

    <div class="card">
        <div class="d-sm-flex align-items-center justify-content-between">
        <h5 class=" mb-0 text-gray-800 pl-3">{{__('Referral Program')}}</h5>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('owner-instructor-dashboard',$owner->username) }}">{{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('owner-instructor-affilate-index',$owner->username) }}">{{__('Referral Program')}}</a></li>
        </ol>
        </div>
    </div>


    <!-- Row -->
    <div class="row mt-3">
      <!-- Datatables -->
      <div class="col-lg-12">

        @include('includes.admin.form-success')

        <div class="card mb-4">

            @if($refer != null)
                <p class="m-4">
                    <b>{{ __('Current Discount') }}:</b> {{ $refer->discount }}%
                </p>
            @endif

          <div class="table-responsive p-3">
            <table class="table align-items-center table-flush" id="geniustable">
              <thead class="thead-light">
                <tr>
                    <th>{{ __('Title') }}</th>
                    <th>{{ __('Times') }}(#)</th>
                    <th>{{ __('Discount') }}(%)</th>
                </tr>
              </thead>
              <tbody>
                @foreach($refers as $ref)
                <tr class="conv">
                  <td>{{$ref->title}}</td>
                  <td>{{$ref->times}}</td>
                  <td>{{$ref->discount}}</td>
                </tr>
              @endforeach
            </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- DataTable with Hover -->

    </div>
    <!--Row-->


@endsection


@section('scripts')

<script type="text/javascript">

$('#geniustable').DataTable({
			   ordering: false,
});

$(function() {
    $(".btn-area").append('<div class="col-sm-12 col-md-4 text-right">'+
        '<a class="btn btn-primary"  href="{{ route('owner-instructor-affilate-history',$owner->username) }}">{{__("Referral History")}}</a>'+
    '</div>');
});
</script>


@endsection
