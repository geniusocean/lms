<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="GeniusOcean">
  <meta name="author" content="GeniusOcean">
  <link href="{{ asset('assets/'.$owner->username.'/owner/images/'.$gs->favicon) }}" rel="icon">
  <title>{{ $gs->title }}</title>
  <link href="{{ asset('assets/admin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/admin/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/admin/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/admin/css/toastr.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/admin/css/select2.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/admin/css/summernote.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/admin/css/tagify.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/admin/css/ruang-admin.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/admin/css/timepicker.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/admin/css/instructor.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/admin/css/custom.css') }}" rel="stylesheet">

  @yield('styles')

</head>

@php
    $user = Auth::guard('web')->user();
@endphp

<body id="page-top">
  <div id="wrapper">
    <!-- Sidebar -->
    @if($user->is_instructor == 2)
    <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('owner-instructor-dashboard',$owner->username)}}">
        <div class="sidebar-brand-icon">
          <img src="{{ asset('assets/'.$owner->username.'/owner/images/'.$gs->invoice_logo) }}">
        </div>

      </a>

      <li class="nav-item">
        <a class="nav-link" href="{{ route('owner-instructor-dashboard',$owner->username) }}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>{{ __('Dashboard') }}</span></a>
      </li>


        @include('owner-instructor.include.site-nav')


    </ul>
    @endif
    <!-- Sidebar -->
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <!-- TopBar -->
        <nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top">
          @if($user->is_instructor == 2)
          <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
          @endif
          <ul class="navbar-nav ml-auto">
            @if($user->is_instructor == 2)
            <li class="nav-item dropdown no-arrow mx-1" onclick="shownotification()">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <span class="badge badge-danger badge-counter" data-href="{{route('owner.instructor.notification.read',$owner->username)}}" id="notclear">{{DB::table('notifications')->where('register_id',$owner->username)->where('user_id',Auth::user()->id)->where('is_read','=',0)->count()}}</span>
              </a>
            <div id="notf-show" data-href="{{ route('owner.instructor.notification.show',$owner->username) }}" class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                aria-labelledby="alertsDropdown">

                @include('owner-load.instructor-notification')

              </div>
            </li>
            @endif
            
            <div class="topbar-divider d-none d-sm-block"></div>
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <img class="img-profile rounded-circle" src="{{ $user->photo ? asset('assets/'.$owner->username.'/owner/images/users/'.$user->photo ):asset('assets/images/noimage.png') }}" style="max-width: 60px">
                <span class="ml-2 d-none d-lg-inline text-white small">{{ $user->first_name . $user->last_name }}</span>
              </a>
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{route('owner-student-dashboard',$owner->username)}}">
                  <i class="fas fa-home fa-sm fa-fw mr-2 text-gray-400"></i>
                  {{ __('Student Panel') }}
                </a>

                <a class="dropdown-item" href="{{ route('owner.instructor.profile.edit',$owner->username) }}">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  {{ __('Profile') }}
                </a>
                <a class="dropdown-item" href="{{ route('owner-instructor-reset',$owner->username) }}">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  {{ __('Change Password') }}
                </a>

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ $user->is_instructor == 2 ?  route('owner.instructor.logout',$owner->username) : route('student-logout') }}">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  {{ __('Logout') }}
                </a>
              </div>
            </li>
          </ul>
        </nav>
        <!-- Topbar -->

        <!-- Container Fluid-->
        <div class="container-fluid" id="container-wrapper">

            @yield('content')

        </div>
        <!---Container Fluid-->
      </div>

    </div>
  </div>

  <!-- Scroll to top -->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <script type="text/javascript">

    var form_error   = "{{ __('Please fill all the required fields') }}";
    var admin_loader = {{ $gs->is_admin_loader }};


  </script>

  <script src="{{ asset('assets/admin/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/admin/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('assets/admin/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/admin/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/admin/js/toastr.js') }}"></script>
  <script src="{{ asset('assets/admin/js/select2.min.js') }}"></script>
  <script src="{{ asset('assets/admin/js/summernote.js') }}"></script>
  <script src="{{ asset('assets/admin/js/tagify.js') }}"></script>
  <script src="{{ asset('assets/admin/js/sortable.js') }}"></script>
  <script src="{{ asset('assets/admin/js/timepicker.js') }}"></script>
  <script src="{{ asset('assets/admin/js/ruang-admin.js') }}"></script>
  <script src="{{ asset('assets/admin/js/instructor.js') }}"></script>

  @yield('scripts')

</body>

</html>
