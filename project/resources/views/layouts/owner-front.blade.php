
<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{ $gs->title }}</title>

	<!-- favicon -->
	<link rel="shortcut icon" href="{{asset('assets/'.$owner->username.'/owner/images/'.$gs->favicon)}}" type="image/x-icon">
	<!-- bootstrap -->
	<link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.min.css')}}">
	<!-- Plugin css -->
    <link rel="stylesheet" href="{{asset('assets/front/css/plugin.css')}}">
    <!-- Toastr css -->
	<link rel="stylesheet" href="{{asset('assets/front/css/toastr.css')}}">
	<!-- stylesheet -->
	<link rel="stylesheet" href="{{asset('assets/front/css/style.css')}}">
	<!-- responsive -->
	@yield('styles')
	<link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}">

	<link rel="stylesheet" id="color" href="{{ asset('assets/front/css/style.php?colors='.str_replace('#','', $gs->colors)) }}">


</head>

<body>

    @if($gs->is_loader == 1)
	<div class="preloader" id="preloader" style="background: url({{asset('assets/'.$owner->username.'/owner/images/'.$gs->loader)}})"></div>
    @endif

	<!--Main-Menu Area Start-->
	<div class="mainmenu-area">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<nav class="my-navbar">
						<div class="logoarea">
							<a class="navbar-brand" href="{{route('owner.front.index',$owner->username)}}">
								<img src="{{asset('assets/'.$owner->username.'/owner/images/'.$gs->logo)}}" alt="logo">
							</a>
							<div class="categorimenu-wrapper">
								<div class="categories_menu">
									<div class="categories_title">
										<h2 class="categori_toggle"><i class="fa fa-th left-icon"></i> {{ __('Categories') }}
											</i>
										</h2>
									</div>
									<div class="categories_menu_inner">
										<ul>

                                            @foreach(\App\Models\Category::whereStatus(1)->where('register_id',$owner->id)->get() as $category)
											<li class="dropdown_list ">
												<div class="img">
													<img src="{{ asset('assets/'.$owner->username.'/owner/images/categories/'.$category->photo) }}"
														alt="">
												</div>
												<div class="link-area">
                                                    <span><a href="{{ route('owner.front.catalog',[$owner->username,$category->slug]) }}">{{ $category->name }}</a></span>
                                                    @if(count($category->subs) > 0)
													<a href="javascript:;">
														<i class="fa fa-angle-right" aria-hidden="true"></i>
                                                    </a>
                                                    @endif
                                                </div>
                                                @if(count($category->subs) > 0)
                                                <ul class="categories_mega_menu column_1">
                                                    @foreach($category->subs as $subcat)
                                                    <li>
                                                        <a href="{{ route('owner.front.catalog',[$owner->username,$category->slug, $subcat->slug]) }}">{{ $subcat->name }}</a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                                @endif
											</li>
                                            @endforeach

										</ul>
									</div>
								</div>
							</div>
							<div class="search-form order-first order-lg-last">
								<form action="{{ route('owner.front.catalog', [$owner->username,Request::route('category'),Request::route('subcategory')]) }}" method="GET">
									<input id="course_name" type="text" name="search" placeholder="{{ __('Search For Anything') }}" autocomplete="off" required>
									<button type="submit"><i class="fas fa-search"></i></button>
									<div class="autocomplete">
										<div id="myInputautocomplete-list" class="autocomplete-items"></div>
									</div>
								</form>
							</div>
						</div>
						<div id="main_menu" class="order-first order-lg-last">
							<ul class="navbar-nav">

								<li class="nav-item my-dropdown" data-toggle="tooltip" data-placement="top" title="Cart">
									<a href="#" class="cart carticon">
										<div class="icon">
											<i class="fas fa-shopping-cart"></i>
											<span class="cart-quantity" id="cart-count">{{ Session::has('cart'.$owner->id) ? count(Session::get('cart'.$owner->id)->items) : '0' }}</span>
										</div>

									</a>
									<div class="my-dropdown-menu" id="cart-items">
                                        @include('owner-load.cart')
									</div>
                                </li>

                                @if(Auth::check())

								<li class="nav-item dropdown">
									<a class="nav-link log-reg-btn dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{__('My Account')}}
                                    </a>
                                    <ul class="dropdown-menu">

										@if(Auth::user()->is_instructor == 2)
                                        <li>
                                            <a class="dropdown-item" href="{{route('owner-instructor-dashboard',$owner->username)}}">
                                                {{ __('Instructor Panel') }}
                                            </a>
										</li>
										@endif


                                        <li>
                                            <a class="dropdown-item {{request()->path() == $owner->username.'/student/profile' ? 'active' : ''}}" href="{{route('owner-student-profile',$owner->username)}}" >
                                                {{ __('Edit Profile') }}
                                            </a>
										</li>

                                        <li>
                                            <a class="dropdown-item {{request()->path() == $owner->username.'/student/account' ? 'active' : ''}}" href="{{route('owner-student-account',$owner->username)}}" >
                                                {{ __('Reset Password') }}
                                            </a>
                                        </li>

                                        <li>
                                            <a class="dropdown-item" href="{{route('owner-student-logout',$owner->username)}}">
                                                {{ __('Logout') }}
                                            </a>
                                        </li>

                                    </ul>
								</li>

                                @else

								<li class="nav-item">
									<a class="nav-link log-reg-btn" href="{{route('owner.student.login',$owner->username)}}">{{__('Login/Register')}}</a>
								</li>

                                @endif
							</ul>
						</div>

					</nav>
				</div>
			</div>
		</div>
	</div>
	<!--Main-Menu Area Start-->

	@yield('content')
	<!-- Footer Area Start -->
	<footer class="footer" id="footer">
		<div class="container">

			<div class="row">
				<div class="col-lg-12">
					<div class="top-links">
						<ul>
							<li>

								<a href="{{route('owner-student-instructor-application',$owner->username)}}">{{__('Become An Instructor')}}</a>
                            </li>

							<li>
								<a href="{{route('owner.front.blog',$owner->username)}}">{{__('Blog')}}</a>
							</li>

							@foreach (DB::table('pages')->whereStatus(1)->where('register_id',$owner->id)->get() as $page)
								<li>
									<a href="{{route('owner.front.page',[$owner->username,$page->slug])}}">{{$page->title}}</a>
								</li>
							@endforeach


							<li>
								<a href="{{route('owner.front.contact',$owner->username)}}">{{__('Contact Us')}}</a>
							</li>


							<li>
								<div class="global-language">
									<select class="language">
										@foreach(DB::table('languages')->where('register_id',$owner->id)->get() as $language)
										<option value="{{route('owner.front.language',[$owner->username,$language->id])}}" {{ Session::has('language'.$owner->id) ? ( Session::get('language'.$owner->id) == $language->id ? 'selected' : '' ) : (DB::table('languages')->where('register_id',$owner->id)->where('is_default','=',1)->first()->id == $language->id ? 'selected' : '') }}>{{$language->language}}</option>
										@endforeach
									</select>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-7">
					<div class="footer-widget about-widget">
						<div class="footer-logo">
							<a href="#">
								<img src="{{ asset('assets/'.$owner->username.'/owner/images/'.$gs->footer_logo)}}" alt="gooter_logo">
							</a>
							<p>{!! $gs->copyright !!}</p>

						</div>
					</div>
				</div>
				<div class="col-md-6  col-lg-5">
					<div class="footer-widget  footer-newsletter-widget">
						<h4 class="title">
							{{ __('Newsletter') }}
						</h4>
						<div class="newsletter-form-area">
                            <form action="{{ route('owner.front.subscribe',$owner->username) }}" id="subscribeform" method="POST">
                                {{ csrf_field() }}
								<input type="email" name="email" placeholder="{{ __('Your Email Address...') }}" required>
								<button id="sub-btn" type="submit">
									<i class="far fa-paper-plane"></i>
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer Area End -->

	<!-- Back to Top Start -->
	<div class="bottomtotop">
		<i class="fas fa-chevron-right"></i>
	</div>
	<!-- Back to Top End -->

	<!-- Become an instructor modal start -->

	<div class="modal fade" id="bcomeinstructormodal"  data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<h5 class="modal-title" id="staticBackdropLabel">{{ __('Become An Instructor') }}</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
			<div class="modal-body">
				@include('includes.admin.form-login')
				<form id="registerform" action="{{route('owner-instructor-register-submit',$owner->username)}}" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="row">
					  <div class="col-md-6">
						<div class="form-group">
						  <label>{{__('First Name')}}:<sup class="text-danger">*</sup></label>
						  <input type="text" name="first_name" class="form-control" value="" required="">
						</div>
					  </div>
					  <div class="col-md-6">
						<div class="form-group">
						  <label>{{__('Last Name')}}:<sup class="text-danger">*</sup></label>
						  <input type="text" class="form-control" name="last_name"  value="" required="">
						</div>
					  </div>
					  </div>
					<div class="row">
					  <div class="col-md-6">
						<div class="form-group">
						  <label>{{__('Instructor Name')}}:<sup class="text-danger">*</sup></label>
						  <input type="text" class="form-control" name="instructor_name"  value="" required="">
						</div>
					  </div>
					  <div class="col-md-6">
						<div class="form-group">
						  <label>{{__('Email')}}:<sup class="text-danger">*</sup></label>
						  <input type="email" class="form-control" name="email"  value="" required="">
						</div>
					  </div>
					  </div>
					<div class="row">
					  <div class="col-md-6">
						<div class="form-group">
						  <label>{{__('Password')}}:<sup class="text-danger">*</sup></label>
						  <input type="password" value="" name="password" class="form-control" >
						</div>
					  </div>
					  <div class="col-md-6">
						<div class="form-group">
						  <label>{{__('Address')}}:<sup class="text-danger">*</sup></label>
						  <input type="text" name="address" class="form-control" value="" required="">
						</div>
					  </div>
					</div>
					<div class="row">
					  <div class="col-md-6">
						<div class="form-group">
						  <label>{{__('Phone')}}:<sup class="text-danger">*</sup></label>
						  <input type="text" value="" name="phone" class="form-control" >
						</div>
					  </div>
					  <div class="col-md-6">
						<div class="form-group">
						  <label>{{__('Any Message')}}:<sup class="text-danger"></sup></label>
						  <input type="text" name="message" class="form-control" value="" required="">
						</div>
					  </div>
					</div>

					<div class="row">
					  <div class="col-md-6">
						@if($gs->is_capcha == 1)
                        <ul class="captcha-area">
                          <li>
                            <p><img class="codeimg1" src="{{asset("assets/images/capcha_code.png")}}" alt=""></p>
                          </li>
                        </ul>

                        <div class="form-input d-flex mb-3">
                          <input type="text" class="form-control" name="codes" placeholder="{{ __('Enter Code') }}" autocomplete="off" required="">
                          <i class="fas fa-redo p-2 refresh_code"></i>
                        </div>
                        @endif
					  </div>

					  <div class="col-md-6">
						<div class="form-group">
							<label>{{__('Upload Document')}}:<sup class="text-danger">*</sup></label>
							<input type="file" name="document" required="">
						  </div>
					  </div>

					</div>

					<div class="box-footer">
					 <button type="submit" class="mybtn1">{{__('Submit')}}</button>
					</div>
				</form>
			</div>
		</div>
		</div>
	</div>

	<!-- Become an instructor modal End -->
	<script>

        var mainurl = "{{ url('/'.$owner->username.'/') }}";

        var gs      = {!! json_encode(DB::table('generalsettings')->where('register_id',$owner->id)->first(['is_loader'])) !!};

        var langg = {
            'cart_already'   : '{{ __('Already Added To Cart.') }}',
			'add_cart'       : '{{ __('Successfully Added To Cart.') }}',
            'add_wish'   	 : '{{ __('Successfully Added To Wishlist.') }}',
			'already_wish'   : '{{ __('Already Added To Wishlist.') }}',
			'wish_remove'    : '{{ __('Successfully Removed From Wishlist.') }}',
            'cart_empty'     : '{{ __('Cart is empty.') }}',
            'authenticating' : '{{ __('Authenticating') }}',
            'processing'     : '{{ __('Processing') }}',
            'checking'       : '{{ __('Checking') }}'

        };

	</script>

	<!-- jquery -->
	<script src="{{asset('assets/front/js/jquery.js')}}"></script>
	<!-- popper -->
    <script src="{{asset('assets/front/js/popper.min.js')}}"></script>
	<!-- bootstrap -->
	<script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
    <!-- plugin js-->
	<script src="{{asset('assets/front/js/plugin.js')}}"></script>
	{{-- toastr.js --}}
	<script src="{{asset('assets/front/js/toastr.js')}}"></script>
	<!-- custom js-->
	<script src="{{asset('assets/front/js/custom.js')}}"></script>
	<!-- main -->
	<script src="{{asset('assets/front/js/main.js')}}"></script>

	@yield('scripts')

	@if(session()->has('success'))
    <script>
       toastr.success("{{__(session('success'))}}");
    </script>
    @endif

    @if(session()->has('error'))
    <script>
       toastr.error("{{__(session('error'))}}");
    </script>
    @endif

</body>

</html>
