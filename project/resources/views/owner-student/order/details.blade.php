@extends('layouts.owner-front')
@section('content')

<!-- User Dashboard Area Start -->
<section class="user-dashboard">


  <div class="user-dashboard-menu-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="heading-title">
                    {{__('Purchase Details')}}
                </h4>
                @include('owner-student.includes.nav')
            </div>
        </div>
    </div>
</div>

<div class="user-dashboard-content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="product-invoice">
                    <main class="columns">
                        <div class="inner-container">
                        <header>
                        <a target="_blank" href="{{ route('owner-student-order-print',[$owner->username,$order->id]) }}" class="mybtn1"><i class="fas fa-print"></i> {{ __('Print Invoice') }}</a>
                        </header>
                        <div class="table-responsive">
                            <table class="table invoice">
                                <tbody>
                                    <tr class="header">
                                        <td class="">
                                            <img src="{{ asset('assets/'.$owner->username.'/owner/images/'.$gs->logo) }}" alt="Company Name">
                                        </td>
                                        <td class="align-right">
                                            <h2>{{ __('Invoice') }}</h2>
                                        </td>
                                    </tr>
                                    <tr class="intro">
                                        <td class="">
                                        <strong>{{ __('Name') }}:</strong> {{ $order->user->showName() }}<br>
                                        <strong>{{ __('Email') }}:</strong> {{ $order->user->email }}
                                        </td>
                                        <td class="text-right">
                                        <strong>{{ __('Order Number') }}:</strong> {{ $order->order_number }}<br>
                                        <strong>{{ __('Purchase Date') }}:</strong> {{ date('l m, Y', strtotime($order->created_at))  }}
                                        </td>
                                    </tr>
                                    <tr class="details">
                                        <td colspan="2">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            {{ __('Course Name') }}
                                                        </th>
                                                        <th>
                                                            {{ __('Total') }}
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($cart['items'] as $course)
                                                    <tr class="item">
                                                        <td>
                                                            {{ $course['item']['title'] }}
                                                        </td>
                                                        <td>
                                                            {{ $order->currency_sign }}{{ round(($course['price'] * $order->currency_value),2) }}
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="totals">
                                        <td></td>
                                        <td>
                                            <table class="table">
                                                <tbody>
                                                    <tr class="total">
                                                        <td class="label text-right">{{ __('Total') }}</td>
                                                        <td class="num pr-2">{{ $order->currency_sign }}{{ round(($order->pay_amount * $order->currency_value),2) }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="additional-info">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>{{ __('Billing Information') }} :</h5>

                                    <p>{{ $order->user->email }}<br>
                                       {{ $order->user->showName() }}<br>
                                       {{ date('l m, Y', strtotime($order->created_at))  }}.<br>
                                       {{ $order->order_number }}
                                    </p>
                                </div>
                            <div class="col-md-6">
                                <h5>{{ __('Payment Information') }} :</h5>
                                <p>{{ $order->method }}<br>
                                {{ __('Txn ID') }}: {{ $order->txnid }}<br>
                                @if($order->method == 'Stripe')
                                {{ __('Charge ID') }}: {{ $order->charge_id }}
                                @endif
                                </p>
                            </div>
                            </div>
                        </div>
                        </div>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

<!-- User Dashboard Area End -->

@endsection
