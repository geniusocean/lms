<ul class="menu-list">
    <li>
        <a href="{{route('owner-student-dashboard',$owner->username)}}" class="{{request()->path() == $owner->username.'/student/dashboard' ? 'active' : ''}}">
            {{__('My Courses')}}
        </a>
    </li>
    <li>
        <a href="{{ route('owner-student-wishlists',$owner->username) }}"
        class="
        @if(request()->path() == $owner->username.'/student/wishlists')active
        @elseif(request()->is($owner->username.'/student/wishlists/*'))active

        @endif
        "
        >
            {{__('Wishlist')}}
        </a>
    </li>
    <li>
        <a href="{{route('owner-student-messages',$owner->username)}}" class="
            @if(request()->path() == $owner->username.'/student/messages') active
            @elseif(request()->path() ==$owner->username.'/student/message')active
            @elseif(request()->is($owner->username.'/student/message/*'))active

            @endif
            ">
            {{__('My messages')}}
        </a>
    </li>

    <li>

        <a href="{{route('owner-student-order-index',$owner->username)}}" class="
            @if(request()->is($owner->username.'/student/orders'))active
            @elseif(request()->is($owner->username.'/student/orders/*'))active
            @endif
            ">
            {{__('Purchase History')}}
        </a>

    </li>
    <li>
        <a href="{{route('owner-student-profile',$owner->username)}}" class="{{request()->path() == $owner->username . '/student/profile' ? 'active' : ''}}">
            {{__('Edit Profile')}}
        </a>
    </li>
    <li>

        <a href="{{route('owner-student-affilate',$owner->username)}}" class="
            @if(request()->is($owner->username.'/student/affilates'))active
            @elseif(request()->is($owner->username.'/student/affilate/history'))active
            @endif
            ">
            {{__('Referral Program')}}
        </a>

    </li>


</ul>
