@extends('layouts.instructor')

@section('content')

    <div class="card">
        <div class="d-sm-flex align-items-center justify-content-between">
        <h5 class=" mb-0 text-gray-800 pl-3">{{ __('Messages') }}</h5>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('instructor-dashboard') }}">{{ __('Dashboard') }}</a></li>
            <li class="breadcrumb-item"><a href="javascript:;">{{ __('Messages') }}</a></li>
        </ol>
        </div>
    </div>


    <!-- Row -->
    <div class="row mt-3">
      <!-- Datatables -->
      <div class="col-lg-12">

        @include('includes.admin.form-success')

        <div class="card mb-4">
          <div class="table-responsive p-3">
            <table class="table align-items-center table-flush" id="geniustable">
              <thead class="thead-light">
                <tr>
                  <th>{{ __('Name') }}</th>
                  <th>{{ __('Message') }}</th>
                  <th>{{ __('Send') }}</th>
                  <th>{{ __('Options') }}</th>
                </tr>
              </thead>
              <tbody>
                @foreach($convs as $conv)

                  <tr class="conv">
                    <input type="hidden" value="{{$conv->id}}">
                    @if($user->id == $conv->sent->id)
                    <td>{{$conv->recieved->first_name . $conv->recieved->last_name}}</td>
                    @else
                    <td>{{$conv->sent->first_name  . $conv->sent->last_name}}</td>
                    @endif
                    <td>{{$conv->subject}}</td>
                    <td>{{$conv->created_at->diffForHumans()}}</td>
                    <td>
                      <a href="{{route('instructor-message',$conv->id)}}" class="btn btn-primary view"><i class="fa fa-eye"></i></a>
                      <a href="javascript:;" data-toggle="modal" data-target="#confirm-delete" data-href="{{route('instructor-message-delete',$conv->id)}}" class="btn btn-danger remove"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- DataTable with Hover -->

    </div>
    <!--Row-->

{{-- MESSAGE MODAL --}}
<div class="message-modal">
    <div class="modal" id="vendorform" tabindex="-1" role="dialog" aria-labelledby="vendorformLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="vendorformLabel">{{ __('Send Message') }}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
        <div class="modal-body">
          <div class="container-fluid p-0">
            <div class="row">
              <div class="col-md-12">
                <div class="contact-form">
                  <form id="emailreply">
                    {{csrf_field()}}
                    <ul class="mylists">

                      <li>
                        <input type="email" class="input-field" id="eml" name="email" placeholder="{{ __('Email') }} *" required="">
                      </li>
                      <li>
                        <input type="text" class="input-field" id="subj" name="subject" placeholder="{{ __('Subject') }} *" required="">
                      </li>

                      <li>
                        <textarea class="input-field textarea" name="message" id="msg" placeholder="{{ __('Your Message') }} *" required=""></textarea>
                      </li>

                      <input type="hidden" name="name" value="{{ Auth::guard('web')->user()->name }}">
                      <input type="hidden" name="user_id" value="{{ Auth::guard('web')->user()->id }}">

                    </ul>
                    <button class="btn btn-success" id="emlsub" type="submit">{{ __('Send Message') }}</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>

  {{-- MESSAGE MODAL ENDS --}}


{{-- DELETE MODAL --}}

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

    <div class="modal-header d-block text-center">
        <h4 class="modal-title d-inline-block">{{ __('Confirm Delete') }}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
    </div>
                <div class="modal-body">
            <p class="text-center">{{ __('You are about to delete this Conversation.') }}</p>
            <p class="text-center">{{ __('Do you want to proceed?') }}</p>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Cancle') }}</button>
                    <a class="btn btn-danger btn-ok">{{ __('Delete') }}</a>
                </div>
            </div>
        </div>
    </div>


{{-- DELETE MODAL ENDS --}}


@endsection


@section('scripts')

<script type="text/javascript">

var table = $('#geniustable').DataTable();
$(function() {
            $(".btn-area").append('<div class="col-sm-12 col-md-4 text-right">'+
                                    '<a class="btn btn-primary"  data-toggle="modal" data-target="#vendorform" href="javascript:;">{{__("Compose Message")}}</a>'+
                            '</div>');
        });

</script>

<script type="text/javascript">

    $(document).on("submit", "#emailreply" , function(){
    var token = $(this).find('input[name=_token]').val();
    var subject = $(this).find('input[name=subject]').val();
    var message =  $(this).find('textarea[name=message]').val();
    var email = $(this).find('input[name=email]').val();
    var name = $(this).find('input[name=name]').val();
    var user_id = $(this).find('input[name=user_id]').val();
    $('#eml').prop('disabled', true);
    $('#subj').prop('disabled', true);
    $('#msg').prop('disabled', true);
    $('#emlsub').prop('disabled', true);
$.ajax({
      type: 'post',
      url: "{{route('instructor-contact')}}",
      data: {
          '_token': token,
          'subject'   : subject,
          'message'  : message,
          'email'   : email,
          'name'  : name,
          'user_id'   : user_id
            },
      success: function( data) {
    $('#eml').prop('disabled', false);
    $('#subj').prop('disabled', false);
    $('#msg').prop('disabled', false);
    $('#subj').val('');
    $('#msg').val('');
    $('#emlsub').prop('disabled', false);
  if(data == 0)
    toastr.error("{{ __('Email Not Found') }}");
  else
    toastr.success("{{ __('Message Sent Successfully.') }}");

  $('.close').click();
  }
  });
    return false;
  });

</script>


@endsection
