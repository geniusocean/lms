@extends('layouts.instructor')

@section('content')

<div class="card">
    <div class="d-sm-flex align-items-center justify-content-between">
    <h5 class=" mb-0 text-gray-800 pl-3">{{ __('Add New Withdraw') }} <a class="btn btn-primary btn-rounded btn-sm" href="{{route('instructor-wt-index')}}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h5>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('instructor-dashboard') }}">{{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('instructor-wt-index') }}">{{ __('Withdraw') }}</a></li>
        <li class="breadcrumb-item"><a href="{{route('instructor-wt-create')}}">{{ __('Add New Withdraw') }}</a></li>
    </ol>
    </div>
</div>

<div class="row justify-content-center mt-3">
  <div class="col-lg-6">
    <!-- Form Basic -->
    <div class="card mb-4">
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">{{ __('Add New Withdraw Form') }}</h6>
      </div>

      <div class="card-body">
        <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
        <form class="geniusform" action="{{route('instructor-wt-store')}}" method="POST" enctype="multipart/form-data">

            @include('includes.admin.form-both')

            {{ csrf_field() }}

            <div class="form-group">
                <label class="control-label col-sm-4" for="name">{{ __('Current Balance') }}
                    {{$curr->sign}}{{round((Auth::user()->balance * $curr->value),2)}}</label>
            </div>




            <div class="form-group">
                <label for="withmethod">{{ __('Withdraw Method') }}</label>
                <select class="form-control mb-3" id="withmethod" name="methods">
                    <option value="">{{ __('Select Withdraw Method') }}</option>
                    <option value="Paypal">{{ __('Paypal') }}</option>
                    <option value="Skrill">{{ __('Skrill') }}</option>
                    <option value="Payoneer">{{ __('Payoneer') }}</option>
                    <option value="Bank">{{ __('Bank') }}</option>
                </select>
              </div>

              <div class="form-group">
                <label for="amount">{{ __('Withdraw Amount') }} *</label>
                <input type="text" class="form-control" id="amount" name="amount"  placeholder="{{ __('Withdraw Amount') }}" value="" required>
            </div>
            <div id="bank" style="display: none;">
            <div class="form-group">
                <label for="acc_email">{{ __('Enter Account Email') }} *</label>
                <input type="email" class="form-control" id="acc_email" name="acc_email"  placeholder="{{ __('Enter Account Email') }}" value="" required>
            </div>

            <div class="form-group">
                <label for="iban">{{ __('Enter IBAN/Account No') }} *</label>
                <input type="text" class="form-control" id="iban" name="iban"  placeholder="{{ __('Enter IBAN/Account No') }}" value="" required>
            </div>

            <div class="form-group">
                <label for="acc_name">{{ __('Enter Account Name') }} *</label>
                <input type="text" class="form-control" id="acc_name" name="acc_name"  placeholder="{{ __('Enter Account Name') }}" value="" required>
            </div>

            <div class="form-group">
                <label for="address">{{ __('Enter Address') }} *</label>
                <input type="text" class="form-control" id="address" name="address"  placeholder="{{ __('Enter Address') }}" value="" required>
            </div>

            <div class="form-group">
                <label for="country">{{ __('Enter Country') }} *</label>
                <input type="text" class="form-control" id="country" name="country"  placeholder="{{ __('Enter Country') }}" value="" required>
            </div>

            <div class="form-group">
                <label for="swift">{{ __('Enter Swift Code') }} *</label>
                <input type="text" class="form-control" id="swift" name="swift"  placeholder="{{ __('Swift Code') }}" value="" required>
            </div>

            </div>
          <div class="form-group">
            <label for="reference">{{ __('Additional Reference(Optional)') }} </label>
            <textarea class="form-control"  id="reference" name="reference" rows="3"  placeholder="{{__('Additional Reference(Optional)')}}"></textarea>
        </div>

        @if($gs->withdraw_fee != 0 && $gs->withdraw_charge != 0)

        <div id="resp" class="col-md-12">
            <span class="help-block">
                <strong>{{ __('Withdraw Fee') }} {{ $sign->sign }}{{ $gs->withdraw_fee }}
                    {{ __('and') }} {{ $gs->withdraw_charge }}%
                    {{ __('will deduct from your account.') }}
                </strong>
            </span>
        </div>

        @endif

        <br>

            <button type="submit" id="submit-btn" class="btn btn-primary">{{ __('Submit') }}</button>

        </form>
      </div>
    </div>

    <!-- Form Sizing -->

    <!-- Horizontal Form -->

  </div>

</div>
<!--Row-->

@endsection


@section('scripts')


<script type="text/javascript">
    $("#withmethod").change(function () {
        var method = $(this).val();
        if (method == "Bank") {

            $("#bank").show();
            $("#bank").find('input, select').attr('required', true);

            $("#paypal").hide();
            $("#paypal").find('input').attr('required', false);

        }
        if (method != "Bank") {
            $("#bank").hide();
            $("#bank").find('input, select').attr('required', false);

            $("#paypal").show();
            $("#paypal").find('input').attr('required', true);
        }
        if (method == "") {
            $("#bank").hide();
            $("#paypal").hide();
        }

    })
</script>

@endsection
