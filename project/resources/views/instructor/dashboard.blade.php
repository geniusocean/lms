@extends('layouts.instructor')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{ __('Dashboard') }}</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('instructor-dashboard') }}">{{ __('Dashboard') }}</a></li>
    </ol>
</div>


<div class="row mb-3">

  <!-- Area Chart -->
  <div class="col-xl-12 col-lg-12">
    <div class="card mb-4">
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">{{ __('Total Sales in Last 30 Days') }}</h6>

      </div>
      <div class="card-body">
        <div class="chart-area">
          <canvas id="lineChart"></canvas>
        </div>
      </div>
    </div>
  </div>

</div>


@endsection

@section('scripts')

<script type="text/javascript">
    
  displayLineChart();

  function displayLineChart() {
      var data = {
          labels: [
          {!!$days!!}
          ],
          datasets: [{
              label: "Prime and Fibonacci",
              fillColor: "#3dbcff",
              strokeColor: "#0099ff",
              pointColor: "rgba(220,220,220,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: [
              {!!$sales!!}
              ]
          }]
      };
      var ctx = document.getElementById("lineChart").getContext("2d");
      var options = {
          responsive: true
      };
      var lineChart = new Chart(ctx).Line(data, options);
  }

</script>

@endsection