
@extends('layouts.instructor')

@section('content')

    <div class="card">
        <div class="d-sm-flex align-items-center justify-content-between">
        <h5 class=" mb-0 text-gray-800 pl-3">{{__('Referral History')}} <a class="btn btn-primary btn-rounded btn-sm" href="{{route('instructor-affilate-index')}}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h5>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('instructor-dashboard') }}">{{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('instructor-affilate-history') }}">{{__('Referral History')}}</a></li>
        </ol>
        </div>
    </div>


    <!-- Row -->
    <div class="row mt-3">
      <!-- Datatables -->
      <div class="col-lg-12">

        @include('includes.admin.form-success')

        <div class="card mb-4">
          <div class="table-responsive p-3">
            <table class="table align-items-center table-flush" id="geniustable">
              <thead class="thead-light">
                <tr>
                    <th>{{ __('User Name') }}</th>
                    <th>{{ __('Course') }}</th>
                </tr>
              </thead>
              <tbody>
                @foreach($refers as $ref)
                <tr>
                    <td>{{$ref->affilate->showName()}}</td>
                    <td><a href="{{ route("front.course",$ref->course->slug) }}">{{ $ref->course->title }}</a></td>
                  </tr>
              @endforeach
            </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- DataTable with Hover -->

    </div>
    <!--Row-->


@endsection


@section('scripts')

<script type="text/javascript">

$('#geniustable').DataTable({
	ordering: false,
});

</script>


@endsection


