@extends('layouts.front')

@section('content')

<!--Main Breadcrumb Area Start -->
<div class="main-breadcrumb-area" style="background-image: url({{ $gs->breadcumb_banner ? asset('assets/images/'.$gs->breadcumb_banner):asset('assets/images/noimage.png') }});">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ul class="pages">
          <li>
            <a href="{{route('front.index')}}">
              {{__('Home')}}
            </a>
          </li>
          <li class="active">
            <a href="{{route('instructor.login')}}">
              {{__('Login/Register')}}
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!--Main Breadcrumb Area End -->




  <!-- Login Area Start -->
  <section class="new-login">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-7">
          <div class="new-login-box">
            <div class="right-content">
              <ul class="nav" id="pills-tab" role="tablist">
                <li class="nav-item login">
                  <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="false"><i class="fas fa-user"></i>{{__('LOGIN')}}</a>
                </li>
                <li class="nav-item register">
                  <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="true">
                    <i class="fas fa-user-plus"></i>{{__('REGISTER')}}
                  </a>
                </li>
              </ul>
              <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade active show" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                  <div class="login-area">
                    <div class="login-form signin-form">
                      @include('includes.admin.form-login')
                      <form id="loginform" action="{{ route('instructor.login.submit') }}"  method="POST">
                        {{ csrf_field() }}
                        <div class="form-input">
                          <input type="email" name="email" placeholder="T{{__('ype Email Address')}}" value="" required="">
                          <i class="fas fa-envelope"></i>
                        </div>
                        <div class="form-input">
                          <input type="password" class="Password" name="password" value="" placeholder="{{__('Type Password')}}" required="">
                          <i class="fas fa-key"></i>
                        </div>
                        <div class="form-forgot-pass">
                          <div class="left">
                            <input class="styled-checkbox" id="styled-checkbox-1" name="remember" type="checkbox" value="value1" {{ old('remember') ? 'checked' : '' }}>
                            <label for="styled-checkbox-1">{{ __('Remember Password') }}</label>
                          </div>
                          <div class="right">
                            <a href="#">
                            <i class="fas fa-lock"></i>	{{ __('Forgot Password?') }}
                            </a>
                          </div>
                        </div>
                        <button type="submit" class="submit-btn">{{ __('Login') }}</button>
                        @if(DB::table('socialsettings')->find(1)->f_check == 1 || DB::table('socialsettings')->find(1)->g_check == 1)
                        <div class="social-area">
                          <h3 class="title">{{ __('Or') }}</h3>
                          <p class="text">{{ __('Sign In with social media') }}</p>
                          <ul class="social-links">
                            @if(DB::table('socialsettings')->find(1)->f_check == 1)
                            <li>
                              <a href="{{ route('social-provider','facebook') }}" class="facebook">
                                <i class="fab fa-facebook-f"></i> {{ __('Facebook') }}
                              </a>
                            </li>
                            @endif
                            @if(DB::table('socialsettings')->find(1)->g_check == 1)
                            <li>
                              <a href="{{ route('social-provider','google') }}" class="google">
                                <i class="fab fa-google-plus-g"></i> {{ __('Google') }}
                              </a>
                            </li>
                            @endif
                          </ul>
                        </div>
                        @endif
                      </form>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                  <div class="login-area">
                    <div class="login-form signup-form">
                      @include('includes.admin.form-login')
                      <form id="registerform" action="{{route('instructor-register-submit')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-input">
                          <input type="text" placeholder="{{__('First Name')}}" name="first_name" required>
                          <i class="fas fa-user"></i>
                        </div>
                        <div class="form-input">
                          <input type="text" placeholder="{{__('Last Name')}}" name="last_name" required>
                          <i class="fas fa-user"></i>
                        </div>

                        <div class="form-input">
                          <input type="text" placeholder="{{__('Instructor Name')}}" name="instructor_name" required>
                          <i class="fas fa-user"></i>
                        </div>
                        <div class="form-input">
                          <input type="email" placeholder="{{__('Enter Email')}}" name="email" required>
                          <i class="fas fa-envelope"></i>
                        </div>
                        <div class="form-input">
                          <input type="password" class="Password" name="password" placeholder="{{__('Password')}}" required>
                          <i class="fas fa-key"></i>
                        </div>

                        <div class="form-input">
                          <input type="text" class="" name="address" placeholder="{{__('Address')}}" required>
                          <i class="far fa-address-card"></i>
                        </div>
                        <div class="form-input">
                          <input type="text" class="" name="phone" placeholder="{{__('Phone')}}" required>
                          <i class="fas fa-phone"></i>
                        </div>
                        <div class="form-input">
                          <input type="text" class="" name="message" placeholder="{{__('Any message')}}">
                          <i class="fas fa-inbox"></i>
                        </div>
                        <div class="form-input">
                          <input type="file" class="" name="document" placeholder="{{__('Document')}}" required>
                          <i class="fas fa-inbox"></i>
                        </div>
                        <small>{{__('If any document you want to share ( .doc, .docs, .pdf, .txt, .png, .jpg, jpeg ) Are accepted')}}</small>

                        @if($gs->is_capcha == 1)
                        <ul class="captcha-area">
                          <li>
                            <p><img class="codeimg1" src="{{asset("assets/images/capcha_code.png")}}" alt=""></p>
                          </li>
                        </ul>

                        <div class="form-input">
                          <input type="text" class="Password" name="codes" placeholder="{{ __('Enter Code') }}" required="">
                          <i class="fas fa-redo refresh_code"></i>
                        </div>

                        @endif
                        <button class="submit-btn">{{ __('Register') }}</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Login Area End -->

@endsection
