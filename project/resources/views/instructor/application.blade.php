@extends('layouts.instructor')

@section('content')
<div class="card">
  <div class="d-sm-flex align-items-center justify-content-between">
  <h5 class=" mb-0 text-gray-800 pl-3">{{ __('Instructor Application') }} </h5>
  <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('student-dashboard') }}">{{ __('Dashboard') }}</a></li>
      <li class="breadcrumb-item"><a href="{{route('student-instructor-application')}}">{{ __('Instructor Application') }}</a></li>
  </ol>
  </div>
</div>

<div class="row justify-content-center mt-3">
<div class="col-lg-12">
  <!-- Form Basic -->
  <div class="card mb-4">
    <div class="card-header px-5 d-flex flex-row align-items-center justify-content-between">
      <h6 class="m-0 font-weight-bold text-primary">{{ __('Instructor Application') }}</h6>
    </div>

    <div class="card-body px-5">
      @if ($user->is_instructor != 1)
      <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>
      <form class="geniusform" action="{{route('student-instructor-application.submit')}}" method="POST" enctype="multipart/form-data">

          @include('includes.admin.form-both')

          {{ csrf_field() }}

        <div class="form-group">
            <label for="name">{{ __('Instructor Name') }}</label>
            <input type="text" class="form-control" id="name" name="instructor_name"  placeholder="{{ __('Instructor Name') }}" value="" required>
        </div>


          <div class="form-group">
              <label for="address">{{ __('Address') }}</label>
              <textarea class="form-control"  id="address" name="address" required rows="3" placeholder="{{__('Address')}}"></textarea>
          </div>

        <div class="form-group">
            <label for="phone">{{ __('Phone') }}</label>
            <input type="text" class="form-control" id="phone" name="phone"  placeholder="{{ __('Phone')}}" value="" required>
        </div>

        <div class="form-group">
            <label for="message">{{ __('Any message') }}</label>
            <textarea class="form-control"  id="message" name="message" required rows="3" placeholder="{{__('Message')}}"></textarea>
        </div>


        <div class="form-group">
            <label for="document">{{ __('Document') }}</label>
            <input type="file" class="form-control" id="document" name="document"  placeholder="{{ __('Document')}}" value="" required>
            <p>{{__('If any document you want to share ( .doc, .docs, .pdf, .txt, .png, .jpg, jpeg ) Are accepted')}}</p>
        </div>

          <button type="submit" id="submit-btn" class="btn btn-primary">{{ __('Submit') }}</button>

      </form>
      @else
      <div class="alert alert-success validation" style="">
        <p class="text-left">
          <div class="text-center"><i class="fas fa-check-circle fa-4x"></i><br>
            <h3>{{ __('Your Documents Submitted Successfully.') }}</h3>
          </div>
        </p>
      </div>
      @endif
    </div>
  </div>

  <!-- Form Sizing -->

  <!-- Horizontal Form -->

</div>

</div>
<!--Row-->


@endsection

@section('scripts')

<script>

$("#seo").change(function() {
    if(this.checked) {
        $('.showbox').removeClass('d-none');
    }else{
        $('.showbox').addClass('d-none');
    }
});

</script>

@endsection
