{{-- SECTION MODAL EDIT --}}

<div class="modal fade" id="sectionModalEdit" tabindex="-1" role="dialog" aria-labelledby="sectionModalEditTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">{{ __("Edit Section") }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body ml-2 mr-2">

                <form class="c-form" id="editSectionForm" action="" method="POST" enctype="multipart/form-data">

                    @include('includes.admin.form-both')

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="title">{{ __('Enter Section Title') }} *</label>
                        <input class="form-control" id="editSectionTitle" type="text" name="title"  id="title" required="">
                    </div>
                    <input type="hidden" name="section_id" id="section_id">
                    <div class="text-right">
                        <button class="btn btn-success submit-btn" type="submit">{{ __('Submit') }}</button>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
            <a href="javascript:;" class="btn btn-secondary" data-dismiss="modal">{{ __("Close") }}</a>
            </div>
        </div>
    </div>
</div>

{{-- SECTION MODAL EDIT ENDS --}}
