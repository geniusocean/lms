{{-- QUIZ QUESTION ADD --}}

<div class="modal fade" id="quizQuestionEdit" tabindex="-1" role="dialog" aria-labelledby="quizQuestionEditTitle" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-scrollable modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="submit-loader">
                <img  src="{{asset('assets/images/'.$gs->loader)}}" alt="">
            </div>

            <div class="modal-header">
            <h5 class="modal-title">{{ __('Edit Question') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body ml-2 mr-2">

                <div class="modal-body ml-2 mr-2">



                </div>

            </div>
            <div class="modal-footer">
            <a href="javascript:;" class="btn btn-secondary" data-dismiss="modal">{{ __("Close") }}</a>
            </div>
        </div>

    </div>
</div>

{{-- QUIZ QUESTION ADD  ENDS --}}
