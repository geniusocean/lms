{{-- QUIZ QUESTION ADD --}}

<div class="modal fade" id="quizQuestionAdd" tabindex="-1" role="dialog" aria-labelledby="quizQuestionAddTitle" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-scrollable modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="submit-loader">
                <img  src="{{asset('assets/images/'.$gs->loader)}}" alt="">
            </div>

            <div class="modal-header">
            <h5 class="modal-title">{{ __('Add New Question') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body ml-2 mr-2">

                <div class="modal-body ml-2 mr-2">

                <form class="c-form" action="{{ route('instructor-question-store') }}" method="POST">

                    {{ csrf_field() }}


                    <input type="hidden" id="quiz_lesson_id" name="quiz_lesson_id">

                    <div class="form-group">
                        <label for="title">{{ __('Question title') }}</label>
                        <input class="form-control" type="text" name="title" id="title" required="">
                    </div>

                    <div class="form-group" id="multiple_choice_question">
                        <label for="number_of_options">{{ __('Number of options') }}</label>
                        <div class="input-group">
                            <input type="number" class="form-control" name="number_of_options" id="number_of_options" min="0" value="0">
                            <div class="input-group-append">
                                <button type="button" id="option-check" class="btn btn-secondary btn-sm pull-right">
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="options">

                    </div>

                    <div class="text-center">
                        <button class="btn btn-success" type="submit">{{ __('Submit') }}</button>
                    </div>

                </form>

                </div>

            </div>
            <div class="modal-footer">
            <a href="javascript:;" class="btn btn-secondary" data-dismiss="modal">{{ __("Close") }}</a>
            </div>
        </div>

    </div>
</div>

{{-- QUIZ QUESTION ADD  ENDS --}}
