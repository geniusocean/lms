{{-- QUIZ MODAL ADD --}}

<div class="modal fade" id="quizModalAdd" tabindex="-1" role="dialog" aria-labelledby="quizModalAddTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">{{ __("Add New Quiz") }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body ml-2 mr-2">

                <form class="c-form" action="{{ route('instructor-lesson-store') }}" method="POST" enctype="multipart/form-data">

                    @include('includes.admin.form-both')

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="section_id">{{ __('Select Section') }} *</label>
                        <select class="form-control" name="section_id" id="section_id" required>
                            @foreach($data->sections()->oldest('id')->get() as $section)
                                <option value="{{ $section->id }}">{{ $section->title }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="title">{{ __('Title') }} *</label>
                        <input class="form-control" type="text" name="title"  id="title" required="">
                    </div>


                    <div class="form-group">
                        <label for="details">{{ __('Details') }} *</label>
                        <textarea class="form-control" name="details" id="details" required></textarea>
                    </div>

                    <input type="hidden" name="type" value="Quiz">

                    <div class="text-right">
                        <button class="btn btn-success submit-btn" type="submit">{{ __('Submit') }}</button>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
            <a href="javascript:;" class="btn btn-secondary" data-dismiss="modal">{{ __("Close") }}</a>
            </div>
        </div>
    </div>
</div>

{{-- QUIZ MODAL ADD ENDS --}}
