
        <form class="c-form" action="{{ route('instructor-lesson-update',$data->id) }}" method="POST" enctype="multipart/form-data">

            @include('includes.admin.form-both')

            {{ csrf_field() }}

            <div class="form-group">
                <label for="section_id">{{ __('Select Section') }} *</label>
                <select class="form-control" name="section_id" id="section_id" required>
                    @foreach($data->section->course->sections()->oldest('id')->get() as $section)
                        <option value="{{ $section->id }}" {{ $section->id == $data->section_id ? 'selected' : '' }}>{{ $section->title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="title">{{ __('Title') }} *</label>
                <input class="form-control" type="text" name="title"  id="title" required="" placeholder="{{ __('Enter Title') }}" value="{{ $data->title }}">
            </div>


            <div class="form-group">
                <label for="details">{{ __('Details') }} *</label>
                <textarea class="form-control" name="details" id="details" placeholder="{{ __('Enter Details') }}" required>{{ $data->details }}</textarea>
            </div>

            <input type="hidden" name="type" value="Quiz">

            <div class="text-right">
                <button class="btn btn-success submit-btn" type="submit">{{ __('Submit') }}</button>
            </div>
        </form>
