<form action="{{ route('instructor-question-sort-update',$data->id) }}" method="POST">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="bg-dragula p-2 p-lg-4">
                                <h5 class="mt-0">{{ __('Questions of') }}: {{ $data->title }}
                                    <div class="row justify-content-center alignToTitle float-right">
                                        <button type="submit" class="btn btn-outline-primary btn-sm btn-rounded alignToTitle mr-1">{{ __('Update sorting') }}</button>
                                        <button type="button" class="btn btn-outline-primary btn-sm btn-rounded alignToTitle add_question" data-id="{{ $data->id }}">{{ __('Add New Question') }}</button>
                                    </div>
                                </h5>
                                <div id="question-list" class="py-2">
                                    @foreach($data->questions()->oldest('pos')->get() as $question)
                                        <!-- Item -->
                                        <div class="card mb-0 mt-2 draggable-item">
                                            <div class="card-body">
                                                <div class="media">
                                                    <div class="media-body">
                                                        <input type="hidden" name="pos[]" value="{{ $question->pos }}">
                                                        <h5 class="mb-1 mt-0">{{ $question->title }}
                                                            <span class="widgets-of-quiz-question d-none">
                                                                <a href="{{ route('admin-question-delete',$question->id) }}" class="alignToTitle float-right text-secondary ml-1"><i class="fas fa-trash"></i></a>
                                                                <a href="javascript:;" data-title="{{ __('Edit Question') }}" data-href="{{ route('admin-question-edit',$question->id) }}" class="alignToTitle float-right text-secondary question-edit"><i class="fas fa-edit"></i></a>
                                                            </span>
                                                        </h5>
                                                    </div> <!-- end media-body -->
                                                </div> <!-- end media -->
                                            </div> <!-- end card-body -->
                                        </div> <!-- end col -->
                                        <!-- item -->
                                    @endforeach
                                </div> <!-- end company-list-1-->
                            </div> <!-- end div.bg-light-->
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div> <!-- end col -->
    </div>
</form>

<script>

$('#question-list .card .card-body').on('mouseover',function(){

$(this).find('.widgets-of-quiz-question').removeClass('d-none');

});

$('#question-list .card .card-body').on('mouseout',function(){

$(this).find('.widgets-of-quiz-question').addClass('d-none');

});

// SORTING SECTION

var el = document.getElementById('question-list');
Sortable.create(el, {
  animation: 100,
  group: 'list-1',
  draggable: '.draggable-item',
  handle: '.draggable-item',
  sort: true,
  filter: '.sortable-disabled',
  chosenClass: 'active'
});

// SORTING SECTION ENDS

</script>


