<form class="c-form" action="{{ route('instructor-question-update',$data->id) }}" method="POST">

    {{ csrf_field() }}


    <div class="form-group">
        <label for="title">{{ __('Question title') }}</label>
        <input class="form-control" type="text" name="title" id="title" value="{{ $data->title }}" required="">
    </div>

    <div class="form-group" id="multiple_choice_question">
        <label for="number_of_options">{{ __('Number of options') }}</label>
        <div class="input-group">
            <input type="number" class="form-control" name="number_of_options" id="number_of_options" min="0" value="{{ count($options) }}">
            <div class="input-group-append">
                <button type="button" id="option-check" class="btn btn-secondary btn-sm pull-right">
                    <i class="fa fa-check"></i>
                </button>
            </div>
        </div>
    </div>

    <div class="options">
        @foreach(array_combine($options,$answers) as $option => $answer)

        <div class="form-group">
            <label>{{ __('Option') }} {{ $loop->index + 1 }}</label>
            <div class="input-group">
                <input type="text" class="form-control" name="options[]" placeholder="{{ __('Option') }} {{ $loop->index + 1 }}" value="{{ $option }}" required="">
                <span class="input-group-text">
                    <input type="hidden" name="answers[]" value="{{ $answer }}">
                    <input type="checkbox" class="answer_check" {{ $answer == 1 ? 'checked' : '' }}>
                </span>
            </div>
        </div>

        @endforeach
    </div>

    <div class="text-center">
        <button class="btn btn-success" type="submit">{{ __('Submit') }}</button>
    </div>

</form>
