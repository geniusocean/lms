<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $fillable = [
        'first_name',
        'last_name',
        'bio',
        'photo',
        'email',
        'password',
        'facebook',
        'twitter',
        'linkedin',
        'status',
        'address',
        'phone',
        'message',
        'document',
        'is_instructor',
        'instructor_name',
        'instructor_slug',
        'balance',
        'affilate_code',
        'referral_id',
        'register_id',
        'preloaded'
    ];

    public function courses()
    {
        return $this->hasMany('App\Models\Course','user_id','id');
    }


    public function enrolled_courses()
    {
        return $this->hasMany('App\Models\EnrolledCourse','user_id');
    }

    public function referral()
    {
    	return $this->belongsTo('App\Models\Referral','referral_id')->withDefault(function ($data) {
			foreach($data->getFillable() as $dt){
				$data[$dt] = __('Deleted');
			}
		});
    }

    public function referral_history(){
        return $this->hasMany('App\Models\ReferralHistory','referrer_id');
    }

    public function instructororders(){
        return $this->hasMany('App\Models\InstructorOrder');
    }


    public function showName()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function owner(){
		return $this->belongsTo('App\Models\Admin','register_id')->withDefault(function ($data) {
		foreach($data->getFillable() as $dt){
		$data[$dt] = __('Deleted');
			}
		});
	}

}
