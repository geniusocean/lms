<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $fillable = ['section_id','type','title','details','pos','video_file','file','url','duration','iframe_code','register_id','preloaded'];
    public $timestamps = false;

    public function questions()
    {
        return $this->hasMany('App\Models\Question');
    }

    public function section()
    {
    	return $this->belongsTo('App\Models\Section');
    }

    public function owner(){
		return $this->belongsTo('App\Models\Admin','register_id')->withDefault(function ($data) {
		foreach($data->getFillable() as $dt){
		$data[$dt] = __('Deleted');
			}
		});
	}

}
