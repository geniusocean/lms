<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $fillable = ['user_id','course_id','register_id','preloaded'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\Models\User')->withDefault(function ($data) {
          foreach($data->getFillable() as $dt){
            $data[$dt] = __('Deleted');
          }
		    });
    }

    public function course()
    {
        return $this->belongsTo('App\Models\Course')->withDefault(function ($data) {
          foreach($data->getFillable() as $dt){
            $data[$dt] = __('Deleted');
          }
		    });
    }

    public function owner(){
      return $this->belongsTo('App\Models\Admin','register_id')->withDefault(function ($data) {
      foreach($data->getFillable() as $dt){
      $data[$dt] = __('Deleted');
        }
      });
    }
}