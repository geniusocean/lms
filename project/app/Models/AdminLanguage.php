<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminLanguage extends Model
{
    public $timestamps = false;

    public function owner(){
		return $this->belongsTo('App\Models\Admin','register_id')->withDefault(function ($data) {
		foreach($data->getFillable() as $dt){
		$data[$dt] = __('Deleted');
			}
		});
	}

}
