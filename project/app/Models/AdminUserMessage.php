<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminUserMessage extends Model
{
    protected $fillable = ['conversation_id','message','user_id','register_id','preloaded'];
	public function conversation()
	{
	    return $this->belongsTo('App\Models\AdminUserConversation','conversation_id')->withDefault(function ($data) {
			foreach($data->getFillable() as $dt){
				$data[$dt] = __('Deleted');
			}
		});
	}

	public function owner(){
		return $this->belongsTo('App\Models\Admin','register_id')->withDefault(function ($data) {
		foreach($data->getFillable() as $dt){
		$data[$dt] = __('Deleted');
			}
		});
	}
}
