<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    protected $fillable = ['register_id', 'title', 'type', 'times', 'discount','register_id','preloaded'];
    public $timestamps = false;

    public function owner(){
		return $this->belongsTo('App\Models\Admin','register_id')->withDefault(function ($data) {
		foreach($data->getFillable() as $dt){
		$data[$dt] = __('Deleted');
			}
		});
	}

}
