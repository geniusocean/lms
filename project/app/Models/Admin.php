<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $guard = 'admin';

    protected $fillable = [
        'name', 'email', 'phone', 'password', 'role_id', 'role', 'parent_id', 'photo', 'created_at', 'updated_at', 'remember_token','biography','address','slug', 'username'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function child(){
        return $this->hasMany('App\Models\Admin','parent_id');
    }

    public function parent(){
        return $this->belongsTo('App\Models\Admin','parent_id')->withDefault(function ($data) {
            foreach($data->getFillable() as $dt){
                $data[$dt] = __('Deleted');
            }
        });
    }

    public function staff_role(){
        return $this->belongsTo('App\Models\Role','role_id')->withDefault(function ($data) {
            foreach($data->getFillable() as $dt){
                $data[$dt] = __('Deleted');
            }
        });
    }

    public function IsSuper(){
        if ($this->role == 'Administrator') {
           return true;
        }
        return false;
    }

    public function IsAdmin(){
        if ($this->role == 'Administrator') {
           return true;
        }
        return false;
    }

    public function IsOwner(){
        if ($this->role == 'Owner') {
           return true;
        }
        return false;
    }

    public function IsAdminStaff(){
        if ($this->role == null && $this->parent_id == 1) {
           return true;
        }
        return false;
    }

    public function IsOwnerStaff($parent_id){
        if ($this->role == null && $this->parent_id == $parent_id) {
           return true;
        }
        return false;
    }


    

    public function sectionCheck($value){
        $sections = explode(" , ", $this->staff_role->section);
        if (in_array($value, $sections)){
            return true;
        }else{
            return false;
        }
    }
    

}
