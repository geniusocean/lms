<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstructorOrder extends Model
{

    protected $fillable = ['user_id','order_id','course_id','price','order_number','status','charge','register_id','preloaded'];

    public $timestamps = false;
    public function user()
    {
        return $this->belongsTo('App\Models\User')->withDefault(function ($data) {
            foreach($data->getFillable() as $dt){
                $data[$dt] = __('Deleted');
            }
        });
    }
    public function order()
    {
        return $this->belongsTo('App\Models\Order')->withDefault(function ($data) {
            foreach($data->getFillable() as $dt){
                $data[$dt] = __('Deleted');
            }
        });
    }

    public function course()
    {
        return $this->belongsTo('App\Models\Course')->withDefault(function ($data) {
            foreach($data->getFillable() as $dt){
                $data[$dt] = __('Deleted');
            }
        });
    }


    public static function totalPrice($order_number,$user_id)
    {
        $totalPrice = 0;
        foreach(InstructorOrder::where('order_number',$order_number)->where('user_id',$user_id)->get() as $data){
            $discnt = $data->charge;
            $val = $data->price;
            $val = $val / 100;
            $sub = $val * $discnt;
            $discount = $sub;
            $price = $data->price - $discount;
            $totalPrice += $price;
        }
        return $totalPrice;

    }


    public static function discountPrice($discount,$price)
    {
            $totalPrice = 0;
            $discnt = $discount;
            $val = $price;
            $val = $val / 100;
            $sub = $val * $discnt;
            $dicount = $sub;
            $price = $price - $dicount;
            $totalPrice = $price;

        return $totalPrice;

    }

    public function owner(){
		return $this->belongsTo('App\Models\Admin','register_id')->withDefault(function ($data) {
		foreach($data->getFillable() as $dt){
		$data[$dt] = __('Deleted');
			}
		});
	}

}
