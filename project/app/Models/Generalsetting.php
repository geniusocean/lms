<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Generalsetting extends Model
{
    protected $fillable = ['logo', 'favicon', 'title', 'footer','copyright','colors','loader','admin_loader','talkto','disqus','currency_format','withdraw_fee','withdraw_charge','tax','mail_driver','mail_host', 'mail_port', 'mail_encryption', 'mail_user','mail_pass','from_email','from_name','is_affilate','affilate_charge','affilate_banner','fixed_commission','percentage_commission','multiple_shipping','vendor_ship_info','is_verification_email','is_capcha','error_banner','popup_title','popup_text','popup_background','invoice_logo','is_secure','footer_logo','maintain_text','breadcumb_banner','register_id','preloaded','deactivated_text'];

    public $timestamps = false;

    public function upload($name,$file,$oldname)
    {
        $file->move('assets/images',$name);
        if($oldname != null)
        {
            if(file_exists(base_path('../assets/images/'.$oldname))){
                unlink(base_path('../assets/images/'.$oldname));
            }
        }
    }

    public function owner(){
		return $this->belongsTo('App\Models\Admin','register_id')->withDefault(function ($data) {
		foreach($data->getFillable() as $dt){
		$data[$dt] = __('Deleted');
			}
		});
    }
    
    public function ownerupload($name,$file,$oldname,$owner)
    {
        $file->move('assets/'.$owner.'/owner/images',$name);
        if($oldname != null)
        {
            if(file_exists(base_path('../assets/'.$owner.'/owner/images/'.$oldname))){
                unlink(base_path('../assets/'.$owner.'/owner/images/'.$oldname));
            }
        }
    }

}
