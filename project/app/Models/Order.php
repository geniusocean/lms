<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $fillable = ['user_id', 'cart', 'method', 'discount', 'pay_amount', 'status', 'payment_status', 'txnid', 'charge_id', 'order_number','discount','register_id','preloaded'];

    public function user()
    {
    	return $this->belongsTo('App\Models\User')->withDefault(function ($data) {
			foreach($data->getFillable() as $dt){
				$data[$dt] = __('Deleted');
			}
		});
    }

    public function instructororders()
    {
        return $this->hasMany('App\Models\InstructorOrder');
    }

    public function instructorTotalPrice($user_id)
    {
        $totalPrice = 0;
        foreach($this->instructororders()->where('user_id','=',$user_id)->get() as $data){
            $discnt = $data->charge;
            $val = $data->price;
            $val = $val / 100;
            $sub = $val * $discnt;
            $discount = $sub;
            $price = $data->price - $discount;
            $totalPrice += $price;
        }
        return $totalPrice;

    }

    public function owner(){
		return $this->belongsTo('App\Models\Admin','register_id')->withDefault(function ($data) {
		foreach($data->getFillable() as $dt){
		$data[$dt] = __('Deleted');
			}
		});
	}

}
