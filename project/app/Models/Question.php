<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['lesson_id','title','options','answers','pos','register_id','preloaded'];
    public $timestamps = false;

    public function lesson()
    {
    	return $this->belongsTo('App\Models\Lesson');
    }

    public function owner(){
		return $this->belongsTo('App\Models\Admin','register_id')->withDefault(function ($data) {
		foreach($data->getFillable() as $dt){
		$data[$dt] = __('Deleted');
			}
		});
	}

}
