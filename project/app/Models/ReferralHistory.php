<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferralHistory extends Model
{
    protected $fillable = ['register_id', 'referrer_id', 'affilate_id', 'course_id', 'type','register_id','preloaded'];
    public $timestamps = false;

    public function referrer()
    {
    	return $this->belongsTo('App\Models\User','referrer_id')->withDefault(function ($data) {
			foreach($data->getFillable() as $dt){
				$data[$dt] = __('Deleted');
			}
		});
    }


    public function affilate()
    {
    	return $this->belongsTo('App\Models\User','affilate_id')->withDefault(function ($data) {
			foreach($data->getFillable() as $dt){
				$data[$dt] = __('Deleted');
			}
		});
    }


    public function course()
    {
    	return $this->belongsTo('App\Models\Course','course_id')->withDefault(function ($data) {
			foreach($data->getFillable() as $dt){
				$data[$dt] = __('Deleted');
			}
		});
	}
	
	public function owner(){
		return $this->belongsTo('App\Models\Admin','register_id')->withDefault(function ($data) {
		foreach($data->getFillable() as $dt){
		$data[$dt] = __('Deleted');
			}
		});
	}

	public function user(){
		return $this->belongsTo('App\Models\User');
	}

}
