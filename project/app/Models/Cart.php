<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;

    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

// **************** ADD TO CART *******************

    public function add($item, $id, $instructor, $affilate_user, $type) {

        $storedItem = ['qty' => 0,  'price' => $item->price, 'item' => $item,'instructor' => $instructor,'affilate_user' => $affilate_user,'type' => $type];
        $storedItem['qty']++;
        $storedItem['price'] = $item->price * $storedItem['qty'];
        $this->items[$id] = $storedItem;
        $this->totalQty++;
        $this->totalPrice += $storedItem['price'];

    }

// **************** ADD TO CART ENDS *******************

// **************** REMOVE FROM CART *******************

    public function removeItem($id) {

        $this->totalQty -= $this->items[$id]['qty'];
        $this->totalPrice -= $this->items[$id]['price'];
        unset($this->items[$id]);

    }

// **************** REMOVE FROM CART ENDS *******************

public function owner(){
    return $this->belongsTo('App\Models\Admin','register_id')->withDefault(function ($data) {
    foreach($data->getFillable() as $dt){
    $data[$dt] = __('Deleted');
        }
    });
}

}
