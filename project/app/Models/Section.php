<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = ['course_id','title','pos','register_id','preloaded'];
    public $timestamps = false;

    public function lessons()
    {
        return $this->hasMany('App\Models\Lesson');
    }


    public function course()
    {
    	return $this->belongsTo('App\Models\Course');
    }

    public function owner(){
		return $this->belongsTo('App\Models\Admin','register_id')->withDefault(function ($data) {
		foreach($data->getFillable() as $dt){
		$data[$dt] = __('Deleted');
			}
		});
	}

}
