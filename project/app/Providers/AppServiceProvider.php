<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Session;
use App;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer('*',function($settings){

            $username = explode('/',request()->path());

            // OWNER SETTING PART

            if(!empty($username[0])){
                if(DB::table('admins')->where('username',$username[0])->where('role','Owner')->exists()){
                    $owner = DB::table('admins')->where('username',$username[0])->where('role','Owner')->first();
                    $gs = DB::table('generalsettings')->where('register_id',$owner->id)->first();
                    $ps = DB::table('pagesettings')->where('register_id',$owner->id)->first();
                    $seo = DB::table('seotools')->where('register_id',$owner->id)->first();
                    $curr = DB::table('currencies')->where('register_id',$owner->id)->where('is_default',1)->first();
                    $settings->with('owner', $owner);

                    // OWNER LANGUAGE SECTION 

                    if (\Request::is('admin') || \Request::is('admin/*')) {
                        $data = DB::table('admin_languages')->where('register_id',$owner->id)->where('is_default','=',1)->first();
                        App::setlocale($data->name);
                    }
                    else {
                        if (Session::has('language'.$owner->id)) {
                            $data = DB::table('languages')->where('register_id',$owner->id)->find(Session::get('language'.$owner->id));
                            App::setlocale($data->name);
                        }
                        else {
                            $data = DB::table('languages')->where('register_id',$owner->id)->where('is_default','=',1)->first();
                            App::setlocale($data->name);
                        }
                    }
                    
                }
                else{
                    $ps = DB::table('pagesettings')->find(1);
                    $gs = DB::table('generalsettings')->find(1);
                    $seo = DB::table('seotools')->find(1);
                    $curr = DB::table('currencies')->where('register_id',0)->where('is_default',1)->first();
                    // MAIN LANGUAGE SECTION 

                    if (\Request::is('admin') || \Request::is('admin/*')) {
                        $data = DB::table('admin_languages')->where('register_id',0)->where('is_default','=',1)->first();
                        App::setlocale($data->name);
                    }
                    else {
                        if (Session::has('language')) {
                            $data = DB::table('languages')->where('register_id',0)->find(Session::get('language'));
                            App::setlocale($data->name);
                        }
                        else {
                            $data = DB::table('languages')->where('register_id',0)->where('is_default','=',1)->first();
                            App::setlocale($data->name);
                        }
                    }

                }
            }
            else{
                $gs = DB::table('generalsettings')->find(1);
                $ps = DB::table('pagesettings')->find(1);
                $seo = DB::table('seotools')->find(1);
                $curr = DB::table('currencies')->where('register_id',0)->where('is_default',1)->first();
                // MAIN LANGUAGE SECTION 

                if (\Request::is('admin') || \Request::is('admin/*')) {
                    $data = DB::table('admin_languages')->where('register_id',0)->where('is_default','=',1)->first();
                    App::setlocale($data->name);
                }
                else {
                    if (Session::has('language')) {
                        $data = DB::table('languages')->where('register_id',0)->find(Session::get('language'));
                        App::setlocale($data->name);
                    }
                    else {
                        $data = DB::table('languages')->where('register_id',0)->where('is_default','=',1)->first();
                        App::setlocale($data->name);
                    }
                }


            }

            $settings->with('gs', $gs);
            $settings->with('ps', $ps);
            $settings->with('seo', $seo);
            $settings->with('curr', $curr);

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
