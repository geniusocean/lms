<?php

namespace App\Exceptions;

use DB;
use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {

        $username = explode('/',request()->path());
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }
        if ($request->is('admin') || $request->is('admin/*')) {
            return redirect()->guest('/admin/login');
        }
        if ($request->is('student') || $request->is('student/*')) {
            return redirect()->guest('/student/login');
        }
        if ($request->is('instructor') || $request->is('instructor/*')) {
            return redirect()->guest('/instructor/login');
        }

        if(DB::table('admins')->where('username',$username[0])->where('role','Owner')->exists()){
            if($username[1] == 'admin'){
                $login = 'owner.login';
                return redirect()->guest(route($login,$username[0]));
            }

            if($username[1] == 'student'){
                $login = 'owner.student.login';
                return redirect()->guest(route($login,$username[0]));
            }

            if($username[1] == 'instructor'){
                $login = 'owner.instructor.login';
                return redirect()->guest(route($login,$username[0]));
            }

        }



        return redirect()->guest('/student/login');
    }

}
