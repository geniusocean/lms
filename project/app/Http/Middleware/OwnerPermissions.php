<?php

namespace App\Http\Middleware;
use Auth;

use Closure;
use App\Models\Admin;

class OwnerPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$data)
    {

        $path = request()->path();
        $username = explode('/',$path);
        $username = $username[0];
        $owner = Admin::where('username',$username)->first();


        if (Auth::guard('admin')->check()) {
            if(Auth::guard('admin')->user()->role == 'Owner'){
                return $next($request);
            }
            if(Auth::guard('admin')->user()->role_id == 0){
                return redirect()->route('owner.dashboard',$owner->username)->with('unsuccess',"You don't have access to that section"); 
            }
            if (Auth::guard('admin')->user()->sectionCheck($data)){
                return $next($request);
            }
        }
        return redirect()->route('owner.dashboard',$owner->username)->with('unsuccess',"You don't have access to that section");
    }
}
