<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Owner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admin')->check()) {
            if (Auth::guard('admin')->user()->IsOwner()){
                return $next($request);
            }
        }
        $path = request()->path();
        $username = explode('/',$path);
        $username = $username[0];
        
        return redirect()->route('owner.dashboard',$username)->with('unsuccess',"You don't have access to that section");
    }
}
