<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;
use App\Models\Admin;

class OwnerAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $path = request()->path();
        $username = explode('/',$path);
        $username = $username[0];
        $parent_id = Admin::where('username',$username)->first()->id;

        if (Auth::guard('admin')->check() ) {
            if (Auth::guard('admin')->user()->IsOwnerStaff($parent_id) || Auth::guard('admin')->user()->IsOwner() ){
                return $next($request);
            }
        }

        return redirect()->back();
    }
}
