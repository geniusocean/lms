<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use DB;
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
      switch ($guard) {
        case 'admin':
          if (Auth::guard($guard)->check()) {
            $username = explode('/',request()->path());
            if(!empty($username[0])){
              if(DB::table('admins')->where('username',$username[0])->where('role','Owner')->exists()){
                if(Auth::guard('admin')->user()->IsAdmin()){
                  return redirect()->back();
                }else{

                  return redirect()->route('owner.dashboard',$username[0]);
                }
              }else{

                return redirect()->route('admin.dashboard');
              }
            }
            else{
  
              return redirect()->route('admin.dashboard');
            }
          }
          break;

        default:
          if (Auth::guard($guard)->check()) {
              return redirect()->route('student-dashboard');
          }
          break;
      }

        return $next($request);
    }
}
