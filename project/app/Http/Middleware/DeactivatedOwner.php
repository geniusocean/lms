<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class DeactivatedOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        $username = explode('/',request()->path());
        if (DB::table('admins')->where('username',$username[0])->where('role','Owner')->exists()){
            if(DB::table('admins')->where('username',$username[0])->where('role','Owner')->first()->status == 1){
                return $next($request);
            }
        }

        return response()->view('front.deactivated');

    }
}
