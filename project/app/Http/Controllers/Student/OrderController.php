<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Order;
use App\Models\PaymentGateway;

class OrderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function orders()
    {
        $user = Auth::guard('web')->user();
        $orders = Order::where('register_id',0)->where('user_id','=',$user->id)->orderBy('id','desc')->get();
        return view('student.order.index',compact('user','orders'));
    }


    public function order($id)
    {
        $user = Auth::guard('web')->user();
        $order = Order::findOrfail($id);
        $cart = json_decode($order->cart, true);
        return view('student.order.details',compact('user','order','cart'));
    }



    public function orderprint($id)
    {
        $user = Auth::guard('web')->user();
        $order = Order::findOrfail($id);
        $cart = json_decode($order->cart, true);
        return view('student.order.print',compact('user','order','cart'));
    }



}
