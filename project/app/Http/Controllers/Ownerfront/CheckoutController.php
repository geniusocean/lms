<?php

namespace App\Http\Controllers\Ownerfront;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Admin;
use App\Models\Referral;
use App\Models\PaymentGateway;
use App\Models\EnrolledCourse;
use Auth;
use Session;



class CheckoutController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function checkout($name=null)
    {

        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;


        if (!Session::has('cart'.$owner_id)) {
            return redirect()->route('owner-front.cart',$owner->username)->with('error',"You don't have any product to checkout.");
        }

        $oldCart = Session::has('cart'.$owner_id) ? Session::get('cart'.$owner_id) : null;
        $cart = new Cart($oldCart);

        $user = Auth::user();
        $courses = EnrolledCourse::whereUserId($user->id)->pluck('course_id');

        $courses = json_decode(json_encode($courses), true);

        foreach($cart->items as $course)
        {
            if (in_array($course['item']['id'], $courses)){
                $cart->removeItem($course['item']['id']);
            }
        }

        if (count($cart->items) > 0) {
            Session::put('cart'.$owner_id, $cart);
        } else {
            Session::forget('cart'.$owner_id);
            return redirect()->back()->with('success',__('You have already purchased these course.'));
        }


        $data['courses'] = $cart->items;

        $discount = 0;
        if($user->referral_id != 0){
            
            $refer = Referral::find($user->referral_id);
            $discnt = $refer->discount;
            $val = $cart->totalPrice;
            $val = $val / 100;
            $sub = $val * $discnt;
            $discount = $sub;

        }

        $data['discount'] = $discount;
        $data['totalPrice'] = $cart->totalPrice - $discount;

        $paypal = PaymentGateway::whereRegisterId($owner_id)->whereKeyword('paypal')->first();
        $data['paypalData'] = $paypal->convertAutoData();
        $data['paypal'] = $paypal;

        $stripe = PaymentGateway::whereRegisterId($owner_id)->whereKeyword('stripe')->first();
        $data['stripeData'] = $stripe->convertAutoData();
        $data['stripe'] = $stripe;

        return view('owner-front.checkout',$data);


    }

    // Redirect To Checkout Page If Payment is Cancelled

    public function paycancle($name=null){
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        return redirect()->route('owner.front.checkout',$owner->username)->with('error',__('Payment Cancelled.'));
     }


    // Redirect To Success Page If Payment is Comleted

     public function payreturn($name=null){


        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        if(Session::has('tempcart'.$owner_id)){
            $oldCart = Session::get('tempcart'.$owner_id);
            $tempcart = new Cart($oldCart);
            $order_id = Session::get('temporder_id'.$owner_id);
            $order = Order::find($order_id);
        }
        else{
            $tempcart = '';
            return redirect()->back();
        }

        return view('owner-front.success',compact('tempcart','order'));
     }

}