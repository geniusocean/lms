<?php

namespace App\Http\Controllers\Ownerfront;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\User;
use App\Models\Course;
use App\Models\Admin;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Session;

use Validator;


class CatalogController extends Controller
{

    // -------------------------------- CATALOG SECTION ----------------------------------------

    public function catalog(Request $request,$name=null, $slug=null, $slug1=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $cat = null;
        $subcat = null;
        $sort = $request->sort;
        $type = $request->type;
        $level = $request->level;
        $rating = $request->rating;
        $rating = $rating * 20;
        $duration = $request->durations;

        $search = $request->search;

        if (!empty($slug)) {
            $cat = Category::where('slug', $slug)->where('register_id',$owner->id)->first();
            $data['cat'] = $cat;
        }
        if (!empty($slug1)) {
            $subcat = Subcategory::where('slug', $slug1)->where('register_id',$owner->id)->firstOrFail();
            $data['subcat'] = $subcat;
        }

        $courses = Course::where('register_id',$owner->id)->when($cat, function ($query, $cat) {
                                    return $query->where('category_id', $cat->id);
                                })
                                ->when($subcat, function ($query, $subcat) {
                                    return $query->where('subcategory_id', $subcat->id);
                                })
                                ->when($search, function ($query, $search) {
                                    return $query->whereRaw('MATCH (title) AGAINST (? IN BOOLEAN MODE)' , array($search));
                                })
                                ->when($type, function ($query, $type) {
                                    if ($type=='free') {
                                      return $query->where('price','=',0);
                                    }
                                    elseif($type=='paid') {
                                      return $query->where('price','!=',0);
                                    }
                                })
                                ->when($level, function ($query, $level) {
                                    if ($level=='beginner') {
                                      return $query->where('level','=','Beginner');
                                    }
                                    elseif($level=='intermediate') {
                                      return $query->where('level','=','Intermediate');
                                    }
                                    elseif($level=='advanced') {
                                        return $query->where('level','=','Advanced');
                                    }
                                })
                                ->when($duration, function ($query, $duration) {
                                    if ($duration == '0-2') {

                                        return $query->join('sections','sections.course_id','=','courses.id')
                                        ->join('lessons','lessons.section_id','=','sections.id')
                                        ->select('courses.id','courses.title','courses.price','courses.user_id','courses.status','courses.language_id','courses.category_id','courses.subcategory_id','courses.is_top','courses.is_free','courses.slug','courses.discount_price','courses.short_description','courses.description','courses.level','courses.requirements','courses.outcomes','courses.course_overview_type','courses.course_overview_url','courses.photo')
                                        ->havingRaw('sum(time_to_sec(lessons.duration)) >= ?', [0])
                                        ->havingRaw('sum(time_to_sec(lessons.duration)) <= ?', [7200])
                                        ->groupBy('courses.id','courses.title','courses.price','courses.status','courses.language_id','courses.category_id','courses.subcategory_id','courses.user_id','courses.is_top','courses.is_free','courses.slug','courses.discount_price','courses.short_description','courses.description','courses.level','courses.requirements','courses.outcomes','courses.course_overview_type','courses.course_overview_url','courses.photo');

                                    }
                                    elseif($duration == '2-6') {

                                        return $query->join('sections','sections.course_id','=','courses.id')
                                        ->join('lessons','lessons.section_id','=','sections.id')
                                        ->select('courses.id','courses.title','courses.price','courses.user_id','courses.status','courses.language_id','courses.category_id','courses.subcategory_id','courses.is_top','courses.is_free','courses.slug','courses.discount_price','courses.short_description','courses.description','courses.level','courses.requirements','courses.outcomes','courses.course_overview_type','courses.course_overview_url','courses.photo')
                                        ->havingRaw('sum(time_to_sec(lessons.duration)) >= ?', [7201])
                                        ->havingRaw('sum(time_to_sec(lessons.duration)) <= ?', [21600])
                                        ->groupBy('courses.id','courses.title','courses.price','courses.status','courses.language_id','courses.category_id','courses.subcategory_id','courses.user_id','courses.is_top','courses.is_free','courses.slug','courses.discount_price','courses.short_description','courses.description','courses.level','courses.requirements','courses.outcomes','courses.course_overview_type','courses.course_overview_url','courses.photo');

                                    }
                                    elseif($duration == '6-12') {


                                        return $query->join('sections','sections.course_id','=','courses.id')
                                        ->join('lessons','lessons.section_id','=','sections.id')
                                        ->select('courses.id','courses.title','courses.price','courses.user_id','courses.status','courses.language_id','courses.category_id','courses.subcategory_id','courses.is_top','courses.is_free','courses.slug','courses.discount_price','courses.short_description','courses.description','courses.level','courses.requirements','courses.outcomes','courses.course_overview_type','courses.course_overview_url','courses.photo')
                                        ->havingRaw('sum(time_to_sec(lessons.duration)) >= ?', [21601])
                                        ->havingRaw('sum(time_to_sec(lessons.duration)) <= ?', [43200])
                                        ->groupBy('courses.id','courses.title','courses.price','courses.status','courses.language_id','courses.category_id','courses.subcategory_id','courses.user_id','courses.is_top','courses.is_free','courses.slug','courses.discount_price','courses.short_description','courses.description','courses.level','courses.requirements','courses.outcomes','courses.course_overview_type','courses.course_overview_url','courses.photo');



                                    }
                                    elseif($duration == '12-18') {
                                        return $query->join('sections','sections.course_id','=','courses.id')
                                        ->join('lessons','lessons.section_id','=','sections.id')
                                        ->select('courses.id','courses.title','courses.price','courses.user_id','courses.status','courses.language_id','courses.category_id','courses.subcategory_id','courses.is_top','courses.is_free','courses.slug','courses.discount_price','courses.short_description','courses.description','courses.level','courses.requirements','courses.outcomes','courses.course_overview_type','courses.course_overview_url','courses.photo')
                                        ->havingRaw('sum(time_to_sec(lessons.duration)) >= ?', [43201])
                                        ->havingRaw('sum(time_to_sec(lessons.duration)) <= ?', [64800])
                                        ->groupBy('courses.id','courses.title','courses.price','courses.status','courses.language_id','courses.category_id','courses.subcategory_id','courses.user_id','courses.is_top','courses.is_free','courses.slug','courses.discount_price','courses.short_description','courses.description','courses.level','courses.requirements','courses.outcomes','courses.course_overview_type','courses.course_overview_url','courses.photo');
                                    }
                                })

                                ->when($rating, function ($query, $rating) {


                                      return $query->join('ratings','ratings.course_id','=','courses.id')
                                      ->select('courses.id','courses.title','courses.price','courses.user_id','courses.status','courses.language_id','courses.category_id','courses.subcategory_id','courses.is_top','courses.is_free','courses.slug','courses.discount_price','courses.short_description','courses.description','courses.level','courses.requirements','courses.outcomes','courses.course_overview_type','courses.course_overview_url','courses.photo')
                                      ->havingRaw('SUM(ratings.rating) >= ?', [$rating])
                                      ->groupBy('courses.id','courses.title','courses.price','courses.status','courses.language_id','courses.category_id','courses.subcategory_id','courses.user_id','courses.is_top','courses.is_free','courses.slug','courses.discount_price','courses.short_description','courses.description','courses.level','courses.requirements','courses.outcomes','courses.course_overview_type','courses.course_overview_url','courses.photo');


                                })

                                ->when($sort, function ($query, $sort) {
                                    if ($sort=='new') {
                                      return $query->latest('id');
                                    }
                                    elseif($sort=='old') {
                                      return $query->oldest('id');
                                    }
                                    elseif($sort=='high_price') {
                                      return $query->latest('price');
                                    }
                                    elseif($sort=='low_price') {
                                      return $query->oldest('price');
                                    }

                                    elseif($sort=='popular') {
                                        return $query->join('enrolled_courses','enrolled_courses.course_id','=','courses.id')
                                                     ->select('courses.id','courses.title','courses.price','courses.user_id','courses.status','courses.language_id','courses.category_id','courses.subcategory_id','courses.is_top','courses.is_free','courses.slug','courses.discount_price','courses.short_description','courses.description','courses.level','courses.requirements','courses.outcomes','courses.course_overview_type','courses.course_overview_url','courses.photo',\DB::raw('count(enrolled_courses.course_id) as popular'))
                                                     ->orderBy('popular','desc')
                                                     ->groupBy('courses.id','courses.title','courses.price','courses.status','courses.language_id','courses.category_id','courses.subcategory_id','courses.user_id','courses.is_top','courses.is_free','courses.slug','courses.discount_price','courses.short_description','courses.description','courses.level','courses.requirements','courses.outcomes','courses.course_overview_type','courses.course_overview_url','courses.photo');
                                      }
                                      elseif($sort=='high_rated') {
                                        return $query->join('ratings','ratings.course_id','=','courses.id')
                                                     ->select('courses.id','courses.title','courses.price','courses.user_id','courses.status','courses.language_id','courses.category_id','courses.subcategory_id','courses.is_top','courses.is_free','courses.slug','courses.discount_price','courses.short_description','courses.description','courses.level','courses.requirements','courses.outcomes','courses.course_overview_type','courses.course_overview_url','courses.photo',\DB::raw('sum(ratings.rating) as top'))
                                                     ->orderBy('top','desc')
                                                     ->groupBy('courses.id','courses.title','courses.price','courses.status','courses.language_id','courses.category_id','courses.subcategory_id','courses.user_id','courses.is_top','courses.is_free','courses.slug','courses.discount_price','courses.short_description','courses.description','courses.level','courses.requirements','courses.outcomes','courses.course_overview_type','courses.course_overview_url','courses.photo');
                                      }

                                })
                                ->when(empty($sort), function ($query, $sort) {
                                    return $query->latest('id');
                                });


                                $courses = $courses->where('status', 1)->paginate(9);

                                $data['courses'] = $courses;

                                if($request->ajax()) {
                                    return view('owner-front.ajax.catalog', $data);

                                }

        return view('owner-front.catalog', $data);
    }

    // -------------------------------- CATALOG SECTION END ----------------------------------------



    // -------------------------------- AUTOSEARCH SECTION ----------------------------------------

    public function autosearch($name=null,$slug)
    {

        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        if(mb_strlen($slug,'UTF-8') > 1){
            $search = ' '.$slug;
            $courses = Course::where('title', 'like', '%' . $search . '%')->where('register_id',$owner->id)->whereStatus(1)->take(10)->get();
            return view('owner-load.suggest',compact('courses','slug'));
        }
        return "";
    }

    // -------------------------------- AUTOSEARCH SECTION ENDS ----------------------------------------


    // -------------------------------- COURSE DETAILS SECTION ----------------------------------------

    public function course(Request $request,$name=null,$slug)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();

      $type = '';
      $affilate_user = 0;
      if(Auth::check()){
          if($request->has('ref')){
              if(!empty($request->ref)){
                  $ref = $request->ref;
                  $user = User::where('affilate_code',$ref)->first();
                  if($user){
                      if(Auth::user()->id != $user->id){
                          $affilate_user = $user->id;
                          $type = $user->is_instructor == 2 ? 'Instructor' : 'Student';
                      }
                  }
              }
          }
      }
        
      $course = Course::where('slug','=',$slug)->where('register_id',$owner->id)->firstOrFail();
      $data['course'] = $course;
      $data['instructor'] = !empty($course->instructor) ? $course->instructor : 'admin';
      $data['admin'] = \DB::table('admins')->where('id',$owner->id)->first();
      $data['affilate_user'] = $affilate_user;
      $data['type'] = $type;

      return view('owner-front.course',$data);
    }

    // -------------------------------- COURSE DETAILS SECTION ENDS ----------------------------------------

    // -------------------------------- INSTRUCTOR DETAILS SECTION ----------------------------------------

    public function instructorDetails(Request $request,$name=null,$slug)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
       

        $instructor = User::where('instructor_slug',$slug)->where('id',$owner->id)->first();
        $type = 'instructor';

        if(empty($instructor)){
            $type = 'admin';
            $instructor = \DB::table('admins')->where('id',$owner->id)->first();
            $courses = Course::where('user_id',0)->where('register_id',$owner->id)->whereStatus(1)->paginate(8);
        }else{
            $courses = $instructor->courses()->whereStatus(1)->whereRegisterId($owner->id)->paginate(8);
        }

        if($request->ajax()) {
            return view('owner-front.ajax.instructor', compact('instructor','type','courses'));

        }

        return view('owner-front.instructor',compact('instructor','type','courses'));
    }

    // -------------------------------- INSTRUCTOR DETAILS SECTION ENDS ----------------------------------------

    // ------------------ RATING SECTION --------------------

    public function reviews($name=null,$id){



        $course = Course::find($id);
        return view('owner-load.reviews',compact('course'));
    }

    public function sideReviews($name=null,$id){


        $course = Course::findOrFail($id);
        return view('owner-load.side-load',compact('course'));
    }

    // ------------------ RATING SECTION ENDS --------------------
}
