<?php

namespace App\Http\Controllers\Ownerfront;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Course;
use App\Models\Generalsetting;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\Models\Admin;

class CartController extends Controller
{

    public function cart($name=null)
    {

        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;


        if (!Session::has('cart'.$owner_id)) {
            return view('owner-front.cart');
        }

        $gs = Generalsetting::where('register_id',$owner->id)->first();
        $oldCart = Session::get('cart'.$owner_id);

        $cart = new Cart($oldCart);

        $courses = $cart->items;
        $totalPrice = $cart->totalPrice;

        return view('owner-front.cart', compact('courses','totalPrice'));
    }

    public function cartview($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        return view('owner-load.cart');
    }



    public function addtocart(Request $request, $name=null , $id)
    {
      
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $course = Course::where('id','=',$id)->where('register_id',$owner->id)->first(['id','user_id','title','slug','photo','price','discount_price','register_id']);

        $oldCart = Session::has('cart'.$owner_id) ? Session::get('cart'.$owner_id) : null;
        $cart = new Cart($oldCart);

        $affilate_user = 0;
        $type = '';
        if(Auth::check()){
            if($request->has('ref')){
                if(!empty($request->ref)){
                    $affilate_user = (int)$request->ref == 0 ? 0 : (int)$request->ref;
                    $type = $request->type;
                }
            }
        }

        if(isset($cart->items[$course->id]))
        {
            return redirect()->route('owner.front.cart',$owner->username)->with('error',__('Already Added To Cart.'));
        }

        $instructor = !empty($course->instructor) ? $course->instructor->showName() : \DB::table('admins')->where('id',$owner->id)->first()->name;

        $cart->add($course, $course->id, $instructor, $affilate_user, $type);

        Session::put('cart'.$owner_id,$cart);
        return redirect()->route('owner.front.cart',$owner->username);
    }

   public function addcart(Request $request,$name=null , $id)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $course = Course::where('id','=',$id)->where('register_id',$owner->id)->first(['id','user_id','title','slug','photo','price','discount_price']);

        $oldCart = Session::has('cart'.$owner_id) ? Session::get('cart'.$owner_id) : null;
        $cart = new Cart($oldCart);

        $affilate_user = 0;
        $type = '';
        if(Auth::check()){
            if($request->has('ref')){
                if(!empty($request->ref)){
                    $affilate_user = (int)$request->ref == 0 ? 0 : (int)$request->ref;
                    $type = $request->type;
                }
            }
        }

        if(isset($cart->items[$course->id]))
        {
            return response()->json('already');
        }

        $instructor = !empty($course->instructor) ? $course->instructor->showName() : \DB::table('admins')->where('id',$owner->id)->first()->name;

        $cart->add($course, $course->id, $instructor, $affilate_user, $type);

        Session::put('cart'.$owner_id,$cart);
        $data[0] = count($cart->items);
        return response()->json($data);
    }




    public function removecart($name=null,$id)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $oldCart = Session::has('cart'.$owner_id) ? Session::get('cart'.$owner_id) : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        if (count($cart->items) > 0) {
            Session::put('cart'.$owner_id, $cart);
            $data[0] = $cart->totalPrice;
            $data[1] = count($cart->items);
            return response()->json($data);
        } else {
            Session::forget('cart'.$owner_id);
            $data = 0;
            return response()->json($data);
        }
    }


}