<?php

namespace App\Http\Controllers\Ownerfront;

use App\Classes\GeniusMailer;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\User;
use App\Models\BlogCategory;
use App\Models\Generalsetting;
use App\Models\Service;
use App\Models\Course;
use App\Models\Subscriber;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Admin;
use Session;


class FrontendController extends Controller
{
    public function __construct()
    {
        //$this->auth_guests();
    }


// -------------------------------- HOME PAGE SECTION ----------------------------------------

	public function index($name=null)
	{
       $owner = Admin::where('username',$name)->where('role','Owner')->first();
       $data['services'] = Service::all();
       $data['new_courses']  = Course::where('status',1)->where('register_id',$owner->id)->take(6)->get();
       $data['top_courses']  = Course::where('status',1)->where('register_id',$owner->id)->where('is_top',1)->take(6)->get();
       $data['blogs'] = Blog::orderby('id','desc')->where('register_id',$owner->id)->take(3)->get();
       return view('owner-front.index',$data);
	}

// -------------------------------- HOME PAGE SECTION ENDS ----------------------------------------

    // LANGUAGE SECTION

    public function language($name=null,$id)
    {
        $this->code_image();
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        Session::put('language'.$owner->id, $id);
        return redirect()->route('owner.front.index',$owner->username);
    }

    // LANGUAGE SECTION ENDS



// -------------------------------- BLOG SECTION ----------------------------------------

	public function blog(Request $request,$name=null)
	{
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;


        $tags = null;
        $tagz = '';
        $name = Blog::pluck('tags')->where('register_id',$owner->id)->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));

        $archives= Blog::orderBy('created_at','desc')->where('register_id',$owner->id)->get()->groupBy(function($item){ return $item->created_at->format('F Y'); })->take(5)->toArray();
        $name = Blog::pluck('tags')->where('register_id',$owner->id)->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));

        $blogs = Blog::orderBy('created_at','desc')->where('register_id',$owner->id)->paginate(8);
        $bcat = BlogCategory::where('register_id',$owner->id)->get();
        if($request->ajax()){
            return view('front.ajax.blog',compact('blogs','bcat','tags'));
        }
		return view('owner-front.blog',compact('blogs','bcat','tags'));
	}

    public function blogcategory(Request $request,$name=null, $slug)
    { 
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $tags = null;
        $tagz = '';
        $name = Blog::pluck('tags')->where('register_id',$owner->id)->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));

        $archives= Blog::orderBy('created_at','desc')->get()->where('register_id',$owner->id)->groupBy(function($item){ return $item->created_at->format('F Y'); })->take(5)->toArray();
        $name = Blog::pluck('tags')->where('register_id',$owner->id)->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));

        $blogs = Blog::orderBy('created_at','desc')->where('register_id',$owner->id)->paginate(9);
        $bcat = BlogCategory::where('register_id',$owner->id)->get();
        $cat = BlogCategory::where('slug', '=', str_replace(' ', '-', $slug))->first();
        $blogs = $cat->blogs()->orderBy('created_at','desc')->where('register_id',$owner->id)->paginate(8);
            if($request->ajax()){
                return view('front.ajax.blog',compact('bcat','blogs','tags'));
            }
        return view('owner-front.blog',compact('bcat','blogs','tags'));
    }

    public function blogtags(Request $request,$name=null , $slug)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $tags = null;
        $tagz = '';
        $name = Blog::pluck('tags')->where('register_id',$owner->id)->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));

        $archives= Blog::orderBy('created_at','desc')->get()->where('register_id',$owner->id)->groupBy(function($item){ return $item->created_at->format('F Y'); })->take(5)->toArray();
        $name = Blog::pluck('tags')->where('register_id',$owner->id)->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));
        $bcat = BlogCategory::where('register_id',$owner->id)->get();
        $blogs = Blog::where('tags', 'like', '%' . $slug . '%')->where('register_id',$owner->id)->paginate(9);
            if($request->ajax()){
                return view('front.ajax.blog',compact('blogs','slug','bcat','tags'));
            }
        return view('owner-front.blog',compact('blogs','slug','bcat','tags'));
    }

    public function blogsearch(Request $request,$name=null)
    { 
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
      
        $owner_id = $owner->id;

        $tags = null;
        $tagz = '';
        $name = Blog::pluck('tags')->where('register_id',$owner->id)->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));

       
        $name = Blog::pluck('tags')->where('register_id',$owner->id)->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));
        $bcat = BlogCategory::where('register_id',$owner->id)->get();
        $search = $request->search;
        $blogs = Blog::where('register_id',$owner->id)->where('title', 'like', '%' . $search . '%')->paginate(9);
  
            if($request->ajax()){
                return view('front.ajax.blog',compact('blogs','search','bcat','tags'));
            }
        return view('owner-front.blog',compact('blogs','search','bcat','tags'));
    }

    public function blogshow($name=null,$id)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $this->code_image();
        $tags = null;
        $tagz = '';
        $bcat = BlogCategory::where('register_id',$owner->id)->get();
        $blog = Blog::findOrFail($id);
        $blog->views = $blog->views + 1;
        $blog->update();
        $name = Blog::pluck('tags')->where('register_id',$owner->id)->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));


        $blog_meta_tag = $blog->meta_tag;
        $blog_meta_description = $blog->meta_description;
        return view('owner-front.blogshow',compact('blog','bcat','tags','blog_meta_tag','blog_meta_description'));
    }


// -------------------------------- BLOG SECTION ENDS----------------------------------------

// -------------------------------- PAGE SECTION ----------------------------------------
    public function page($name=null,$slug)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $this->code_image();
        $page =  DB::table('pages')->where('register_id',$owner->id)->where('slug',$slug)->first();
        if(empty($page))
        {
            return response()->view('owner-error.404')->setStatusCode(404);
        }

        return view('owner-front.page',compact('page'));
    }
// -------------------------------- PAGE SECTION ENDS----------------------------------------

// -------------------------------- CONTACT SECTION ----------------------------------------

	public function contact($name=null)
	{ 
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        

        $this->code_image();
        if(DB::table('generalsettings')->where('register_id',$owner->id)->first()->is_contact== 0){
            return redirect()->back();
        }
        $ps =  DB::table('pagesettings')->where('register_id',$owner->id)->first();
		return view('owner-front.contact',compact('ps'));
	}


    //Send email to admin
    public function contactemail(Request $request,$name=null)
    { 
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $gs = Generalsetting::where('register_id',$owner->id)->first();

        if($gs->is_capcha == 1)
        {

        // Capcha Check
        $value = session('captcha_string');
        if ($request->codes != $value){
            return response()->json(array('errors' => [ 0 => 'Please enter Correct Capcha Code.' ]));
        }

        }

        // Login Section
        $ps = DB::table('pagesettings')->where('register_id','=',$owner->id)->first();
        $subject = "Email From Of ".$request->name;
        $to = $request->to;
        $name = $request->name;
        $phone = $request->phone;
        $from = $request->email;
        $msg = "Name: ".$name."\nEmail: ".$from."\nPhone: ".$phone."\nMessage: ".$request->text;
        if($gs->is_smtp)
        {

        $data = [
            'to' => $to,
            'subject' => $subject,
            'body' => $msg,
        ];

        $mailer = new GeniusMailer();
        $mailer->sendCustomMail($data);
        }
        else
        {

        $headers = "From: ".$gs->from_name."<".$gs->from_email.">";
        mail($to,$subject,$msg,$headers);
        }
        // Login Section Ends

        // Redirect Section
        return response()->json(__('Success! Thanks for contacting us, we will get back to you shortly.'));
    }

    // Refresh Capcha Code
    public function refresh_code(){
        $this->code_image();
        return "done";
    }

// -------------------------------- CONTACT SECTION ENDS ----------------------------------------

// -------------------------------- SUBSCRIBE SECTION ----------------------------------------

    public function subscribe(Request $request,$name=null)
    { 
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $subs = Subscriber::where('email','=',$request->email)->where('register_id',$owner->id)->first();
        if(isset($subs)){
        return response()->json(array('errors' => [ 0 =>  __('This Email Has Already Been Taken.')]));
        }
        $subscribe = new Subscriber;
        $input = $request->all();
        $input['register_id'] = $owner_id;
        $subscribe->create($input);
        return response()->json(__('You Have Subscribed Successfully.'));
    }

// -------------------------------- SUBSCRIBE SECTION ENDS ----------------------------------------

// Maintenance Mode

    public function maintenance($name=null)
    {   
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $gs = Generalsetting::where('register_id',$owner->id)->first();
            if($gs->is_maintain != 1) {
                return redirect()->route('owner-front.index');
            }

        return view('owner-front.maintenance');
    }



    // Vendor Subscription Check
    public function subcheck($name=null){
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $settings = Generalsetting::where('register_id',$owner->id)->first();
        $today = Carbon::now()->format('Y-m-d');
        $newday = strtotime($today);
        foreach (DB::table('users')->where('is_vendor','=',2)->where('register_id',$owner->id)->get() as  $user) {
                $lastday = $user->date;
                $secs = strtotime($lastday)-$newday;
                $days = $secs / 86400;
                if($days <= 5)
                {
                  if($user->mail_sent == 1)
                  {
                    if($settings->is_smtp == 1)
                    {
                        $data = [
                            'to' => $user->email,
                            'type' => "subscription_warning",
                            'cname' => $user->name,
                            'oamount' => "",
                            'aname' => "",
                            'aemail' => "",
                            'onumber' => ""
                        ];
                        $mailer = new GeniusMailer();
                        $mailer->sendAutoMail($data);
                    }
                    else
                    {
                    $headers = "From: ".$settings->from_name."<".$settings->from_email.">";
                    mail($user->email,'Your subscription plan duration will end after five days. Please renew your plan otherwise all of your products will be deactivated.Thank You.',$headers);
                    }
                    DB::table('users')->where('id',$user->id)->update(['mail_sent' => 0]);
                  }
                }
                if($today > $lastday)
                {
                    DB::table('users')->where('id',$user->id)->update(['is_vendor' => 1]);
                }
            }
    }
    // Vendor Subscription Check Ends

    public function trackload($id,$name=null)
{     
     $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $order = Order::where('order_number','=',$id)->where('register_id',$owner->id)->first();
        $datas = array('Pending','Processing','On Delivery','Completed');
        return view('owner-load.track-load',compact('order','datas'));

    }



    // Capcha Code Image
    private function  code_image()
    {
        $actual_path = str_replace('project','',base_path());
        $image = imagecreatetruecolor(200, 50);
        $background_color = imagecolorallocate($image, 255, 255, 255);
        imagefilledrectangle($image,0,0,200,50,$background_color);

        $pixel = imagecolorallocate($image, 0,0,255);
        for($i=0;$i<500;$i++)
        {
            imagesetpixel($image,rand()%200,rand()%50,$pixel);
        }

        $font = $actual_path.'assets/front/fonts/NotoSans-Bold.ttf';
        $allowed_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $length = strlen($allowed_letters);
        $letter = $allowed_letters[rand(0, $length-1)];
        $word='';
        //$text_color = imagecolorallocate($image, 8, 186, 239);
        $text_color = imagecolorallocate($image, 0, 0, 0);
        $cap_length=6;// No. of character in image
        for ($i = 0; $i< $cap_length;$i++)
        {
            $letter = $allowed_letters[rand(0, $length-1)];
            imagettftext($image, 25, 1, 35+($i*25), 35, $text_color, $font, $letter);
            $word.=$letter;
        }
        $pixels = imagecolorallocate($image, 8, 186, 239);
        for($i=0;$i<500;$i++)
        {
            imagesetpixel($image,rand()%200,rand()%50,$pixels);
        }
        session(['captcha_string' => $word]);
        imagepng($image, $actual_path."assets/images/capcha_code.png");
    }

// -------------------------------- CONTACT SECTION ENDS----------------------------------------

}