<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use DB;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function show_notf(){
      return view('load.notification');
    }

    public function read(){
      $datas = Notification::where('register_id',0)->where('is_read','=',0)->get();
      if($datas->count() > 0){
        foreach($datas as $data){
          $data->is_read = 1;
          $data->update();
        }
      } 
    }

    public function notf_clear()
    {
      DB::table('notifications')->truncate();
    } 

}