<?php

namespace App\Http\Controllers\Admin;

use Datatables;
use App\Models\BlogCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;

class BlogCategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables()
    {
         $datas = BlogCategory::where('register_id',0)->orderBy('id','desc')->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->editColumn('preloaded', function(BlogCategory $data) {
                                $status      = $data->preloaded == 1 ? __('Yes') : __('No');
                                $status_sign = $data->preloaded == 1 ? 'success'   : 'danger';

                                return '<div class="btn-group mb-1">
                                <button type="button" class="btn btn-'.$status_sign.' btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                '.$status .'
                                </button>
                                <div class="dropdown-menu" x-placement="bottom-start">
                                <a href="javascript:;" data-toggle="modal" data-target="#statusModal" class="dropdown-item" data-href="'. route('admin-preloaded-status',['blog_categories','id1' => $data->id, 'id2' => 1]).'">'.__("Yes").'</a>
                                <a href="javascript:;" data-toggle="modal" data-target="#statusModal" class="dropdown-item" data-href="'. route('admin-preloaded-status',['blog_categories','id1' => $data->id, 'id2' => 0]).'">'.__("No").'</a>
                                </div>
                            </div>';

                            })
                            ->addColumn('action', function(BlogCategory $data) {
                                return '<div class="actions-btn"><a href="' . route('admin-cblog-edit',$data->id) . '" class="btn btn-primary btn-sm btn-rounded">
                                <i class="fas fa-edit"></i> '.__("Edit").'
                              </a><button type="button" data-toggle="modal" data-target="#deleteModal"  data-href="' . route('admin-cblog-delete',$data->id) . '" class="btn btn-danger btn-sm btn-rounded">
                                <i class="fas fa-trash"></i>
                              </button></div>';
                            })
                            ->rawColumns(['action','preloaded'])
                            ->toJson();//--- Returning Json Data To Client Side
    }

    //*** GET Request
    public function index()
    {
        return view('admin.cblog.index');
    }

    //*** GET Request
    public function create()
    {
        return view('admin.cblog.create');
    }

    //*** POST Request
    public function store(Request $request)
    {
        //--- Validation Section

        if(BlogCategory::where('slug',$request->slug)->where('register_id',0)->exists()){
            return response()->json(array('errors' => [0 =>'This slug has already been taken.']));
        }
        if(BlogCategory::where('name',$request->name)->where('register_id',0)->exists()){
            return response()->json(array('errors' => [0 =>'This name has already been taken.']));
        }

        //--- Validation Section Ends

        //--- Logic Section
        $data = new BlogCategory;
        $input = $request->all();
        $data->fill($input)->save();
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'New Data Added Successfully.'.' '.'<a href="'.route('admin-bcat-index').'"> '.__('View Lists.').'</a>';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    //*** GET Request
    public function edit($id)
    {
        $data = BlogCategory::findOrFail($id);
        return view('admin.cblog.edit',compact('data'));
    }

    //*** POST Request
    public function update(Request $request, $id)
    {
        //--- Validation Section

        if(BlogCategory::where('slug',$request->slug)->where('id','!=',$id)->where('register_id',0)->exists()){
            return response()->json(array('errors' => [0 =>'This slug has already been taken.']));
        }
        if(BlogCategory::where('name',$request->name)->where('id','!=',$id)->where('register_id',0)->exists()){
            return response()->json(array('errors' => [0 =>'This name has already been taken.']));
        }

        //--- Validation Section Ends

        //--- Logic Section
        $data = BlogCategory::findOrFail($id);
        $input = $request->all();
        $data->update($input);
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'Data Updated Successfully.'.' '.'<a href="'.route('admin-bcat-index').'"> '.__('View Lists.').'</a>';
        return response()->json($msg);
        //--- Redirect Section Ends

    }

    //*** GET Request
    public function destroy($id)
    {
        $data = BlogCategory::findOrFail($id);

        //--- Check If there any blogs available, If Available Then Delete it
        if($data->blogs->count() > 0)
        {
            foreach ($data->blogs as $element) {
                $element->delete();
            }
        }
        $data->delete();
        //--- Redirect Section
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }
}
