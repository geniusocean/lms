<?php

namespace App\Http\Controllers\Admin;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\AdminLanguage;
use App\Models\Course;
use App\Models\Counter;
use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\Category;
use App\Models\CategorySectionTitle;
use App\Models\EmailTemplate;
use App\Models\Faq;
use App\Models\Generalsetting;
use App\Models\Owner;
use App\Models\User;
use App\Models\Page;
use App\Models\Language;
use App\Models\PaymentGateway;
use App\Models\Pagesetting;
use App\Models\Seotool;
use App\Models\Service;
use App\Models\Coupon;
use App\Models\Socialsetting;
use App\Models\TitleDescription;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use DB;
use Zipper;
use Illuminate\Support\Str;

class OwnerController extends Controller
{
        public function __construct()
        {
            $this->middleware('auth:admin');
        }

        //*** JSON Request
        public function datatables()
        {
            $datas = Admin::where('role','Owner')->orderBy('id')->get();
            //--- Integrating This Collection Into Datatables
            return DataTables::of($datas)
                             
                                ->addColumn('status', function(Admin $data) {
                                    $status      = $data->status == 1 ? __('Activated') : __('Deactivated');
                                    $status_sign = $data->status == 1 ? 'success'   : 'danger';
    
                                    return '<div class="btn-group mb-1">
                                    <button type="button" class="btn btn-'.$status_sign.' btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      '.$status .'
                                    </button>
                                    <div class="dropdown-menu" x-placement="bottom-start">
                                      <a href="javascript:;" data-toggle="modal" data-target="#statusModal" class="dropdown-item" data-href="'. route('admin-owner-status',['id1' => $data->id, 'id2' => 1]).'">'.__("Activate").'</a>
                                      <a href="javascript:;" data-toggle="modal" data-target="#statusModal" class="dropdown-item" data-href="'. route('admin-owner-status',['id1' => $data->id, 'id2' => 0]).'">'.__("Deactivate").'</a>
                                    </div>
                                  </div>';
                                })


                                ->addColumn('action', function(Admin $data) {
                                    return '<div class="actions-btn">
                                  <div class="dropdown">
                                  <a class="btn btn-primary btn-sm btn-rounded dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> '.__("Options").' </a>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a href="' . route('admin-owner-secret',$data->id) . '" class="dropdown-item options">
                                    <i class="fas fa-cog"></i> '.__("Curriculam").'
                                    </a>
                                    <a href="' . route('admin-owner-secret',$data->id) . '" class="dropdown-item options">
                                    <i class="fas fa-cog"></i> '.__("Curriculam").'
                                    </a>
                                    <a href="' . route('admin-user-show',$data->id) . '" class="dropdown-item options">
                                    <i class="fas fa-edit"></i> '.__("Edit").'
                                  </a><button type="button" data-toggle="modal" data-target="#deleteModal"  data-href="' . route('admin-course-delete',$data->id) . '" class="dropdown-item options">
                                    <i class="fas fa-trash"></i> '.__("Delete").'
                                  </button>
    
                                  </div>
                                </div>
                                    </div>';
                                })



                                ->editColumn('created_at', function(Admin $data) {
                                    $created_at = $data->created_at ? $data->created_at->diffForHumans() : '';
                                    return $created_at;
                                })
                                ->rawColumns(['status','action','created_at'])
                                ->toJson(); //--- Returning Json Data To Client Side
        }

       //*** JSON Request
       public function staffdatatables($id)
       {
            $datas = Admin::findOrFail($id)->child;
            //--- Integrating This Collection Into Datatables
            return Datatables::of($datas)
                               ->addColumn('action', function(Admin $data) {
                                   $delete = $data->id == 1 ? '':'<a href="javascript:;" data-href="' . route('admin-mstaff-delete',$data->id) . '" data-toggle="modal" data-target="#confirm-delete" class="delete"><i class="fas fa-trash-alt"></i></a>';
                                   return '<div class="action-list"><a data-href="' . route('owner-staff-show',$data->id) . '" class="view" data-toggle="modal" data-target="#modal1"> <i class="fas fa-eye"></i>View Details</a>'.$delete.'</div>';
                               }) 
                               ->rawColumns(['action'])
                               ->toJson(); //--- Returning Json Data To Client Side
       }

    //*** JSON Request
    public function userdatatables($id)
    {
        $datas = Admin::findOrFail($id)->users;
        //--- Integrating This Collection Into Datatables
        return Datatables::of($datas)
                            ->addColumn('action', function(User $data) {
                                $delete = $data->id == 1 ? '':'<a href="javascript:;" data-href="' . route('owner-user-delete',$data->id) . '" data-toggle="modal" data-target="#confirm-delete" class="delete"><i class="fas fa-trash-alt"></i></a>';
                                return '<div class="action-list"><a data-href="' . route('owner-user-show',$data->id) . '" class="view" data-toggle="modal" data-target="#modal1"> <i class="fas fa-eye"></i>View Details</a>'.$delete.'</div>';
                            }) 
                            ->rawColumns(['action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

    public function staff($id){
        return view('admin.owner.staff',compact('id'));
    }

    public function staffshow($id)
    {
        $username = Admin::findOrFail($id)->parent->username;
        $data = Admin::findOrFail($id);
        return view('admin.owner.staffshow',compact('data','username'));
    }

    public function usershow($id)
    {
        $username = User::findOrFail($id)->owner->username;
        $data = User::findOrFail($id);
        return view('admin.owner.usershow',compact('data','username'));
    }

    //*** GET Request
    public function secret($id)
    {
        Auth::guard('admin')->logout();
        $data = Admin::findOrFail($id);
        Auth::guard('admin')->login($data); 
        return redirect()->route('owner.dashboard',$data->username);
    }

    //*** GET Request
    public function deactivate()
    {
        return view('admin.owner.deactivated-holder');
    }



	//*** GET Request
    public function status($id1,$id2)
    {
        $user = Admin::findOrFail($id1);
        $user->status = $id2;
        $user->update();
        //--- Redirect Section        
        $msg[0] = __('Status Updated Successfully.');
        return response()->json($msg);      
        //--- Redirect Section Ends    

    }


    public function staffdelete($id){
        $username = Admin::findOrFail($id)->parent->username;
        if($id == 1)
    	{
        return "You don't have access to remove this admin";
    	}
        $data = Admin::findOrFail($id);
        //If Photo Doesn't Exist
        if($data->photo == null){
            $data->delete();
            //--- Redirect Section     
            $msg = 'Data Deleted Successfully.';
            return response()->json($msg);      
            //--- Redirect Section Ends     
        }
        //If Photo Exist
        if (file_exists(public_path().'/assets/'.$username.'/owner/images/staff/'.$data->photo)) {
            unlink(public_path().'/assets/'.$username.'/owner/images/staff/'.$data->photo);
        }
        $data->delete();
        //--- Redirect Section     
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);      
        //--- Redirect Section Ends    
    }

    public function userdelete($id){
        $username = User::findOrFail($id)->owner->username;
        $data = User::findOrFail($id);

        //If Photo Doesn't Exist
        if($data->photo == null){
            $data->delete();
            //--- Redirect Section     
            $msg = 'Data Deleted Successfully.';
            return response()->json($msg);      
            //--- Redirect Section Ends     
        }
        //If Photo Exist
        if (file_exists(public_path().'/assets/'.$username.'/owner/images/users/'.$data->photo)) {
            unlink(public_path().'/assets/'.$username.'/owner/images/users/'.$data->photo);
        }
        $data->delete();
        //--- Redirect Section     
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);      
        //--- Redirect Section Ends  
    }

    public function user($id){
        return view('admin.owner.user',compact('id'));
    }


    public function index(){
        return view('admin.owner.index');
    }

    public function create(){
        return view('admin.owner.create');
    }

    public function store(Request $request){

        $rules = [
            'username' => 'required|unique:admins',
            'name'     => 'required',
            'email'     => 'required|unique:admins',
            'password'     => 'required',
            'photo'     => 'required|mimes:jpg,jpeg,png',
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }

        $data = new Admin();
        $input = $request->all();

        if($file = $request->file('photo')){
            $name = time().$file->getClientOriginalName();
            $file->move('assets/images/owners/',$name);
            $input['photo'] = $name;
        }

        $input['role'] = 'Owner';
        $input['password'] = bcrypt($request['password']);

		if($request->is_preloaded == "")
		{
		   $input['is_preloaded'] = 0;
		}
		if($request->is_future_update == "")
		{
		   $input['is_future_update'] = 0;
		}


        $data->fill($input)->save();
        $last_id = $data->id;

   
        $username = $request->username;

        if($username){
            mkdir('assets/'.$username.'/owner/images', 0777, true);
            mkdir('assets/'.$username.'/owner/images/courses', 0777, true);
            mkdir('assets/'.$username.'/owner/images/sliders', 0777, true);
            mkdir('assets/'.$username.'/owner/images/services', 0777, true);
            mkdir('assets/'.$username.'/owner/images/categories', 0777, true);
            mkdir('assets/'.$username.'/owner/images/blogs', 0777, true);
            mkdir('assets/'.$username.'/owner/images/thumbnails', 0777, true);
            $dir = 'assets/images';
            $files = preg_grep('~\.(gif|jpg|png)$~', scandir($dir));
            foreach($files as $file){
                copy('assets/images/'.$file, 'assets/'.$username.'/owner/images/'.$file);
            }

        }

        if($data->is_preloaded == 1){
            
            // INSERT SERVICE 
            
            if(DB::table('services')->whereRegisterId(0)->wherePreloaded(1)->count() > 0){

                $services = DB::table('services')->whereRegisterId(0)->wherePreloaded(1)->get()->map( function($item) use ($last_id,$username){
                    copy('assets/images/services/'.$item->photo, 'assets/'.$username.'/owner/images/services/'.$item->photo);
                    unset($item->id);
                    $item->register_id = $last_id;
                    $item->preloaded = 0;
                    return $item;
                });

                $services = json_decode(json_encode($services), true); 
    
                Service::insert($services);
            }

       

        // INSERT CATEGORY 
  
            if(DB::table('categories')->whereRegisterId(0)->wherePreloaded(1)->count() > 0){
            
                $categories = DB::table('categories')->whereRegisterId(0)->wherePreloaded(1)->get()->map( function($item) use ($last_id,$username){
                    copy('assets/images/categories/'.$item->photo, 'assets/'.$username.'/owner/images/categories/'.$item->photo);
                    unset($item->id);
                    $item->register_id = $last_id;
                    $item->preloaded = 0;
                    return $item;
                });

                $categories = json_decode(json_encode($categories), true); 

                Category::insert($categories);
            }

        // INSERT BLOG 

            if(DB::table('blogs')->whereRegisterId(0)->wherePreloaded(1)->count() > 0){
                
                $blogs = DB::table('blogs')->whereRegisterId(0)->wherePreloaded(1)->get()->map( function($item) use ($last_id,$username){
                    copy('assets/images/blogs/'.$item->photo, 'assets/'.$username.'/owner/images/blogs/'.$item->photo);
                    unset($item->id);
                    $item->register_id = $last_id;
                    $item->preloaded = 0;
                    return $item;
                });

                $blogs = json_decode(json_encode($blogs), true); 

                Blog::insert($blogs);
            }


        // INSERT Courses 
        
        if(DB::table('courses')->whereRegisterId(0)->wherePreloaded(1)->count() > 0){

                $auctions = DB::table('courses')->whereRegisterId(0)->wherePreloaded(1)->get()->map( function($item) use ($last_id,$username){
                    copy('assets/images/courses/'.$item->photo, 'assets/'.$username.'/owner/images/courses/'.$item->photo);
                    unset($item->id);
                    $item->register_id = $last_id;
                    $item->preloaded = 0;
                    $item->user_id = 0;
                    return $item;
                });

                $auctions = json_decode(json_encode($auctions), true); 

                Course::insert($auctions);
            }



        // INSERT BLOGCATEGORY 
        
        if(DB::table('blog_categories')->whereRegisterId(0)->wherePreloaded(1)->count() > 0){
                $blog_categories = DB::table('blog_categories')->whereRegisterId(0)->wherePreloaded(1)->get()->map( function($item) use ($last_id){
                    unset($item->id);
                    $item->register_id = $last_id;
                    $item->preloaded = 0;
                    return $item;
                });
                $blog_categories = json_decode(json_encode($blog_categories), true); 

                BlogCategory::insert($blog_categories);
        }

        // INSERT PAGE 
        
        if(DB::table('pages')->whereRegisterId(0)->wherePreloaded(1)->count() > 0){

                $pages = DB::table('pages')->whereRegisterId(0)->wherePreloaded(1)->get()->map( function($item) use ($last_id){
                    unset($item->id);
                    $item->register_id = $last_id;
                    $item->preloaded = 0;
                    return $item;
                });
                $pages = json_decode(json_encode($pages), true); 

                Page::insert($pages);
            }

        }

        if(DB::table('currencies')->IsDefault(1)->whereRegisterId(0)->count() > 0){

                $currencies = DB::table('currencies')->whereRegisterId(0)->wherePreloaded(1)->get()->map( function($item) use ($last_id){
                    unset($item->id);
                    $item->register_id = $last_id;
                    $item->preloaded = 0;
                    return $item;
                });
                $currencies = json_decode(json_encode($currencies), true); 

                Currency::insert($currencies);
            }


        // INSERT ADMINLANGUAGE 
        
        if(DB::table('languages')->IsDefault(1)->whereRegisterId(0)->count() > 0){
            $languages = DB::table('languages')->whereRegisterId(0)->wherePreloaded(1)->get()->map( function($item) use ($last_id,$username){
                $auth_username = time().Str::random(8);
                $default_language = file_get_contents(resource_path('lang/') . $item->file);
                $json_file =  $auth_username  . '.json';
                $path = resource_path('lang/') . $json_file;
                File::put($path, $default_language);
                unset($item->id);
                $item->register_id = $last_id;
                $item->preloaded = 0;
                $item->file = $json_file;
                $item->name = $auth_username;
                return $item;
            });

            $languages = json_decode(json_encode($languages), true); 

            Language::insert($languages);
        }


        // INSERT ADMINLANGUAGE 
        
        if(DB::table('admin_languages')->IsDefault(1)->whereRegisterId(0)->count() > 0){
            $admin_languages = DB::table('admin_languages')->whereRegisterId(0)->wherePreloaded(1)->get()->map( function($item) use ($last_id,$username){
                $auth_username = time().Str::random(8);
                $default_language = file_get_contents(resource_path('lang/') . $item->file);
                $json_file =  $auth_username  . '.json';
                $path = resource_path('lang/') . $json_file;
                File::put($path, $default_language);
                unset($item->id);
                $item->register_id = $last_id;
                $item->preloaded = 0;
                $item->file = $json_file;
                $item->name = $auth_username;
                return $item;
            });

            $admin_languages = json_decode(json_encode($admin_languages), true); 

            AdminLanguage::insert($admin_languages);
        }


  

       // INSERT EMAILTEMPLATE 
       
       $email_templates = DB::table('email_templates')->whereRegisterId(0)->get()->map( function($item) use ($last_id){
            unset($item->id);
            $item->register_id = $last_id;
            return $item;
        });
        $email_templates = json_decode(json_encode($email_templates), true); 

        EmailTemplate::insert($email_templates);

       // INSERT COUNTER 
       
       $counters = \DB::table('counters')->whereRegisterId(0)->get()->map( function($item) use ($last_id){
            unset($item->id);
            $item->register_id = $last_id;
            $item->total_count = 20;
            return $item;
        });
        $counters = json_decode(json_encode($counters), true); 

        Counter::insert($counters);

   

       // INSERT PAGESETTING 

        $page = Pagesetting::find(1);
        if(!empty($page)){
            $pagesettings = $page->replicate();
            $pagesettings->register_id = $last_id;
            $pagesettings->save();
        }

       // INSERT SOCIALSETTING 

        $social = Socialsetting::find(1);
        if(!empty($page)){
            $social = $social->replicate();
            $social->register_id = $last_id;
            $social->save();
        }

       // INSERT GENERALSETTING 

        $generalsetting = Generalsetting::find(1);
        if(!empty($generalsetting)){
            $generalsetting = $generalsetting->replicate();
            $generalsetting->register_id = $last_id;
            $generalsetting->withdraw_fee = 0;
            $generalsetting->withdraw_charge = 0;
            $generalsetting->save();
        }

       // INSERT SEOTOOL 

        $seo = Seotool::find(1);
        if(!empty($seo)){
           
            $seotool = $seo->replicate();
            $seotool->register_id = $last_id;
            $seotool->save();
        }

    

        $msg = 'New Data Added Successfully.';
        return response()->json($msg); 
    }

    public function edit($id){
        $data = Admin::findOrFail($id);
        return view('admin.owner.edit',compact('data'));
    }

	    //*** POST Request
	    public function update(Request $request, $id)
	    {
	        //--- Validation Section
	        $rules = [
                'username'  => 'required|unique:admins,username,'.$id,
                'name'      => 'required',
                'email'     => 'required|unique:admins,email,'.$id,
                'photo'     => 'mimes:jpg,jpeg,png',
	                ];

	        $validator = Validator::make($request->all(), $rules);
	        
	        if ($validator->fails()) {
	          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
	        }
	        //--- Validation Section Ends

	        $user = Admin::findOrFail($id);
			$data = $request->all();
			
	        if ($file = $request->file('photo'))
	        {
	            $name = time().$file->getClientOriginalName();
	            $file->move('assets/images/owners',$name);
	            if($user->photo != null)
	            {
	                if (file_exists(public_path().'/assets/images/owners/'.$user->photo)) {
	                    unlink(public_path().'/assets/images/owners/'.$user->photo);
	                }
	            }
	            $data['photo'] = $name;
			}

			if($request->is_preloaded == "")
			{
			   $data['is_preloaded'] = 0;
			}

			if($request->is_future_update == "")
			{
			   $data['is_future_update'] = 0;
			}

			if(!empty($request->password)){
				$data['password'] = bcrypt($request['password']);
			}else {
				$data['password'] = $user->password;
			}

	        $user->update($data);
	        $msg = 'Customer Information Updated Successfully.';
	        return response()->json($msg);   
	    }


    public function show($id){
        $data = Admin::findOrFail($id);
        return view('admin.owner.show',compact('data','username'));
    }

    //*** HELPER FUNCTION

    function delete_files($dir) { 
        foreach(glob($dir . '/*') as $file) { 
          if(is_dir($file)) $this->delete_files($file); else unlink($file); 
        } if(!rmdir($dir)) {
           return false;
          }
      }

	//*** GET Request
    public function destroy($id)
    {
        $data = Admin::find($id);

        // DELETING DIRECTORIES

        $dir = 'assets/'.$data->username.'/owner/images';
        $this->delete_files($dir);
        rmdir('assets/'.$data->username.'/owner');
        rmdir('assets/'.$data->username);

        // DELETING RELATED DATAS

        DB::table('conversations')->where('register_id',$data->id)->delete();
        DB::table('messages')->where('register_id',$data->id)->delete();

        DB::table('instructor_orders')->where('register_id',$data->id)->delete();
        DB::table('orders')->where('register_id',$data->id)->delete();

        DB::table('services')->where('register_id',$data->id)->delete();
        DB::table('subcategories')->where('register_id',$data->id)->delete();
        DB::table('categories')->where('register_id',$data->id)->delete();
        DB::table('courses')->where('register_id',$data->id)->delete();

        DB::table('blog_categories')->where('register_id',$data->id)->delete();
        DB::table('blogs')->where('register_id',$data->id)->delete();
        DB::table('pages')->where('register_id',$data->id)->delete();
        DB::table('email_templates')->where('register_id',$data->id)->delete();
        DB::table('pagesettings')->where('register_id',$data->id)->delete();
        DB::table('socialsettings')->where('register_id',$data->id)->delete();
        DB::table('generalsettings')->where('register_id',$data->id)->delete();
        DB::table('seotools')->where('register_id',$data->id)->delete();

        DB::table('users')->where('register_id',$data->id)->delete();

        if($data->languages->count() > 0)
        {
            foreach ($data->languages as $gal) {
                unlink(resource_path('lang/').$gal->file);
                $gal->delete();
            }
        }


        if (file_exists(public_path().'/assets/images/owners/'.$data->photo)) {
            unlink(public_path().'/assets/images/owners/'.$data->photo);
         }

        // MAIN DATA DELETE
        $data->delete();

        //--- Redirect Section     
        $msg = __('Owner Deleted Successfully.');
        return response()->json($msg);      
        //--- Redirect Section Ends   

    }


}

