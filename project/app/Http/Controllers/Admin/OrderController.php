<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Order;
use Carbon\Carbon;
use Datatables;


class OrderController extends Controller
{

    public $curr;

    public function __construct()
    {
        $this->middleware('auth:admin');
  
        $this->curr = \DB::table('currencies')->where('register_id',0)->where('is_default','=',1)->first();
  
    }
    //*** JSON Request
    public function datatables()
    {
         $datas = Order::where('register_id',0)->orderBy('id','desc')->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->editColumn('created_at', function(Order $data) {
                                $created_at = Carbon::parse($data->created_at)->format('M d Y');
                                return $created_at;
                            })
                            ->addColumn('price', function(Order $data) {
                                return $data->currency_sign.round(($data->pay_amount * $data->currency_value),2);
                            })

                            ->addColumn('details', function(Order $data) {
                                $details = '<a href="'. route('admin.purchase.details',$data->id) .'" class="btn btn-primary btn-sm btn-rounded">
                                <i class="fas fa-edit"></i> '.__("Purchase Details").'
                              </a>';
                                return $details;
                            })
                            ->rawColumns(['created_at','details'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

    //*** GET Request
    public function index()
    {
        return view('admin.order.index');
    }

    public function purchasedetails($id)
    {
        $order = Order::findOrfail($id);
        $cart = json_decode($order->cart, true);
        return view('admin.order.details',compact('order','cart'));
    }
}