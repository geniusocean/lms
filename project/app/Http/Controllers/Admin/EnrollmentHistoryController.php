<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables;
use App\Models\EnrolledCourse;
use App\Models\User;
use App\Models\Course;
use Carbon\Carbon;
use DB;
use Validator;

class EnrollmentHistoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

        //*** JSON Request
        public function datatables()
        {
             $datas = EnrolledCourse::where('register_id',0)->orderBy('id','desc')->get();
             //--- Integrating This Collection Into Datatables
             return Datatables::of($datas)
                                ->editColumn('user_id', function(EnrolledCourse $data) {
                                   $user = User::findOrFail($data->user_id);
                                   $user_name = $user->first_name.' '.$user->last_name;
                                   return $user_name;
                                })
                                ->editColumn('course_id', function(EnrolledCourse $data) {
                                    $course = Course::findOrFail($data->course_id);
                                    $course_name = $course->title;
                                    return $course_name;
                                 })
                                 ->editColumn('created_at', function(EnrolledCourse $data) {
                                    $created_at = Carbon::parse($data->created_at)->format('M d Y');
                                    return $created_at;
                                 })
                                ->addColumn('action', function(EnrolledCourse $data) {
                                    return '<div class="actions-btn"><button type="button" data-toggle="modal" data-target="#deleteModal"  data-href="' . route('admin-enroll-history-delete',$data->id) . '" class="btn btn-danger btn-sm btn-rounded">
                                    <i class="fas fa-trash"></i>
                                  </button></div>';
                                })
                                ->rawColumns(['user_id','course_id','created_at','action'])
                                ->toJson(); //--- Returning Json Data To Client Side
        }

        //*** GET Request
        public function index()
        {
            return view('admin.enrollment.index');
        }

        //*** GET Request Delete
    public function destroy($id)
    {
        $data = EnrolledCourse::findOrFail($id);
        $data->delete();
        //--- Redirect Section
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }
}
