<?php

namespace App\Http\Controllers\Admin;

use Datatables;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;

class SubCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables()
    {
         $datas = Subcategory::where('register_id',0)->orderBy('id','desc')->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->addColumn('category', function(Subcategory $data) {
                                return $data->category->name;
                            })
                            ->addColumn('status', function(Subcategory $data) {
                                $status      = $data->status == 1 ? __('Activated') : __('Deactivated');
                                $status_sign = $data->status == 1 ? 'success'   : 'danger';

                                return '<div class="btn-group mb-1">
                                <button type="button" class="btn btn-'.$status_sign.' btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  '.$status .'
                                </button>
                                <div class="dropdown-menu" x-placement="bottom-start">
                                  <a href="javascript:;" data-toggle="modal" data-target="#statusModal" class="dropdown-item" data-href="'. route('admin-cat-status',['id1' => $data->id, 'id2' => 1]).'">'.__("Activate").'</a>
                                  <a href="javascript:;" data-toggle="modal" data-target="#statusModal" class="dropdown-item" data-href="'. route('admin-cat-status',['id1' => $data->id, 'id2' => 0]).'">'.__("Deactivate").'</a>
                                </div>
                              </div>';
                            })
                            ->editColumn('preloaded', function(Subcategory $data) {
                              $status      = $data->preloaded == 1 ? __('Yes') : __('No');
                              $status_sign = $data->preloaded == 1 ? 'success'   : 'danger';

                              return '<div class="btn-group mb-1">
                              <button type="button" class="btn btn-'.$status_sign.' btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              '.$status .'
                              </button>
                              <div class="dropdown-menu" x-placement="bottom-start">
                              <a href="javascript:;" data-toggle="modal" data-target="#statusModal" class="dropdown-item" data-href="'. route('admin-preloaded-status',['subcategories','id1' => $data->id, 'id2' => 1]).'">'.__("Yes").'</a>
                              <a href="javascript:;" data-toggle="modal" data-target="#statusModal" class="dropdown-item" data-href="'. route('admin-preloaded-status',['subcategories','id1' => $data->id, 'id2' => 0]).'">'.__("No").'</a>
                              </div>
                          </div>';

                          })
                            ->addColumn('action', function(Subcategory $data) {
                                return '<div class="actions-btn"><a href="' . route('admin-subcat-edit',$data->id) . '" class="btn btn-primary btn-sm btn-rounded">
                                <i class="fas fa-edit"></i> '.__("Edit").'
                              </a><button type="button" data-toggle="modal" data-target="#deleteModal"  data-href="' . route('admin-subcat-delete',$data->id) . '" class="btn btn-danger btn-sm btn-rounded">
                                <i class="fas fa-trash"></i>
                              </button></div>';
                            })
                            ->rawColumns(['status','action','preloaded'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }


    //*** GET Request
    public function index()
    {
        return view('admin.subcategory.index');
    }

    //*** GET Request
    public function create()
    {
        return view('admin.subcategory.create');
    }

    //*** POST Request
    public function store(Request $request)
    {
        //--- Validation Section
    
      if(Subcategory::where('slug',$request->slug)->where('register_id',0)->exists()){
          return response()->json(array('errors' => [0 =>'This slug has already been taken.']));
      }

        //--- Validation Section Ends

        //--- Logic Section
        $data = new Subcategory();
        $input = $request->all();
        $data->fill($input)->save();
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'New Data Added Successfully.'.' '.'<a href="'.route('admin-subcat-index').'"> '.__('View Lists.').'</a>';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    //*** GET Request
    public function edit($id)
    {
        $data = Subcategory::findOrFail($id);
        return view('admin.subcategory.edit',compact('data'));
    }

    //*** POST Request
    public function update(Request $request, $id)
    {
        //--- Validation Section
        if(Subcategory::where('slug',$request->slug)->where('id','!=',$id)->where('register_id',0)->exists()){
          return response()->json(array('errors' => [0 =>'This slug has already been taken.']));
        }
  
        //--- Validation Section Ends

        //--- Logic Section
        $data = Subcategory::findOrFail($id);
        $input = $request->all();
        $data->update($input);
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'Data Updated Successfully.'.' '.'<a href="'.route('admin-subcat-index').'"> '.__('View Lists.').'</a>';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

      //*** GET Request Status
      public function status($id1,$id2)
      {
            $data = Subcategory::findOrFail($id1);
            $data->status = $id2;
            $data->update();

            //--- Redirect Section
            $msg = __('Status Updated Successfully.');
            return response()->json($msg);
            //--- Redirect Section Ends

      }

    //*** GET Request
    public function load($id)
    {
        $cat = Category::find($id);
        return view('admin.subcategory.load.subcategory',compact('cat'));
    }

    //*** GET Request Delete
    public function destroy($id)
    {
        $data = Subcategory::findOrFail($id);
        $courses = $data->category->courses;
        
        if($courses->count()>0){
          foreach($courses as $course){
              if($course->ratings->count()>0){
                foreach($course->ratings as $rating){
                    $rating->delete();
                }
            }

            if($data->wishlist->count()>0){
              foreach($data->wishlist as $wishlist){
                $wishlist->delete();
              }
            }

            if($course->enrolled_courses->count()>0){
                foreach($course->enrolled_courses as $enroll){
                    $enroll->delete();
                }
            }
            $course->delete();
          }
        }

        $data->delete();
        //--- Redirect Section
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }
}
