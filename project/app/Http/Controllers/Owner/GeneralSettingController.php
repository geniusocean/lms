<?php

namespace App\Http\Controllers\Owner;
use App\Models\Generalsetting;
use Artisan;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Currency;
use App\Models\Admin;
use App\Http\Controllers\Controller;

use Validator;

class GeneralSettingController extends Controller
{

    protected $rules =
    [
        'logo'              => 'mimes:jpeg,jpg,png,svg',
        'favicon'           => 'mimes:jpeg,jpg,png,svg',
        'loader'            => 'mimes:gif',
        'admin_loader'      => 'mimes:gif',
        'affilate_banner'   => 'mimes:jpeg,jpg,png,svg',
        'error_banner'      => 'mimes:jpeg,jpg,png,svg',
        'popup_background'  => 'mimes:jpeg,jpg,png,svg',
        'invoice_logo'      => 'mimes:jpeg,jpg,png,svg',
        'breadcumb_banner'  => 'mimes:jpeg,jpg,png,svg',
        'footer_logo'       => 'mimes:jpeg,jpg,png,svg',
    ];

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    private function setEnv($name=null,$key, $value,$prev)
    {
        file_put_contents(app()->environmentFilePath(), str_replace(
            $key . '=' . $prev,
            $key . '=' . $value,
            file_get_contents(app()->environmentFilePath())
        ));
    }

    // Genereal Settings All post requests will be done in this method
    public function generalupdate(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        //--- Validation Section
        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        else {
        $input = $request->all();
        $data = Generalsetting::where('register_id',$owner_id)->first();
            if ($file = $request->file('logo'))
            {
                $name = time().str_replace(' ', '', $file->getClientOriginalName());
                $data->ownerupload($name,$file,$data->logo,$owner->username);
                $input['logo'] = $name;
            }
            if ($file = $request->file('favicon'))
            {
                $name = time().str_replace(' ', '', $file->getClientOriginalName());
                $data->ownerupload($name,$file,$data->favicon,$owner->username);
                $input['favicon'] = $name;
            }
            if ($file = $request->file('loader'))
            {
                $name = time().str_replace(' ', '', $file->getClientOriginalName());
                $data->ownerupload($name,$file,$data->loader,$owner->username);
                $input['loader'] = $name;
            }
            if ($file = $request->file('admin_loader'))
            {
                $name = time().str_replace(' ', '', $file->getClientOriginalName());
                $data->ownerupload($name,$file,$data->admin_loader,$owner->username);
                $input['admin_loader'] = $name;
            }
            if ($file = $request->file('affilate_banner'))
            {
                $name = time().str_replace(' ', '', $file->getClientOriginalName());
                $data->ownerupload($name,$file,$data->affilate_banner,$owner->username);
                $input['affilate_banner'] = $name;
            }
             if ($file = $request->file('error_banner'))
            {
                $name = time().str_replace(' ', '', $file->getClientOriginalName());
                $data->ownerupload($name,$file,$data->error_banner,$owner->username);
                $input['error_banner'] = $name;
            }
            if ($file = $request->file('popup_background'))
            {
                $name = time().str_replace(' ', '', $file->getClientOriginalName());
                $data->ownerupload($name,$file,$data->popup_background,$owner->username);
                $input['popup_background'] = $name;
            }
            if ($file = $request->file('invoice_logo'))
            {
                $name = time().str_replace(' ', '', $file->getClientOriginalName());
                $data->ownerupload($name,$file,$data->invoice_logo,$owner->username);
                $input['invoice_logo'] = $name;
            }
            if ($file = $request->file('breadcumb_banner'))
            {
                $name = time().str_replace(' ', '', $file->getClientOriginalName());
                $data->ownerupload($name,$file,$data->breadcumb_banner,$owner->username);
                $input['breadcumb_banner'] = $name;
            }

            if ($file = $request->file('footer_logo'))
            {
                $name = time().str_replace(' ', '', $file->getClientOriginalName());
                $data->ownerupload($name,$file,$data->footer_logo,$owner->username);
                $input['footer_logo'] = $name;
            }

        $data->update($input);
        //--- Logic Section Ends


        Artisan::call('cache:clear');
        Artisan::call('config:clear');
        Artisan::call('route:clear');
        Artisan::call('view:clear');

        //--- Redirect Section
        $msg = 'Data Updated Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
        }
    }

    public function generalupdatepayment(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        //--- Validation Section
        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        else {
        $input = $request->all();
        $curr = Currency::where('is_default','=',1)->where('register_id',$owner_id)->first();
        $data = Generalsetting::where('register_id',$owner_id)->first();
        $prev = $data->molly_key;

        if ($request->vendor_ship_info == ""){
            $input['vendor_ship_info'] = 0;
        }

        if ($request->instamojo_sandbox == ""){
            $input['instamojo_sandbox'] = 0;
        }

        if ($request->paypal_mode == ""){
            $input['paypal_mode'] = 'live';
        }
        else {
            $input['paypal_mode'] = 'sandbox';
        }

        if ($request->paytm_mode == ""){
            $input['paytm_mode'] = 'live';
        }
        else {
            $input['paytm_mode'] = 'sandbox';
        }
        $input['fixed_commission'] = $input['fixed_commission'] / $curr->value;
        $data->update($input);


        $this->setEnv('MOLLIE_KEY',$data->molly_key,$prev);
        // Set Molly ENV

        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'Data Updated Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
        }
    }

    public function logo($name=null)
    {
        return view('owner.generalsetting.logo');
    }

    public function breadcumb($name=null)
    {
        return view('owner.generalsetting.breadcumb');
    }

    public function fav($name=null)
    {
        return view('owner.generalsetting.favicon');
    }

     public function load($name=null)
    {
        return view('owner.generalsetting.loader');
    }

     public function contents($name=null)
    {
        return view('owner.generalsetting.websitecontent');
    }

     public function header($name=null)
    {
        return view('owner.generalsetting.header');
    }

     public function footer($name=null)
    {
        return view('owner.generalsetting.footer');
    }

    public function paymentsinfo($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        $curr = Currency::where('is_default','=',1)->where('register_id',$owner_id)->first();
        return view('owner.generalsetting.paymentsinfo',compact('curr'));
    }

    public function affilate($name=null)
    {
        return view('owner.generalsetting.affilate');
    }

    public function errorbanner($name=null)
    {
        return view('owner.generalsetting.error_banner');
    }

    public function popup($name=null)
    {
        return view('owner.generalsetting.popup');
    }

    public function maintain($name=null)
    {
        return view('owner.generalsetting.maintain');
    }

    // Status Change Method -> GET Request
    public function status($name=null,$field,$value)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $prev = '';
        $data = Generalsetting::where('register_id',$owner_id)->first();
        if($field == 'is_debug'){
            $prev = $data->is_debug == 1 ? 'true':'false';
        }
        $data[$field] = $value;
        $data->update();
        if($field == 'is_debug'){
            $now = $data->is_debug == 1 ? 'true':'false';
            $this->setEnv('APP_DEBUG',$now,$prev);
        }
        //--- Redirect Section
        $msg = __('Status Updated Successfully.');
        return response()->json($msg);
        //--- Redirect Section Ends

    }

}
