<?php

namespace App\Http\Controllers\Owner;

use Datatables;
use App\Models\Currency;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;

class CurrencyController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

         $datas = Currency::orderBy('id')->where('register_id',$owner_id)->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->addColumn('action', function(Currency $data) {
                                $owner = $data->owner->username;
                                $delete = $data->is_default == 1 ? '':'<a href="javascript:;" data-href="' . route('owner-currency-delete',[$owner,$data->id]) . '" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger btn-sm btn-rounded"><i class="fas fa-trash-alt"></i></a>';
                                $default = $data->is_default == 1 ? '<a href="javascript:;" class="status btn btn-primary btn-sm btn-rounded"><i class="fa fa-check"></i> Default</a>' : '<a class="status btn btn-primary btn-sm btn-rounded" href="' . route('owner-currency-status',['name'=>$owner,'id1'=>$data->id,'id2'=>1]) . '">'.__('Set Default').'</a>';
                                return '<div class="actions-btn"><a href="' . route('owner-currency-edit',[$owner,$data->id]) . '" class="btn btn-primary btn-sm btn-rounded"> <i class="fas fa-edit"></i>Edit</a>'.$delete.$default.'</div>';
                            })
                            ->rawColumns(['action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

    //*** GET Request
    public function index($name=null)
    {
        return view('owner.currency.index');
    }

    //*** GET Request
    public function create($name=null)
    {
        return view('owner.currency.create');
    }

    //*** POST Request
    public function store(Request $request,$name=null)
    {   $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;


        //--- Logic Section
        $data = new Currency();
        $input = $request->all();
        $input['register_id'] = $owner_id;
        $data->fill($input)->save();
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = __('New Data Added Successfully.');
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    //*** GET Request
    public function edit($name=null,$id)
    {
        $data = Currency::findOrFail($id);
        return view('owner.currency.edit',compact('data'));
    }

    //*** POST Request
    public function update(Request $request,$name=null, $id)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        //--- Logic Section
        $data = Currency::findOrFail($id);
        $input = $request->all();
        $data->update($input);
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = __('Data Updated Successfully.');
        return response()->json($msg);
        //--- Redirect Section Ends
    }

      public function status($name=null,$id1,$id2)
        {
           
            $owner = Admin::where('username',$name)->where('role','Owner')->first();
            $data = Currency::findOrFail($id1);
            $data->is_default = $id2;
            $data->update();
            $data = Currency::where('id','!=',$id1)->where('register_id',$owner->id)->update(['is_default' => 0]);

            //--- Redirect Section
            $msg = __('Data Updated Successfully.');
            return redirect()->route('owner-currency-index',$owner->username)->with('success',$msg);
            //--- Redirect Section Ends
        }

    //*** GET Request Delete
    public function destroy($name=null,$id)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();


        if(Currency::where('register_id',$owner->id)->count() == 1)
        {
        return __("You can't remove all languages.");
        }

        $data = Currency::findOrFail($id);
        $data->delete();
       
        $curr = Currency::where('register_id','=',$owner->id)->first();
        $curr->update([
            'is_default' => 1
        ]);
        
        //--- Redirect Section
        $msg = __('Data Deleted Successfully.');
        return response()->json($msg);
        //--- Redirect Section Ends
    }

}
