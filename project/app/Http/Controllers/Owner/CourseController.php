<?php

namespace App\Http\Controllers\Owner;

use Validator;
use Datatables;
use App\Models\Course;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{

  public $curr;

  public function __construct()
  {
      $this->middleware('auth:web');

      $this->curr = \DB::table('currencies')->where('register_id',0)->where('is_default','=',1)->first();

  }

    //*** JSON Request
    public function datatables($name=null)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $owner_id = $owner->id;

         $datas = Course::orderBy('id','desc')->where('register_id',$owner_id)->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)

                            ->editColumn('price', function(Course $data) {
                                $price = $data->price * $this->curr->value;
                                $show_price = $price == 0 ? __('Free') : $this->curr->sign.$price;
                                return $show_price;
                            })

                            ->addColumn('status', function(Course $data) {
                                $owner = $data->owner->username;
                                $status      = $data->status == 1 ? __('Activated') : __('Deactivated');
                                $status_sign = $data->status == 1 ? 'success'   : 'danger';

                                return '<div class="btn-group mb-1">
                                <button type="button" class="btn btn-'.$status_sign.' btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  '.$status .'
                                </button>
                                <div class="dropdown-menu" x-placement="bottom-start">
                                  <a href="javascript:;" data-toggle="modal" data-target="#statusModal" class="dropdown-item" data-href="'. route('owner-course-status',['name'=>$owner,'id1' => $data->id, 'id2' => 1]).'">'.__("Activate").'</a>
                                  <a href="javascript:;" data-toggle="modal" data-target="#statusModal" class="dropdown-item" data-href="'. route('owner-course-status',['name'=>$owner,'id1' => $data->id, 'id2' => 0]).'">'.__("Deactivate").'</a>
                                </div>
                              </div>';
                            })
                            ->addColumn('action', function(Course $data) {
                              $owner = $data->owner->username;
                                return '<div class="actions-btn">
                              <div class="dropdown">
                              <a class="btn btn-primary btn-sm btn-rounded dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> '.__("Options").' </a>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                              <a href="' . route('owner-course-curriculam',[$owner,$data->id]) . '" class="dropdown-item options">
                              <i class="fas fa-cog"></i> '.__("Curriculam").'
                            </a>
                                <a href="' . route('owner-course-edit',[$owner,$data->id]) . '" class="dropdown-item options">
                                <i class="fas fa-edit"></i> '.__("Edit").'
                              </a><button type="button" data-toggle="modal" data-target="#deleteModal"  data-href="' . route('owner-course-delete',[$owner,$data->id]) . '" class="dropdown-item options">
                                <i class="fas fa-trash"></i> '.__("Delete").'
                              </button>

                              </div>
                            </div>
                                </div>';
                            })
                            ->rawColumns(['status','action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

    //*** GET Request
    public function index($name=null)
    {
        return view('owner.course.index');
    }

    //*** GET Request
    public function create($name=null)
    {
        return view('owner.course.create');
    }

    //*** POST Request
    public function store(Request $request,$name=null)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $owner_id = $owner->id;

           if(Course::where('title',$request->title)->where('register_id',$owner_id)->exists()){
        return response()->json(array('errors' => [0 =>'This title has already been taken.']));
      }
    
        $rules = [
            'photo' => 'mimes:jpeg,jpg,png,svg',
            ];
        $customs = [
            'photo.mimes' => __('Thumbnail Type is Invalid.'),
            ];
        $validator = Validator::make($request->all(), $rules, $customs);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section

        $data = new Course();

        $input = $request->all();
        $input['register_id'] = $owner_id;
        if ($file = $request->file('photo'))
         {
            $name = time().str_replace(' ', '', $file->getClientOriginalName());
            $file->move('assets/'.$owner->username.'/owner/images/courses',$name);
            $input['photo'] = $name;
        }
        if ($request->is_top){
            $input['is_top'] = 1;
        }
        if ($request->is_free){
            $input['is_free'] = 1;
            $input['price'] = 0;
            $input['discount_price'] = 0;
        }else{
            $input['is_free'] = 0;
            $input['price'] = ($input['price'] / $this->curr->value);
            $input['discount_price'] = ($input['discount_price'] / $this->curr->value);
        }

        $input['requirements'] = implode(',,,', $request->requirements);
        $input['outcomes'] = implode(',,,', $request->outcomes);
        $input['include_icon'] = implode(',,,', $request->include_icon);
        $input['include_text'] = implode(',,,', $request->include_text);
        $input['status'] = 1;
        if(!empty($request->meta_keywords)){
            $input['meta_keywords'] = str_replace(["value", "{", "}", "[","]",":","\""], '', $request->meta_keywords);
        }

        $input['slug'] = str_replace(" ","-",$input['title']);

        $data->fill($input)->save();

        //--- Logic Section Ends

        //--- Redirect Section
        $msg = __('New Data Added Successfully.').' '.'<a href="'.route('owner-course-index',$owner->username).'"> '.__('View Lists.').'</a>';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    //*** GET Request
    public function edit($name=null,$id)
    {
        $data = Course::findOrFail($id);
        return view('owner.course.edit',compact('data'));
    }

    //*** POST Request
    public function update(Request $request,$name=null, $id)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $owner_id = $owner->id;

      if(Course::where('title',$request->title)->where('id','!=',$id)->where('register_id',$owner_id)->exists()){
        return response()->json(array('errors' => [0 =>'This title has already been taken.']));
      }
        //--- Validation Section
        $rules = [
        	'photo' => 'mimes:jpeg,jpg,png,svg',
        		];
        $customs = [
        	'photo.mimes' => __('Icon Type is Invalid.'),
        		];
        $validator = Validator::make($request->all(), $rules, $customs);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = Course::findOrFail($id);

        $input = $request->all();
        if ($file = $request->file('photo'))
         {
            if(file_exists(base_path('../assets/'.$owner->username.'/owner/images/courses/'.$data->photo))){
                unlink(base_path('../assets/'.$owner->username.'/owner/images/courses/'.$data->photo));
            }

            $name = time().str_replace(' ', '', $file->getClientOriginalName());
            $file->move('assets/'.$owner->username.'/owner/images/courses',$name);
            $input['photo'] = $name;
        }
        if ($request->is_top){
            $input['is_top'] = 1;
        }
        if ($request->is_free){
            $input['is_free'] = 1;
            $input['price'] = 0;
            $input['discount_price'] = 0;
        }else{
            $input['is_free'] = 0;
            $input['price'] = ($input['price'] / $this->curr->value);
            $input['discount_price'] = ($input['discount_price'] / $this->curr->value);
        }

        $input['requirements'] = implode(',,,', $request->requirements);
        $input['outcomes'] = implode(',,,', $request->outcomes);
        $input['include_icon'] = implode(',,,', $request->include_icon);
        $input['include_text'] = implode(',,,', $request->include_text);
        if(!empty($request->meta_keywords)){
            $input['meta_keywords'] = str_replace(["value", "{", "}", "[","]",":","\""], '', $request->meta_keywords);
        }

        $input['slug'] = str_replace(" ","-",$input['title']);

        $data->update($input);
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = __('Data Updated Successfully.').' '.'<a href="'.route('owner-course-index',$name=null).'">'.__('View Lists.').'</a>';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

      //*** GET Request Status
      public function status($name=null,$id1,$id2)
      {
        $data = Course::findOrFail($id1);
        $data->status = $id2;
        $data->update();

        //--- Redirect Section
        $msg = __('Status Updated Successfully.');
        return response()->json($msg);
        //--- Redirect Section Ends

      }


    //*** GET Request Delete
    public function destroy($name=null,$id)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $data = Course::findOrFail($id);

        $courses = $data->courses;
    
        if($courses->count()>0){
          foreach($data->courses as $course){

            if($course->ratings->count()>0){
                foreach($course->ratings as $rating){
                    $rating->delete();
                }
            }

            if($data->wishlist->count()>0){
              foreach($data->wishlist as $wishlist){
                $wishlist->delete();
              }
            }

            if($course->enrolled_courses->count()>0){
                foreach($course->enrolled_courses as $enroll){
                    $enroll->delete();
                }
            }
            $course->delete();
          }
        }

        if($data->enrolled_courses->count()>0){
          foreach($data->enrolled_courses as $enroll){
            $enroll->delete();
          }
        }

        if($data->instructororders->count()>0){
          foreach($data->instructororders as $order){
            $order->delete();
          }
        }

        //If Photo Doesn't Exist
        if($data->photo == null){
            $data->delete();
            //--- Redirect Section
            $msg = __('Data Deleted Successfully.');
            return response()->json($msg);
            //--- Redirect Section Ends
        }
        //If Photo Exist
        if(file_exists(base_path('../assets/'.$owner->username.'/owner/images/courses/'.$data->photo))){
            unlink(base_path('../assets/'.$owner->username.'/owner/images/courses/'.$data->photo));
          }

        $data->delete();
        //--- Redirect Section
        $msg = __('Data Deleted Successfully.');
        return response()->json($msg);
        //--- Redirect Section Ends
    }
}
