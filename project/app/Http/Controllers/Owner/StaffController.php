<?php

namespace App\Http\Controllers\Owner;

use Datatables;
use App\Models\Admin;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;


class StaffController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        $datas = Admin::where('parent_id',$owner_id)->orderBy('id')->get();

         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->addColumn('role', function(Admin $data) {
                                $role = $data->role_id == 0 ? 'No Role' : $data->role->name;
                                return $role;
                            }) 
                            ->addColumn('action', function(Admin $data) {
                                $owner = $data->parent->username;
                                $delete ='<a href="javascript:;" data-href="' . route('owner-staff-delete',[$owner,$data->id]) . '" data-toggle="modal" data-target="#confirm-delete" class="delete"><i class="fas fa-trash-alt"></i></a>';
                                return '<div class="action-list"><a data-href="' . route('owner-staff-show',[$owner,$data->id]) . '" class="view details-width" data-toggle="modal" data-target="#modal1"> <i class="fas fa-eye"></i>Details</a><a data-href="' . route('owner-staff-edit',[$owner,$data->id]) . '" class="edit" data-toggle="modal" data-target="#modal1"> <i class="fas fa-edit"></i>Edit</a>'.$delete.'</div>';
                            }) 
                            ->rawColumns(['action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

    //*** GET Request
  	public function index($name=null)
    {
        return view('owner.staff.index');
    }

    //*** GET Request
    public function create($name=null)
    {
        return view('owner.staff.create');
    }

    //*** POST Request
    public function store(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        if(Admin::where('email',$request->email)->where('parent_id',$owner_id)->exists()){
            return response()->json(array('errors' => [0 =>'This email has already been taken.']));
        }
        if(Admin::where('username',$request->username)->where('parent_id',$owner_id)->exists()){
            return response()->json(array('errors' => [0 =>'This username has already been taken.']));
        }

        //--- Validation Section
        $rules = [
               'photo'      => 'required|mimes:jpeg,jpg,png,svg',
                ];

        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = new Admin();
        $input = $request->all();
        $input['parent_id'] = $owner_id;

        if ($file = $request->file('photo')) 
         {      
            $name = time().str_replace(' ', '', $file->getClientOriginalName());
            $file->move('assets/'.$owner->username.'/owner/images/admins',$name);           
            $input['photo'] = $name;
        } 
        $input['role'] = 'Staff';
        $input['password'] = bcrypt($request['password']);
        $data->fill($input)->save();
        //--- Logic Section Ends

        //--- Redirect Section        
        $msg = 'New Data Added Successfully.';
        return response()->json($msg);      
        //--- Redirect Section Ends    
    }


    public function edit($name=null,$id)
    {
        $data = Admin::findOrFail($id);  
        return view('owner.staff.edit',compact('data'));
    }

    public function update(Request $request,$name=null,$id)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        if(Admin::where('email',$request->email)->where('id','!=',$id)->where('parent_id',$owner_id)->exists()){
            return response()->json(array('errors' => [0 =>'This email has already been taken.']));
        }
        if(Admin::where('username',$request->username)->where('id','!=',$id)->where('parent_id',$owner_id)->exists()){
            return response()->json(array('errors' => [0 =>'This username has already been taken.']));
        }

        //--- Validation Section
        if($id != Auth::guard('admin')->user()->id)
        {
            $rules =
            [
                'photo' => 'mimes:jpeg,jpg,png,svg',
            ];

            $validator = Validator::make($request->all(), $rules);
            
            if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
            }
            //--- Validation Section Ends
            $input = $request->all();  
            $data = Admin::findOrFail($id);        
                if ($file = $request->file('photo')) 
                {              
                    $name = time().str_replace(' ', '', $file->getClientOriginalName());
                    $file->move('assets/'.$owner->username.'/owner/images/admins/',$name);
                    if($data->photo != null)
                    {
                        if (file_exists(public_path().'/assets/'.$owner->username.'/owner/images/admins/'.$data->photo)) {
                            unlink(public_path().'/assets/'.$owner->username.'/owner/images/admins/'.$data->photo);
                        }
                    }            
                $input['photo'] = $name;
                } 
            if($request->password == ''){
                $input['password'] = $data->password;
            }
            else{
                $input['password'] = Hash::make($request->password);
            }
            $data->update($input);
            $msg = 'Data Updated Successfully.';
            return response()->json($msg);
        }
        else{
            $msg = 'You can not change your role.';
            return response()->json($msg);            
        }
 
    }

    //*** GET Request
    public function show($name=null,$id)
    {
        $data = Admin::findOrFail($id);
        return view('owner.staff.show',compact('data'));
    }

    //*** GET Request Delete
    public function destroy($name=null,$id)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

    	if($id == 1)
    	{
        return "You don't have access to remove this admin";
    	}
        $data = Admin::findOrFail($id);
        //If Photo Doesn't Exist
        if($data->photo == null){
            $data->delete();
            //--- Redirect Section     
            $msg = 'Data Deleted Successfully.';
            return response()->json($msg);      
            //--- Redirect Section Ends     
        }
        //If Photo Exist
        if (file_exists(public_path().'/assets/'.$owner->username.'/owner/images/admins/'.$data->photo)) {
            unlink(public_path().'/assets/'.$owner->username.'/owner/images/admins/'.$data->photo);
        }
        $data->delete();
        //--- Redirect Section     
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);      
        //--- Redirect Section Ends    
    }
}
