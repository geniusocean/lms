<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use Datatables;
use App\Models\EnrolledCourse;
use App\Models\User;
use App\Models\Admin;
use App\Models\Course;
use Carbon\Carbon;


class EnrollmentHistoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

        //*** JSON Request
        public function datatables($name=null)
        {
            $owner = Admin::where('username',$name)->where('role','Owner')->first();
            $owner_id = $owner->id;
            $courses = Course::where('register_id',$owner_id)->pluck('id');
            $datas = EnrolledCourse::whereIn('course_id',$courses)->orderBy('id','desc')->get();
             //--- Integrating This Collection Into Datatables
             return Datatables::of($datas)
                                ->editColumn('user_id', function(EnrolledCourse $data) {
                                   $user = User::findOrFail($data->user_id);
                                   $user_name = $user->first_name.' '.$user->last_name;
                                   return $user_name;
                                })
                                ->editColumn('course_id', function(EnrolledCourse $data) {
                                    $course = Course::findOrFail($data->course_id);
                                    $course_name = $course->title;
                                    return $course_name;
                                 })
                                 ->editColumn('created_at', function(EnrolledCourse $data) {
                                    $created_at = Carbon::parse($data->created_at)->format('M d Y');
                                    return $created_at;
                                 })
                                ->addColumn('action', function(EnrolledCourse $data) {
                                    $owner = $data->owner->username;
                                    return '<div class="actions-btn"><button type="button" data-toggle="modal" data-target="#deleteModal"  data-href="' . route('admin-enroll-history-delete',[$owner,$data->id]) . '" class="btn btn-danger btn-sm btn-rounded">
                                    <i class="fas fa-trash"></i>
                                  </button></div>';
                                })
                                ->rawColumns(['user_id','course_id','created_at','action'])
                                ->toJson(); //--- Returning Json Data To Client Side
        }

        //*** GET Request
        public function index($name=null)
        {
            return view('owner.enrollment.index');
        }

        //*** GET Request Delete
    public function destroy($name=null,$id)
    {
        $data = EnrolledCourse::findOrFail($id);
        $data->delete();
        //--- Redirect Section
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }
}
