<?php

namespace App\Http\Controllers\Owner;

use App\Models\Lesson;
use App\Models\Admin;
use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** POST Request
    public function store(Request $request,$name=null)
    {
        //--- Logic Section

        $input = $request->all();
        $options = $input['options'];
        $answers = $input['answers'];

        $input['options'] = json_encode($options);
        $input['answers'] = json_encode($answers);

        $data = Lesson::find($input['quiz_lesson_id']);

        $input['pos'] = $data->questions()->count() + 1;

        $ck = $data->questions()->where('title',$input['title'])->count();
        if($ck == 1){
            return response()->json(array('errors' => [ 0 => __('This Title Already Exists.') ]));
        }else{
            $data->questions()->create($input);
        }

        //--- Logic Section Ends

        //--- Redirect Section
        $msg = __('Question Created Successfully.');
        return response()->json($msg);
        //--- Redirect Section Ends

    }

   //*** GET Request
   public function edit($name=null,$id)
   {
       //--- Logic Section

       $data = Question::find($id);
       $options = json_decode($data->options,true);
       $answers = json_decode($data->answers,true);


       //--- Logic Section Ends

       //--- Redirect Section

        return view('owner.course.load.question_edit',compact('data','options','answers'));

       //--- Redirect Section Ends

   }

    //*** POST Request
    public function update(Request $request, $name=null, $id)
    {
        //--- Logic Section

        $data = Question::find($id);
        $lsn = Lesson::find($data->lesson_id);

        $input = $request->all();
        $options = $input['options'];
        $answers = $input['answers'];

        $input['options'] = json_encode($options);
        $input['answers'] = json_encode($answers);


        $ck = $lsn->questions()->where('id','!=',$data->id)->where('title',$input['title'])->count();
        if($ck == 1){
            return response()->json(array('errors' => [ 0 => __('This Title Already Exists.') ]));
        }else{
            $data->update($input);
        }

        //--- Logic Section Ends

        //--- Redirect Section
        $msg = __('Question Updated Successfully.');
        return response()->json($msg);
        //--- Redirect Section Ends

    }



    //*** POST Request
    public function SortUpdate(Request $request, $name=null, $id)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        //--- Logic Section

        $data = Lesson::find($id);
        $input = $request->all();
        $i = 0;
        $pos = $input['pos'];

        foreach($data->questions()->oldest('pos')->get() as $lesson){
            $lesson->pos = $pos[$i];
            $lesson->update();
            $i++;
        }

        //--- Logic Section Ends

        //--- Redirect Section
        $msg = __('Question Sorted Successfully.');
        return redirect()->route('owner-course-curriculam',[$owner->username,$data->section->course_id])->with('success',$msg);
        //--- Redirect Section Ends

    }


   //*** GET Request
   public function delete($name=null,$id)
   {
    $owner = Admin::where('username',$name)->where('role','Owner')->first();
       //--- Logic Section

       $data = Question::find($id);
       $data->delete();

       //--- Logic Section Ends

       //--- Redirect Section

        $msg = __('Question Deleted Successfully.');

       return redirect()->route('owner-course-curriculam',[$name->username,$data->lesson->section->course_id])->with('success',$msg);

       //--- Redirect Section Ends

   }


}
