<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\Admin;
use DB;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function show_notf($name=null){
      return view('owner-load.notification');
    }

    public function read($name=null){
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $datas = Notification::where('register_id',$owner->id)->where('is_read','=',0)->get();
      if($datas->count() > 0){
        foreach($datas as $data){
          $data->is_read = 1;
          $data->update();
        }
      } 
    }

    public function notf_clear($name=null)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      DB::table('notifications')->where('register_id',$owner->id)->delete();
    } 

}