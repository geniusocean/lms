<?php

namespace App\Http\Controllers\Owner;

use Datatables;
use App\Models\Admin;
use App\Models\BlogCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class BlogCategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

         $datas = BlogCategory::orderBy('id','desc')->where('register_id',$owner_id)->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->addColumn('action', function(BlogCategory $data) {
                                $owner = $data->owner->username;
                                return '<div class="actions-btn"><a href="' . route('owner-cblog-edit',[$owner,$data->id]) . '" class="btn btn-primary btn-sm btn-rounded">
                                <i class="fas fa-edit"></i> '.__("Edit").'
                              </a><button type="button" data-toggle="modal" data-target="#deleteModal"  data-href="' . route('owner-cblog-delete',[$owner,$data->id]) . '" class="btn btn-danger btn-sm btn-rounded">
                                <i class="fas fa-trash"></i>
                              </button></div>';
                            })
                            ->toJson();//--- Returning Json Data To Client Side
    }

    //*** GET Request
    public function index($name=null)
    {
        return view('owner.cblog.index');
    }

    //*** GET Request
    public function create($name=null)
    {
        return view('owner.cblog.create');
    }

    //*** POST Request
    public function store(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        //--- Validation Section


        if(BlogCategory::where('slug',$request->slug)->where('register_id',$owner_id)->exists()){
            return response()->json(array('errors' => [0 =>'This slug has already been taken.']));
        }
        if(BlogCategory::where('name',$request->name)->where('register_id',$owner_id)->exists()){
            return response()->json(array('errors' => [0 =>'This name has already been taken.']));
        }

        $rules = [
               'name' => 'unique:blog_categories'
                ];
        $customs = [
               'name.unique' => 'This name has already been taken.'
                   ];
        $validator = Validator::make($request->all(), $rules, $customs);
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = new BlogCategory;
        $input = $request->all();
        $input['register_id'] = $owner_id;
        $data->fill($input)->save();
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'New Data Added Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    //*** GET Request
    public function edit($name=null,$id)
    {
        $data = BlogCategory::findOrFail($id);
        return view('owner.cblog.edit',compact('data'));
    }

    //*** POST Request
    public function update(Request $request,$name=null, $id)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        //--- Validation Section

        if(BlogCategory::where('slug',$request->slug)->where('id','!=',$id)->where('register_id',$owner_id)->exists()){
            return response()->json(array('errors' => [0 =>'This slug has already been taken.']));
        }
        if(BlogCategory::where('name',$request->name)->where('id','!=',$id)->where('register_id',$owner_id)->exists()){
            return response()->json(array('errors' => [0 =>'This name has already been taken.']));
        }

        $rules = [
               'name' => 'unique:blog_categories,name,'.$id
                ];
        $customs = [
               'name.unique' => 'This name has already been taken.'
                   ];
        $validator = Validator::make($request->all(), $rules, $customs);
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = BlogCategory::findOrFail($id);
        $input = $request->all();
        $data->update($input);
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'Data Updated Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends

    }

    //*** GET Request
    public function destroy($name=null,$id)
    {
        $data = BlogCategory::findOrFail($id);

        //--- Check If there any blogs available, If Available Then Delete it
        if($data->blogs->count() > 0)
        {
            foreach ($data->blogs as $element) {
                $element->delete();
            }
        }
        $data->delete();
        //--- Redirect Section
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }
}
