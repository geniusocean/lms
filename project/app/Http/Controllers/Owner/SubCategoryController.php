<?php

namespace App\Http\Controllers\Owner;

use Datatables;
use App\Models\Category;
use App\Models\Admin;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;

class SubCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables($name=null)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $owner_id = $owner->id;

         $datas = Subcategory::orderBy('id','desc')->where('register_id',$owner_id)->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->addColumn('category', function(Subcategory $data) {
                                return $data->category->name;
                            })
                            ->addColumn('status', function(Subcategory $data) {
                              $owner = $data->owner->username;
                                $status      = $data->status == 1 ? __('Activated') : __('Deactivated');
                                $status_sign = $data->status == 1 ? 'success'   : 'danger';

                                return '<div class="btn-group mb-1">
                                <button type="button" class="btn btn-'.$status_sign.' btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  '.$status .'
                                </button>
                                <div class="dropdown-menu" x-placement="bottom-start">
                                  <a href="javascript:;" data-toggle="modal" data-target="#statusModal" class="dropdown-item" data-href="'. route('owner-cat-status',['name'=>$owner,'id1' => $data->id, 'id2' => 1]).'">'.__("Activate").'</a>
                                  <a href="javascript:;" data-toggle="modal" data-target="#statusModal" class="dropdown-item" data-href="'. route('owner-cat-status',['name'=>$owner,'id1' => $data->id, 'id2' => 0]).'">'.__("Deactivate").'</a>
                                </div>
                              </div>';
                            })
                            ->addColumn('action', function(Subcategory $data) {
                              $owner = $data->owner->username;
                                return '<div class="actions-btn"><a href="' . route('owner-subcat-edit',[$owner,$data->id]) . '" class="btn btn-primary btn-sm btn-rounded">
                                <i class="fas fa-edit"></i> '.__("Edit").'
                              </a><button type="button" data-toggle="modal" data-target="#deleteModal"  data-href="' . route('owner-subcat-delete',[$owner,$data->id]) . '" class="btn btn-danger btn-sm btn-rounded">
                                <i class="fas fa-trash"></i>
                              </button></div>';
                            })
                            ->rawColumns(['status','action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }


    //*** GET Request
    public function index($name=null)
    {
        return view('owner.subcategory.index');
    }

    //*** GET Request
    public function create($name=null)
    {
        return view('owner.subcategory.create');
    }

    //*** POST Request
    public function store(Request $request,$name=null)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $owner_id = $owner->id;

      if(Subcategory::where('slug',$request->slug)->where('register_id',$owner_id)->exists()){
        return response()->json(array('errors' => [0 =>'This slug has already been taken.']));
      }

        //--- Logic Section
        $data = new Subcategory();
        $input = $request->all();
        $input['register_id'] = $owner_id;
        $data->fill($input)->save();
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'New Data Added Successfully.'.' '.'<a href="'.route('owner-subcat-index',$owner->username).'"> '.__('View Lists.').'</a>';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    //*** GET Request
    public function edit($name=null,$id)
    {
        $data = Subcategory::findOrFail($id);
        return view('owner.subcategory.edit',compact('data'));
    }

    //*** POST Request
    public function update(Request $request,$name=null, $id)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $owner_id = $owner->id;

      if(Subcategory::where('slug',$request->slug)->where('register_id',$owner_id)->where('id','!=',$id)->exists()){
        return response()->json(array('errors' => [0 =>'This slug has already been taken.']));
    }

      
        //--- Validation Section Ends

        //--- Logic Section
        $data = Subcategory::findOrFail($id);
        $input = $request->all();
        $data->update($input);
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'Data Updated Successfully.'.' '.'<a href="'.route('owner-subcat-index',$owner->username).'"> '.__('View Lists.').'</a>';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

      //*** GET Request Status
      public function status($name=null,$id1,$id2)
      {
            $data = Subcategory::findOrFail($id1);
            $data->status = $id2;
            $data->update();

            //--- Redirect Section
            $msg = __('Status Updated Successfully.');
            return response()->json($msg);
            //--- Redirect Section Ends

      }

    //*** GET Request
    public function load($name=null,$id)
    {
        $cat = Category::find($id);
        return view('owner.subcategory.load.subcategory',compact('cat'));
    }


    //*** GET Request Delete
    public function destroy($name=null,$id)
    {
        $data = Subcategory::findOrFail($id);

        $courses = $data->category->courses;
        
        if($courses->count()>0){
          foreach($courses as $course){
              if($course->ratings->count()>0){
                foreach($course->ratings as $rating){
                    $rating->delete();
                }
            }

            if($data->wishlist->count()>0){
              foreach($data->wishlist as $wishlist){
                $wishlist->delete();
              }
            }

            if($course->enrolled_courses->count()>0){
                foreach($course->enrolled_courses as $enroll){
                    $enroll->delete();
                }
            }
            $course->delete();
          }
        }


        $data->delete();
        //--- Redirect Section
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }
}
