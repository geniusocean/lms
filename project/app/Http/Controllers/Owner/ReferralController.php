<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Referral;
use App\Models\ReferralHistory;
use Datatables;

class ReferralController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables($name=null)
    {

        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

         $datas = Referral::whereRegisterId($owner_id)->orderBy('id')->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->addColumn('action', function(Referral $data) {
                                $owner = $data->owner->username;
                                return '<div class="actions-btn"><a href="' . route('owner-referral-edit',[$owner,$data->id]) . '" class="btn btn-primary btn-sm btn-rounded"> <i class="fas fa-edit"></i>'.__("Edit").'</a><a href="javascript:;" data-href="' . route('owner-referral-delete',[$owner,$data->id]) . '" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger btn-sm btn-rounded"><i class="fas fa-trash-alt"></i></a></div>';
                            })
                            ->rawColumns(['action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }


    //*** JSON Request
    public function h_datatables($name=null)
    {

        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

         $datas = ReferralHistory::whereRegisterId($owner_id)->orderBy('id')->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->editColumn('referrer_id', function(ReferralHistory $data) {
                                return $data->referrer->showName();
                            })
                            ->editColumn('affilate_id', function(ReferralHistory $data) {
                                return $data->affilate->showName();
                            })
                            ->editColumn('course_id', function(ReferralHistory $data) {
                                $owner = $data->owner->username;
                                return '<a href="'.route("owner.front.course",[$owner,$data->course->slug]).'">'.$data->course->title.'</a>';
                            })
                            ->rawColumns(['course_id'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

    //*** GET Request
    public function index($name=null)
    {
        return view('owner.referral.index');
    }



    //*** GET Request
    public function h_index($name=null)
    {
        return view('owner.referral.history');
    }


    //*** GET Request
    public function create($name=null)
    {
        return view('owner.referral.create');
    }

    //*** POST Request
    public function store(Request $request,$name=null)
    {

        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        //--- Logic Section
        $data = new Referral();
        $input = $request->all();

        $input['register_id'] = $owner_id;

        $data->fill($input)->save();
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = __('New Data Added Successfully.').' '.'<a href="'.route('owner-referral-index',$owner->username).'"> '.__('View Lists.').'</a>';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    //*** GET Request
    public function edit($name=null,$id)
    {
        $data = Referral::findOrFail($id);
        return view('owner.referral.edit',compact('data'));
    }

    //*** POST Request
    public function update(Request $request,$name=null , $id)
    {
        //--- Logic Section
        $data = Referral::findOrFail($id);
        $input = $request->all();
        $data->update($input);
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = __('Data Updated Successfully.').' '.'<a href="'.route('owner-referral-index',$owner->username).'"> '.__('View Lists.').'</a>';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    //*** GET Request Delete
    public function destroy($name=null,$id)
    {
        $data = Referral::findOrFail($id);
        $data->delete();
        //--- Redirect Section
        $msg = __('Data Deleted Successfully.');
        return response()->json($msg);
        //--- Redirect Section Ends
    }

}