<?php

namespace App\Http\Controllers\Owner;

use Datatables;
use App\Classes\GeniusMailer;
use App\Models\Admin;
use App\Models\EmailTemplate;
use App\Models\Generalsetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Subscriber;
use App\Models\User;

class EmailController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

         $datas = EmailTemplate::orderBy('id','desc')->where('register_id',$owner_id)->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->addColumn('action', function(EmailTemplate $data) {
                                $owner = $data->owner->username;
                                return '<div class="action-list"><a class="btn btn-primary btn-sm btn-rounded" href="' . route('owner-mail-edit',[$owner,$data->id]) . '"> <i class="fas fa-edit"></i>Edit</a></div>';
                            }) 
                            ->toJson();//--- Returning Json Data To Client Side
    }

    public function index($name=null)
    {
        return view('owner.email.index');
    }

    public function config($name=null)
    {
        return view('owner.email.config');
    }

    public function edit($name=null,$id)
    {
        $data = EmailTemplate::findOrFail($id);
        return view('owner.email.edit',compact('data'));
    }

    public function groupemail($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        $config = Generalsetting::where('register_id',$owner_id)->first();
        return view('owner.email.group',compact('config'));
    }

    public function groupemailpost(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $config = Generalsetting::where('register_id',$owner_id)->first();
        if($request->type == 0)
        {
        $users = User::where('register_id',$owner_id)->get();
        //Sending Email To Users
        foreach($users as $user)
        {
            if($config->is_smtp == 1)
            {
                $data = [
                    'to' => $user->email,
                    'subject' => $request->subject,
                    'body' => $request->body,
                ];

                $mailer = new GeniusMailer();
                $mailer->sendCustomMail($data);            
            }
            else
            {
               $to = $user->email;
               $subject = $request->subject;
               $msg = $request->body;
                $headers = "From: ".$config->from_name."<".$config->from_email.">";
               mail($to,$subject,$msg,$headers);
            }  
        } 
        //--- Redirect Section          
        $msg = 'Email Sent Successfully.';
        return response()->json($msg);    
        //--- Redirect Section Ends  
        }

        else if($request->type == 1)
        {
            $users = User::where('is_vendor','=','2')->get();
            //Sending Email To Vendors        
            foreach($users as $user)
            {
                if($config->is_smtp == 1)
                {
                    $data = [
                        'to' => $user->email,
                        'subject' => $request->subject,
                        'body' => $request->body,
                    ];

                    $mailer = new GeniusMailer();
                    $mailer->sendCustomMail($data);            
                }
                else
                {
                $to = $user->email;
                $subject = $request->subject;
                $msg = $request->body;
                    $headers = "From: ".$config->from_name."<".$config->from_email.">";
                mail($to,$subject,$msg,$headers);
                }  
            }
        } 
        else
        {
            $users = Subscriber::where('register_id',$owner_id)->get();
            //Sending Email To Subscribers
            foreach($users as $user)
            {
                if($config->is_smtp == 1)
                {
                    $data = [
                        'to' => $user->email,
                        'subject' => $request->subject,
                        'body' => $request->body,
                    ];

                    $mailer = new GeniusMailer();
                    $mailer->sendCustomMail($data);            
                }
                else
                {
                $to = $user->email;
                $subject = $request->subject;
                $msg = $request->body;
                    $headers = "From: ".$config->from_name."<".$config->from_email.">";
                mail($to,$subject,$msg,$headers);
                }  
            }   
        }

        //--- Redirect Section          
        $msg = 'Email Sent Successfully.';
        return response()->json($msg);    
        //--- Redirect Section Ends  
    }


    

    public function update(Request $request, $id)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $data = EmailTemplate::findOrFail($id);
        $input = $request->all();
        $data->update($input);
        //--- Redirect Section          
        $msg = 'Data Updated Successfully.'.'<a href="'.route("owner-mail-index",$owner->username).'">View Template Lists</a>';
        return response()->json($msg);    
        //--- Redirect Section Ends  
    }

}
