<?php

namespace App\Http\Controllers\Owner;

use Datatables;
use App\Models\Page;
use App\Models\Admin;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;

class PageController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth:admin');
  }

    //*** JSON Request
    public function datatables($name=null)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $owner_id = $owner->id;

         $datas = Page::orderBy('id','desc')->where('register_id',$owner_id )->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)


                            ->editColumn('status', function(Page $data) {
                              $status      = $data->status == 1 ? __('Activated') : __('Deativated');
                              $status_sign = $data->status == 1 ? 'success'   : 'danger';
                              $owner = $data->owner->username;
                              return '<div class="btn-group mb-1">
                              <button type="button" class="btn btn-'.$status_sign.' btn-sm btn-rounded dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                '.$status .'
                              </button>
                              <div class="dropdown-menu" x-placement="bottom-start">
                                <a href="javascript:;" data-toggle="modal" data-target="#statusModal" class="dropdown-item" data-href="'. route('owner-page-status',['name'=>$owner,'id1' => $data->id, 'id2' => 1]).'">'.__("Active").'</a>
                                <a href="javascript:;" data-toggle="modal" data-target="#statusModal" class="dropdown-item" data-href="'. route('owner-page-status',['name'=>$owner,'id1' => $data->id, 'id2' => 0]).'">'.__("Deativate").'</a>
                              </div>
                            </div>';
                          })

                            ->addColumn('action', function(Page $data) {
                              $owner = $data->owner->username;
                                return '<div class="actions-btn"><a href="' . route('owner-page-edit',[$owner,$data->id]) . '" class="btn btn-primary btn-sm btn-rounded">
                                <i class="fas fa-edit"></i> '.__("Edit").'
                              </a><button type="button" data-toggle="modal" data-target="#deleteModal"  data-href="' . route('owner-page-delete',[$owner,$data->id]) . '" class="btn btn-danger btn-sm btn-rounded">
                                <i class="fas fa-trash"></i>
                              </button></div>';
                            })
                            ->rawColumns(['status','action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

    //*** GET Request
    public function index($name=null)
    {
        return view('owner.page.index');
    }

    //*** GET Request
    public function create($name=null)
    {
        return view('owner.page.create');
    }

    //*** POST Request
    public function store(Request $request,$name=null)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $owner_id = $owner->id;
        //--- Validation Section
       
        if(Page::where('slug',$request->slug)->where('register_id',$owner_id)->exists()){
          return response()->json(array('errors' => [0 =>'This slug has already been taken.']));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = new Page();
        $input = $request->all();
        $input['register_id'] = $owner_id;
        $common_rep   = ["value", "{", "}", "[","]",":","\""];
          $metatag = str_replace($common_rep, '', $request->meta_tag);

        if ($metatag)
         {
            $input['meta_tag'] = $metatag;
         }
        if ($request->secheck == "")
         {
            $input['meta_tag'] = null;
            $input['meta_description'] = null;
         }
        $data->fill($input)->save();
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'New Data Added Successfully.'.'<a href="'.route("owner-page-index",$owner->username).'">View Page Lists</a>';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    //*** GET Request
    public function edit($name=null,$id)
    {
        $data = Page::findOrFail($id);
        return view('owner.page.edit',compact('data'));
    }

    //*** POST Request
    public function update(Request $request,$name=null, $id)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $owner_id = $owner->id;


      if(Page::where('slug',$request->slug)->where('id','!=',$id)->where('register_id',$owner_id)->exists()){
        return response()->json(array('errors' => [0 =>'This slug has already been taken.']));
      }
        //--- Validation Section Ends

        //--- Logic Section
        $data = Page::findOrFail($id);
        $input = $request->all();
        $common_rep   = ["value", "{", "}", "[","]",":","\""];
          $metatag = str_replace($common_rep, '', $request->meta_tag);

        if ($metatag)
         {
            $input['meta_tag'] = $metatag;
         }
         else {
            $input['meta_tag'] = null;
         }
        if ($request->secheck == "")
         {
            $input['meta_tag'] = null;
            $input['meta_description'] = null;
         }
        $data->update($input);
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = 'Data Updated Successfully.'.'<a href="'.route("owner-page-index",$owner->username).'">View Page Lists</a>';
        return response()->json($msg);
        //--- Redirect Section Ends
    }
      //*** GET Request Header
      public function status($name=null,$id1,$id2)
        {
            $data = Page::findOrFail($id1);
            $data->status = $id2;
            $data->update();
            $mgs = __('Data Update Successfully.');
            return response()->json($mgs);
        }



    //*** GET Request Delete
    public function destroy($name=null,$id)
    {
        $data = Page::findOrFail($id);
        $data->delete();
        //--- Redirect Section
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);
        //--- Redirect Section Ends
    }
}
