<?php

namespace App\Http\Controllers\Owner;

use Datatables;
use App\Models\Admin;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

         $datas = Service::where('user_id','=',0)->where('register_id',$owner_id)->orderBy('id','desc')->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->editColumn('photo', function(Service $data) {
                                $owner = $data->owner->username;
                                $photo = $data->photo ? url('assets/'.$owner.'/owner/images/services/'.$data->photo):url('assets/'.$owner.'/owner/images/noimage.png');
                                return '<img src="' . $photo . '" alt="Image">';
                            })
                            ->editColumn('title', function(Service $data) {
                                $title = mb_strlen(strip_tags($data->title),'utf-8') > 250 ? mb_substr(strip_tags($data->title),0,250,'utf-8').'...' : strip_tags($data->title);
                                return  $title;
                            })
                            ->editColumn('details', function(Service $data) {
                                $details = mb_strlen(strip_tags($data->details),'utf-8') > 250 ? mb_substr(strip_tags($data->details),0,250,'utf-8').'...' : strip_tags($data->details);
                                return  $details;
                            })
                            ->addColumn('action', function(Service $data) {
                                $owner = $data->owner->username;
                                return '<div class="actions-btn"><a href="' . route('owner-service-edit',[$owner,$data->id]) . '" class="btn btn-primary btn-sm btn-rounded">
                                <i class="fas fa-edit"></i> '.__("Edit").'
                              </a><button type="button" data-toggle="modal" data-target="#deleteModal"  data-href="' . route('owner-service-delete',[$owner,$data->id]) . '" class="btn btn-danger btn-sm btn-rounded">
                                <i class="fas fa-trash"></i>
                              </button></div>';
                            })
                            ->rawColumns(['photo', 'action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

    //*** GET Request
    public function index($name=null)
    {
        return view('owner.service.index');
    }

    //*** GET Request
    public function create($name=null)
    {
        return view('owner.service.create');
    }

    //*** POST Request
    public function store(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        //--- Validation Section
        $rules = [
               'photo'      => 'required|mimes:jpeg,jpg,png,svg',
                ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = new Service();
        $input = $request->all();
        $input['register_id'] = $owner_id;

        if ($file = $request->file('photo'))
         {
            $name = time().str_replace(' ', '', $file->getClientOriginalName());
            $file->move('assets/'.$owner->username.'/owner/images/services',$name);
            $input['photo'] = $name;
        }
        $data->fill($input)->save();
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = __('New Data Added Successfully.').' '.'<a href="'.route('owner-student-index',$owner->username).'">'.__('View Lists.').'</a>';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    //*** GET Request
    public function edit($name=null,$id)
    {
        $data = Service::findOrFail($id);
        return view('owner.service.edit',compact('data'));
    }

    //*** POST Request
    public function update(Request $request,$name=null, $id)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        //--- Validation Section
        $rules = [
               'photo'      => 'mimes:jpeg,jpg,png,svg',
                ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = Service::findOrFail($id);
        $input = $request->all();
        if ($file = $request->file('photo'))
        {
            if($data->photo != null)
            {
                if(file_exists(base_path('../assets/'.$owner->username.'/owner/images/services/'.$data->photo))){
                    unlink(base_path('../assets/'.$owner->username.'/owner/images/services/'.$data->photo));
                }
            }
            $name = time().str_replace(' ', '', $file->getClientOriginalName());
            $file->move('assets/'.$owner->username.'/owner/images/services',$name);
            $input['photo'] = $name;
        }
        $data->update($input);
        //--- Logic Section Ends

        //--- Redirect Section
        $msg = __('Data Updated Successfully.').' '.'<a href="'.route('owner-service-index',$owner->username).'">'.__('View Lists.').'</a>';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

    //*** GET Request Delete
    public function destroy($name=null,$id)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();

        $data = Service::findOrFail($id);
        //If Photo Doesn't Exist
        if($data->photo == null){
            $data->delete();
            //--- Redirect Section
            $msg = __('Data Deleted Successfully.');
            return response()->json($msg);
            //--- Redirect Section Ends
        }
        //If Photo Exist
        if(file_exists(base_path('../assets/'.$owner->username.'/owner/images/services/'.$data->photo))){
            unlink(base_path('../assets/'.$owner->username.'/owner/images/services/'.$data->photo));
        }
        $data->delete();
        //--- Redirect Section
        $msg = __('Data Deleted Successfully.');
        return response()->json($msg);
        //--- Redirect Section Ends
    }
}
