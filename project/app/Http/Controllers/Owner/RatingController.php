<?php

namespace App\Http\Controllers\Owner;

use Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Rating;
use App\Models\Admin;

class RatingController extends Controller
{
	public function __construct()
	    {
	        $this->middleware('auth:admin');
	    }

	    //*** JSON Request
	    public function datatables($name=null)
	    {
			$owner = Admin::where('username',$name)->where('role','Owner')->first();
			$owner_id = $owner->id;
		
	         $datas = Rating::orderBy('id')->where('register_id',$owner_id)->get();
	         //--- Integrating This Collection Into Datatables
	         return Datatables::of($datas)
	                            ->addColumn('product', function(Rating $data) {
									$owner = $data->owner->username;
	                                $name = mb_strlen(strip_tags($data->product->name),'utf-8') > 50 ? mb_substr(strip_tags($data->product->name),0,50,'utf-8').'...' : strip_tags($data->product->name);
	                                $product = '<a href="'.route('owner.front.product',[$owner,$data->product->slug]).'" target="_blank">'.$name.'</a>';
	                                return $product;
	                            })
	                            ->addColumn('reviewer', function(Rating $data) {
	                                $name = $data->user->name;
	                                return $name;
	                            })
	                            ->addColumn('review', function(Rating $data) {
	                                $text = mb_strlen(strip_tags($data->review),'utf-8') > 250 ? mb_substr(strip_tags($data->review),0,250,'utf-8').'...' : strip_tags($data->review);
	                                return $text;
	                            })
	                            ->addColumn('action', function(Rating $data) {
									$owner = $data->owner->username;
	                                return '<div class="action-list"><a data-href="' . route('owner-rating-show',[$owner,$data->id]) . '" class="view details-width" data-toggle="modal" data-target="#modal1"> <i class="fas fa-eye"></i>Details</a><a href="javascript:;" data-href="' . route('owner-rating-delete',[$owner,$data->id]) . '" data-toggle="modal" data-target="#confirm-delete" class="delete"><i class="fas fa-trash-alt"></i></a></div>';
	                            }) 
	                            ->rawColumns(['product','action'])
	                            ->toJson(); //--- Returning Json Data To Client Side
	    }
	    //*** GET Request
	    public function index($name=null)
	    {
	        return view('owner.rating.index');
	    }

	    //*** GET Request
	    public function show($name=null,$id)
	    {
	        $data = Rating::findOrFail($id);
	        return view('owner.rating.show',compact('data'));
	    }


	    //*** GET Request Delete
		public function destroy($name=null,$id)
		{
		    $rating = Rating::findOrFail($id);
		    $rating->delete();
		    //--- Redirect Section     
		    $msg = 'Data Deleted Successfully.';
		    return response()->json($msg);      
		    //--- Redirect Section Ends    
		}
}
