<?php

namespace App\Http\Controllers\Owner;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin;
use App\Models\Order;


class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index($name=null)
    {

        $owner = Admin::where('username',$name)->where('role','Owner')->first();

        $data['days'] = "";
        $data['sales'] = "";
        for($i = 0; $i < 30; $i++) {
            $data['days'] .= "'".date("d M", strtotime('-'. $i .' days'))."',";

            $data['sales'] .=  "'".Order::where('status','=','completed')->where('register_id',$owner->id)->whereDate('created_at', '=', date("Y-m-d", strtotime('-'. $i .' days')))->count()."',";
        }

        return view('owner.dashboard',$data);
    }

  


    public function profile($name=null)
    {
        $data = Auth::guard('admin')->user();
        return view('owner.profile',compact('data'));
    }

    public function profileupdate(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        //--- Validation Section

        $rules =
        [
            'photo' => 'mimes:jpeg,jpg,png,svg',
            'email' => 'unique:admins,email,'.Auth::guard('admin')->user()->id
        ];


        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends
        $input = $request->all();
        $data = Auth::guard('admin')->user();
            if ($file = $request->file('photo'))
            {
                $name = time().str_replace(' ', '', $file->getClientOriginalName());
                $file->move('assets/'.$owner->username.'/owner/images/admins/',$name);
                if($data->photo != null)
                {
                    if (file_exists(public_path().'/assets/'.$owner.'/owner/images/admins/'.$data->photo)) {
                        unlink(public_path().'/assets/'.$owner.'/owner/images/admins/'.$data->photo);
                    }
                }
            $input['photo'] = $name;
            }

            $input['slug'] = str_replace(" ","-",$input['name']);

        $data->update($input);
        $msg = 'Successfully updated your profile';
        return response()->json($msg);
    }

    public function passwordreset($name=null)
    {
        $data = Auth::guard('admin')->user();
        return view('owner.password',compact('data'));
    }

    public function changepass(Request $request,$name=null)
    {
        $admin = Auth::guard('admin')->user();
        if ($request->cpass){
            if (Hash::check($request->cpass, $admin->password)){
                if ($request->newpass == $request->renewpass){
                    $input['password'] = Hash::make($request->newpass);
                }else{
                    return response()->json(array('errors' => [ 0 => __('Confirm password does not match.') ]));
                }
            }else{
                return response()->json(array('errors' => [ 0 => __('Current password Does not match.') ]));
            }
        }
        $admin->update($input);
        $msg = __('Successfully change your password');
        return response()->json($msg);
    }



}