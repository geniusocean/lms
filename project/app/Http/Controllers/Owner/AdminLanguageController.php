<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use App\Models\Admin;
use App\Models\AdminLanguage;
use Illuminate\Support\Str;


class AdminLanguageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

         $datas = AdminLanguage::orderBy('id')->where('register_id',$owner_id)->get();
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                          
                            ->addColumn('action', function(AdminLanguage $data) {
                                $owner = $data->owner->username;
                                $delete = $data->is_default == 1 ? '':'<a href="javascript:;" data-href="' . route('owner-tlang-delete',[$owner,$data->id]) . '" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger btn-sm btn-rounded"><i class="fas fa-trash-alt"></i></a>';
                                $default = $data->is_default == 1 ? '<a href="javascript:;" class="btn btn-primary btn-sm btn-rounded"><i class="fa fa-check"></i> Default</a>' : '<a class="status btn btn-primary btn-sm btn-rounded" href="javascript:;" data-href="' . route('owner-tlang-st',[$owner,$data->id,1]) . '">'.__('Set Default').'</a>';
                                return '<div class="actions-btn"><a href="' . route('owner-tlang-edit',[$owner,$data->id]) . '" class="btn btn-primary btn-sm btn-rounded"> <i class="fas fa-edit"></i>Edit</a>'.$delete.$default.'</div>';
                            })
                            ->rawColumns(['action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

    //*** GET Request
    public function index($name=null)
    {

        return view('owner.adminlanguage.index');
    }

    //*** GET Request
    public function create($name=null)
    {

        return view('owner.adminlanguage.create');
    }

     //*** POST Request
     public function store(Request $request,$name=null)
     {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
         //--- Logic Section
         $new = null;
         $input = $request->all();
         $data = new AdminLanguage();
         $data->language = $input['language'];
         $name = time().Str::random(8);
         $data->name = $name;
         $data->file = $name.'.json';
         $data->rtl = $input['rtl'];
         $data->register_id = $owner->id;
         $data->save();
         unset($input['_token']);
         unset($input['language']);
         $keys = $request->keys;
         $values = $request->values;
         foreach(array_combine($keys,$values) as $key => $value)
         {
             $n = str_replace("_"," ",$key);
             $new[$n] = $value;
         }  
         $mydata = json_encode($new);
         file_put_contents(public_path().'/assets/languages/'.$data->file, $mydata); 
         //--- Logic Section Ends
 
         //--- Redirect Section        
         $msg = 'New Data Added Successfully.';
         return response()->json($msg);      
         //--- Redirect Section Ends    
     }
 
     //*** GET Request
     public function edit($name=null,$id)
     {
          $data = AdminLanguage::findOrFail($id);
          $data_results = file_get_contents(public_path().'/assets/languages/'.$data->file);
         $lang = json_decode($data_results, true);
         return view('owner.adminlanguage.edit',compact('data','lang'));
     }
 
     //*** POST Request
     public function update(Request $request,$name=null, $id)
     {
     
         //--- Logic Section
         $new = null;
         $input = $request->all();
         $data = AdminLanguage::findOrFail($id);
         if (file_exists(public_path().'/assets/languages/'.$data->file)) {
             unlink(public_path().'/assets/languages/'.$data->file);
         }
         $data->language = $input['language'];
         $name = time().Str::random(8);
         $data->name = $name;
         $data->file = $name.'.json';
         $data->rtl = $input['rtl'];
         $data->update();
         unset($input['_token']);
         unset($input['language']);
         $keys = $request->keys;
         $values = $request->values;
         foreach(array_combine($keys,$values) as $key => $value)
         {
             $n = str_replace("_"," ",$key);
             $new[$n] = $value;
         }        
         $mydata = json_encode($new);
         file_put_contents(public_path().'/assets/languages/'.$data->file, $mydata); 
         //--- Logic Section Ends
 
         //--- Redirect Section     
         $msg = 'Data Updated Successfully.';
         return response()->json($msg);      
         //--- Redirect Section Ends            
     }
 
       public function status($name=null,$id1,$id2)
         {

            $owner = Admin::where('username',$name)->where('role','Owner')->first();

             $data = AdminLanguage::findOrFail($id1);
             $data->is_default = $id2;
             $data->update();
             $data = AdminLanguage::where('id','!=',$id1)->where('register_id','=',$owner->id)->update(['is_default' => 0]);
             //--- Redirect Section     
             $msg = __('Data Updated Successfully.');
             return response()->json($msg);      
             //--- Redirect Section Ends  
         }
 
     //*** GET Request Delete
    public function destroy($name=null,$id)
    {

        $owner = Admin::where('username',$name)->where('role','Owner')->first();

        if(AdminLanguage::where('register_id',$owner->id)->count() == 1)
        {
        return __("You can't remove all languages.");
        }
        $data = AdminLanguage::findOrFail($id);
        if($data->is_default == 1)
        {
        return "You can not remove default language.";            
        }
        if (file_exists(public_path().'/project/resources/lang/'.$data->file)) {
            unlink(public_path().'/project/resources/lang/'.$data->file);
        }
        $data->delete();
        //--- Redirect Section     
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);      
        //--- Redirect Section Ends     
    }
}
