<?php

namespace App\Http\Controllers\Owner;


use App\Models\Seotool;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductClick;
use App\Models\Admin;

class SeoToolController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function analytics($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $tool = Seotool::where('register_id',$owner_id)->first();
        return view('owner.seotool.googleanalytics',compact('tool'));
    }

    public function analyticsupdate(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $tool = Seotool::where('register_id',$owner_id)->first();
        $tool->update($request->all());
        $msg = 'Data Updated Successfully.';
        return response()->json($msg);  
    }  

    public function keywords($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $tool = Seotool::where('register_id',$owner_id)->first();
        return view('owner.seotool.meta-keywords',compact('tool'));
    }

    public function keywordsupdate(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $tool = Seotool::where('register_id',$owner_id)->first();
        $tool->update($request->all());
        $msg = 'Data Updated Successfully.';
        return response()->json($msg);  
    }
     
 

}
