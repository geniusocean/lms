<?php

namespace App\Http\Controllers\Owner;

use App\Classes\GeniusMailer;
use App\Models\Generalsetting;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use Validator;


class LoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:admin', ['except' => ['logout']]);
    }

    public function showLoginForm($name=null)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      if(!empty($owner)){
        return view('owner.login');
      }else{
        return view('errors.404');
      }
    }

    public function login(Request $request,$name=null)
    {
        //--- Validation Section
        $rules = [
                  'email'   => 'required|email',
                  'password' => 'required'
                ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

      // Attempt to log the user in
      if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location

      // Check If It Is Owner
      if(Auth::guard('admin')->user()->role == 'Administrator')
      {
        Auth::guard('admin')->logout();
        return response()->json(array('errors' => [ 0 => __('Credentials Doesn\'t Match !') ]));   
      }

      // Check If The Staff's parent Is Owner
      if(Auth::guard('admin')->user()->parent_id != null)
      {
        if(Auth::guard('admin')->user()->parent->username != $name)
        {
          Auth::guard('admin')->logout();
          return response()->json(array('errors' => [ 0 => __('Credentials Doesn\'t Match !') ]));   
        }
      }


        return response()->json(route('owner.dashboard',$name));
      }

      // if unsuccessful, then redirect back to the login with the form data
          return response()->json(array('errors' => [ 0 => __('Credentials Doesn\'t Match !') ]));
    }

    public function showForgotForm($name=null)
    {
      return view('owner.forgot');
    }

    public function forgot(Request $request,$name=null)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $owner_id = $owner->id;

      $gs = Generalsetting::where('register_id',$owner_id)->first();
      $input =  $request->all();
      if (Admin::where('email', '=', $request->email)->count() > 0) {
      // user found
      $admin = Admin::where('email', '=', $request->email)->firstOrFail();
      $token = md5(time().$admin->name.$admin->email);

      $file = fopen(public_path().'/project/storage/tokens/'.$token.'.data','w+');
      fwrite($file,$admin->id);
      fclose($file);

      $subject = "Reset Password Request";
      $msg = "Please click this link : ".'<a href="'.route('owner.change.token',[$owner->username,$token]).'">'.route('owner.change.token',$token).'</a>'.' to change your password.';
      if($gs->is_smtp == 1)
      {
          $data = [
                  'to' => $request->email,
                  'subject' => $subject,
                  'body' => $msg,
          ];

          $mailer = new GeniusMailer();
          $mailer->sendCustomMail($data);
      }
      else
      {
          $headers = "From: ".$gs->from_name."<".$gs->from_email.">";
          mail($request->email,$subject,$msg,$headers);
      }
      return response()->json(__('Verification Link Sent Successfully!. Please Check your email.'));
      }
      else{
      // user not found
      return response()->json(array('errors' => [ 0 => __('No Account Found With This Email.') ]));
      }
    }

    public function showChangePassForm($token)
    {
      if (file_exists(public_path().'/project/storage/tokens/'.$token.'.data')){
        $id = file_get_contents(public_path().'/project/storage/tokens/'.$token.'.data');
        return view('owner.changepass',compact('id','token'));
      }
    }

    public function changepass(Request $request,$name=null)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $id = $request->admin_id;
        $admin =  Admin::findOrFail($id);
        $token = $request->file_token;
        if ($request->cpass){
            if (Hash::check($request->cpass, $admin->password)){
                if ($request->newpass == $request->renewpass){
                    $input['password'] = Hash::make($request->newpass);
                }else{
                    return response()->json(array('errors' => [ 0 => __('Confirm password does not match.') ]));
                }
            }else{
                return response()->json(array('errors' => [ 0 => __('Current password Does not match.') ]));
            }
        }
        $admin->update($input);

        unlink(public_path().'/project/storage/tokens/'.$token.'.data');

        $msg = __('Successfully changed your password.').'<a href="'.route('owner.login',$owner->username).'">'.__('Login Now').'</a>';
        return response()->json($msg);
    }


    public function logout($name=null)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      if($owner){
        Auth::guard('admin')->logout();
        return redirect(route('owner.front.index',$owner->username));
      }
    }
}
