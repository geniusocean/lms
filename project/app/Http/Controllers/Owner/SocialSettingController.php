<?php

namespace App\Http\Controllers\Owner;

use App\Models\Socialsetting;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SocialSettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    // Spcial Settings All post requests will be done in this method
    public function socialupdate(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        //--- Logic Section
        $input = $request->all(); 
        $data = Socialsetting::where('register_id',$owner_id)->first();   
        $data->update($input);
        //--- Logic Section Ends
        
        //--- Redirect Section        
        $msg = 'Data Updated Successfully.';
        return response()->json($msg);      
        //--- Redirect Section Ends               

    }


    // Spcial Settings All post requests will be done in this method
    public function socialupdateall(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        //--- Logic Section
        $input = $request->all(); 
        $data = Socialsetting::where('register_id',$owner_id)->first();    
        if ($request->f_status == ""){
            $input['f_status'] = 0;
        }
        if ($request->t_status == ""){
            $input['t_status'] = 0;
        }
        if ($request->g_status == ""){
            $input['g_status'] = 0;
        }
        if ($request->l_status == ""){
            $input['l_status'] = 0;
        }
        if ($request->d_status == ""){
            $input['d_status'] = 0;
        }
        $data->update($input);
        //--- Logic Section Ends
        
        //--- Redirect Section        
        $msg = 'Data Updated Successfully.';
        return response()->json($msg);      
        //--- Redirect Section Ends               

    }


    public function index($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

    	 $data = Socialsetting::where('register_id',$owner_id)->first();   
        return view('owner.socialsetting.index',compact('data'));
    }

    public function facebook($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

    	 $data = Socialsetting::where('register_id',$owner_id)->first();   
        return view('owner.socialsetting.facebook',compact('data'));
    }

    public function google($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

    	 $data = Socialsetting::where('register_id',$owner_id)->first();   
        return view('owner.socialsetting.google',compact('data'));
    }


    public function facebookup($name=null,$status)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $data = Socialsetting::where('register_id',$owner_id)->first();   
        $data->f_check = $status;
        $data->update();
    }

    public function googleup($name=null,$status)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $data = Socialsetting::where('register_id',$owner_id)->first();   
        $data->g_check = $status;
        $data->update();
    }

}
