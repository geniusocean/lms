<?php

namespace App\Http\Controllers\Owner;
use App\Models\Pagesetting;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Validator;


class PageSettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    protected $rules =
    [
        'best_seller_banner' => 'mimes:jpeg,jpg,png,svg',
        'big_save_banner'    => 'mimes:jpeg,jpg,png,svg',
        'best_seller_banner1' => 'mimes:jpeg,jpg,png,svg',
        'big_save_banner1'    => 'mimes:jpeg,jpg,png,svg'
    ];


    protected $customs =
    [
        'best_seller_banner.mimes'  => 'Photo type must be in jpeg, jpg, png, svg.',
        'big_save_banner.mimes'     => 'Photo type must be in jpeg, jpg, png, svg.',
        'best_seller_banner1.mimes' => 'Photo type must be in jpeg, jpg, png, svg.',
        'big_save_banner1.mimes'    => 'Photo type must be in jpeg, jpg, png, svg.'

    ];


    // Page Settings All post requests will be done in this method
    public function update(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        //--- Validation Section
        $validator = Validator::make($request->all(), $this->rules,$this->customs);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        $data = Pagesetting::where('register_id',$owner_id)->first();
        $input = $request->all();

        if ($file = $request->file('hero_bg'))
        {
            $name = time().str_replace(' ', '', $file->getClientOriginalName());
            $data->ownerupload($name,$file,$data->hero_bg,$owner->username);
            $input['hero_bg'] = $name;
        }

        if ($file = $request->file('instructor_img'))
        {
            $name = time().str_replace(' ', '', $file->getClientOriginalName());
            $data->ownerupload($name,$file,$data->instructor_img,$owner->username);
            $input['instructor_img'] = $name;
        }


        $data->update($input);
        $msg = 'Data Updated Successfully.';
        return response()->json($msg);
    }


    public function homeupdate(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $data = Pagesetting::where('register_id',$owner_id)->first();
        $input = $request->all();
        $input['register_id'] = $owner_id;
        if ($request->slider == ""){
            $input['slider'] = 0;
        }
        if ($request->service == ""){
            $input['service'] = 0;
        }
        if ($request->featured == ""){
            $input['featured'] = 0;
        }
        if ($request->small_banner == ""){
            $input['small_banner'] = 0;
        }
        if ($request->best == ""){
            $input['best'] = 0;
        }
        if ($request->top_rated == ""){
            $input['top_rated'] = 0;
        }
        if ($request->large_banner == ""){
            $input['large_banner'] = 0;
        }
        if ($request->big == ""){
            $input['big'] = 0;
        }
        if ($request->hot_sale == ""){
            $input['hot_sale'] = 0;
        }
        if ($request->review_blog == ""){
            $input['review_blog'] = 0;
        }
        if ($request->partners == ""){
            $input['partners'] = 0;
        }
        if ($request->bottom_small == ""){
            $input['bottom_small'] = 0;
        }
        if ($request->flash_deal == ""){
            $input['flash_deal'] = 0;
        }
        if ($request->featured_category == ""){
            $input['featured_category'] = 0;
        }
        $data->update($input);
        $msg = 'Data Updated Successfully.';
        return response()->json($msg);
    }


    public function contact($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $data = Pagesetting::where('register_id',$owner_id)->first();
        return view('owner.pagesetting.contact',compact('data'));
    }

    public function customize($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

       $data = Pagesetting::where('register_id',$owner_id)->first();
        return view('owner.pagesetting.customize',compact('data'));
    }

    public function best_seller($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
       $data = Pagesetting::where('register_id',$owner_id)->first();
        return view('owner.pagesetting.best_seller',compact('data'));
    }

    public function big_save($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
       $data = Pagesetting::where('register_id',$owner_id)->first();
        return view('owner.pagesetting.big_save',compact('data'));
    }

    public function herosection($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        $ps = Pagesetting::where('register_id',$owner_id)->first();
        return view('owner.pagesetting.herosection',compact('ps'));
    }
    public function instructorsection ($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        $ps = Pagesetting::where('register_id',$owner_id)->first();
        return view('owner.pagesetting.instructor',compact('ps'));
    }

    //Upadte About Page Section Settings

    //Upadte FAQ Page Section Settings
    public function faqupdate($name=null,$status)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        $page = Pagesetting::where('register_id',$owner_id)->first();
        $page->f_status = $status;
        $page->update();
        Session::flash('success', 'FAQ Status Upated Successfully.');
        return redirect()->back();
    }

    public function contactup($name=null,$status)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        $page = Pagesetting::where('register_id',$owner_id)->first();
        $page->c_status = $status;
        $page->update();
        Session::flash('success', 'Contact Status Upated Successfully.');
        return redirect()->back();
    }

    //Upadte Contact Page Section Settings
    public function contactupdate(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
        $page = Pagesetting::where('register_id',$owner_id)->first();
        $input = $request->all();
        $page->update($input);
        Session::flash('success', 'Contact page content updated successfully.');
        return redirect()->route('owner-ps-contact',$owner->username);
    }

}
