<?php

namespace App\Http\Controllers\Front;

use App\Models\Cart;
use App\Models\Order;
use App\Classes\GeniusMailer;
use App\Models\Notification;
use App\Models\UserNotification;
use App\Models\InstructorOrder;
use App\Models\User;
use App\Models\Course;
use App\Models\Currency;
use App\Models\Generalsetting;
use App\Models\PaymentGateway;
use App\Models\Referral;
use App\Models\ReferralHistory;

use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Auth;
use Session;
use Redirect;
use Validator;

class PaypalController extends Controller
{

    public $curr;

    private $_api_context;

    public function __construct()
    {

        $this->curr = \DB::table('currencies')->where('register_id',0)->where('is_default','=',1)->first();

        $data = PaymentGateway::where('register_id',0)->whereKeyword('paypal')->first();
        $paydata = $data->convertAutoData();
        $paypal_conf = \Config::get('paypal');
        $paypal_conf['client_id'] = $paydata['client_id'];
        $paypal_conf['secret'] = $paydata['client_secret'];
        $paypal_conf['settings']['mode'] = $paydata['sandbox_check'] == 1 ? 'sandbox' : 'live';
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function store(Request $request)
    {

        $input = $request->all();
        $gs = Generalsetting::findOrFail(1);


        $pay_amount = $request->pay_amount;



        if (!Session::has('cart')) {
            return redirect()->route('front.cart')->with('success',__("You don't have any product to checkout."));
        }

        $order['item_name'] = $gs->title." Order";
        $order['item_number'] = Str::random(4).time();
        $order['item_amount'] =  $pay_amount;
        $cancel_url = route('front.payment.cancle');
        $notify_url = route('paypal.notify');

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_1 = new Item();
        $item_1->setName($order['item_name']) /** item name **/
            ->setCurrency($this->curr->name)
            ->setQuantity(1)
            ->setPrice($order['item_amount']); /** unit price **/
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        $amount = new Amount();
        $amount->setCurrency($this->curr->name)
            ->setTotal($order['item_amount']);
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription($order['item_name'].' Via Paypal');
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl($notify_url) /** Specify return URL **/
            ->setCancelUrl($cancel_url);
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            return redirect()->back()->with('error',$ex->getMessage());
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('input_data',$input);
        Session::put('order_data',$order);
        Session::put('order_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return \Redirect::away($redirect_url);
        }
        return redirect()->back()->with('error',__('Unknown error occurred'));

    }

    public function notify(Request $request)
    {
        $user = Auth::user();
        $input = Session::get('input_data');
        $order_data = Session::get('order_data');
        $success_url = route('front.payment.return');
        $cancel_url = route('front.payment.cancle');
        $input_data = $request->all();
        $gs = Generalsetting::findOrFail(1);
        $comission = $gs->percentage_commission;


        /** Get the payment ID before session clear **/
        $payment_id = Session::get('order_payment_id');

        /** clear the session payment ID **/
        if (empty( $input_data['PayerID']) || empty( $input_data['token'])) {
            return redirect($cancel_url);
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($input_data['PayerID']);
        /**Execute the payment **/

        $result = $payment->execute($execution, $this->_api_context);

        if ($result->getState() == 'approved') {
            $resp = json_decode($payment, true);

            $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);
            $t_oldCart = Session::get('cart');
            $t_cart = new Cart($t_oldCart);
            $new_cart = [];
            $new_cart['totalQty'] = $t_cart->totalQty;
            $new_cart['totalPrice'] = $t_cart->totalPrice;
            $new_cart['items'] = $t_cart->items;
            $new_cart = json_encode($new_cart);


            $order = new Order;
            $order['user_id'] = $user->id;
            $order['txnid'] = $resp['transactions'][0]['related_resources'][0]['sale']['id'];
            $order['cart'] = $new_cart;
            $order['order_number'] = $order_data['item_number'];
            $order['discount'] = $input['discount'];
            $order['pay_amount'] = $order_data['item_amount'] / $this->curr->value;
            $order['method'] = $input['method'];

            $order['currency_sign'] = $this->curr->sign;
            $order['currency_value'] = $this->curr->value;

            $order['status'] = 'Completed';
            $order['payment_status'] = 'Completed';
            $order->save();

            foreach($cart->items as  $course)
            {
                EnrolledCourse::create(['user_id' => $user->id, 'course_id' => $course['item']['id']]);

                if($course['affilate_user'] != 0){
                    if(Auth::user()->id != $course['affilate_user']){
                        ReferralHistory::create([
                            'referrer_id' => $course['affilate_user'],
                            'affilate_id' => Auth::user()->id,
                            'course_id' => $course['item']['id'],
                            'type' => $course['type']
                        ]);

                        $total_refer = ReferralHistory::where('register_id',0)->where('referrer_id',$course['affilate_user'])->count();

                        foreach(Referral::whereType($course['type'])->where('register_id',0)->get() as $refer){
                            if($refer->times == $total_refer){
                                User::find($course['affilate_user'])->update(['referral_id' => $refer->id]);
                            }
                        }
                    }
                }

                if($course['item']['user_id'] != 0){

                    $instructor = User::find($course['item']['user_id']);

                    if($instructor->referral_id != 0){
                        $comission -= $instructor->referral->discount;
                    }

                    InstructorOrder::create([
                        'user_id' => $course['item']['user_id'],
                        'order_id' => $order->id,
                        'course_id' => $course['item']['id'],
                        'price' => $course['price'],
                        'order_number' => $order->order_number,
                        'status' => 'Completed',
                        'charge' => $comission
                    ]);

                    $instructor->balance += $course['price'];
                    $instructor->update();


                    //User Notification.......
                    $user_notification = new UserNotification();
                    $user_notification->user_id = $course['item']['user_id'];
                    $user_notification->order_number = $order->order_number;
                    $user_notification->save();

                }

            }

            //Sending Email To Buyer
            $data = [
                'to' => $user->email,
                'type' => "new_order",
                'cname' => $user->first_name.' '.$user->last_name,
                'oamount' => "",
                'aname' => "",
                'aemail' => "",
                'wtitle' => "",
                'onumber' => $order->order_number,
            ];

            $mailer = new GeniusMailer();
            $mailer->sendAutoMail($data,$order->id);            

            //Sending Email To Admin
            $data = [
                'to' => DB::table('pagesettings')->first()->contact_email,
                'subject' => "New Order Recieved!!",
                'body' => "Hello Admin!<br>Your store has received a new order.<br>Order Number is ".$order->order_number.".Please login to your panel to check. <br>Thank you.",
            ];
            $mailer = new GeniusMailer();
            $mailer->sendCustomMail($data);  
            
            $notification = new Notification();
			$notification->order_id = $order->id;
			$notification->save();


            Session::put('temporder_id',$order->id);
            Session::put('tempcart',$cart);
            Session::forget('cart');

            return redirect($success_url);

        }
        return redirect($cancel_url);
    }

}
