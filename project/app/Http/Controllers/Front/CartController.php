<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Course;
use App\Models\Generalsetting;
use Illuminate\Http\Request;
use Session;
use Auth;

class CartController extends Controller
{

    public function cart()
    {
        $this->code_image();
        if (!Session::has('cart')) {
            return view('front.cart');
        }

        $gs = Generalsetting::findOrFail(1);
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $courses = $cart->items;
        $totalPrice = $cart->totalPrice;

        return view('front.cart', compact('courses','totalPrice'));
    }

    public function cartview()
    {
        return view('load.cart');
    }

   public function addtocart(Request $request, $id)
    {
        $course = Course::where('id','=',$id)->first(['id','register_id','user_id','title','slug','photo','price','discount_price']);

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);

        $affilate_user = 0;
        $type = '';
        if(Auth::check()){
            if($request->has('ref')){
                if(!empty($request->ref)){
                    $affilate_user = (int)$request->ref == 0 ? 0 : (int)$request->ref;
                    $type = $request->type;
                }
            }
        }

        if(isset($cart->items[$course->id]))
        {
            return redirect()->route('front.cart')->with('error',__('Already Added To Cart.'));
        }

        $instructor = !empty($course->instructor) ? $course->instructor->showName() : \DB::table('admins')->first()->name;

        $cart->add($course, $course->id, $instructor, $affilate_user, $type);

        Session::put('cart',$cart);
        return redirect()->route('front.cart');
    }

   public function addcart(Request $request, $id)
    {
        $course = Course::where('id','=',$id)->first(['id','register_id','user_id','title','slug','photo','price','discount_price']);

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);

        $affilate_user = 0;
        $type = '';
        if(Auth::check()){
            if($request->has('ref')){
                if(!empty($request->ref)){
                    $affilate_user = (int)$request->ref == 0 ? 0 : (int)$request->ref;
                    $type = $request->type;
                }
            }
        }

        if(isset($cart->items[$course->id]))
        {
            return response()->json('already');
        }

        $instructor = !empty($course->instructor) ? $course->instructor->showName() : \DB::table('admins')->first()->name;

        $cart->add($course, $course->id, $instructor, $affilate_user, $type);

        Session::put('cart',$cart);
        $data[0] = count($cart->items);
        return response()->json($data);
    }

    public function removecart($id)
    {

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
            $data[0] = $cart->totalPrice;
            $data[1] = count($cart->items);
            return response()->json($data);
        } else {
            Session::forget('cart');
            $data = 0;
            return response()->json($data);
        }
    }

}