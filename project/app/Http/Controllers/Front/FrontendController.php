<?php

namespace App\Http\Controllers\Front;

use App\Classes\GeniusMailer;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\User;
use App\Models\BlogCategory;
use App\Models\Generalsetting;
use App\Models\Service;
use App\Models\Course;
use App\Models\Subscriber;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;



class FrontendController extends Controller
{
    public function __construct()
    {
        //$this->auth_guests();
    }


// -------------------------------- HOME PAGE SECTION ----------------------------------------

	public function index()
	{
       $data['services'] = Service::all();
       $data['new_courses']  = Course::where('register_id',0)->where('status',1)->take(6)->get();
       $data['top_courses']  = Course::where('register_id',0)->where('status',1)->where('is_top',1)->take(6)->get();
       $data['blogs'] = Blog::where('register_id',0)->orderby('id','desc')->take(3)->get();
       return view('front.index',$data);
	}

// -------------------------------- HOME PAGE SECTION ENDS ----------------------------------------

// LANGUAGE SECTION

public function language($id)
{
    $this->code_image();
    Session::put('language', $id);
    return redirect()->route('front.index');
}

// LANGUAGE SECTION ENDS



// -------------------------------- BLOG SECTION ----------------------------------------

	public function blog(Request $request)
	{
        $tags = null;
        $tagz = '';
        $name = Blog::pluck('tags')->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));

        $archives= Blog::where('register_id',0)->orderBy('created_at','desc')->get()->groupBy(function($item){ return $item->created_at->format('F Y'); })->take(5)->toArray();
        $name = Blog::pluck('tags')->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));

        $blogs = Blog::where('register_id',0)->orderBy('created_at','desc')->paginate(8);
        $bcat = BlogCategory::where('register_id',0)->get();
        if($request->ajax()){
            return view('front.ajax.blog',compact('blogs','bcat','tags'));
        }
		return view('front.blog',compact('blogs','bcat','tags'));
	}

    public function blogcategory(Request $request, $slug)
    {
        $tags = null;
        $tagz = '';
        $name = Blog::pluck('tags')->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));

        $archives= Blog::where('register_id',0)->orderBy('created_at','desc')->get()->groupBy(function($item){ return $item->created_at->format('F Y'); })->take(5)->toArray();
        $name = Blog::pluck('tags')->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));

        $blogs = Blog::where('register_id',0)->orderBy('created_at','desc')->paginate(9);
        $bcat = BlogCategory::where('register_id',0)->get();
        $cat = BlogCategory::where('register_id',0)->where('slug', '=', str_replace(' ', '-', $slug))->first();
        $blogs = $cat->blogs()->orderBy('created_at','desc')->paginate(8);
            if($request->ajax()){
                return view('front.ajax.blog',compact('bcat','blogs','tags'));
            }
        return view('front.blog',compact('bcat','blogs','tags'));
    }

    public function blogtags(Request $request, $slug)
    {
        $tags = null;
        $tagz = '';
        $name = Blog::pluck('tags')->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));

        $archives= Blog::where('register_id',0)->orderBy('created_at','desc')->get()->groupBy(function($item){ return $item->created_at->format('F Y'); })->take(5)->toArray();
        $name = Blog::pluck('tags')->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));
        $bcat = BlogCategory::where('register_id',0)->get();
        $blogs = Blog::where('register_id',0)->where('tags', 'like', '%' . $slug . '%')->paginate(9);
            if($request->ajax()){
                return view('front.ajax.blog',compact('blogs','slug','bcat','tags'));
            }
        return view('front.blog',compact('blogs','slug','bcat','tags'));
    }

    public function blogsearch(Request $request)
    {
        $tags = null;
        $tagz = '';
        $name = Blog::pluck('tags')->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));

        $archives= Blog::where('register_id',0)->orderBy('created_at','desc')->get()->groupBy(function($item){ return $item->created_at->format('F Y'); })->take(5)->toArray();
        $name = Blog::pluck('tags')->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));
        $bcat = BlogCategory::where('register_id',0)->get();
        $search = $request->search;
        $blogs = Blog::where('register_id',0)->where('title', 'like', '%' . $search . '%')->orWhere('details', 'like', '%' . $search . '%')->paginate(9);
            if($request->ajax()){
                return view('front.ajax.blog',compact('blogs','search','bcat','tags'));
            }
        return view('front.blog',compact('blogs','search','bcat','tags'));
    }

    public function blogshow($id)
    {
        $this->code_image();
        $tags = null;
        $tagz = '';
        $bcat = BlogCategory::where('register_id',0)->get();
        $blog = Blog::findOrFail($id);
        $blog->views = $blog->views + 1;
        $blog->update();
        $name = Blog::pluck('tags')->toArray();
        foreach($name as $nm)
        {
            $tagz .= $nm.',';
        }
        $tags = array_unique(explode(',',$tagz));


        $blog_meta_tag = $blog->meta_tag;
        $blog_meta_description = $blog->meta_description;
        return view('front.blogshow',compact('blog','bcat','tags','blog_meta_tag','blog_meta_description'));
    }


// -------------------------------- BLOG SECTION ENDS----------------------------------------


// -------------------------------- PAGE SECTION ----------------------------------------
    public function page($slug)
    {


        $page =  DB::table('pages')->where('register_id',0)->where('slug',$slug)->first();
        if(empty($page))
        {
            $name = \Request::path();
            if(DB::table('admins')->whereRole('Owner')->where('username',$name)->exists()){

                $owner = DB::table('admins')->whereRole('Owner')->where('username',$name)->first();
                if($owner->status == 0){
                    return view('front.deactivated');
                }
                if(!empty($owner)){
                    $data['services'] = Service::where('register_id',$owner->id)->get();
                    $data['new_courses']  = Course::where('status',1)->where('register_id',$owner->id)->take(6)->get();
                    $data['top_courses']  = Course::where('status',1)->where('register_id',$owner->id)->where('is_top',1)->take(6)->get();
                    $data['blogs'] = Blog::orderby('id','desc')->where('register_id',$owner->id)->take(3)->get();
                    return view('owner-front.index',$data);
                }else{
                      return view('errors.404');
                }


            }
            return view('errors.404');            
        }
        
        return view('front.page',compact('page'));




         $page =  DB::table('pages')->where('register_id',0)->where('slug',$slug)->first();

        $page =  DB::table('pages')->where('slug',$slug)->first();
        if(empty($page))
        {
            return response()->view('errors.404')->setStatusCode(404);
        }

        return view('front.page',compact('page'));
    }
// -------------------------------- PAGE SECTION ENDS----------------------------------------

// -------------------------------- CONTACT SECTION ----------------------------------------

	public function contact()
	{
        $this->code_image();
        if(DB::table('generalsettings')->find(1)->is_contact== 0){
            return redirect()->back();
        }
        $ps =  DB::table('pagesettings')->where('id','=',1)->first();
		return view('front.contact',compact('ps'));
	}


    //Send email to admin
    public function contactemail(Request $request)
    {
        $gs = Generalsetting::findOrFail(1);

        if($gs->is_capcha == 1)
        {

        // Capcha Check
        $value = session('captcha_string');
        if ($request->codes != $value){
            return response()->json(array('errors' => [ 0 => 'Please enter Correct Capcha Code.' ]));
        }

        }

        // Login Section
        $ps = DB::table('pagesettings')->where('id','=',1)->first();
        $subject = "Email From Of ".$request->name;
        $to = $request->to;
        $name = $request->name;
        $phone = $request->phone;
        $from = $request->email;
        $msg = "Name: ".$name."\nEmail: ".$from."\nPhone: ".$phone."\nMessage: ".$request->text;
        if($gs->is_smtp)
        {

        $data = [
            'to' => $to,
            'subject' => $subject,
            'body' => $msg,
        ];

        $mailer = new GeniusMailer();
        $mailer->sendCustomMail($data);
        }
        else
        {

        $headers = "From: ".$gs->from_name."<".$gs->from_email.">";
        mail($to,$subject,$msg,$headers);
        }
        // Login Section Ends

        // Redirect Section
        return response()->json(__('Success! Thanks for contacting us, we will get back to you shortly.'));
    }

    // Refresh Capcha Code
    public function refresh_code(){
        $this->code_image();
        return "done";
    }

// -------------------------------- CONTACT SECTION ENDS ----------------------------------------

// -------------------------------- SUBSCRIBE SECTION ----------------------------------------

    public function subscribe(Request $request)
    {
        $subs = Subscriber::where('email','=',$request->email)->first();
        if(isset($subs)){
        return response()->json(array('errors' => [ 0 =>  __('This Email Has Already Been Taken.')]));
        }
        $subscribe = new Subscriber;
        $subscribe->fill($request->all());
        $subscribe->save();
        return response()->json(__('You Have Subscribed Successfully.'));
    }

// -------------------------------- SUBSCRIBE SECTION ENDS ----------------------------------------

// Maintenance Mode

    public function maintenance()
    {
        $gs = Generalsetting::find(1);
            if($gs->is_maintain != 1) {

                    return redirect()->route('front.index');

            }

        return view('front.maintenance');
    }







    // Capcha Code Image
    private function  code_image()
    {
        $actual_path = str_replace('project','',base_path());
        $image = imagecreatetruecolor(200, 50);
        $background_color = imagecolorallocate($image, 255, 255, 255);
        imagefilledrectangle($image,0,0,200,50,$background_color);

        $pixel = imagecolorallocate($image, 0,0,255);
        for($i=0;$i<500;$i++)
        {
            imagesetpixel($image,rand()%200,rand()%50,$pixel);
        }

        $font = $actual_path.'assets/front/fonts/NotoSans-Bold.ttf';
        $allowed_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $length = strlen($allowed_letters);
        $letter = $allowed_letters[rand(0, $length-1)];
        $word='';
        //$text_color = imagecolorallocate($image, 8, 186, 239);
        $text_color = imagecolorallocate($image, 0, 0, 0);
        $cap_length=6;// No. of character in image
        for ($i = 0; $i< $cap_length;$i++)
        {
            $letter = $allowed_letters[rand(0, $length-1)];
            imagettftext($image, 25, 1, 35+($i*25), 35, $text_color, $font, $letter);
            $word.=$letter;
        }
        $pixels = imagecolorallocate($image, 8, 186, 239);
        for($i=0;$i<500;$i++)
        {
            imagesetpixel($image,rand()%200,rand()%50,$pixels);
        }
        session(['captcha_string' => $word]);
        imagepng($image, $actual_path."assets/images/capcha_code.png");
    }

// -------------------------------- CONTACT SECTION ENDS----------------------------------------


}
