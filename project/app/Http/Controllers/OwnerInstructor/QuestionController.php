<?php

namespace App\Http\Controllers\Ownerinstructor;

use Validator;
use Datatables;
use App\Models\Course;
use App\Models\Lesson;
use App\Models\Section;
use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class QuestionController extends Controller
{

    //*** POST Request
    public function store(Request $request)
    {
        //--- Logic Section

        $input = $request->all();
        $options = $input['options'];
        $answers = $input['answers'];

        $input['options'] = json_encode($options);
        $input['answers'] = json_encode($answers);

        $data = Lesson::find($input['quiz_lesson_id']);

        $input['pos'] = $data->questions()->count() + 1;

        $ck = $data->questions()->where('title',$input['title'])->count();
        if($ck == 1){
            return response()->json(array('errors' => [ 0 => __('This Title Already Exists.') ]));
        }else{
            $data->questions()->create($input);
        }

        //--- Logic Section Ends

        //--- Redirect Section
        $msg = __('Question Created Successfully.');
        return response()->json($msg);
        //--- Redirect Section Ends

    }

   //*** GET Request
   public function edit($id)
   {
       //--- Logic Section

       $data = Question::find($id);
       $options = json_decode($data->options,true);
       $answers = json_decode($data->answers,true);


       //--- Logic Section Ends

       //--- Redirect Section

        return view('instructor.course.load.question_edit',compact('data','options','answers'));

       //--- Redirect Section Ends

   }

    //*** POST Request
    public function update(Request $request, $id)
    {
        //--- Logic Section

        $data = Question::find($id);
        $lsn = Lesson::find($data->lesson_id);

        $input = $request->all();
        $options = $input['options'];
        $answers = $input['answers'];

        $input['options'] = json_encode($options);
        $input['answers'] = json_encode($answers);


        $ck = $lsn->questions()->where('id','!=',$data->id)->where('title',$input['title'])->count();
        if($ck == 1){
            return response()->json(array('errors' => [ 0 => __('This Title Already Exists.') ]));
        }else{
            $data->update($input);
        }

        //--- Logic Section Ends

        //--- Redirect Section
        $msg = __('Question Updated Successfully.');
        return response()->json($msg);
        //--- Redirect Section Ends

    }



    //*** POST Request
    public function SortUpdate(Request $request, $id)
    {
        //--- Logic Section

        $data = Lesson::find($id);
        $input = $request->all();
        $i = 0;
        $pos = $input['pos'];

        foreach($data->questions()->oldest('pos')->get() as $lesson){
            $lesson->pos = $pos[$i];
            $lesson->update();
            $i++;
        }

        //--- Logic Section Ends

        //--- Redirect Section
        $msg = __('Question Sorted Successfully.');
        return redirect()->route('instructor-course-curriculam',$data->section->course_id)->with('success',$msg);
        //--- Redirect Section Ends

    }


   //*** GET Request
   public function delete($id)
   {
       //--- Logic Section

       $data = Question::find($id);
       $data->delete();

       //--- Logic Section Ends

       //--- Redirect Section

        $msg = __('Question Deleted Successfully.');

       return redirect()->route('instructor-course-curriculam',$data->lesson->section->course_id)->with('success',$msg);

       //--- Redirect Section Ends

   }


}
