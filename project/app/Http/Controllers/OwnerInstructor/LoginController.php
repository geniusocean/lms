<?php

namespace App\Http\Controllers\Ownerinstructor;

use App\Classes\GeniusMailer;
use App\Models\Generalsetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Admin;
use App\Models\User;
use Validator;


class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout', 'userLogout']]);
    }

    public function showLoginForm($name=null)
    {
    
      $this->code_image();
      return view('owner-instructor.login');
    }

    public function login(Request $request,$name=null)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();

      $user  = User::where('email',$request->email)->where('register_id',$owner->id)->first();

      if($user){

        //--- Validation Section
        $rules = [
                  'email'   => 'required|email',
                  'password' => 'required'
                ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        
      if(!User::where('register_id',$owner->id)->where('is_instructor',2)->where('email',$request->email)->exists()){
        return response()->json(array('errors' => [ 0 => 'Credentials Doesn\'t Match !' ]));
      }
      // Attempt to log the user in
      if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        return response()->json(route('owner-instructor-dashboard',$owner->username));
      }

      // if unsuccessful, then redirect back to the login with the form data
          return response()->json(array('errors' => [ 0 => 'Credentials Doesn\'t Match !' ]));

        }else{
          return response()->json(array('errors' => [ 0 => 'Please do registration to this system.' ]));
        }

    }

  
    public function forgot(Request $request,$name=null)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $owner_name = $owner->name;

      $gs = Generalsetting::where('register_id',$owner->id)->first();
      $input =  $request->all();
      if (Instructor::where('email', '=', $request->email)->count() > 0) {
      // user found
      $instructor = Instructor::where('email', '=', $request->email)->firstOrFail();
      $token = md5(time().$instructor->name.$instructor->email);

      $file = fopen(public_path().'/project/storage/tokens/'.$token.'.data','w+');
      fwrite($file,$instructor->id);
      fclose($file);

      $subject = "Reset Password Request";
      $msg = "Please click this link : ".'<a href="'.route('owner-instructor.change.token',$token).'">'.route('owner-instructor.change.token',$token).'</a>'.' to change your password.';
      if($gs->is_smtp == 1)
      {
          $data = [
                  'to' => $request->email,
                  'subject' => $subject,
                  'body' => $msg,
          ];

          $mailer = new GeniusMailer();
          $mailer->sendCustomMail($data);
      }
      else
      {
          $headers = "From: ".$gs->from_name."<".$gs->from_email.">";
          mail($request->email,$subject,$msg,$headers);
      }
      return response()->json('Verification Link Sent Successfully!. Please Check your email.');
      }
      else{
      // user not found
      return response()->json(array('errors' => [ 0 => 'No Account Found With This Email.' ]));
      }
    }

    public function showChangePassForm($token)
    {
      if (file_exists(public_path().'/project/storage/tokens/'.$token.'.data')){
        $id = file_get_contents(public_path().'/project/storage/tokens/'.$token.'.data');
        return view('admin.changepass',compact('id','token'));
      }
    }

    
    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect('/');
    }

     // Capcha Code Image
     private function  code_image()
     {
         $actual_path = str_replace('project','',base_path());
         $image = imagecreatetruecolor(200, 50);
         $background_color = imagecolorallocate($image, 255, 255, 255);
         imagefilledrectangle($image,0,0,200,50,$background_color);

         $pixel = imagecolorallocate($image, 0,0,255);
         for($i=0;$i<500;$i++)
         {
             imagesetpixel($image,rand()%200,rand()%50,$pixel);
         }

         $font = $actual_path.'assets/front/fonts/NotoSans-Bold.ttf';
         $allowed_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
         $length = strlen($allowed_letters);
         $letter = $allowed_letters[rand(0, $length-1)];
         $word='';
         //$text_color = imagecolorallocate($image, 8, 186, 239);
         $text_color = imagecolorallocate($image, 0, 0, 0);
         $cap_length=6;// No. of character in image
         for ($i = 0; $i< $cap_length;$i++)
         {
             $letter = $allowed_letters[rand(0, $length-1)];
             imagettftext($image, 25, 1, 35+($i*25), 35, $text_color, $font, $letter);
             $word.=$letter;
         }
         $pixels = imagecolorallocate($image, 8, 186, 239);
         for($i=0;$i<500;$i++)
         {
             imagesetpixel($image,rand()%200,rand()%50,$pixels);
         }
         session(['captcha_string' => $word]);
         imagepng($image, $actual_path."assets/images/capcha_code.png");
     }
}
