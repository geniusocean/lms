<?php

namespace App\Http\Controllers\Ownerinstructor;

use App\Models\Generalsetting;

use Illuminate\Http\Request;
use App\Classes\GeniusMailer;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Support\Str;

class ForgotController extends Controller
{


    public function showForgotForm()
    {

      return view('instructor.forgot');
    }

    public function forgot(Request $request,$name=null)
    {
      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $owner_id = $owner->id;

      $gs = Generalsetting::where('register_id',$owner_id)->first();

      $input =  $request->all();
      if (User::where('email', '=', $request->email)->where('register_id',$owner_id)->count() > 0) {
      // user found
      $admin = User::where('email', '=', $request->email)->where('register_id',$owner_id)->first();
      $autopass = Str::random(8);
      $input['password'] = bcrypt($autopass);
      $admin->update($input);
      $subject = "Reset Password Request";
      $msg = "Your New Password is : ".$autopass;
      if($gs->is_smtp == 1)
      {
          $data = [
                  'to' => $request->email,
                  'subject' => $subject,
                  'body' => $msg,
          ];

          $mailer = new GeniusMailer();
          $mailer->sendCustomMail($data);                
      }
      else
      {
          $headers = "From: ".$gs->from_name."<".$gs->from_email.">";
          mail($request->email,$subject,$msg,$headers);            
      }
      return response()->json('Your Password Reseted Successfully. Please Check your email for new Password.');
      }
      else{
      // user not found
      return response()->json(array('errors' => [ 0 => 'No Account Found With This Email.' ]));    
      }  
    }

}
