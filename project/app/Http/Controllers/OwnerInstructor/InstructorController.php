<?php

namespace App\Http\Controllers\Ownerinstructor;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Models\Admin;
use App\Models\Referral;
use App\Models\ReferralHistory;
use App\Models\Order;

class InstructorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index ($name=null)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $data['days'] = "";
        $data['sales'] = "";
        for($i = 0; $i < 30; $i++) {
            $data['days'] .= "'".date("d M", strtotime('-'. $i .' days'))."',";
            $data['sales'] .=  "'".Order::whereHas('instructororders', function($query) use ($user_id) {
                $query->where('user_id', '=', $user_id);
            })->whereDate('created_at', '=', date("Y-m-d", strtotime('-'. $i .' days')))->count()."',";
        }

        return view('owner-instructor.dashboard',$data);
    }


    public function profile($name=null)
    {
        $data = Auth::user();
        return view('owner-instructor.profile',compact('data'));
    }


    public function profileupdate(Request $request,$name=null)
      {

        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_name = $owner->name;

        $data = Auth::user();
         //--- Validation Section
         $rules = [
          'photo' => 'mimes:jpeg,jpg,png,svg',
          'first_name' => 'required',
          'last_name' => 'required',
          'email' => 'unique:users,email,'.$data->id,
          ];

      $customs = [
          'photo.mimes' => __('Thumbnail Type is Invalid.'),
          'email.unique' => __('This email has already been taken.')
          ];
      $validator = Validator::make($request->all(), $rules, $customs);

      if ($validator->fails()) {
        return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
      }
      //--- Validation Section Ends\\

      // logic section //

      $input = $request->all();


      $input['password'] = bcrypt($request['password']);
      if ($file = $request->file('photo'))
      {
        if(file_exists(base_path('../assets/'.$owner_name.'/owner/images/users/'.$data->photo))){
          unlink(base_path('../assets/'.$owner_name.'/owner/images/users/'.$data->photo));
        }
         $name = time().str_replace(' ', '', $file->getClientOriginalName());
         $file->move('assets/'.$owner_name.'/owner/images/users',$name);
         $input['photo'] = $name;
     }


     $data->update($input);

     $mgs = __('Data Update Successfully');
     return response()->json($mgs);

    }


    public function resetform()
    {
        return view('owner-instructor.password');
    }

    public function changepass(Request $request)
    {
        $instructor = Auth::user();

        if ($request->cpass){
            if (Hash::check($request->cpass, $instructor->password)){
                if ($request->newpass == $request->renewpass){
                    $input['password'] = Hash::make($request->newpass);
                }else{
                    return response()->json(array('errors' => [ 0 => __('Confirm password does not match.') ]));
                }
            }else{
                return response()->json(array('errors' => [ 0 => __('Current password Does not match.') ]));
            }
        }
        $instructor->update($input);
        $msg = __('Successfully change your password');
        return response()->json($msg);
    }

    public function affilate($name=null)
    {
        
        $instructor = Auth::user();
        $refer = null;
        if($instructor->referral_id != 0){
            $refer = Referral::find($instructor->referral_id);
        }

        $refers = Referral::whereRegisterId($instructor->register_id)->whereType('Instructor')->orderBy('id')->get();
        return view('owner-instructor.affilate.index',compact('refers','refer','instructor'));
    }

    public function affilate_history($name=null)
    {
        $instructor = Auth::user();
        $refers = ReferralHistory::whereRegisterId($instructor->register_id)->where('referrer_id',$instructor->id)->orderBy('id')->get();
        return view('owner-instructor.affilate.history',compact('instructor','refers'));
    }
}
