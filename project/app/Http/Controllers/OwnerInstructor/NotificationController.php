<?php

namespace App\Http\Controllers\OwnerInstructor;

use App\Http\Controllers\Controller;
use App\Models\UserNotification;
use Auth;
use App\Models\Admin;

class NotificationController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function show_notf($name=null){
        return view('owner-load.instructor-notification');
    }
  
    public function read($name=null){

      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $owner_id = $owner->id;

        $instructor = Auth::user();
        $datas = UserNotification::where('register_id',owner_id)->where('user_id',$instructor->id)->get();
        if($datas->count() > 0){
          foreach($datas as $data){
            $data->is_read = 1;
            $data->update();
          }
        } 
    }
  
    public function notf_clear($name=null){


      $owner = Admin::where('username',$name)->where('role','Owner')->first();
      $owner_id = $owner->id;

        $instructor = Auth::user();
        $datas = UserNotification::where('register_id',$owner_id)->where('user_id',$instructor->id)->get();
        if($datas->count() > 0){
            foreach($datas as $data){
              $data->delete();
            }
        } 
    } 

}