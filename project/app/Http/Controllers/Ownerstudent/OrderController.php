<?php

namespace App\Http\Controllers\Ownerstudent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Order;
use App\Models\Admin;
use App\Models\PaymentGateway;

class OrderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function orders($name=null)
    { 
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $user = Auth::guard('web')->user();
        $orders = Order::where('user_id','=',$user->id)->where('register_id',$owner->id)->orderBy('id','desc')->get();
  
        return view('owner-student.order.index',compact('user','orders'));
    }


    public function order($name=null,$id)
    {
        $user = Auth::guard('web')->user();
        $order = Order::findOrfail($id);
        $cart = json_decode($order->cart, true);
        return view('owner-student.order.details',compact('user','order','cart'));
    }



    public function orderprint($name=null,$id)
    {
        $user = Auth::guard('web')->user();
        $order = Order::findOrfail($id);
        $cart = json_decode($order->cart, true);
        return view('owner-student.order.print',compact('user','order','cart'));
    }



}
