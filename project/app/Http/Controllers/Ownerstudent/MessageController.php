<?php

namespace App\Http\Controllers\Ownerstudent;

use App\Classes\GeniusMailer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Conversation;
use App\Models\Message;
use App\Models\Generalsetting;
use App\Models\Notification;
use App\Models\User;
use App\Models\Admin;


class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

   public function messages($name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();

        $user = Auth::guard('web')->user();
        $convs = Conversation::where('register_id',$owner->id)->where('sent_user','=',$user->id)->orWhere('recieved_user','=',$user->id)->get();

        $path = explode('/',request()->path());
      
        if($path[1] == 'student'){
            
            return view('owner-student.message.index',compact('user','convs'));
        }else{
            if($user->is_instructor == 2){
                return view('owner-instructor.message.index',compact('user','convs'));
            }else{
                return view('owner-student.message.index',compact('user','convs'));
            }
        }
    }

    public function message($name=null,$id)
    {

        $user = Auth::guard('web')->user();
        $conv = Conversation::findOrfail($id);
          

            $path = explode('/',request()->path());
            
            if($path[1] == 'student'){
                return view('owner-student.message.create',compact('user','conv'));
            }else{
                if($user->is_instructor == 2){
                    return view('owner-instructor.message.create',compact('user','conv'));
                }else{
                    return view('owner-student.message.create',compact('user','conv'));
                }
            }


    }

    public function messagedelete($name=null,$id)
    {
            $conv = Conversation::findOrfail($id);
            if($conv->messages->count() > 0)
            {
                foreach ($conv->messages as $key) {
                    $key->delete();
                }
            }
            $conv->delete();
            $mgs = __('Message Deleted Successfully');
            return response()->json($mgs);
    }

    public function msgload($name=null,$id)
    {
            $conv = Conversation::findOrfail($id);
            return view('owner-load.usermsg',compact('conv'));
    }

    //Send email to user
    public function studentcontact(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
  
        $data = 1;
        $user = User::findOrFail($request->user_id);
        $instructor = User::where('email','=',$request->email)->first();
        if(empty($instructor))
        {
            $data = 0;
            return response()->json($data);
        }

        $subject = $request->subject;
        $to = $instructor->email;
        $name = $request->name;
        $from = $request->email;
        $msg = "Name: ".$name."\nEmail: ".$from."\nMessage: ".$request->message;
        $gs = Generalsetting::where('register_id',$owner_id)->first();
        if($gs->is_smtp == 1)
        {
        $data = [
            'to' => $instructor->email,
            'subject' => $request->subject,
            'body' => $msg,
        ];

        $mailer = new GeniusMailer();
        $mailer->sendCustomMail($data);
        }
        else
        {
        $headers = "From: ".$gs->from_name."<".$gs->from_email.">";
        mail($to,$subject,$msg,$headers);
        }

        $conv = Conversation::where('sent_user','=',$user->id)->where('subject','=',$subject)->first();
        if(isset($conv)){
            $msg = new Message();
            $msg->conversation_id = $conv->id;
            $msg->message = $request->message;
            $msg->sent_user = $user->id;
            $msg->register_id = $owner_id;
            $msg->save();
            return response()->json($data);
        }
        else{
            $message = new Conversation();
            $message->subject = $subject;
            $message->sent_user= $request->user_id;
            $message->recieved_user = $instructor->id;
            $message->message = $request->message;
            $message->register_id = $owner_id;
            $message->save();

            $msg = new Message();
            $msg->conversation_id = $message->id;
            $msg->message = $request->message;
            $msg->sent_user = $request->user_id;
            $msg->register_id = $owner_id;
            $msg->save();
            return response()->json($data);
        }
    }

    public function postmessage(Request $request,$name=null)
    {

        $owner = Admin::where('username',$name)->where('role','Owner')->first();

        $msg = new Message();
        $input = $request->all();
        $input['register_id'] = $owner->id;
        $msg->fill($input)->save();
        //--- Redirect Section
        $msg = 'Message Sent!';
        return response()->json($msg);
        //--- Redirect Section Ends
    }

 

  
}
