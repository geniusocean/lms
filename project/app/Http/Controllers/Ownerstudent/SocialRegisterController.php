<?php

namespace App\Http\Controllers\Ownerstudent;

use App\Models\Notification;
use App\Models\SocialProvider;
use App\Models\Socialsetting;
use App\Models\User;
use App\Models\Admin;
use App\Http\Controllers\Controller;
use Auth;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Socialite;

class SocialRegisterController extends Controller
{

    public function __construct()
    {
      $link = Socialsetting::findOrFail(1);
      Config::set('services.google.client_id', $link->gclient_id);
      Config::set('services.google.client_secret', $link->gclient_secret);
      Config::set('services.google.redirect', url('/auth/google/callback'));
      Config::set('services.facebook.client_id', $link->fclient_id);
      Config::set('services.facebook.client_secret', $link->fclient_secret);
      $url = url('/auth/facebook/callback');
      $url = preg_replace("/^http:/i", "https:", $url);
      Config::set('services.facebook.redirect', $url);
    }

    public function redirectToProvider($name=null,$provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($name=null,$provider)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;
  
        try
        {
            $socialUser = Socialite::driver($provider)->user();
        }
        catch(\Exception $e)
        {
            return redirect('/');
        }
        //check if we have logged provider
        $socialProvider = SocialProvider::where('provider_id',$socialUser->getId())->first();
        if(!$socialProvider)
        {

            if(User::where('email',$socialUser->email)->exists())
            {
                $auser = User::where('email',$socialUser->email)->first();
                Auth::guard('web')->login($auser); 
                return redirect()->route('owner-student-dashboard');
            }
            //create a new user and provider
            $user = new User;
            $user->email = $socialUser->email;
            $user->first_name = $socialUser->name;
            $user->last_name = $socialUser->name;
            $user->name = $socialUser->name;
            $user->photo = $socialUser->avatar_original;
            $user->register_id = $owner_id;
            $user->save();

            $user->socialProviders()->create(
                ['provider_id' => $socialUser->getId(), 'provider' => $provider]
            );
            $notification = new Notification;
            $notification->user_id = $user->id;
            $notification->save();

        }
        else
        {


            if(User::where('email',$socialUser->email)->exists())
            {
                $auser = User::where('email',$socialUser->email)->first();
                Auth::guard('web')->login($auser); 
                return redirect()->route('owner-student-dashboard');
            }

            $user = $socialProvider->user;
        }

        Auth::guard('web')->login($user); 
        return redirect()->route('owner-student-dashboard');

    }
}
