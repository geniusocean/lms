<?php

namespace App\Http\Controllers\Ownerstudent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Generalsetting;
use App\Models\Admin;
use App\Models\User;
use App\Classes\GeniusMailer;
use App\Models\Notification;
use Auth;
use Validator;

class RegisterController extends Controller
{

    public function register(Request $request,$name=null)
    {

		$owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;


    	$gs = Generalsetting::where('register_id',$owner_id);

    	if($gs->is_capcha == 1)
    	{
	        $value = session('captcha_string');
	        if ($request->codes != $value){
	            return response()->json(array('errors' => [ 0 => 'Please enter Correct Capcha Code.' ]));    
	        }    		
    	}

		if(User::where('email',$request->email)->where('register_id',$owner_id)->exists()){
			return response()->json(array('errors' => [0 =>'This email has already been taken.']));
		}

        //--- Validation Section

        $rules = [
		        'password' => 'required'
                ];
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

	        $user = new Student;
	        $input = $request->all();        
	        $input['password'] = bcrypt($request['password']);
	        $input['register_id'] = $owner_id;
	        $token = md5(time().$request->name.$request->email);
	        
			$user->fill($input)->save();

	        if($gs->is_verification_email == 1)
	        {
	        $to = $request->email;
	        $subject = 'Verify your email address.';
	        $msg = "Dear Customer,<br> We noticed that you need to verify your email address. <a href=".url('user/register/verify/'.$token).">Simply click here to verify. </a>";
	        //Sending Email To Customer
	        if($gs->is_smtp == 1)
	        {
	        $data = [
	            'to' => $to,
	            'subject' => $subject,
	            'body' => $msg,
	        ];

	        $mailer = new GeniusMailer();
	        $mailer->sendCustomMail($data);
	        }
	        else
	        {
	        $headers = "From: ".$gs->from_name."<".$gs->from_email.">";
	        mail($to,$subject,$msg,$headers);
	        }
          	return response()->json('We need to verify your email address. We have sent an email to '.$to.' to verify your email address. Please click link in that email to continue.');
	        }
	        else {
            $user->update();
	        $notification = new Notification;
			$notification->user_id = $user->id;
			
	        $notification->save();
			Auth::guard('web')->login($user);
			 
          	return response()->json(['status'=>1,'route'=>route('owner-student-dashboard')]);
	        }

    }

 
}