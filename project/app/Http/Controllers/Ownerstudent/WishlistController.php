<?php

namespace App\Http\Controllers\Ownerstudent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wishlist;
use App\Models\Course;
use App\Models\Admin;
use Auth;


class WishlistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function wishlists(Request $request,$name=null)
    {
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $owner_id = $owner->id;

        $user = Auth::user();
        $wishes = Wishlist::where('user_id','=',$user->id)->where('register_id',$owner->id)->pluck('course_id');
        $wishlists = Course::where('status','=',1)->whereIn('id',$wishes)->where('register_id',$owner_id)->latest('id')->paginate(8);
        if($request->ajax())
        {
            return view('owner-front.ajax.wishlist',compact('user','wishlists'));            
        }
        return view('owner-student.wishlist',compact('user','wishlists'));
    }

    public function addwish(Request $request,$name=null,$id)
    {      
        $owner = Admin::where('username',$name)->where('role','Owner')->first();
        $user = Auth::user();
        $data[0] = 0; 
        $ck = Wishlist::where('user_id','=',$user->id)->where('course_id','=',$id)->get()->count();
        if($ck > 0)
        {
            $data['error'] = __('Already Added To The Wishlist.');
            if($request->ajax())
            {
                return response()->json($data);
            }
            else{
                return redirect()->route('owner-student-wishlists')->with('error',$data['error']);
            } 
        }
        $wish = new Wishlist();
        $wish->user_id = $user->id;
        $wish->course_id = $id;
        $wish->register_id = $owner->id;
        $wish->save();
        $data[0] = 1; 
        $data['success'] = __('Successfully Added To The Wishlist.');

        if($request->ajax())
        {
            return response()->json($data);
        }
        else{
            return redirect()->route('owner-student-wishlists',$owner->username)->with('success',$data['success']);
        } 

    }

    public function removewish(Request $request,$name=null,$id)
    {
        $user = Auth::user();
        $wish = Wishlist::findOrFail($id);
        $wish->delete();        
        $data[0] = 1; 
        $data['success'] = __('Successfully Removed From Wishlist.');
        return response()->json($data);      
    }

}