<?php

namespace App\Http\Controllers\Instructor;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\Order;
use App\Models\User;
use App\Models\Referral;
use App\Models\ReferralHistory;
use Illuminate\Http\Request;
use Auth;
use Validator;

class InstructorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index ()
    {

        $user = Auth::user();
        $user_id = $user->id;
        $data['days'] = "";
        $data['sales'] = "";
        for($i = 0; $i < 30; $i++) {
            $data['days'] .= "'".date("d M", strtotime('-'. $i .' days'))."',";
            $data['sales'] .=  "'".Order::whereHas('instructororders', function($query) use ($user_id) {
                $query->where('user_id', '=', $user_id);
            })->whereDate('created_at', '=', date("Y-m-d", strtotime('-'. $i .' days')))->count()."',";
        }


        return view('instructor.dashboard',$data);
    }


    public function profile()
    {
        $data = Auth::user();
        return view('instructor.profile',compact('data'));
    }


    public function profileupdate(Request $request )
      {
        $data = Auth::user();

        if(User::where('email',$request->email)->where('id','!=',$data->id)->where('register_id',0)->exists()){
            return response()->json(array('errors' => [0 =>'This email has already been taken.']));
        }
        if(User::where('instructor_name',$request->instructor_name)->where('id','!=',$data->id)->where('register_id',0)->exists()){
            return response()->json(array('errors' => [0 =>'This instructor name has already been taken.']));
        }
         //--- Validation Section
         $rules = [
          'photo' => 'mimes:jpeg,jpg,png,svg',
          'first_name' => 'required',
          'last_name' => 'required',
          ];

      $customs = [
          'photo.mimes' => __('Thumbnail Type is Invalid.'),
          ];
      $validator = Validator::make($request->all(), $rules, $customs);

      if ($validator->fails()) {
        return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
      }
      //--- Validation Section Ends\\

      // logic section //

      $input = $request->all();

      $input['password'] = bcrypt($request['password']);
      if ($file = $request->file('image'))
      {
        if(file_exists(base_path('../assets/images/instructors/'.$data->image))){
          unlink(base_path('../assets/images/instructors/'.$data->image));
        }
         $name = time().str_replace(' ', '', $file->getClientOriginalName());
         $file->move('assets/images/instructors',$name);
         $input['image'] = $name;
     }


     $data->update($input);

     $mgs = __('Data Update Successfully');
     return response()->json($mgs);

    }


    public function resetform()
    {
        return view('instructor.password');
    }

    public function changepass(Request $request)
    {
        $instructor = Auth::user();

        if ($request->cpass){
            if (Hash::check($request->cpass, $instructor->password)){
                if ($request->newpass == $request->renewpass){
                    $input['password'] = Hash::make($request->newpass);
                }else{
                    return response()->json(array('errors' => [ 0 => __('Confirm password does not match.') ]));
                }
            }else{
                return response()->json(array('errors' => [ 0 => __('Current password Does not match.') ]));
            }
        }
        $instructor->update($input);
        $msg = __('Successfully change your password');
        return response()->json($msg);
    }

    public function affilate()
    {
        $instructor = Auth::user();
        $refer = null;
        if($instructor->referral_id != 0){
            $refer = Referral::find($instructor->referral_id);
        }

        $refers = Referral::whereRegisterId(0)->whereType('Instructor')->orderBy('id')->get();
        return view('instructor.affilate.index',compact('refers','refer','instructor'));
    }

    public function affilate_history()
    {
        $instructor = Auth::user();
        $refers = ReferralHistory::whereRegisterId(0)->where('referrer_id',$instructor->id)->orderBy('id')->get();
        return view('instructor.affilate.history',compact('instructor','refers'));
    }

}
