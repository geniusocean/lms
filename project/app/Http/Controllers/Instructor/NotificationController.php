<?php

namespace App\Http\Controllers\Instructor;

use App\Http\Controllers\Controller;
use App\Models\UserNotification;
use Auth;

class NotificationController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function show_notf(){
        return view('load.instructor-notification');
    }
  
    public function read(){
        $instructor = Auth::user();
        $datas = UserNotification::where('register_id',0)->where('user_id',$instructor->id)->get();
        if($datas->count() > 0){
          foreach($datas as $data){
            $data->is_read = 1;
            $data->update();
          }
        } 
    }
  
    public function notf_clear(){
        $instructor = Auth::user();
        $datas = UserNotification::where('register_id',0)->where('user_id',$instructor->id)->get();
        if($datas->count() > 0){
            foreach($datas as $data){
              $data->delete();
            }
        } 
    } 

}