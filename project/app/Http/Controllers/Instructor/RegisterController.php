<?php

namespace App\Http\Controllers\Instructor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Generalsetting;
use App\Models\User;
use App\Classes\GeniusMailer;
use App\Models\Notification;
use Auth;

use Validator;

class RegisterController extends Controller
{

    public function register(Request $request)
    {

    	$gs = Generalsetting::findOrFail(1);

    	if($gs->is_capcha == 1)
    	{
	        $value = session('captcha_string');
	        if ($request->codes != $value){
	            return response()->json(array('errors' => [ 0 => 'Please enter Correct Capcha Code.' ]));
	        }
    	}



        //--- Validation Section

        $rules = [
		        'email'   => 'required|email|unique:users',
				'password' => 'required',
				'address'      => 'required',
				'phone'      => 'required',
				'instructor_name'      => 'required|unique:users,instructor_name',
				'document'       => 'required|mimes:doc,docs,pdf,txt,png,jpg,jpeg',
                ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
		}

        //--- Validation Section Ends

	        $user = new User;
			$input = $request->all();

			if($request->file('document')){
                $file = $request->document;
                $name = time().str_replace(' ', '', $file->getClientOriginalName());
                $file->move('assets/application/',$name);
                $input['document'] = $name;
             }


            $input['affilate_code'] = md5($request->first_name.$request->last_name.$request->email);

			$input['is_instructor'] = 1;
			$input['instructor_slug'] = str_replace(" ","-",$input['instructor_name']);

	        $input['password'] = bcrypt($request['password']);
			$user->fill($input)->save();

	        if($gs->is_verification_email == 1)
	        {
	        $to = $request->email;
	        $subject = 'Verify your email address.';
	        $msg = "Dear Customer,<br> We noticed that you need to verify your email address. <a href=".url('user/register/verify/'.$token).">Simply click here to verify. </a>";
	        //Sending Email To Customer
	        if($gs->is_smtp == 1)
	        {
	        $data = [
	            'to' => $to,
	            'subject' => $subject,
	            'body' => $msg,
	        ];

	        $mailer = new GeniusMailer();
	        $mailer->sendCustomMail($data);
	        }
	        else
	        {
	        $headers = "From: ".$gs->from_name."<".$gs->from_email.">";
	        mail($to,$subject,$msg,$headers);
	        }
          	return response()->json('We need to verify your email address. We have sent an email to '.$to.' to verify your email address. Please click link in that email to continue.');
	        }
	        else {
            $user->update();
	        $notification = new Notification;
			$notification->user_id = $user->id;

	        $notification->save();
			Auth::guard('web')->login($user);

          	return response()->json(['status'=>1,'route'=>route('student-instructor-application')]);
	        }


    }

    public function token($token)
    {
        $gs = Generalsetting::findOrFail(1);

        if($gs->is_verification_email == 1)
	        {
        $user = User::where('verification_link','=',$token)->first();
        if(isset($user))
        {
            $user->email_verified = 'Yes';
            $user->update();
	        $notification = new Notification;
	        $notification->user_id = $user->id;
	        $notification->save();
            Auth::guard('web')->login($user);
            return redirect()->route('user-dashboard')->with('success','Email Verified Successfully');
        }
    		}
    		else {
    		return redirect()->back();
    		}
    }
}
