<?php

// ************************************ ADMIN SECTION **********************************************


Route::group(['middleware'=>'DeactivatedOwner'],function(){

Route::prefix('/{name?}')->group(function() {

Route::prefix('admin')->group(function() {
  //------------ ADMIN LOGIN SECTION ------------

  Route::get('/login', 'Owner\LoginController@showLoginForm')->name('owner.login');
  Route::post('/login', 'Owner\LoginController@login')->name('owner.login.submit');
  Route::get('/forgot', 'Owner\LoginController@showForgotForm')->name('owner.forgot');
  Route::post('/forgot', 'Owner\LoginController@forgot')->name('owner.forgot.submit');
  Route::get('/change-password/{token}', 'Owner\LoginController@showChangePassForm')->name('owner.change.token');
  Route::post('/change-password', 'Owner\LoginController@changepass')->name('owner.change.password');
  Route::get('/logout', 'Owner\LoginController@logout')->name('owner.logout');

  //------------ ADMIN LOGIN SECTION ENDS ------------

  Route::group(['middleware'=>'owneradmin','owner'],function(){

  //------------ ADMIN DASHBOARD & PROFILE SECTION ------------
  Route::get('/', 'Owner\DashboardController@index')->name('owner.dashboard');
  Route::get('/profile', 'Owner\DashboardController@profile')->name('owner.profile');
  Route::post('/profile/update', 'Owner\DashboardController@profileupdate')->name('owner.profile.update');
  Route::get('/password', 'Owner\DashboardController@passwordreset')->name('owner.password');
  Route::post('/password/update', 'Owner\DashboardController@changepass')->name('owner.password.update');
  //------------ ADMIN DASHBOARD & PROFILE SECTION ENDS ------------

  Route::get('/show-notification', 'Owner\NotificationController@show_notf')->name('owner.notification.show');
  Route::get('/read-notification', 'Owner\NotificationController@read')->name('owner.notification.read');
  Route::get('/delete-notification', 'Owner\NotificationController@notf_clear')->name('owner.delete.clear');



  Route::group(['middleware'=>'ownerpermissions:Manage Instructors'],function(){

//------------ ADMIN INSTRUCTORS SECTION ------------
    Route::get('/instructor/datatables', 'Owner\InstructorController@datatables')->name('owner-instructor-datatables'); //JSON REQUEST
    Route::get('/instructor', 'Owner\InstructorController@index')->name('owner-instructor-index');
    Route::get('/instructor/create', 'Owner\InstructorController@create')->name('owner-instructor-create');
    Route::post('/instructor/create', 'Owner\InstructorController@store')->name('owner-instructor-store');
    Route::get('/instructor/edit/{id}', 'Owner\InstructorController@edit')->name('owner-instructor-edit');
    Route::get('/instructor/curriculam/{id}', 'Owner\InstructorController@curriculam')->name('owner-instructor-curriculam');
    Route::post('/instructor/update/{id}', 'Owner\InstructorController@update')->name('owner-instructor-update');
    Route::get('/instructor/delete/{id}', 'Owner\InstructorController@destroy')->name('owner-instructor-delete');
    Route::get('/instructor/status/{id1}/{id2}', 'Owner\Instructorontroller@status')->name('owner-instructor-status');

    // INSTRUCTOR REQUEST APPLICATION
    Route::get('/application/datatables', 'Owner\InstructorController@applicationdatatables')->name('owner-instructor-application-datatables'); //JSON REQUEST
    Route::get('/applications', 'Owner\InstructorController@applications')->name('owner-instructor-application');
    Route::get('/application/details/{id}', 'admin\instructorcontroller@applicationsdetails')->name('owner.application.details');
    Route::get('/application/status/{id1}/{id2}', 'admin\instructorcontroller@applicationstatus')->name('owner.application.status');
    Route::get('/application/delete/{id}', 'admin\instructorcontroller@applicationdelete')->name('owner.application.delete');
    Route::get('/application/download/{id}', 'admin\instructorcontroller@applicationdownload')->name('owner.application.download');


    //------------ ADMIN WITHDRAW SECTION ------------

    Route::get('/instructor/withdraws/datatables', 'Owner\InstructorController@withdrawdatatables')->name('owner-withdraw-datatables'); //JSON REQUEST
    Route::get('/instructor/withdraws', 'Owner\InstructorController@withdraws')->name('owner-withdraw-index');
    Route::get('/instructor/withdraw/{id}/show', 'Onwer\InstructorController@withdrawdetails')->name('owner-withdraw-show');
    Route::get('/instructor/withdraws/accept/{id}','Owner\Owner\InstructorController@accept')->name('owner-withdraw-accept');
    Route::get('/instructor/withdraws/reject/{id}', 'Owner\InstructorController@reject')->name('owner-withdraw-reject');

    //------------ ADMIN WITHDRAW SECTION ENDS------------

  });


  Route::group(['middleware'=>'ownerpermissions:Manage Students'],function(){

  //------------ ADMIN STUDENT SECTION ------------

    Route::get('/student/datatables', 'Owner\CategoryController@datatables')->name('owner-student-datatables'); //JSON REQUEST
    Route::get('/student', 'Owner\StudentController@index')->name('owner-student-index');
    Route::get('/student/create', 'Owner\StudentController@create')->name('owner-student-create');
    Route::post('/student/create', 'Owner\StudentController@store')->name('owner-student-store');
    Route::get('/student/edit/{id}', 'Owner\StudentController@edit')->name('owner-student-edit');
    Route::get('/student/curriculam/{id}', 'Owner\StudentController@curriculam')->name('owner-student-curriculam');
    Route::post('/student/update/{id}', 'Owner\StudentController@update')->name('owner-student-update');
    Route::get('/student/delete/{id}', 'Owner\StudentController@destroy')->name('owner-student-delete');
    Route::get('/student/status/{id1}/{id2}', 'Owner\StudentController@status')->name('owner-student-status');

    Route::get('/student/assign/', 'Owner\StudentController@assign')->name('owner-student-assign-modal');
    Route::post('/student/assign/course/', 'Owner\StudentController@assignCourse')->name('owner-student-assign-course');


    Route::get('/enroll/history/datatables', 'Owner\EnrollmentHistoryController@datatables')->name('owner-enroll-history-datatables'); //JSON REQUEST
    Route::get('/enroll/history', 'Owner\EnrollmentHistoryController@index')->name('owner-enroll-history-index');

    Route::get('/enroll/history/delete/{id}', 'Owner\EnrollmentHistoryController@destroy')->name('owner-enroll-history-delete');
    //------------ ADMIN STUDENT SECTION ENDS------------
  });


  Route::group(['middleware'=>'ownerpermissions:Manage Categories'],function(){

  //------------ ADMIN CATEGORY SECTION ------------
    Route::get('/category/datatables', 'Owner\CategoryController@datatables')->name('owner-cat-datatables'); //JSON REQUEST
    Route::get('/category', 'Owner\CategoryController@index')->name('owner-cat-index');
    Route::get('/category/create', 'Owner\CategoryController@create')->name('owner-cat-create');
    Route::post('/category/store', 'Owner\CategoryController@store')->name('owner-cat-store');
    Route::get('/category/edit/{id}', 'Owner\CategoryController@edit')->name('owner-cat-edit');
    Route::post('/category/update/{id}', 'Owner\CategoryController@update')->name('owner-cat-update');
    Route::get('/category/delete/{id}', 'Owner\CategoryController@destroy')->name('owner-cat-delete');
    Route::get('/category/status/{id1}/{id2}', 'Owner\CategoryController@status')->name('owner-cat-status');


      //------------ ADMIN SUBCATEGORY SECTION ------------

      Route::get('/subcategory/datatables', 'Owner\SubCategoryController@datatables')->name('owner-subcat-datatables'); //JSON REQUEST
      Route::get('/subcategory', 'Owner\SubCategoryController@index')->name('owner-subcat-index');
      Route::get('/subcategory/create', 'Owner\SubCategoryController@create')->name('owner-subcat-create');
      Route::post('/subcategory/create', 'Owner\SubCategoryController@store')->name('owner-subcat-store');
      Route::get('/subcategory/edit/{id}', 'Owner\SubCategoryController@edit')->name('owner-subcat-edit');
      Route::post('/subcategory/edit/{id}', 'Owner\SubCategoryController@update')->name('owner-subcat-update');
      Route::get('/subcategory/delete/{id}', 'Owner\SubCategoryController@destroy')->name('owner-subcat-delete');
      Route::get('/subcategory/status/{id1}/{id2}', 'Owner\SubCategoryController@status')->name('owner-subcat-status');
      Route::get('/load/subcategories/{id}/', 'Owner\SubCategoryController@load')->name('owner-subcat-load'); //JSON REQUEST
  
    //------------ ADMIN SUBCATEGORY SECTION ENDS------------


  //------------ ADMIN CATEGORY SECTION ENDS------------
});


Route::group(['middleware'=>'ownerpermissions:Manage Courses'],function(){
  //------------ ADMIN COURSE SECTION ------------

    Route::get('/course/datatables', 'Owner\CourseController@datatables')->name('owner-course-datatables'); //JSON REQUEST
    Route::get('/course', 'Owner\CourseController@index')->name('owner-course-index');
    Route::get('/course/create', 'Owner\CourseController@create')->name('owner-course-create');
    Route::post('/course/create', 'Owner\CourseController@store')->name('owner-course-store');
    Route::get('/course/edit/{id}', 'Owner\CourseController@edit')->name('owner-course-edit');
    Route::post('/course/update/{id}', 'Owner\CourseController@update')->name('owner-course-update');
    Route::get('/course/status/{id1}/{id2}', 'Owner\CourseController@status')->name('owner-course-status');
    Route::get('/course/delete/{id}', 'Owner\CourseController@destroy')->name('owner-course-delete');

    Route::get('/purchase/datatables', 'Owner\OrderController@datatables')->name('owner-purchase-datatables'); //JSON REQUEST
    Route::get('/purchase', 'Owner\OrderController@index')->name('owner-purchase-index');
    Route::get('/purchase/details/{id}', 'Owner\OrderController@purchasedetails')->name('owner.purchase.details');


    //------------ ADMIN CURRICULAM SECTION ------------
    Route::post('/course/section/sort/update/{id}', 'Owner\CurriculamController@sectionSortUpdate')->name('owner-section-sort-update');
    Route::post('/course/section/store/{id}', 'Owner\CurriculamController@sectionStore')->name('owner-section-store');
    Route::post('/course/section/update/{id}', 'Owner\CurriculamController@sectionUpdate')->name('owner-section-update');
    Route::get('/course/section/delete/{id}', 'Owner\CurriculamController@sectionDelete')->name('owner-section-delete');
    Route::post('/course/lesson/store', 'Owner\CurriculamController@lessonStore')->name('owner-lesson-store');
    Route::post('/course/lesson/update/{id}', 'Owner\CurriculamController@lessonUpdate')->name('owner-lesson-update');
    Route::get('/course/lesson/edit/{id}', 'Owner\CurriculamController@lessonEdit')->name('owner-lesson-edit');
    Route::get('/course/lesson/quiz/{id}', 'Owner\CurriculamController@lessonQuiz')->name('owner-lesson-quiz');
    Route::get('/course/lesson/sort/{id}', 'Owner\CurriculamController@lessonSort')->name('owner-lesson-sort');
    Route::get('/course/lesson/delete/{id}', 'Owner\CurriculamController@lessonDelete')->name('owner-lesson-delete');
    Route::post('/course/lesson/sort/update/{id}', 'Owner\CurriculamController@lessonSortUpdate')->name('owner-lesson-sort-update');
    Route::get('/course/curriculam/{id}', 'Owner\CurriculamController@curriculam')->name('owner-course-curriculam');
    Route::get('/course/curriculam/lesson/{id}', 'Owner\CurriculamController@curriculamLesson')->name('owner-course-curriculam-lesson');
    Route::post('/course/curriculam/quiz/result', 'Owner\CurriculamController@result')->name('owner-course-quiz-result');
    Route::get('/course/curriculam-status/{id}', 'Owner\CurriculamController@curriculamStatus')->name('owner-course-curriculam-status');

    //------------ ADMIN CURRICULAM SECTION ENDS ------------

    //------------ ADMIN QUESTION SECTION ------------

    Route::post('/course/lesson/quiz/question/store', 'Owner\QuestionController@store')->name('owner-question-store');
    Route::get('/course/lesson/quiz/question/edit/{id}', 'Owner\QuestionController@edit')->name('owner-question-edit');
    Route::post('/course/lesson/quiz/question/update/{id}', 'Owner\QuestionController@update')->name('owner-question-update');
    Route::post('/course/lesson/quiz/question/sort/update/{id}', 'Owner\QuestionController@sortUpdate')->name('owner-question-sort-update');
    Route::get('/course/lesson/quiz/question/delete/{id}', 'Owner\QuestionController@delete')->name('owner-question-delete');

    //------------ ADMIN QUESTION SECTION ENDS ------------

    //------------ ADMIN COURSE SECTION ENDS------------

  });


  Route::group(['middleware'=>'ownerpermissions:Manage Blog'],function(){
  //------------ ADMIN BLOG SECTION ------------

  Route::get('/blog/datatables', 'Owner\BlogController@datatables')->name('owner-blog-datatables'); //JSON REQUEST
  Route::get('/blog', 'Owner\BlogController@index')->name('owner-blog-index');
  Route::get('/blog/create', 'Owner\BlogController@create')->name('owner-blog-create');
  Route::post('/blog/create', 'Owner\BlogController@store')->name('owner-blog-store');
  Route::get('/blog/edit/{id}', 'Owner\BlogController@edit')->name('owner-blog-edit');
  Route::post('/blog/edit/{id}', 'Owner\BlogController@update')->name('owner-blog-update');
  Route::get('/blog/delete/{id}', 'Owner\BlogController@destroy')->name('owner-blog-delete');

  Route::get('/blog/category/datatables', 'Owner\BlogCategoryController@datatables')->name('owner-cblog-datatables'); //JSON REQUEST
  Route::get('/blog/category', 'Owner\BlogCategoryController@index')->name('owner-cblog-index');
  Route::get('/blog/category/create', 'Owner\BlogCategoryController@create')->name('owner-cblog-create');
  Route::post('/blog/category/create', 'Owner\BlogCategoryController@store')->name('owner-cblog-store');
  Route::get('/blog/category/edit/{id}', 'Owner\BlogCategoryController@edit')->name('owner-cblog-edit');
  Route::post('/blog/category/edit/{id}', 'Owner\BlogCategoryController@update')->name('owner-cblog-update');
  Route::get('/blog/category/delete/{id}', 'Owner\BlogCategoryController@destroy')->name('owner-cblog-delete');

  //------------ ADMIN BLOG SECTION ENDS ------------
});

  
Route::group(['middleware'=>'ownerpermissions:General Settings'],function(){

  //------------ ADMIN GENERAL SETTINGS SECTION ------------

  Route::get('/general-settings/logo', 'Owner\GeneralSettingController@logo')->name('owner-gs-logo');
  Route::get('/general-settings/favicon', 'Owner\GeneralSettingController@fav')->name('owner-gs-fav');
  Route::get('/general-settings/loader', 'Owner\GeneralSettingController@load')->name('owner-gs-load');
  Route::get('/general-settings/contents', 'Owner\GeneralSettingController@contents')->name('owner-gs-contents');
  Route::get('/general-settings/breadcumb', 'Owner\GeneralSettingController@breadcumb')->name('owner-gs-breadcumb');
  Route::get('/general-settings/footer', 'Owner\GeneralSettingController@footer')->name('owner-gs-footer');
  Route::get('/general-settings/affilate', 'Owner\GeneralSettingController@affilate')->name('owner-gs-affilate');
  Route::get('/general-settings/error-banner', 'Owner\GeneralSettingController@errorbanner')->name('owner-gs-error-banner');
  Route::get('/general-settings/popup', 'Owner\GeneralSettingController@popup')->name('owner-gs-popup');
  Route::get('/general-settings/maintenance', 'Owner\GeneralSettingController@maintain')->name('owner-gs-maintenance');

  //------------ ADMIN GENERAL SETTINGS JSON SECTION ------------

  // General Setting Section
  Route::get('/general-settings/home/{status}', 'Owner\GeneralSettingController@ishome')->name('owner-gs-ishome');
  Route::get('/general-settings/disqus/{status}', 'Owner\GeneralSettingController@isdisqus')->name('owner-gs-isdisqus');
  Route::get('/general-settings/loader/{status}', 'Owner\GeneralSettingController@isloader')->name('owner-gs-isloader');
  Route::get('/general-settings/email-verify/{status}', 'Owner\GeneralSettingController@isemailverify')->name('owner-gs-is-email-verify');
  Route::get('/general-settings/popup/{status}', 'Owner\GeneralSettingController@ispopup')->name('owner-gs-ispopup');
  Route::get('/general-settings/admin/loader/{status}', 'Owner\GeneralSettingController@isadminloader')->name('owner-gs-is-owner-loader');


  Route::get('/general-settings/maintain/{status}', 'Owner\GeneralSettingController@ismaintain')->name('owner-gs-maintain');
  //  Affilte Section

  Route::get('/general-settings/affilate/{status}', 'Owner\GeneralSettingController@isaffilate')->name('owner-gs-isaffilate');

  //  Capcha Section

  Route::get('/general-settings/capcha/{status}', 'Owner\GeneralSettingController@iscapcha')->name('owner-gs-iscapcha');


  //------------ ADMIN GENERAL SETTINGS JSON SECTION ENDS------------

  Route::get('/general-settings/contact/{status}', 'Owner\GeneralSettingController@iscontact')->name('owner-gs-iscontact');

  Route::post('/general-settings/update/all', 'Owner\GeneralSettingController@generalupdate')->name('owner-gs-update');
  Route::post('/general-settings/update/payment', 'Owner\GeneralSettingController@generalupdatepayment')->name('owner-gs-update-payment');
  Route::get('/general-settings/status/{field}/{status}', 'Owner\GeneralSettingController@status')->name('owner-gs-status');

  //------------ ADMIN GENERAL SETTINGS SECTION ENDS ------------

});


Route::group(['middleware'=>'ownerpermissions:Home Page Settings'],function(){
  //------------ ADMIN PAGE SETTINGS SECTION ------------
  Route::get('/page-settings/customize', 'Owner\PageSettingController@customize')->name('owner-ps-customize');
  Route::get('/page-settings/big-save', 'Owner\PageSettingController@big_save')->name('owner-ps-big-save');
  Route::get('/page-settings/best-seller', 'Owner\PageSettingController@best_seller')->name('owner-ps-best-seller');

  Route::get('/hero/section','Owner\PagesettingController@herosection')->name('owner-hero-section-index');
  Route::get('/instructor/section','Owner\PagesettingController@instructorsection')->name('owner-instructor-section-index');

  Route::post('/page-settings/update/all', 'Owner\PageSettingController@update')->name('owner-ps-update');
  Route::post('/page-settings/update/home', 'Owner\PageSettingController@homeupdate')->name('owner-ps-homeupdate');

  //------------ ADMIN SERVICE SECTION ------------
  Route::get('/service/datatables', 'Owner\ServiceController@datatables')->name('owner-service-datatables'); //JSON REQUEST
  Route::get('/service', 'Owner\ServiceController@index')->name('owner-service-index');
  Route::get('/service/create', 'Owner\ServiceController@create')->name('owner-service-create');
  Route::post('/service/create', 'Owner\ServiceController@store')->name('owner-service-store');
  Route::get('/service/edit/{id}', 'Owner\ServiceController@edit')->name('owner-service-edit');
  Route::post('/service/edit/{id}', 'Owner\ServiceController@update')->name('owner-service-update');
  Route::get('/service/delete/{id}', 'Owner\ServiceController@destroy')->name('owner-service-delete');



  //------------ ADMIN SERVICE SECTION ENDS ------------

  //------------ ADMIN HOME PAGE SETTINGS SECTION ENDS ------------
});

Route::group(['middleware'=>'ownerpermissions:Menu Page Settings'],function(){

    //------------ ADMIN PAGE SECTION ------------
    Route::get('/page/datatables', 'Owner\PageController@datatables')->name('owner-page-datatables'); //JSON REQUEST
    Route::get('/page', 'Owner\PageController@index')->name('owner-page-index');
    Route::get('/page/create', 'Owner\PageController@create')->name('owner-page-create');
    Route::post('/page/create', 'Owner\PageController@store')->name('owner-page-store');
    Route::get('/page/edit/{id}', 'Owner\PageController@edit')->name('owner-page-edit');
    Route::post('/page/update/{id}', 'Owner\PageController@update')->name('owner-page-update');
    Route::get('/page/delete/{id}', 'Owner\PageController@destroy')->name('owner-page-delete');
    Route::get('/page/header/{id1}/{id2}', 'Owner\PageController@status')->name('owner-page-status');

    Route::get('/page-settings/contact', 'Owner\PageSettingController@contact')->name('owner-ps-contact');
    Route::post('/page-settings/update/all', 'Owner\PageSettingController@update')->name('owner-ps-update');
    //------------ ADMIN PAGE SECTION ENDS------------
});


Route::group(['middleware'=>'ownerpermissions:Email Settings'],function(){
    //------------ ADMIN EMAIL SETTINGS SECTION ------------
    Route::get('/email-templates/datatables', 'Owner\EmailController@datatables')->name('owner-mail-datatables');
    Route::get('/email-templates', 'Owner\EmailController@index')->name('owner-mail-index');
    Route::get('/email-templates/{id}', 'Owner\EmailController@edit')->name('owner-mail-edit');
    Route::post('/email-templates/{id}', 'Owner\EmailController@update')->name('owner-mail-update');
    Route::get('/email-config', 'Owner\EmailController@config')->name('owner-mail-config');
    Route::get('/groupemail', 'Owner\EmailController@groupemail')->name('owner-group-show');
    Route::post('/groupemailpost', 'Owner\EmailController@groupemailpost')->name('owner-group-submit');
    //------------ ADMIN EMAIL SETTINGS SECTION ENDS ------------
});


Route::group(['middleware'=>'ownerpermissions:Payment Settings'],function(){

  //------------ ADMIN PAYMENT SETTINGS SECTION ------------
  Route::get('/payment/information', 'Owner\PaymentGatewayController@paymentinfo')->name('owner-payment-info');
  Route::get('/paymentgateway/datatables', 'Owner\PaymentGatewayController@datatables')->name('owner-payment-datatables'); //JSON REQUEST
  Route::get('/paymentgateway', 'Owner\PaymentGatewayController@index')->name('owner-payment-index');
  Route::get('/paymentgateway/create', 'Owner\PaymentGatewayController@create')->name('owner-payment-create');
  Route::post('/paymentgateway/create', 'Owner\PaymentGatewayController@store')->name('owner-payment-store');
  Route::get('/paymentgateway/edit/{id}', 'Owner\PaymentGatewayController@edit')->name('owner-payment-edit');
  Route::post('/paymentgateway/update/{id}', 'Owner\PaymentGatewayController@update')->name('owner-payment-update');
  Route::delete('/paymentgateway/delete/{id}', 'Owner\PaymentGatewayController@destroy')->name('owner-payment-delete');
  Route::get('/paymentgateway/status/{id1}/{id2}', 'Owner\PaymentGatewayController@status')->name('owner-payment-status');


  // MULTIPLE CURRENCY
  Route::get('/general-settings/currency/{status}', 'Owner\GeneralSettingController@currency')->name('owner-gs-iscurrency');
  Route::get('/currency/datatables', 'Owner\CurrencyController@datatables')->name('owner-currency-datatables'); //JSON REQUEST
  Route::get('/currency', 'Owner\CurrencyController@index')->name('owner-currency-index');
  Route::get('/currency/create', 'Owner\CurrencyController@create')->name('owner-currency-create');
  Route::post('/currency/create', 'Owner\CurrencyController@store')->name('owner-currency-store');
  Route::get('/currency/edit/{id}', 'Owner\CurrencyController@edit')->name('owner-currency-edit');
  Route::post('/currency/update/{id}', 'Owner\CurrencyController@update')->name('owner-currency-update');
  Route::get('/currency/delete/{id}', 'Owner\CurrencyController@destroy')->name('owner-currency-delete');
  Route::get('/currency/status/{id1}/{id2}', 'Owner\CurrencyController@status')->name('owner-currency-status');
  
  //------------ ADMIN PAYMENT SETTINGS SECTION ENDS------------

});


Route::group(['middleware'=>'ownerpermissions:Manage Referrals'],function(){
  

  Route::get('/referral/datatables', 'Owner\ReferralController@datatables')->name('owner-referral-datatables'); //JSON REQUEST
  Route::get('/referral', 'Owner\ReferralController@index')->name('owner-referral-index');

  Route::get('/referral/history/datatables', 'Owner\ReferralController@h_datatables')->name('owner-referral-history-datatables'); //JSON REQUEST
  Route::get('/referral/history', 'Owner\ReferralController@h_index')->name('owner-referral-history-index');

  Route::get('/referral/create', 'Owner\ReferralController@create')->name('owner-referral-create');
  Route::post('/referral/create', 'Owner\ReferralController@store')->name('owner-referral-store');
  Route::get('/referral/edit/{id}', 'Owner\ReferralController@edit')->name('owner-referral-edit');
  Route::post('/referral/update/{id}', 'Owner\ReferralController@update')->name('owner-referral-update');
  Route::get('/referral/delete/{id}', 'Owner\ReferralController@destroy')->name('owner-referral-delete');


});

Route::group(['middleware'=>'ownerpermissions:Social Settings'],function(){

    //------------ ADMIN SOCIAL SETTINGS SECTION ------------
    Route::get('/social', 'Owner\SocialSettingController@index')->name('owner-social-index');
    Route::post('/social/update', 'Owner\SocialSettingController@socialupdate')->name('owner-social-update');
    Route::post('/social/update/all', 'Owner\SocialSettingController@socialupdateall')->name('owner-social-update-all');
    Route::get('/social/facebook', 'Owner\SocialSettingController@facebook')->name('owner-social-facebook');
    Route::get('/social/google', 'Owner\SocialSettingController@google')->name('owner-social-google');
    Route::get('/social/facebook/{status}', 'Owner\SocialSettingController@facebookup')->name('owner-social-facebookup');
    Route::get('/social/google/{status}', 'Owner\SocialSettingController@googleup')->name('owner-social-googleup');
    //------------ ADMIN SOCIAL SETTINGS SECTION ENDS------------
});


  Route::group(['middleware'=>'ownerpermissions:Language Settings'],function(){

  //------------ ADMIN LANGUAGE SETTINGS SECTION ------------
  Route::get('/general-settings/language/{status}', 'Owner\GeneralSettingController@language')->name('owner-gs-islanguage');


  Route::get('/languages/datatables', 'Owner\LanguageController@datatables')->name('owner-lang-datatables'); //JSON REQUEST
  Route::get('/languages', 'Owner\LanguageController@index')->name('owner-lang-index');
  Route::get('/languages/create', 'Owner\LanguageController@create')->name('owner-lang-create');
  Route::get('/languages/edit/{id}', 'Owner\LanguageController@edit')->name('owner-lang-edit');
  Route::post('/languages/create', 'Owner\LanguageController@store')->name('owner-lang-store');
  Route::post('/languages/edit/{id}', 'Owner\LanguageController@update')->name('owner-lang-update');
  Route::get('/languages/status/{id1}/{id2}', 'Owner\LanguageController@status')->name('owner-lang-st');
  Route::get('/languages/delete/{id}', 'Owner\LanguageController@destroy')->name('owner-lang-delete');


  //------------ ADMIN PANEL LANGUAGE SETTINGS SECTION ------------

  Route::get('/adminlanguages/datatables', 'Owner\AdminLanguageController@datatables')->name('owner-tlang-datatables'); //JSON REQUEST
  Route::get('/adminlanguages', 'Owner\AdminLanguageController@index')->name('owner-tlang-index');
  Route::get('/adminlanguages/create', 'Owner\AdminLanguageController@create')->name('owner-tlang-create');
  Route::get('/adminlanguages/edit/{id}', 'Owner\AdminLanguageController@edit')->name('owner-tlang-edit');
  Route::post('/adminlanguages/create', 'Owner\AdminLanguageController@store')->name('owner-tlang-store');
  Route::post('/adminlanguages/edit/{id}', 'Owner\AdminLanguageController@update')->name('owner-tlang-update');
  Route::get('/adminlanguages/status/{id1}/{id2}', 'Owner\AdminLanguageController@status')->name('owner-tlang-st');
  Route::get('/adminlanguages/delete/{id}', 'Owner\AdminLanguageController@destroy')->name('owner-tlang-delete');

  //------------ ADMIN LANGUAGE SETTINGS SECTION ENDS ------------
});


Route::group(['middleware'=>'ownerpermissions:Seo Tools'],function(){
  //------------ ADMIN SEOTOOL SETTINGS SECTION ------------

  Route::get('/seotools/analytics', 'Owner\SeoToolController@analytics')->name('owner-seotool-analytics');
  Route::post('/seotools/analytics/update', 'Owner\SeoToolController@analyticsupdate')->name('owner-seotool-analytics-update');
  Route::get('/seotools/keywords', 'Owner\SeoToolController@keywords')->name('owner-seotool-keywords');
  Route::post('/seotools/keywords/update', 'Owner\SeoToolController@keywordsupdate')->name('owner-seotool-keywords-update');
  Route::get('/products/popular/{id}','Owner\SeoToolController@popular')->name('owner-prod-popular');

  //------------ ADMIN SEOTOOL SETTINGS SECTION ------------

});


Route::group(['middleware'=>'ownerpermissions:Manage Staff'],function(){

  //------------ ADMIN STAFF SECTION ------------
  Route::get('/staff/datatables', 'Owner\StaffController@datatables')->name('owner-staff-datatables');
  Route::get('/staff', 'Owner\StaffController@index')->name('owner-staff-index');
  Route::get('/staff/create', 'Owner\StaffController@create')->name('owner-staff-create');
  Route::post('/staff/create', 'Owner\StaffController@store')->name('owner-staff-store');
  Route::get('/staff/edit/{id}', 'Owner\StaffController@edit')->name('owner-staff-edit');
  Route::post('/staff/update/{id}', 'Owner\StaffController@update')->name('owner-staff-update');
  Route::get('/staff/show/{id}', 'Owner\StaffController@show')->name('owner-staff-show');
  Route::get('/staff/delete/{id}', 'Owner\StaffController@destroy')->name('owner-staff-delete');
  //------------ ADMIN STAFF SECTION ENDS------------

});


Route::group(['middleware'=>'ownerpermissions:Subscribers'],function(){

  //------------ ADMIN SUBSCRIBERS SECTION ------------
  Route::get('/subscribers/datatables', 'Owner\SubscriberController@datatables')->name('owner-subs-datatables'); //JSON REQUEST
  Route::get('/subscribers', 'Owner\SubscriberController@index')->name('owner-subs-index');
  Route::get('/subscribers/download', 'Owner\SubscriberController@download')->name('owner-subs-download');
  //------------ ADMIN SUBSCRIBERS ENDS ------------

});


Route::group(['middleware'=>'ownerpermissions:Manage Roles'],function(){
    // ------------ ROLE SECTION ----------------------
    Route::get('/role/datatables', 'Owner\RoleController@datatables')->name('owner-role-datatables');
    Route::get('/role', 'Owner\RoleController@index')->name('owner-role-index');
    Route::get('/role/create', 'Owner\RoleController@create')->name('owner-role-create');
    Route::post('/role/create', 'Owner\RoleController@store')->name('owner-role-store');
    Route::get('/role/edit/{id}', 'Owner\RoleController@edit')->name('owner-role-edit');
    Route::post('/role/edit/{id}', 'Owner\RoleController@update')->name('owner-role-update');
    Route::get('/role/delete/{id}', 'Owner\RoleController@destroy')->name('owner-role-delete');
    // ------------ ROLE SECTION ENDS ----------------------
});

});


  Route::get('/cache/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    return redirect()->route('owner.dashboard')->with('cache','System Cache Has Been Removed.');
  })->name('owner-cache-clear');


});

// ************************************ ADMIN SECTION ENDS**********************************************

// *********************************** INSTRUCTOR SECTION *********************************************

Route::prefix('instructor')->group(function() {

  // INSTRUCTOR LOGIN
  Route::get('/login', 'OwnerInstructor\logincontroller@showloginform')->name('owner.instructor.login');
  Route::post('/login', 'OwnerInstructor\logincontroller@login')->name('owner.instructor.login.submit');
  // INSTRUCTOR LOGIN END

  // STUDENT FORGOT
  Route::get('/forgot', 'OwnerInstructor\ForgotController@showforgotform')->name('owner-instructor-forgot');
  Route::post('/forgot', 'OwnerInstructor\ForgotController@forgot')->name('owner-instructor-forgot-submit');
  // STUDENT FORGOT ENDS


    // STUDENT REGISTER
    Route::get('/register', 'OwnerInstructor\RegisterController@showRegisterForm')->name('owner-instructor-register');
    Route::post('/register', 'OwnerInstructor\RegisterController@register')->name('owner-instructor-register-submit');
    Route::get('/register/verify/{token}', 'OwnerInstructor\RegisterController@token')->name('owner-instructor-register-token');
    // STUDENT REGISTER END

    Route::group(['middleware'=>'OwnerUser'],function(){


    // Student instructor application start
    Route::get('/application/form','Ownerstudent\StudentController@application')->name('owner-student-owner-instructor-application');
    route::post('/application/submit','Ownerstudent\Studentcontroller@applicationsubmit')->name('owner-student-owner-instructor-application.submit');
    // Student instructor application end



  //------------ INSTRUCTOR NOTIFICATION SECTION ------------

  Route::get('/show-notification', 'OwnerInstructor\NotificationController@show_notf')->name('owner.instructor.notification.show');
  Route::get('/read-notification', 'OwnerInstructor\NotificationController@read')->name('owner.instructor.notification.read');
  Route::get('/delete-notification', 'OwnerInstructor\NotificationController@notf_clear')->name('owner.instructor.delete.clear');

  //------------ INSTRUCTOR NOTIFICATION SECTION ENDS ------------

    Route::group(['middleware'=>'instructor'],function(){

    // INSTRUCTOR DASHBOARD
    Route::get('/dashboard', 'OwnerInstructor\InstructorController@index')->name('owner-instructor-dashboard');

    // INSTRUCTOR LOGOUT
    Route::get('/logout', 'OwnerInstructor\logincontroller@logout')->name('owner.instructor.logout');

    // INSTRUCTOR PROFILE
    Route::get('/profile', 'OwnerInstructor\instructorcontroller@profile')->name('owner.instructor.profile.edit');
    Route::post('/profile', 'OwnerInstructor\InstructorController@profileupdate')->name('owner-instructor-profile-update');
    // INSTRUCTOR PROFILE ENDS

    // INSTRUCTOR RESET
    Route::get('/reset', 'OwnerInstructor\InstructorController@resetform')->name('owner-instructor-reset');
    Route::post('/reset', 'OwnerInstructor\InstructorController@changepass')->name('owner-instructor-reset-submit');
    // INSTRUCTOR RESET END


    // STUDENT INSTRUCTOR SEND MESSAGE
    Route::post('/instructor/contact', 'Ownerstudent\MessageController@studentcontact')->name('owner-instructor-contact');
    Route::get('/messages', 'Ownerstudent\MessageController@messages')->name('owner-instructor-messages');
    Route::get('/message/{id}', 'Ownerstudent\MessageController@message')->name('owner-instructor-message');
    Route::post('/message/post', 'Ownerstudent\MessageController@postmessage')->name('owner-instructor-message-post');
    Route::get('/message/{id}/delete', 'Ownerstudent\MessageController@messagedelete')->name('owner-instructor-message-delete');
    Route::get('/message/load/{id}', 'Ownerstudent\MessageController@msgload')->name('owner-instructor-message-load');
    // STUDENT INSTRUCTOR SEND MESSAGE ENDS


  //------------ INSTRUCTOR COURSE SECTION ------------



    Route::get('/course/datatables', 'OwnerInstructor\CourseController@datatables')->name('owner-instructor-course-datatables'); //JSON REQUEST
    Route::get('/course', 'OwnerInstructor\CourseController@index')->name('owner-instructor-course-index');
    Route::get('/course/create', 'OwnerInstructor\CourseController@create')->name('owner-instructor-course-create');
    Route::post('/course/create', 'OwnerInstructor\CourseController@store')->name('owner-instructor-course-store');
    Route::get('/course/edit/{id}', 'OwnerInstructor\CourseController@edit')->name('owner-instructor-course-edit');
    Route::post('/course/update/{id}', 'OwnerInstructor\CourseController@update')->name('owner-instructor-course-update');
    Route::get('/course/status/{id1}/{id2}', 'OwnerInstructor\CourseController@status')->name('owner-instructor-course-status');
    Route::get('/course/delete/{id}', 'OwnerInstructor\CourseController@destroy')->name('owner-instructor-course-delete');
    Route::get('/load/subcategories/{id}/', 'OwnerInstructor\CourseController@load')->name('owner-instructor-subcat-load'); //JSON REQUEST

    Route::get('/purchase/datatables', 'OwnerInstructor\OrderController@datatables')->name('owner-instructor-purchase-datatables'); //JSON REQUEST
    Route::get('/purchase', 'OwnerInstructor\OrderController@index')->name('owner-instructor-purchase-index');
    Route::get('/purchase/details/{order_number}', 'OwnerInstructor\OrderController@purchasedetails')->name('owner.instructor.purchase.details');

    //------------ INSTRUCTOR COURSE SECTION ENDS------------

  //------------ INSTRUCTOR CURRICULAM SECTION ------------

  Route::post('/course/section/sort/update/{id}', 'OwnerInstructor\CurriculamController@sectionSortUpdate')->name('owner-instructor-section-sort-update');
  Route::post('/course/section/store/{id}', 'OwnerInstructor\CurriculamController@sectionStore')->name('owner-instructor-section-store');
  Route::post('/course/section/update/{id}', 'OwnerInstructor\CurriculamController@sectionUpdate')->name('owner-instructor-section-update');
  Route::get('/course/section/delete/{id}', 'OwnerInstructor\CurriculamController@sectionDelete')->name('owner-instructor-section-delete');
  Route::post('/course/lesson/store', 'OwnerInstructor\CurriculamController@lessonStore')->name('owner-instructor-lesson-store');
  Route::post('/course/lesson/update/{id}', 'OwnerInstructor\CurriculamController@lessonUpdate')->name('owner-instructor-lesson-update');
  Route::get('/course/lesson/edit/{id}', 'OwnerInstructor\CurriculamController@lessonEdit')->name('owner-instructor-lesson-edit');
  Route::get('/course/lesson/quiz/{id}', 'OwnerInstructor\CurriculamController@lessonQuiz')->name('owner-instructor-lesson-quiz');
  Route::get('/course/lesson/sort/{id}', 'OwnerInstructor\CurriculamController@lessonSort')->name('owner-instructor-lesson-sort');
  Route::get('/course/lesson/delete/{id}', 'OwnerInstructor\CurriculamController@lessonDelete')->name('owner-instructor-lesson-delete');
  Route::post('/course/lesson/sort/update/{id}', 'OwnerInstructor\CurriculamController@lessonSortUpdate')->name('owner-instructor-lesson-sort-update');
  Route::get('/course/curriculam/{id}', 'OwnerInstructor\CurriculamController@curriculam')->name('owner-instructor-course-curriculam');
  Route::get('/course/curriculam/lesson/{id}', 'OwnerInstructor\CurriculamController@curriculamLesson')->name('owner-instructor-course-curriculam-lesson');
  Route::post('/course/curriculam/quiz/result', 'OwnerInstructor\CurriculamController@result')->name('owner-instructor-course-quiz-result');
  Route::get('/course/curriculam-status/{id}', 'OwnerInstructor\CurriculamController@curriculamStatus')->name('owner-instructor-course-curriculam-status');

  //------------ INSTRUCTOR CURRICULAM SECTION ENDS ------------

  //------------ INSTRUCTOR QUESTION SECTION ------------

  Route::post('/course/lesson/quiz/question/store', 'OwnerInstructor\QuestionController@store')->name('owner-instructor-question-store');
  Route::get('/course/lesson/quiz/question/edit/{id}', 'OwnerInstructor\QuestionController@edit')->name('owner-instructor-question-edit');
  Route::post('/course/lesson/quiz/question/update/{id}', 'OwnerInstructor\QuestionController@update')->name('owner-instructor-question-update');
  Route::post('/course/lesson/quiz/question/sort/update/{id}', 'OwnerInstructor\QuestionController@sortUpdate')->name('owner-instructor-question-sort-update');
  Route::get('/course/lesson/quiz/question/delete/{id}', 'OwnerInstructor\QuestionController@delete')->name('owner-instructor-question-delete');

  //------------ INSTRUCTOR QUESTION SECTION ENDS ------------

    //------------ INSTRUCTOR WITHDRAW SECTION ------------

    Route::get('/withdraw/datatables', 'Ownerinstructor\WithdrawController@datatables')->name('owner-instructor-wt-datatables');
    Route::get('/withdraw', 'Ownerinstructor\WithdrawController@index')->name('owner-instructor-wt-index');
    Route::get('/withdraw/create', 'Ownerinstructor\WithdrawController@create')->name('owner-instructor-wt-create');
    Route::post('/withdraw/create', 'Ownerinstructor\WithdrawController@store')->name('owner-instructor-wt-store');
  
    //------------ INSTRUCTOR WITHDRAW SECTION ENDS ------------
  
      // INSTRUCTOR AFFILATE
      Route::get('/affilates', 'Ownerinstructor\InstructorController@affilate')->name('owner-instructor-affilate-index');
      Route::get('/affilate/history', 'Ownerinstructor\InstructorController@affilate_history')->name('owner-instructor-affilate-history');
      // INSTRUCTOR AFFILATE ENDS
  
  

    });

    });

  });


// *********************************** INSTRUCTOR SECTION ENDS*********************************************

// *********************************** STUDENT SECTION *********************************************

Route::prefix('student')->group(function() {


  // STUDENT LOGIN
  Route::get('/login', 'Ownerstudent\logincontroller@showloginform')->name('owner.student.login');
  Route::post('/login', 'Ownerstudent\logincontroller@login')->name('owner.student.login.submit');
  // STUDENT LOGIN ENDS

  // STUDENT REGISTER
  Route::get('/register', 'Ownerstudent\RegisterController@showRegisterForm')->name('owner-student-register');
  Route::post('/register', 'Ownerstudent\RegisterController@register')->name('owner-student-register-submit');
  Route::get('/register/verify/{token}', 'Ownerstudent\RegisterController@token')->name('owner-student-register-token');
  // STUDENT REGISTER ENDS

  // STUDENT LOGOUT
  Route::get('/logout', 'Ownerstudent\LoginController@logout')->name('owner-student-logout');
  // STUDENT LOGOUT ENDS


  Route::group(['middleware'=>'OwnerUser'],function(){


  // STUDENT DASHBOARD
  Route::get('/dashboard', 'Ownerstudent\StudentController@index')->name('owner-student-dashboard');

    // STUDENT DASHBOARD
    Route::get('/enroll', 'Ownerstudent\StudentController@assignCourse')->name('owner-student-enroll');


  // STUDENT RESET
  Route::get('/reset', 'Ownerstudent\StudentController@resetform')->name('owner-student-reset');
  Route::post('/reset', 'Ownerstudent\StudentController@reset')->name('owner-student-reset-submit');
  // STUDENT RESET ENDS

  // STUDENT PROFILE
  Route::get('/profile', 'Ownerstudent\StudentController@profile')->name('owner-student-profile');
  Route::get('/account', 'Ownerstudent\StudentController@account')->name('owner-student-account');
  Route::post('/account/update', 'Ownerstudent\StudentController@accountUpdate')->name('owner-student-account-update');
  Route::post('/profile', 'Ownerstudent\StudentController@profileupdate')->name('owner-student-profile-update');
  // STUDENT PROFILE ENDS

    // STUDENT AFFILATE
    Route::get('/affilates', 'Ownerstudent\StudentController@affilate')->name('owner-student-affilate');
    Route::get('/affilate/history', 'Ownerstudent\StudentController@affilate_history')->name('owner-student-affilate-history');
    // STUDENT AFFILATE ENDS


  Route::get('/orders', 'Ownerstudent\OrderController@orders')->name('owner-student-order-index');
  Route::get('/orders/{id}', 'Ownerstudent\OrderController@order')->name('owner-student-order-details');
  Route::get('/orders/print/{id}', 'Ownerstudent\OrderController@orderprint')->name('owner-student-order-print');

  // STUDENT WISHLIST
  Route::get('/wishlists','Ownerstudent\WishlistController@wishlists')->name('owner-student-wishlists');
  Route::get('/wishlist/add/{id}','Ownerstudent\WishlistController@addwish')->name('owner-student-wishlist-add');
  Route::get('/wishlist/remove/{id}','Ownerstudent\WishlistController@removewish')->name('owner-student-wishlist-remove');
  // STUDENT WISHLIST ENDS

  Route::get('/course/curriculam/lesson/{id}', 'Ownerstudent\CurriculamController@curriculamLesson')->name('owner-student-course-curriculam-lesson');
  Route::post('/course/curriculam/quiz/result', 'Ownerstudent\CurriculamController@result')->name('owner-student-course-quiz-result');


  Route::post('/course/review','Ownerstudent\StudentController@reviewsubmit')->name('owner.student.review.submit');


  // STUDENT INSTRUCTOR SEND MESSAGE
  Route::post('/student/contact', 'Ownerstudent\MessageController@studentcontact')->name('owner-student-contact');
  Route::get('/messages', 'Ownerstudent\MessageController@messages')->name('owner-student-messages');
  Route::get('/message/{id}', 'Ownerstudent\MessageController@message')->name('owner-student-message');
  Route::post('/message/post', 'Ownerstudent\MessageController@postmessage')->name('owner-student-message-post');
  Route::get('/message/{id}/delete', 'Ownerstudent\MessageController@messagedelete')->name('owner-student-message-delete');
  Route::get('/message/load/{id}', 'Ownerstudent\MessageController@msgload')->name('owner-student-message-load');
  // STUDENT INSTRUCTOR SEND MESSAGE ENDS

  // STUDENT FORGOT
  Route::get('/forgot', 'Ownerstudent\ForgotController@showforgotform')->name('owner-student-forgot');
  Route::post('/forgot', 'Ownerstudent\ForgotController@forgot')->name('owner-student-forgot-submit');
  // STUDENT FORGOT ENDS

  // Student instructor application start
  Route::get('/instructor/application/form','Ownerstudent\StudentController@application')->name('owner-student-instructor-application');
  route::post('/instructor/application/submit','Ownerstudent\Studentcontroller@applicationsubmit')->name('owner-student-instructor-application.submit');
  // Student instructor application end

  // CHECKOUT
  Route::get('/checkout','Ownerfront\CheckoutController@checkout')->name('owner.front.checkout');
  Route::get('/checkout/payment/return', 'Ownerfront\CheckoutController@payreturn')->name('owner.front.payment.return');
  Route::get('/checkout/payment/cancle', 'Ownerfront\CheckoutController@paycancle')->name('owner.front.payment.cancle');

  });

});

// *********************************** STUDENT SECTION ENDS *********************************************



// ************************************ FRONT SECTION **********************************************

  // HOME SECTION
  Route::get('/', 'Ownerfront\FrontendController@index')->name('owner.front.index');
  // HOME SECTION ENDS

  Route::get('/language/{id}', 'Ownerfront\FrontendController@language')->name('owner.front.language');

  // CATALOG SECTION
  Route::get('/catalog/{category?}/{subcategory?}','Ownerfront\CatalogController@catalog')->name('owner.front.catalog');
  // CATALOG SECTION ENDS

  // COURSE SECTION START
  route::get('/course/{slug}/','Ownerfront\Catalogcontroller@course')->name('owner.front.course');
  // COURSE SECTION END

  // INSTRUCTOR ROUTE
  route::get('instructor/{slug}','Ownerfront\CatalogController@instructordetails')->name('owner.instructor.details');
  // INSTRUCTOR ROUTE END

  // RATING ROUTE
  Route::get('/course/view/review/{id}','Ownerfront\CatalogController@reviews')->name('owner.front.reviews');
  Route::get('/course/view/side/review/{id}','Ownerfront\CatalogController@sideReviews')->name('owner.front.side.reviews');
  // RATING ROUTE END

  // BLOG SECTION
  Route::get('/blog','Ownerfront\FrontendController@blog')->name('owner.front.blog');
  Route::get('/blog/{id}','Ownerfront\FrontendController@blogshow')->name('owner.front.blogshow');
  Route::get('/blog/category/{slug}','Ownerfront\FrontendController@blogcategory')->name('owner.front.blogcategory');
  Route::get('/blog/tag/{slug}','Ownerfront\FrontendController@blogtags')->name('owner.front.blogtags');
  Route::get('/blog-search','Ownerfront\FrontendController@blogsearch')->name('owner.front.blogsearch');
  Route::get('/blog/archive/{slug}','Ownerfront\FrontendController@blogarchive')->name('owner.front.blogarchive');
  // BLOG SECTION ENDS

  // CONTACT SECTION
  Route::get('/contact','Ownerfront\FrontendController@contact')->name('owner.front.contact');
  Route::post('/contact','Ownerfront\FrontendController@contactemail')->name('owner.front.contact.submit');
  Route::get('/contact/refresh_code','Ownerfront\FrontendController@refresh_code');
  // CONTACT SECTION  ENDS

  // PRODCT AUTO SEARCH SECTION
  Route::get('/autosearch/course/{slug}','Ownerfront\CatalogController@autosearch');
  // PRODCT AUTO SEARCH SECTION ENDS


  // CART SECTION
  Route::get('/carts/view','Ownerfront\CartController@cartview');
  Route::get('/carts','Ownerfront\CartController@cart')->name('owner.front.cart');
  Route::get('/addcart/{id}','Ownerfront\CartController@addcart')->name('owner.course.cart.add');
  Route::get('/addtocart/{id}','Ownerfront\CartController@addtocart')->name('owner.course.cart.quickadd');
  Route::get('/removecart/{id}','Ownerfront\CartController@removecart')->name('owner.course.cart.remove');
  // CART SECTION ENDS

  // SUBSCRIBE SECTION
  Route::post('/subscriber/store', 'Ownerfront\FrontendController@subscribe')->name('owner.front.subscribe');
  // SUBSCRIBE SECTION ENDS



  // PAYPAL
  Route::post('/checkout/payment/paypal-submit', 'Ownerfront\PaypalController@store')->name('owner.paypal.submit');
  Route::get('/checkout/payment/paypal-notify', 'Ownerfront\PaypalController@notify')->name('owner.paypal.notify');

  // STRIPE
  Route::post('/checkout/payment/stripe-submit', 'Ownerfront\StripeController@store')->name('owner.stripe.submit');

  // LOGIN WITH FACEBOOK OR GOOGLE SECTION
  Route::get('auth/{provider}', 'User\SocialRegisterController@redirectToProvider')->name('owner-social-provider');
  Route::get('auth/{provider}/callback', 'User\SocialRegisterController@handleProviderCallback');
  // LOGIN WITH FACEBOOK OR GOOGLE SECTION ENDS

  // PAGE SECTION
  Route::get('/{slug}','Ownerfront\FrontendController@page')->name('owner.front.page');
  // PAGE SECTION ENDS

// ************************************ FRONT SECTION ENDS**********************************************



  
});


});
