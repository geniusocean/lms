<?php

// ************************************ ADMIN SECTION **********************************************

Route::prefix('admin')->group(function() {

  //------------ ADMIN LOGIN SECTION ------------

  Route::get('/login', 'Admin\LoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'Admin\LoginController@login')->name('admin.login.submit');
  Route::get('/forgot', 'Admin\LoginController@showForgotForm')->name('admin.forgot');
  Route::post('/forgot', 'Admin\LoginController@forgot')->name('admin.forgot.submit');
  Route::get('/change-password/{token}', 'Admin\LoginController@showChangePassForm')->name('admin.change.token');
  Route::post('/change-password', 'Admin\LoginController@changepass')->name('admin.change.password');
  Route::get('/logout', 'Admin\LoginController@logout')->name('admin.logout');

  //------------ ADMIN LOGIN SECTION ENDS ------------

  Route::group(['middleware'=>'adminstaff','super'],function(){
  //------------ ADMIN NOTIFICATION SECTION ------------

  Route::get('/show-notification', 'Admin\NotificationController@show_notf')->name('admin.notification.show');
  Route::get('/read-notification', 'Admin\NotificationController@read')->name('admin.notification.read');
  Route::get('/delete-notification', 'Admin\NotificationController@notf_clear')->name('admin.delete.clear');

  //------------ ADMIN NOTIFICATION SECTION ENDS ------------

  //------------ ADMIN DASHBOARD & PROFILE SECTION ------------
  Route::get('/', 'Admin\DashboardController@index')->name('admin.dashboard');
  Route::get('/profile', 'Admin\DashboardController@profile')->name('admin.profile');
  Route::post('/profile/update', 'Admin\DashboardController@profileupdate')->name('admin.profile.update');
  Route::get('/password', 'Admin\DashboardController@passwordreset')->name('admin.password');
  Route::post('/password/update', 'Admin\DashboardController@changepass')->name('admin.password.update');
  //------------ ADMIN DASHBOARD & PROFILE SECTION ENDS ------------

  Route::group(['middleware'=>'permissions:Manage Instructors'],function(){

//------------ ADMIN INSTRUCTORS SECTION ------------

    Route::get('/instructor/datatables', 'Admin\InstructorController@datatables')->name('admin-instructor-datatables'); //JSON REQUEST
    Route::get('/instructor', 'Admin\InstructorController@index')->name('admin-instructor-index');
    Route::get('/instructor/create', 'Admin\InstructorController@create')->name('admin-instructor-create');
    Route::post('/instructor/create', 'Admin\InstructorController@store')->name('admin-instructor-store');
    Route::get('/instructor/edit/{id}', 'Admin\InstructorController@edit')->name('admin-instructor-edit');
    Route::get('/instructor/curriculam/{id}', 'Admin\InstructorController@curriculam')->name('admin-instructor-curriculam');
    Route::post('/instructor/update/{id}', 'Admin\InstructorController@update')->name('admin-instructor-update');
    Route::get('/instructor/delete/{id}', 'Admin\InstructorController@destroy')->name('admin-instructor-delete');
    Route::get('/instructor/status/{id1}/{id2}', 'Admin\Instructorontroller@status')->name('admin-instructor-status');

    // INSTRUCTOR REQUEST APPLICATION
    Route::get('/application/datatables', 'Admin\InstructorController@applicationdatatables')->name('admin-instructor-application-datatables'); //JSON REQUEST
    Route::get('/applications', 'Admin\InstructorController@applications')->name('admin-instructor-application');
    Route::get('/application/details/{id}', 'admin\instructorcontroller@applicationsdetails')->name('admin.application.details');
    Route::get('/application/status/{id1}/{id2}', 'admin\instructorcontroller@applicationstatus')->name('admin.application.status');
    Route::get('/application/delete/{id}', 'admin\instructorcontroller@applicationdelete')->name('admin.application.delete');
    Route::get('/application/download/{id}', 'admin\instructorcontroller@applicationdownload')->name('admin.application.download');


    
    //------------ ADMIN WITHDRAW SECTION ------------

    Route::get('/instructor/withdraws/datatables', 'Admin\InstructorController@withdrawdatatables')->name('admin-withdraw-datatables'); //JSON REQUEST
    Route::get('/instructor/withdraws', 'Admin\InstructorController@withdraws')->name('admin-withdraw-index');
    Route::get('/instructor/withdraw/{id}/show', 'Admin\InstructorController@withdrawdetails')->name('admin-withdraw-show');
    Route::get('/instructor/withdraws/accept/{id}', 'Admin\InstructorController@accept')->name('admin-withdraw-accept');
    Route::get('/instructor/withdraws/reject/{id}', 'Admin\InstructorController@reject')->name('admin-withdraw-reject');

   //------------ ADMIN WITHDRAW SECTION ENDS------------

  });


//------------ ADMIN STUDENT SECTION ------------

Route::group(['middleware'=>'permissions:Manage Students'],function(){


    Route::get('/student/datatables', 'Admin\StudentController@datatables')->name('admin-student-datatables'); //JSON REQUEST
    Route::get('/student', 'Admin\StudentController@index')->name('admin-student-index');
    Route::get('/student/create', 'Admin\StudentController@create')->name('admin-student-create');
    Route::post('/student/create', 'Admin\StudentController@store')->name('admin-student-store');
    Route::get('/student/edit/{id}', 'Admin\StudentController@edit')->name('admin-student-edit');
    Route::get('/student/curriculam/{id}', 'Admin\StudentController@curriculam')->name('admin-student-curriculam');
    Route::post('/student/update/{id}', 'Admin\StudentController@update')->name('admin-student-update');
    Route::get('/student/delete/{id}', 'Admin\StudentController@destroy')->name('admin-student-delete');
    Route::get('/student/status/{id1}/{id2}', 'Admin\StudentController@status')->name('admin-student-status');

    Route::get('/student/assign/', 'Admin\StudentController@assign')->name('admin-student-assign-modal');
    Route::post('/student/assign/course/', 'Admin\StudentController@assignCourse')->name('admin-student-assign-course');


    Route::get('/enroll/history/datatables', 'Admin\EnrollmentHistoryController@datatables')->name('admin-enroll-history-datatables'); //JSON REQUEST
    Route::get('/enroll/history', 'Admin\EnrollmentHistoryController@index')->name('admin-enroll-history-index');
    Route::get('/enroll/history/delete/{id}', 'Admin\EnrollmentHistoryController@destroy')->name('admin-enroll-history-delete');


//------------ ADMIN STUDENT SECTION ENDS------------

});

Route::group(['middleware'=>'permissions:Manage Categories'],function(){


  //------------ ADMIN CATEGORY SECTION ------------

    Route::get('/category/datatables', 'Admin\CategoryController@datatables')->name('admin-cat-datatables'); //JSON REQUEST
    Route::get('/category', 'Admin\CategoryController@index')->name('admin-cat-index');
    Route::get('/category/create', 'Admin\CategoryController@create')->name('admin-cat-create');
    Route::post('/category/store', 'Admin\CategoryController@store')->name('admin-cat-store');
    Route::get('/category/edit/{id}', 'Admin\CategoryController@edit')->name('admin-cat-edit');
    Route::post('/category/update/{id}', 'Admin\CategoryController@update')->name('admin-cat-update');
    Route::get('/category/delete/{id}', 'Admin\CategoryController@destroy')->name('admin-cat-delete');
    Route::get('/category/status/{id1}/{id2}', 'Admin\CategoryController@status')->name('admin-cat-status');


  //------------ ADMIN CATEGORY SECTION ENDS------------

  //------------ ADMIN SUBCATEGORY SECTION ------------

    Route::get('/subcategory/datatables', 'Admin\SubCategoryController@datatables')->name('admin-subcat-datatables'); //JSON REQUEST
    Route::get('/subcategory', 'Admin\SubCategoryController@index')->name('admin-subcat-index');
    Route::get('/subcategory/create', 'Admin\SubCategoryController@create')->name('admin-subcat-create');
    Route::post('/subcategory/create', 'Admin\SubCategoryController@store')->name('admin-subcat-store');
    Route::get('/subcategory/edit/{id}', 'Admin\SubCategoryController@edit')->name('admin-subcat-edit');
    Route::post('/subcategory/edit/{id}', 'Admin\SubCategoryController@update')->name('admin-subcat-update');
    Route::get('/subcategory/delete/{id}', 'Admin\SubCategoryController@destroy')->name('admin-subcat-delete');
    Route::get('/subcategory/status/{id1}/{id2}', 'Admin\SubCategoryController@status')->name('admin-subcat-status');
    Route::get('/load/subcategories/{id}/', 'Admin\SubCategoryController@load')->name('admin-subcat-load'); //JSON REQUEST

  //------------ ADMIN SUBCATEGORY SECTION ENDS------------

});


Route::group(['middleware'=>'permissions:Manage Courses'],function(){

  //------------ ADMIN COURSE SECTION ------------

    Route::get('/course/datatables', 'Admin\CourseController@datatables')->name('admin-course-datatables'); //JSON REQUEST
    Route::get('/course', 'Admin\CourseController@index')->name('admin-course-index');
    Route::get('/course/create', 'Admin\CourseController@create')->name('admin-course-create');
    Route::post('/course/create', 'Admin\CourseController@store')->name('admin-course-store');
    Route::get('/course/edit/{id}', 'Admin\CourseController@edit')->name('admin-course-edit');
    Route::post('/course/update/{id}', 'Admin\CourseController@update')->name('admin-course-update');
    Route::get('/course/status/{id1}/{id2}', 'Admin\CourseController@status')->name('admin-course-status');
    Route::get('/course/delete/{id}', 'Admin\CourseController@destroy')->name('admin-course-delete');

    Route::get('/purchase/datatables', 'Admin\OrderController@datatables')->name('admin-purchase-datatables'); //JSON REQUEST
    Route::get('/purchase', 'Admin\OrderController@index')->name('admin-purchase-index');
    Route::get('/purchase/details/{id}', 'Admin\OrderController@purchasedetails')->name('admin.purchase.details');


  //------------ ADMIN CURRICULAM SECTION ------------
    Route::post('/course/section/sort/update/{id}', 'Admin\CurriculamController@sectionSortUpdate')->name('admin-section-sort-update');
    Route::post('/course/section/store/{id}', 'Admin\CurriculamController@sectionStore')->name('admin-section-store');
    Route::post('/course/section/update/{id}', 'Admin\CurriculamController@sectionUpdate')->name('admin-section-update');
    Route::get('/course/section/delete/{id}', 'Admin\CurriculamController@sectionDelete')->name('admin-section-delete');
    Route::post('/course/lesson/store', 'Admin\CurriculamController@lessonStore')->name('admin-lesson-store');
    Route::post('/course/lesson/update/{id}', 'Admin\CurriculamController@lessonUpdate')->name('admin-lesson-update');
    Route::get('/course/lesson/edit/{id}', 'Admin\CurriculamController@lessonEdit')->name('admin-lesson-edit');
    Route::get('/course/lesson/quiz/{id}', 'Admin\CurriculamController@lessonQuiz')->name('admin-lesson-quiz');
    Route::get('/course/lesson/sort/{id}', 'Admin\CurriculamController@lessonSort')->name('admin-lesson-sort');
    Route::get('/course/lesson/delete/{id}', 'Admin\CurriculamController@lessonDelete')->name('admin-lesson-delete');
    Route::post('/course/lesson/sort/update/{id}', 'Admin\CurriculamController@lessonSortUpdate')->name('admin-lesson-sort-update');
    Route::get('/course/curriculam/{id}', 'Admin\CurriculamController@curriculam')->name('admin-course-curriculam');
    Route::get('/course/curriculam/lesson/{id}', 'Admin\CurriculamController@curriculamLesson')->name('admin-course-curriculam-lesson');
    Route::post('/course/curriculam/quiz/result', 'Admin\CurriculamController@result')->name('admin-course-quiz-result');
    Route::get('/course/curriculam-status/{id}', 'Admin\CurriculamController@curriculamStatus')->name('admin-course-curriculam-status');
  //------------ ADMIN CURRICULAM SECTION ENDS ------------


  //------------ ADMIN QUESTION SECTION ------------
  Route::post('/course/lesson/quiz/question/store', 'Admin\QuestionController@store')->name('admin-question-store');
  Route::get('/course/lesson/quiz/question/edit/{id}', 'Admin\QuestionController@edit')->name('admin-question-edit');
  Route::post('/course/lesson/quiz/question/update/{id}', 'Admin\QuestionController@update')->name('admin-question-update');
  Route::post('/course/lesson/quiz/question/sort/update/{id}', 'Admin\QuestionController@sortUpdate')->name('admin-question-sort-update');
  Route::get('/course/lesson/quiz/question/delete/{id}', 'Admin\QuestionController@delete')->name('admin-question-delete');
  //------------ ADMIN QUESTION SECTION ENDS ------------


    //------------ ADMIN COURSE SECTION ENDS------------
});

 

   Route::group(['middleware'=>'permissions:Manage Blog'],function(){

  //------------ ADMIN BLOG SECTION ------------
  Route::get('/blog/datatables', 'Admin\BlogController@datatables')->name('admin-blog-datatables'); //JSON REQUEST
  Route::get('/blog', 'Admin\BlogController@index')->name('admin-blog-index');
  Route::get('/blog/create', 'Admin\BlogController@create')->name('admin-blog-create');
  Route::post('/blog/create', 'Admin\BlogController@store')->name('admin-blog-store');
  Route::get('/blog/edit/{id}', 'Admin\BlogController@edit')->name('admin-blog-edit');
  Route::post('/blog/edit/{id}', 'Admin\BlogController@update')->name('admin-blog-update');
  Route::get('/blog/delete/{id}', 'Admin\BlogController@destroy')->name('admin-blog-delete');

  Route::get('/blog/category/datatables', 'Admin\BlogCategoryController@datatables')->name('admin-cblog-datatables'); //JSON REQUEST
  Route::get('/blog/category', 'Admin\BlogCategoryController@index')->name('admin-cblog-index');
  Route::get('/blog/category/create', 'Admin\BlogCategoryController@create')->name('admin-cblog-create');
  Route::post('/blog/category/create', 'Admin\BlogCategoryController@store')->name('admin-cblog-store');
  Route::get('/blog/category/edit/{id}', 'Admin\BlogCategoryController@edit')->name('admin-cblog-edit');
  Route::post('/blog/category/edit/{id}', 'Admin\BlogCategoryController@update')->name('admin-cblog-update');
  Route::get('/blog/category/delete/{id}', 'Admin\BlogCategoryController@destroy')->name('admin-cblog-delete');
  //------------ ADMIN BLOG SECTION ENDS ------------
  
   });



  //------------- Owner Section --------------
  Route::get('/owner/datatables', 'Admin\OwnerController@datatables')->name('admin-owner-datatables');
  Route::get('/owner', 'Admin\OwnerController@index')->name('admin-owner-index');
  Route::get('/owner/create', 'Admin\OwnerController@create')->name('admin-owner-create');
  Route::post('/owner', 'Admin\OwnerController@store')->name('admin-owner-store');

  Route::get('/owner/deactivated/text', 'Admin\OwnerController@deactivate')->name('admin-owner-deactivate');

  Route::get('/owner/edit/{id}', 'Admin\OwnerController@edit')->name('admin-owner-edit');
  Route::post('/owner/edit/{id}', 'Admin\OwnerController@update')->name('admin-owner-update');
  Route::get('/owner/secret/{id}', 'Admin\OwnerController@secret')->name('admin-owner-secret'); 
  Route::get('/owner/status/{id1}/{id2}', 'Admin\OwnerController@status')->name('admin-owner-status'); 
  Route::get('/owner/show/{id}', 'Admin\OwnerController@show')->name('admin-owner-show'); 
  Route::get('/owner/user/{id}', 'Admin\OwnerController@user')->name('admin-user-show'); 
  Route::get('/owner/staff/{id}', 'Admin\OwnerController@staff')->name('admin-staff-show');
  Route::get('/owner/user/datatables/{id}', 'Admin\OwnerController@userdatatables')->name('admin-user-datatables');
  Route::get('/owner/user/show/{id}', 'Admin\OwnerController@usershow')->name('owner-user-show'); 
  Route::get('/owner/user/delete/{id}', 'Admin\OwnerController@userdelete')->name('owner-user-delete'); 
  Route::get('/owner/staff/datatables/{id}', 'Admin\OwnerController@staffdatatables')->name('admin-staff-datatables');
  Route::get('/owner/staff/show/{id}', 'Admin\OwnerController@staffshow')->name('owner-staff-show'); 
  Route::get('/owner/staff/delete/{id}', 'Admin\OwnerController@staffdelete')->name('owner-staff-delete'); 
  Route::get('/owner/delete/{id}', 'Admin\OwnerController@destroy')->name('admin-owner-delete'); 
  //------------- Owner Section Ends--------------


  Route::group(['middleware'=>'permissions:General Settings'],function(){

  //------------ ADMIN GENERAL SETTINGS SECTION ------------
  Route::get('/general-settings/logo', 'Admin\GeneralSettingController@logo')->name('admin-gs-logo');
  Route::get('/general-settings/favicon', 'Admin\GeneralSettingController@fav')->name('admin-gs-fav');
  Route::get('/general-settings/loader', 'Admin\GeneralSettingController@load')->name('admin-gs-load');
  Route::get('/general-settings/contents', 'Admin\GeneralSettingController@contents')->name('admin-gs-contents');
  Route::get('/general-settings/breadcumb', 'Admin\GeneralSettingController@breadcumb')->name('admin-gs-breadcumb');
  Route::get('/general-settings/footer', 'Admin\GeneralSettingController@footer')->name('admin-gs-footer');
  Route::get('/general-settings/affilate', 'Admin\GeneralSettingController@affilate')->name('admin-gs-affilate');
  Route::get('/general-settings/error-banner', 'Admin\GeneralSettingController@errorbanner')->name('admin-gs-error-banner');
  Route::get('/general-settings/popup', 'Admin\GeneralSettingController@popup')->name('admin-gs-popup');
  Route::get('/general-settings/maintenance', 'Admin\GeneralSettingController@maintain')->name('admin-gs-maintenance');
  //------------ ADMIN GENERAL SETTINGS JSON SECTION ------------

  // General Setting Section
  Route::get('/general-settings/home/{status}', 'Admin\GeneralSettingController@ishome')->name('admin-gs-ishome');
  Route::get('/general-settings/disqus/{status}', 'Admin\GeneralSettingController@isdisqus')->name('admin-gs-isdisqus');
  Route::get('/general-settings/loader/{status}', 'Admin\GeneralSettingController@isloader')->name('admin-gs-isloader');
  Route::get('/general-settings/email-verify/{status}', 'Admin\GeneralSettingController@isemailverify')->name('admin-gs-is-email-verify');
  Route::get('/general-settings/popup/{status}', 'Admin\GeneralSettingController@ispopup')->name('admin-gs-ispopup');
  Route::get('/general-settings/admin/loader/{status}', 'Admin\GeneralSettingController@isadminloader')->name('admin-gs-is-admin-loader');


  Route::get('/general-settings/maintain/{status}', 'Admin\GeneralSettingController@ismaintain')->name('admin-gs-maintain');
  //  Affilte Section

  Route::get('/general-settings/affilate/{status}', 'Admin\GeneralSettingController@isaffilate')->name('admin-gs-isaffilate');

  //  Capcha Section

  Route::get('/general-settings/capcha/{status}', 'Admin\GeneralSettingController@iscapcha')->name('admin-gs-iscapcha');

  Route::get('/general-settings/contact/{status}', 'Admin\GeneralSettingController@iscontact')->name('admin-gs-iscontact');


  Route::post('/general-settings/update/all', 'Admin\GeneralSettingController@generalupdate')->name('admin-gs-update');
  Route::post('/general-settings/update/payment', 'Admin\GeneralSettingController@generalupdatepayment')->name('admin-gs-update-payment');
  Route::get('/general-settings/status/{field}/{status}', 'Admin\GeneralSettingController@status')->name('admin-gs-status');

  //------------ ADMIN GENERAL SETTINGS JSON SECTION ENDS------------

  });



//------------ ADMIN HOME SECTION ENDS ------------


Route::group(['middleware'=>'permissions:Home Page Settings'],function(){

  //------------ ADMIN PAGE SETTINGS SECTION ------------
  Route::get('/page-settings/customize', 'Admin\PageSettingController@customize')->name('admin-ps-customize');
  Route::get('/page-settings/big-save', 'Admin\PageSettingController@big_save')->name('admin-ps-big-save');
  Route::get('/page-settings/best-seller', 'Admin\PageSettingController@best_seller')->name('admin-ps-best-seller');

  Route::get('/hero/section','Admin\PagesettingController@herosection')->name('admin-hero-section-index');
  Route::get('/instructor/section','Admin\PagesettingController@instructorsection')->name('admin-instructor-section-index');

  Route::post('/page-settings/update/all', 'Admin\PageSettingController@update')->name('admin-ps-update');
  Route::post('/page-settings/update/home', 'Admin\PageSettingController@homeupdate')->name('admin-ps-homeupdate');


  //------------ ADMIN SERVICE SECTION ------------
  Route::get('/service/datatables', 'Admin\ServiceController@datatables')->name('admin-service-datatables'); //JSON REQUEST
  Route::get('/service', 'Admin\ServiceController@index')->name('admin-service-index');
  Route::get('/service/create', 'Admin\ServiceController@create')->name('admin-service-create');
  Route::post('/service/create', 'Admin\ServiceController@store')->name('admin-service-store');
  Route::get('/service/edit/{id}', 'Admin\ServiceController@edit')->name('admin-service-edit');
  Route::post('/service/edit/{id}', 'Admin\ServiceController@update')->name('admin-service-update');
  Route::get('/service/delete/{id}', 'Admin\ServiceController@destroy')->name('admin-service-delete');
  //------------ ADMIN SERVICE SECTION ENDS ------------

  //------------ ADMIN HOME PAGE SETTINGS SECTION ENDS ------------

});

Route::group(['middleware'=>'permissions:Menu Page Settings'],function(){

  //------------ ADMIN MENU PAGE SETTINGS SECTION ------------
  Route::get('/page-settings/contact', 'Admin\PageSettingController@contact')->name('admin-ps-contact');
  Route::post('/page-settings/update/all', 'Admin\PageSettingController@update')->name('admin-ps-update');


  //------------ ADMIN PAGE SECTION ------------
  Route::get('/page/datatables', 'Admin\PageController@datatables')->name('admin-page-datatables'); //JSON REQUEST
  Route::get('/page', 'Admin\PageController@index')->name('admin-page-index');
  Route::get('/page/create', 'Admin\PageController@create')->name('admin-page-create');
  Route::post('/page/create', 'Admin\PageController@store')->name('admin-page-store');
  Route::get('/page/edit/{id}', 'Admin\PageController@edit')->name('admin-page-edit');
  Route::post('/page/update/{id}', 'Admin\PageController@update')->name('admin-page-update');
  Route::get('/page/delete/{id}', 'Admin\PageController@destroy')->name('admin-page-delete');
  Route::get('/page/status/{id1}/{id2}', 'Admin\PageController@status')->name('admin-page-status');
  //------------ ADMIN PAGE SECTION ENDS------------

//------------ ADMIN MENU PAGE SETTINGS SECTION ENDS ------------

});


Route::group(['middleware'=>'permissions:Email Settings'],function(){
  //------------ ADMIN EMAIL SETTINGS SECTION ------------

  Route::get('/email-templates/datatables', 'Admin\EmailController@datatables')->name('admin-mail-datatables');
  Route::get('/email-templates', 'Admin\EmailController@index')->name('admin-mail-index');
  Route::get('/email-templates/{id}', 'Admin\EmailController@edit')->name('admin-mail-edit');
  Route::post('/email-templates/{id}', 'Admin\EmailController@update')->name('admin-mail-update');
  Route::get('/email-config', 'Admin\EmailController@config')->name('admin-mail-config');
  Route::get('/groupemail', 'Admin\EmailController@groupemail')->name('admin-group-show');
  Route::post('/groupemailpost', 'Admin\EmailController@groupemailpost')->name('admin-group-submit');

  //------------ ADMIN EMAIL SETTINGS SECTION ENDS ------------
});



Route::group(['middleware'=>'permissions:Payment Settings'],function(){
  //------------ ADMIN PAYMENT SETTINGS SECTION ------------
    Route::post('/general-settings/update/all', 'Admin\GeneralSettingController@generalupdate')->name('admin-gs-update');
    Route::get('/payment/information', 'Admin\PaymentGatewayController@paymentinfo')->name('admin-payment-info');
    Route::get('/paymentgateway/datatables', 'Admin\PaymentGatewayController@datatables')->name('admin-payment-datatables'); //JSON REQUEST
    Route::get('/paymentgateway', 'Admin\PaymentGatewayController@index')->name('admin-payment-index');
    Route::get('/paymentgateway/create', 'Admin\PaymentGatewayController@create')->name('admin-payment-create');
    Route::post('/paymentgateway/create', 'Admin\PaymentGatewayController@store')->name('admin-payment-store');
    Route::get('/paymentgateway/edit/{id}', 'Admin\PaymentGatewayController@edit')->name('admin-payment-edit');
    Route::post('/paymentgateway/update/{id}', 'Admin\PaymentGatewayController@update')->name('admin-payment-update');
    Route::delete('/paymentgateway/delete/{id}', 'Admin\PaymentGatewayController@destroy')->name('admin-payment-delete');
    Route::get('/paymentgateway/status/{id1}/{id2}', 'Admin\PaymentGatewayController@status')->name('admin-payment-status');


  // MULTIPLE CURRENCY

  Route::get('/general-settings/currency/{status}', 'Admin\GeneralSettingController@currency')->name('admin-gs-iscurrency');
  Route::get('/currency/datatables', 'Admin\CurrencyController@datatables')->name('admin-currency-datatables'); //JSON REQUEST
  Route::get('/currency', 'Admin\CurrencyController@index')->name('admin-currency-index');
  Route::get('/currency/create', 'Admin\CurrencyController@create')->name('admin-currency-create');
  Route::post('/currency/create', 'Admin\CurrencyController@store')->name('admin-currency-store');
  Route::get('/currency/edit/{id}', 'Admin\CurrencyController@edit')->name('admin-currency-edit');
  Route::post('/currency/update/{id}', 'Admin\CurrencyController@update')->name('admin-currency-update');
  Route::get('/currency/delete/{id}', 'Admin\CurrencyController@destroy')->name('admin-currency-delete');
  Route::get('/currency/status/{id1}/{id2}', 'Admin\CurrencyController@status')->name('admin-currency-status');

});
//------------ ADMIN PAYMENT SETTINGS SECTION ENDS------------


Route::group(['middleware'=>'permissions:Manage Referrals'],function(){

  Route::get('/referral/datatables', 'Admin\ReferralController@datatables')->name('admin-referral-datatables'); //JSON REQUEST
  Route::get('/referral', 'Admin\ReferralController@index')->name('admin-referral-index');

  Route::get('/referral/history/datatables', 'Admin\ReferralController@h_datatables')->name('admin-referral-history-datatables'); //JSON REQUEST
  Route::get('/referral/history', 'Admin\ReferralController@h_index')->name('admin-referral-history-index');

  Route::get('/referral/create', 'Admin\ReferralController@create')->name('admin-referral-create');
  Route::post('/referral/create', 'Admin\ReferralController@store')->name('admin-referral-store');
  Route::get('/referral/edit/{id}', 'Admin\ReferralController@edit')->name('admin-referral-edit');
  Route::post('/referral/update/{id}', 'Admin\ReferralController@update')->name('admin-referral-update');
  Route::get('/referral/delete/{id}', 'Admin\ReferralController@destroy')->name('admin-referral-delete');


});


  Route::group(['middleware'=>'permissions:Social Settings'],function(){

  //------------ ADMIN SOCIAL SETTINGS SECTION ------------
  Route::get('/social', 'Admin\SocialSettingController@index')->name('admin-social-index');
  Route::post('/social/update', 'Admin\SocialSettingController@socialupdate')->name('admin-social-update');
  Route::post('/social/update/all', 'Admin\SocialSettingController@socialupdateall')->name('admin-social-update-all');
  Route::get('/social/facebook', 'Admin\SocialSettingController@facebook')->name('admin-social-facebook');
  Route::get('/social/google', 'Admin\SocialSettingController@google')->name('admin-social-google');
  Route::get('/social/facebook/{status}', 'Admin\SocialSettingController@facebookup')->name('admin-social-facebookup');
  Route::get('/social/google/{status}', 'Admin\SocialSettingController@googleup')->name('admin-social-googleup');
  //------------ ADMIN SOCIAL SETTINGS SECTION ENDS------------

});



  //------------ ADMIN LANGUAGE SETTINGS SECTION ------------

  Route::group(['middleware'=>'permissions:Language Settings'],function(){


  Route::get('/general-settings/language/{status}', 'Admin\GeneralSettingController@language')->name('admin-gs-islanguage');


  Route::get('/languages/datatables', 'Admin\LanguageController@datatables')->name('admin-lang-datatables'); //JSON REQUEST
  Route::get('/languages', 'Admin\LanguageController@index')->name('admin-lang-index');
  Route::get('/languages/create', 'Admin\LanguageController@create')->name('admin-lang-create');
  Route::get('/languages/edit/{id}', 'Admin\LanguageController@edit')->name('admin-lang-edit');
  Route::post('/languages/create', 'Admin\LanguageController@store')->name('admin-lang-store');
  Route::post('/languages/edit/{id}', 'Admin\LanguageController@update')->name('admin-lang-update');
  Route::get('/languages/status/{id1}/{id2}', 'Admin\LanguageController@status')->name('admin-lang-st');
  Route::get('/languages/delete/{id}', 'Admin\LanguageController@destroy')->name('admin-lang-delete');


  //------------ ADMIN PANEL LANGUAGE SETTINGS SECTION ------------

  Route::get('/adminlanguages/datatables', 'Admin\AdminLanguageController@datatables')->name('admin-tlang-datatables'); //JSON REQUEST
  Route::get('/adminlanguages', 'Admin\AdminLanguageController@index')->name('admin-tlang-index');
  Route::get('/adminlanguages/create', 'Admin\AdminLanguageController@create')->name('admin-tlang-create');
  Route::get('/adminlanguages/edit/{id}', 'Admin\AdminLanguageController@edit')->name('admin-tlang-edit');
  Route::post('/adminlanguages/create', 'Admin\AdminLanguageController@store')->name('admin-tlang-store');
  Route::post('/adminlanguages/edit/{id}', 'Admin\AdminLanguageController@update')->name('admin-tlang-update');
  Route::get('/adminlanguages/status/{id1}/{id2}', 'Admin\AdminLanguageController@status')->name('admin-tlang-st');
  Route::get('/adminlanguages/delete/{id}', 'Admin\AdminLanguageController@destroy')->name('admin-tlang-delete');

  //------------ ADMIN PANEL LANGUAGE SETTINGS SECTION ENDS ------------

  //------------ ADMIN LANGUAGE SETTINGS SECTION ENDS ------------

});

Route::group(['middleware'=>'permissions:Seo Tools'],function(){

  //------------ ADMIN SEOTOOL SETTINGS SECTION ------------

  Route::get('/seotools/analytics', 'Admin\SeoToolController@analytics')->name('admin-seotool-analytics');
  Route::post('/seotools/analytics/update', 'Admin\SeoToolController@analyticsupdate')->name('admin-seotool-analytics-update');
  Route::get('/seotools/keywords', 'Admin\SeoToolController@keywords')->name('admin-seotool-keywords');
  Route::post('/seotools/keywords/update', 'Admin\SeoToolController@keywordsupdate')->name('admin-seotool-keywords-update');
  Route::get('/products/popular/{id}','Admin\SeoToolController@popular')->name('admin-prod-popular');

  //------------ ADMIN SEOTOOL SETTINGS SECTION ------------

});


Route::group(['middleware'=>'permissions:Manage Staff'],function(){
  //------------ ADMIN STAFF SECTION ------------

  Route::get('/staff/datatables', 'Admin\StaffController@datatables')->name('admin-staff-datatables');
  Route::get('/staff', 'Admin\StaffController@index')->name('admin-staff-index');
  Route::get('/staff/create', 'Admin\StaffController@create')->name('admin-staff-create');
  Route::post('/staff/create', 'Admin\StaffController@store')->name('admin-staff-store');
  Route::get('/staff/edit/{id}', 'Admin\StaffController@edit')->name('admin-staff-edit');
  Route::post('/staff/update/{id}', 'Admin\StaffController@update')->name('admin-staff-update');
  Route::get('/staff/delete/{id}', 'Admin\StaffController@destroy')->name('admin-staff-delete');

  //------------ ADMIN STAFF SECTION ENDS------------

});

Route::group(['middleware'=>'permissions:Subscribers'],function(){

  //------------ ADMIN SUBSCRIBERS SECTION ------------

  Route::get('/subscribers/datatables', 'Admin\SubscriberController@datatables')->name('admin-subs-datatables'); //JSON REQUEST
  Route::get('/subscribers', 'Admin\SubscriberController@index')->name('admin-subs-index');
  Route::get('/subscribers/download', 'Admin\SubscriberController@download')->name('admin-subs-download');

  //------------ ADMIN SUBSCRIBERS ENDS ------------

});

});



  Route::get('/cache/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    return redirect()->route('admin.dashboard')->with('cache','System Cache Has Been Removed.');
  })->name('admin-cache-clear');



  Route::group(['middleware'=>'permissions:Manage Roles'],function(){

  // ------------ ROLE SECTION ----------------------

  Route::get('/role/datatables', 'Admin\RoleController@datatables')->name('admin-role-datatables');
  Route::get('/role', 'Admin\RoleController@index')->name('admin-role-index');
  Route::get('/role/create', 'Admin\RoleController@create')->name('admin-role-create');
  Route::post('/role/create', 'Admin\RoleController@store')->name('admin-role-store');
  Route::get('/role/edit/{id}', 'Admin\RoleController@edit')->name('admin-role-edit');
  Route::post('/role/edit/{id}', 'Admin\RoleController@update')->name('admin-role-update');
  Route::get('/role/delete/{id}', 'Admin\RoleController@destroy')->name('admin-role-delete');

  // ------------ ROLE SECTION ENDS ----------------------
});


Route::get('/{table}/status/{id1}/{id2}', 'Admin\DashboardController@Preloadedstatus')->name('admin-preloaded-status');


});

// ************************************ ADMIN SECTION ENDS**********************************************





// *********************************** STUDENT SECTION *********************************************

Route::prefix('student')->group(function() {



    // STUDENT LOGIN
    Route::get('/login', 'student\logincontroller@showloginform')->name('student.login');
    Route::post('/login', 'student\logincontroller@login')->name('student.login.submit');
    // STUDENT LOGIN ENDS

    // STUDENT REGISTER
    Route::get('/register', 'Student\RegisterController@showRegisterForm')->name('student-register');
    Route::post('/register', 'Student\RegisterController@register')->name('student-register-submit');
    Route::get('/register/verify/{token}', 'Student\RegisterController@token')->name('student-register-token');
    // STUDENT REGISTER ENDS

    // STUDENT LOGOUT
    Route::get('/logout', 'Student\LoginController@logout')->name('student-logout');
    // STUDENT LOGOUT ENDS


    Route::group(['middleware'=>'AdminUser'],function(){


    // STUDENT DASHBOARD
    Route::get('/dashboard', 'Student\StudentController@index')->name('student-dashboard');


    // STUDENT DASHBOARD
    Route::get('/enroll', 'Student\StudentController@assignCourse')->name('student-enroll');

    // STUDENT RESET
    Route::get('/reset', 'Student\StudentController@resetform')->name('student-reset');
    Route::post('/reset', 'Student\StudentController@reset')->name('student-reset-submit');
    // STUDENT RESET ENDS

    // STUDENT PROFILE
    Route::get('/profile', 'Student\StudentController@profile')->name('student-profile');
    Route::get('/account', 'Student\StudentController@account')->name('student-account');
    Route::post('/account/update', 'Student\StudentController@accountUpdate')->name('student-account-update');
    Route::post('/profile', 'Student\StudentController@profileupdate')->name('student-profile-update');
    // STUDENT PROFILE ENDS

    // STUDENT AFFILATE
    Route::get('/affilates', 'Student\StudentController@affilate')->name('student-affilate');
    Route::get('/affilate/history', 'Student\StudentController@affilate_history')->name('student-affilate-history');
    // STUDENT AFFILATE ENDS

    Route::get('/orders', 'Student\OrderController@orders')->name('student-order-index');
    Route::get('/orders/{id}', 'Student\OrderController@order')->name('student-order-details');
    Route::get('/orders/print/{id}', 'Student\OrderController@orderprint')->name('student-order-print');

    // STUDENT WISHLIST
    Route::get('/wishlists','Student\WishlistController@wishlists')->name('student-wishlists');
    Route::get('/wishlist/add/{id}','Student\WishlistController@addwish')->name('student-wishlist-add');
    Route::get('/wishlist/remove/{id}','Student\WishlistController@removewish')->name('student-wishlist-remove');
    // STUDENT WISHLIST ENDS

    Route::get('/course/curriculam/lesson/{id}', 'Student\CurriculamController@curriculamLesson')->name('student-course-curriculam-lesson');
    Route::post('/course/curriculam/quiz/result', 'Student\CurriculamController@result')->name('student-course-quiz-result');


    Route::post('/course/review','Student\StudentController@reviewsubmit')->name('student.review.submit');


    // STUDENT INSTRUCTOR SEND MESSAGE
    Route::post('/student/contact', 'Student\MessageController@studentcontact')->name('student-contact');
    Route::get('/messages', 'Student\MessageController@messages')->name('student-messages');
    Route::get('/message/{id}', 'Student\MessageController@message')->name('student-message');
    Route::post('/message/post', 'Student\MessageController@postmessage')->name('student-message-post');
    Route::get('/message/{id}/delete', 'Student\MessageController@messagedelete')->name('student-message-delete');
    Route::get('/message/load/{id}', 'Student\MessageController@msgload')->name('student-message-load');
    // STUDENT INSTRUCTOR SEND MESSAGE ENDS

    // STUDENT FORGOT
    Route::get('/forgot', 'Student\ForgotController@showforgotform')->name('student-forgot');
    Route::post('/forgot', 'Student\ForgotController@forgot')->name('student-forgot-submit');
    // STUDENT FORGOT ENDS

    // Student instructor application start
    Route::get('/instructor/application/form','Student\StudentController@application')->name('student-instructor-application');
    route::post('/instructor/application/submit','student\studentcontroller@applicationsubmit')->name('student-instructor-application.submit');
    // Student instructor application end

    });


  });

// *********************************** STUDENT SECTION ENDS *********************************************


// *********************************** INSTRUCTOR SECTION *********************************************

Route::prefix('instructor')->group(function() {

    // INSTRUCTOR LOGIN
    Route::get('/login', 'instructor\logincontroller@showloginform')->name('instructor.login');
    Route::post('/login', 'instructor\logincontroller@login')->name('instructor.login.submit');
    // INSTRUCTOR LOGIN END

    // STUDENT REGISTER
    Route::get('/register', 'Instructor\RegisterController@showRegisterForm')->name('instructor-register');
    Route::post('/register', 'Instructor\RegisterController@register')->name('instructor-register-submit');
    Route::get('/register/verify/{token}', 'Instructor\RegisterController@token')->name('instructor-register-token');
    // STUDENT REGISTER END


    Route::group(['middleware'=>'AdminUser'],function(){

    // Student instructor application start
    Route::get('/application/form','Student\StudentController@application')->name('student-instructor-application');
    route::post('/application/submit','student\studentcontroller@applicationsubmit')->name('student-instructor-application.submit');
    // Student instructor application end


  //------------ INSTRUCTOR NOTIFICATION SECTION ------------

  Route::get('/show-notification', 'Instructor\NotificationController@show_notf')->name('instructor.notification.show');
  Route::get('/read-notification', 'Instructor\NotificationController@read')->name('instructor.notification.read');
  Route::get('/delete-notification', 'Instructor\NotificationController@notf_clear')->name('instructor.delete.clear');

  //------------ INSTRUCTOR NOTIFICATION SECTION ENDS ------------


    Route::group(['middleware'=>'instructor'],function(){

    // INSTRUCTOR DASHBOARD
    Route::get('/dashboard', 'Instructor\InstructorController@index')->name('instructor-dashboard');

    // INSTRUCTOR LOGOUT
    Route::get('/logout', 'instructor\logincontroller@logout')->name('instructor.logout');

    // INSTRUCTOR PROFILE
    Route::get('/profile', 'instructor\instructorcontroller@profile')->name('instructor.profile.edit');
    Route::post('/profile', 'Instructor\InstructorController@profileupdate')->name('instructor-profile-update');
    // INSTRUCTOR PROFILE ENDS

    // INSTRUCTOR RESET
    Route::get('/reset', 'Instructor\InstructorController@resetform')->name('instructor-reset');
    Route::post('/reset', 'Instructor\InstructorController@changepass')->name('instructor-reset-submit');
    // INSTRUCTOR RESET END

    // INSTRUCTOR FORGOT
    Route::get('/forgot', 'Instructor\ForgotController@showforgotform')->name('instructor-forgot');
    Route::post('/forgot', 'Instructor\ForgotController@forgot')->name('instructor-forgot-submit');
    // INSTRUCTOR FORGOT ENDS

    // STUDENT INSTRUCTOR SEND MESSAGE
    Route::post('/instructor/contact', 'Student\MessageController@studentcontact')->name('instructor-contact');
    Route::get('/messages', 'Student\MessageController@messages')->name('instructor-messages');
    Route::get('/message/{id}', 'Student\MessageController@message')->name('instructor-message');
    Route::post('/message/post', 'Student\MessageController@postmessage')->name('instructor-message-post');
    Route::get('/message/{id}/delete', 'Student\MessageController@messagedelete')->name('instructor-message-delete');
    Route::get('/message/load/{id}', 'Student\MessageController@msgload')->name('instructor-message-load');
    // STUDENT INSTRUCTOR SEND MESSAGE ENDS


  //------------ INSTRUCTOR COURSE SECTION ------------



    Route::get('/course/datatables', 'Instructor\CourseController@datatables')->name('instructor-course-datatables'); //JSON REQUEST
    Route::get('/course', 'Instructor\CourseController@index')->name('instructor-course-index');
    Route::get('/course/create', 'Instructor\CourseController@create')->name('instructor-course-create');
    Route::post('/course/create', 'Instructor\CourseController@store')->name('instructor-course-store');
    Route::get('/course/edit/{id}', 'Instructor\CourseController@edit')->name('instructor-course-edit');
    Route::post('/course/update/{id}', 'Instructor\CourseController@update')->name('instructor-course-update');
    Route::get('/course/status/{id1}/{id2}', 'Instructor\CourseController@status')->name('instructor-course-status');
    Route::get('/course/delete/{id}', 'Instructor\CourseController@destroy')->name('instructor-course-delete');
    Route::get('/load/subcategories/{id}/', 'Instructor\CourseController@load')->name('instructor-subcat-load'); //JSON REQUEST

    Route::get('/purchase/datatables', 'Instructor\OrderController@datatables')->name('instructor-purchase-datatables'); //JSON REQUEST
    Route::get('/purchase', 'Instructor\OrderController@index')->name('instructor-purchase-index');
    Route::get('/purchase/details/{order_number}', 'Instructor\OrderController@purchaseDetails')->name('instructor.purchase.details');

    //------------ INSTRUCTOR COURSE SECTION ENDS------------


    // INSTRUCTOR AFFILATE
    Route::get('/affilates', 'Instructor\InstructorController@affilate')->name('instructor-affilate-index');
    Route::get('/affilate/history', 'Instructor\InstructorController@affilate_history')->name('instructor-affilate-history');
    // INSTRUCTOR AFFILATE ENDS

  //------------ INSTRUCTOR CURRICULAM SECTION ------------

  Route::post('/course/section/sort/update/{id}', 'Instructor\CurriculamController@sectionSortUpdate')->name('instructor-section-sort-update');
  Route::post('/course/section/store/{id}', 'Instructor\CurriculamController@sectionStore')->name('instructor-section-store');
  Route::post('/course/section/update/{id}', 'Instructor\CurriculamController@sectionUpdate')->name('instructor-section-update');
  Route::get('/course/section/delete/{id}', 'Instructor\CurriculamController@sectionDelete')->name('instructor-section-delete');
  Route::post('/course/lesson/store', 'Instructor\CurriculamController@lessonStore')->name('instructor-lesson-store');
  Route::post('/course/lesson/update/{id}', 'Instructor\CurriculamController@lessonUpdate')->name('instructor-lesson-update');
  Route::get('/course/lesson/edit/{id}', 'Instructor\CurriculamController@lessonEdit')->name('instructor-lesson-edit');
  Route::get('/course/lesson/quiz/{id}', 'Instructor\CurriculamController@lessonQuiz')->name('instructor-lesson-quiz');
  Route::get('/course/lesson/sort/{id}', 'Instructor\CurriculamController@lessonSort')->name('instructor-lesson-sort');
  Route::get('/course/lesson/delete/{id}', 'Instructor\CurriculamController@lessonDelete')->name('instructor-lesson-delete');
  Route::post('/course/lesson/sort/update/{id}', 'Instructor\CurriculamController@lessonSortUpdate')->name('instructor-lesson-sort-update');
  Route::get('/course/curriculam/{id}', 'Instructor\CurriculamController@curriculam')->name('instructor-course-curriculam');
  Route::get('/course/curriculam/lesson/{id}', 'Instructor\CurriculamController@curriculamLesson')->name('instructor-course-curriculam-lesson');
  Route::post('/course/curriculam/quiz/result', 'Instructor\CurriculamController@result')->name('instructor-course-quiz-result');
  Route::get('/course/curriculam-status/{id}', 'Instructor\CurriculamController@curriculamStatus')->name('instructor-course-curriculam-status');

  //------------ INSTRUCTOR CURRICULAM SECTION ENDS ------------

  //------------ INSTRUCTOR QUESTION SECTION ------------

  Route::post('/course/lesson/quiz/question/store', 'Instructor\QuestionController@store')->name('instructor-question-store');
  Route::get('/course/lesson/quiz/question/edit/{id}', 'Instructor\QuestionController@edit')->name('instructor-question-edit');
  Route::post('/course/lesson/quiz/question/update/{id}', 'Instructor\QuestionController@update')->name('instructor-question-update');
  Route::post('/course/lesson/quiz/question/sort/update/{id}', 'Instructor\QuestionController@sortUpdate')->name('instructor-question-sort-update');
  Route::get('/course/lesson/quiz/question/delete/{id}', 'Instructor\QuestionController@delete')->name('instructor-question-delete');

  //------------ INSTRUCTOR QUESTION SECTION ENDS ------------


  //------------ INSTRUCTOR WITHDRAW SECTION ------------

  Route::get('/withdraw/datatables', 'Instructor\WithdrawController@datatables')->name('instructor-wt-datatables');
  Route::get('/withdraw', 'Instructor\WithdrawController@index')->name('instructor-wt-index');
  Route::get('/withdraw/create', 'Instructor\WithdrawController@create')->name('instructor-wt-create');
  Route::post('/withdraw/create', 'Instructor\WithdrawController@store')->name('instructor-wt-store');

  //------------ INSTRUCTOR WITHDRAW SECTION ENDS ------------


    });

    });

  });


// *********************************** INSTRUCTOR SECTION ENDS*********************************************

// ************************************ FRONT SECTION **********************************************

  // HOME SECTION
  Route::get('/', 'Front\FrontendController@index')->name('front.index');
  // HOME SECTION ENDS

  Route::get('/language/{id}', 'Front\Frontendcontroller@language')->name('front.language');


  // CATALOG SECTION
  Route::get('/catalog/{category?}/{subcategory?}','Front\CatalogController@catalog')->name('front.catalog');
  // CATALOG SECTION ENDS

  // COURSE SECTION START
  route::get('/course/{slug}/','Front\Catalogcontroller@course')->name('front.course');
  // COURSE SECTION END

  // INSTRUCTOR ROUTE
  route::get('instructor/{slug}','Front\CatalogController@instructordetails')->name('instructor.details');
  // INSTRUCTOR ROUTE END

  // RATING ROUTE
  Route::get('/course/view/review/{id}','Front\CatalogController@reviews')->name('front.reviews');
  Route::get('/course/view/side/review/{id}','Front\CatalogController@sideReviews')->name('front.side.reviews');
  // RATING ROUTE END

  // BLOG SECTION
  Route::get('/blog','Front\FrontendController@blog')->name('front.blog');
  Route::get('/blog/{id}','Front\FrontendController@blogshow')->name('front.blogshow');
  Route::get('/blog/category/{slug}','Front\FrontendController@blogcategory')->name('front.blogcategory');
  Route::get('/blog/tag/{slug}','Front\FrontendController@blogtags')->name('front.blogtags');
  Route::get('/blog-search','Front\FrontendController@blogsearch')->name('front.blogsearch');
  Route::get('/blog/archive/{slug}','Front\FrontendController@blogarchive')->name('front.blogarchive');
  // BLOG SECTION ENDS

  // CONTACT SECTION
  Route::get('/contact','Front\FrontendController@contact')->name('front.contact');
  Route::post('/contact','Front\FrontendController@contactemail')->name('front.contact.submit');
  Route::get('/contact/refresh_code','Front\FrontendController@refresh_code');
  // CONTACT SECTION  ENDS

  // PRODCT AUTO SEARCH SECTION
  Route::get('/autosearch/course/{slug}','Front\CatalogController@autosearch');
  // PRODCT AUTO SEARCH SECTION ENDS


  // CART SECTION
  Route::get('/carts/view','Front\CartController@cartview');
  Route::get('/carts','Front\CartController@cart')->name('front.cart');
  Route::get('/addcart/{id}','Front\CartController@addcart')->name('course.cart.add');
  Route::get('/addtocart/{id}','Front\CartController@addtocart')->name('course.cart.quickadd');
  Route::get('/removecart/{id}','Front\CartController@removecart')->name('course.cart.remove');
  // CART SECTION ENDS

  // SUBSCRIBE SECTION
  Route::post('/subscriber/store', 'Front\FrontendController@subscribe')->name('front.subscribe');
  // SUBSCRIBE SECTION ENDS

  // CHECKOUT
  Route::get('/checkout','Front\CheckoutController@checkout')->name('front.checkout');
  Route::get('/checkout/payment/return', 'Front\CheckoutController@payreturn')->name('front.payment.return');
  Route::get('/checkout/payment/cancle', 'Front\CheckoutController@paycancle')->name('front.payment.cancle');

  // PAYPAL
  Route::post('/checkout/payment/paypal-submit', 'Front\PaypalController@store')->name('paypal.submit');
  Route::get('/checkout/payment/paypal-notify', 'Front\PaypalController@notify')->name('paypal.notify');

  // STRIPE
  Route::post('/checkout/payment/stripe-submit', 'Front\StripeController@store')->name('stripe.submit');

  // LOGIN WITH FACEBOOK OR GOOGLE SECTION
  Route::get('auth/{provider}', 'User\SocialRegisterController@redirectToProvider')->name('social-provider');
  Route::get('auth/{provider}/callback', 'User\SocialRegisterController@handleProviderCallback');
  // LOGIN WITH FACEBOOK OR GOOGLE SECTION ENDS

  // PAGE SECTION
  Route::get('/{slug}','Front\FrontendController@page')->name('front.page');
  // PAGE SECTION ENDS

// ************************************ FRONT SECTION ENDS**********************************************

